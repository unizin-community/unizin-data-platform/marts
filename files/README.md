# Data files

The CSV files in this directory should be created as tables in the `mart_helper` dataset of a BigQuery environment. The tables are used by other context tables and marts in this project.

## Identify missing URL-based tool names
In any given project, you can identify which LTI Tool URLs are missing by running the following query:

```sql
SELECT
  lti.launch_app_url AS url
  , COUNT(1) num
FROM
  mart_general.lti_tool AS lti
WHERE
  lti.tool_name = 'Unknown tool'
GROUP BY
  lti.launch_app_url
ORDER BY
  num DESC
```
