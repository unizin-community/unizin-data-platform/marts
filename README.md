# UDP data context and data marts

While comprehensive, the UDP's context and event stores are large data artifacts that make even routine research, business analysis, or other inquiry challenging to conduct. Analysts must dive into a comprehensive data dictionary and learn new tools before even getting started with the UDP's data. These and other factors create high barriers of entry and steep learning curves even for seasoned data analysts who may be working with learning data for the first time.

In short, working with the "foundational stores" of the UDP data lake is difficult for many end-users.

In practice, the practical interest and needs of learning stakeholders can be classified into use-case domains. Related use-cases narrow the range of data and metrics relevant to answering the questions of a particular domain. For example, once can identify a range of questions around "course design" separately from a range of questions around "student performance."

Identifying and classifying use-case domains creates a foundation for a data marts paradigm. Narrow, purposeful data marts that occupy a function within a broader theme can serve as a technique that enables UDP-based learning data to reach a greater audience of data analysts and researchers.

This repository is an attempt to articulate a data warehouse and data marts strategy for the UDP. In a way, this repository is nothing more than a bunch of SQL files whose names would correspond to a set of tables that live somewhere (perhaps BigQuery). It's the organization of these tables into thematic folders and, also, the way they build on each other that substantively presents the overall approach.

- [Organizational approach](#organizational-approach)
    - [Files](#files)
    - [Context](#context)
    - [Event](#event)
    - [Mart](#mart)
- [Naming conventions & Dependencies](#naming-conventions--dependencies)
- [Mermaid Charts](#mermaid-charts)
- [Airflow & Cloud Composer](#airflow--cloud-composer)
    - [Schedules](#schedules)
    - [Configs](#configs)
        - [Base Config](#base-config)
        - [Mart Configs](#mart-configs)
    - [SQL Overrides](#sql-overrides)
    - [Run Airflow locally](#run-airflow-locally)
        - [DAG initial order](#dag-initial-order)
        - [Cleanup and useful commands](#cleanup-and-useful-commands)
- [SQL Linting and Validation](#sql-linting-and-validation)

---

# Organizational approach

The directory structure in this repository enacts a set of organizational principles. Each directory may contain sub-directories or SQL files. The SQL files contain the code to create a table from a SELECT statement. The name of file itself is intended to be the name of table created from the SELECT. Note that there exists files of the same name across this repository. The intention is that a file's path in a directory structure defines its uniqueness and may be considered in its name.

This repository of SQL code assumes that you're using the UDP's `context_store` database, formatted in the UCDM standard. The SQL code defines a set of feature tables derived from the raw `context_store` data that may be useful to research analysis.

| Directory | Description |
| ---      |  ------  |
| `files`  | The tables in this directory create data that that is not-native to the UCDM and that is useful to producing a variety of marts. For example, a table that contains every date over some period of time is useful when creating marts that present data in temporal terms or on timelines. |
| `context/base` | The tables in this directory are re-representations of the `entity` schema tables in the `context_store`, but with some alterations that are typical when building datasets for data scientists/analysts. For example, all elements whose values are defined by option sets are exploded into distinct dummy/boolean variables. All boolean variables (including the aforementioned metrics) have their values converted from true and false to 1 and 0, respectively. The purpose of the `base` data tables is to create a context_store foundation from which any other tables could be generated. |
| `context/*` | All non-`base` directories under the `context` directory are used to create tables that aggregate, compute, or otherwise derive metrics and values from the `base` tables. These aggregations, computations, and derivations are not designed for any particular business use-case. However, they do generate and capture data that is relevant to particular domains of business or learning problems. The pattern is that a directory name corresponds to a concept or entity that the tables within the directory describe. For example, the `context/student` tables summarize data about students along different dimensions – by academic term, by weeks in the academic term, by their credit history. The idea behind the `context/*` directories is that, collectively, information is categorized and derivations are pre-computed to serve domains of business problems. The value is that researchers, BAs, and other stakeholders will find a context organization more convenient to use than the foundational stores. |
| `mart` | The directories and tables in `/mart` are intended to be the tables used by end-users for particular reporting, analytics, and other purposes. The marts are primarily organized by UCDM entities (e.g., "learner activity") or relevant themes (e.g., "student" as a subset of overall persons) that belong to particular use-case domains. For any given entity or theme, a variety of tables are generated that address a narrow set of questions. One feature of these marts is that they combine context and behavior data and their value largely turns on being near-real-time. |
| `dags` | This directory contains Airflow DAGs to execute the SQL definition of the `udp-marts` marts via BigQuery jobs that output to tables that are visible to end users|

---

# Execution Pipeline

The generation of the `batch_features` marts follows this order:
1. `load_static_tables` (`files`)
2. `context`
3. `event_hourly`
4. `event_daily`
5. `mart_hourly` & `mart_daily`

## Files

The assets in the `files/` directory are really only run once. These are just helper tables for some things in `context` and are not institution-specific, so there's no need to automate these really. However, it is important to make sure everything in `files` exists in BQ before running anything else in `batch_features`.

## Context

The assets in the `context/` directory are the mart building blocks with dependencies only on the UDP `context_store` data. These SQL scripts operate on a hierarchy that takes data in the `context_store` at a base level then perform joins, aggregations, and filters that generate use-case informed building blocks. Executing this hierarchy of transformation on UDP `context_store` data is the main mechanism for translating the UCDM into use cases for end users.

The `context/config.yaml` file is the best record for the scope and sequence of context tables that are executed based on the `context_store`.

## Event

The assets in the `event/` directory are the mart building blocks with dependencies only on the UDP `event_store` data. Filtering noise out of events is a common theme in these files, so one will notice hefty `WHERE` clauses throughout. There is not as much joining here compared to `context` tables.

## Mart

The assets in the `mart/` directory are the final presentation marts that we ultimately surface to users and document on our [resources site](https://resources.unizin.org/display/UDP/Data+marts). Dependencies here are only `context` and/or `event` tables. There are no direct calls to UDP `context_store` or `event_store` data. If our mart hierarchy is working as designed, the base layer of data is completely abstracted in favor of use-case driven language and schemas.

Naturally, the `mart` tables need to run after `context` and `event` tables run.

---

# Naming conventions & Dependencies

Generally speaking, the marts in this repository preserve the names for entities and elements in the UCDM. Certain tables diverge from this convention where it is appropriate to describe the meaning of the table's data.

An attempt is made in each SQL file to provide a name and description of the table created by the code. Additionally, a list of the table's dependencies is presented. The idea is that, for any given table, you'd know which tables its SELECT code depends on and therefore what other tables need to exist before the table in question can be created.

For example, consider this documentation from the `/mart/course_offering/status.sql` file:

```
/*
  Context / Base / Course section enrollment

    Imports Course section enrollment data from the context store.

  Properties:

    Export table:  mart_helper.context__course_section_enrollment
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base/ Academic term
    * Context / Base/ Academic session
    * Context / Base/ Course offering
    * Context / Base/ Course section
    * Context / Base/ Person

  To do:
    *
*/
```

The documentation indicates in which BigQuery dataset to install this table; what name to give the table; how frequently you should run the table (if using scheduled queries); whether to overwrite or append the query results to the table; and, finally, what other tables must be installed first for this query to work.

---

# Mermaid Charts

The mermaid charts under `mermaid/` are generated with the `mermaid/generate_mermaids.py` script. It uses the base and mart [configs](#configs) to determine the structure.

### To use

```bash
# setup python virtual environment
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# install mermaid cli
npm install -g @mermaid-js/mermaid-cli
```

### Run it

```bash
python mermaid/generate_mermaids.py

# to generate a single chart
## using mart name
python mermaid/generate_mermaids.py -m mart__general__lms_tool
## using BQ table name
python mermaid/generate_mermaids.py -m mart_general.lms_tool
```

---

# Airflow & Cloud Composer

---

# Schedules

After testing this with our largest data sets (`event_store` ~34.19 TB + `context_store` ~839 GB), the following run times for each DAG was as follows:
| DAG                | H:MM:SS |
|--------------------|---------|
| load_static_tables | 0:00:11 |
| context            | 0:58:02 |
| event_hourly       | 0:03:07 |
| event_daily        | 0:03:08 |
| mart_hourly        | 0:02:29 |
| mart_daily         | 0:08:19 |

With the goal of having a full run of the daily `context` DAG and subsequent DAGs be finished by 9am eastern I have crafted the following default schedule (times are in 24hr):

> Note that each institution may have varied cron schedules to better coordinate with their `context_store` ingestion and mart run times.

| DAG                | cron schedule | UTC (24hr) |
|--------------------|---------------|------------|
| load_static_tables |  `0 10 * * 1` |    `10:00` |
| context            | `30 10 * * *` |    `10:30` |
| event_hourly       |   `0 * * * *` |    `**:00` |
| event_daily        | `20 12 * * *` |    `12:20` |
| mart_hourly        |  `40 * * * *` |    `**:40` |
| mart_daily         | `40 12 * * *` |    `12:40` |

# Configs

## Env Config

The base config file the DAGs use to determine runtime tenants/environment is located at `./env.yaml` when **in use**, otherwise the environment configs are found at `./cicd/${ENV}.env.yaml`.

> On deploy the specified environment config is moved to the base of the GCS `dags/` folder so that Airflow will pick it up, to use locally simple copy the desired config to the root of the repo with:
> ```sh
> cp ./cicd/${ENV}.env.yaml ./env.yaml
> ```
> If the `IS_CLOUD_COMPOSER` environment variable is set and `env.yaml` cannot be found Airflow will error out.

The following values are supported:

- `env` - **(Required)** The target UDP environment the DAGs will run against.
- `tenants`- **(Required)** List of UDP tenants to create DAGs for. Each tenant must have an active `context_store` and `event_store`.
- `write_to_project` - (Optional) Overrides any instance of `_WRITE_TO_PROJECT_` in SQL files. Used to change which GCP project the DAGs will write output datasets and tables to. Defaults to `udp-{tenant}-{env}`, overridden by the `WRITE_TO_PROJECT` environment variable. **NOTE**: this should only be used when running against a single tenant, otherwise each tenant DAGs will write to the same datasets.
- `read_from_project` - (Optional) Overrides any instance of `_READ_FROM_PROJECT_` in SQL files. Used to change which GCP project the DAGs will read datasets and tables from. Defaults to `udp-{tenant}-{env}`, overridden by the `READ_FROM_PROJECT` environment variable. **NOTE**: this should only be used when running against a single tenant, otherwise each tenant DAGs will duplicate work.

## Base Config

The base config file the DAGs use to locate the mart config files is located at `./config.yaml`. The following values are supported:

- `mart_config_paths` - **(Required)** Mart names and file paths to their configs. Formatted as `marts: mart_dir/config{_schedule}.yaml`.
- `static_tables` - (Optional) List of static table definitions. Structure is documented below.
- `static_tables_schedule` - (Optional) Schedule for `load_static_tables` DAG, in cron format. Defaults to `0 10 * * 1`, overridden by the `STATIC_TABLES_SCHEDULE` environment variable.
- `monitor_schedule` - (Optional) Schedule for `airflow_unizin_monitor` DAG, in cron format. Defaults to `15 14 * * *`, overridden by the `MONITOR_SCHEDULE` environment variable.
- `env` - (Optional) The target UDP environment the DAGs will run against. This is only used if `env.yaml` is not found _and_ the `IS_CLOUD_COMPOSER` environment variable is not set.
- `tenants`- (Optional) List of UDP tenants to create DAGs for. Each tenant must have an active `context_store` and `event_store`. This is only used if `env.yaml` is not found _and_ the `IS_CLOUD_COMPOSER` environment variable is not set.
- `write_to_project` - (Optional) Overrides any instance of `_WRITE_TO_PROJECT_` in SQL files. Used to change which GCP project the DAGs will write output datasets and tables to. Defaults to `udp-{tenant}-{env}`, overridden first by setting `write_to_project` in `env.yaml` and then by the `WRITE_TO_PROJECT` environment variable. **NOTE**: this should only be used when running against a single tenant, otherwise each tenant DAGs will write to the same datasets.
- `read_from_project` - (Optional) Overrides any instance of `_READ_FROM_PROJECT_` in SQL files. Used to change which GCP project the DAGs will read datasets and tables from. Defaults to `udp-{tenant}-{env}`, overridden first by setting `read_from_project` in `env.yaml` and then by by the `READ_FROM_PROJECT` environment variable. **NOTE**: this should only be used when running against a single tenant, otherwise each tenant DAGs will duplicate work.
- `dry_run` - (Optional) Specifies queries will not be executed and BQ resources will not be created. Used to validate BQ jobs and SQL within the DAGs. Setting to any value other than '`false`' will enable dry run mode. Overridden the '`DRY_RUN`' env variable.

---

The `static_tables` list supports:

- `target_table` - **(Required)** The output BigQuery table. Formatted as `dataset.table`.
- `file` - **(Required)** The file used to populate the contents of the table with.
- `schema` - **(Required)** List of schema fields and their type. Structure is documented below.

---

The `schema` list supports:

- `field` - **(Required)** The field name. The name must contain only letters (a-z, A-Z), numbers (0-9), or underscores (_), and must start with a letter or underscore. The maximum length is 300 characters.
- `type` - **(Required)** The field data type. Possible values can be found [here](https://cloud.google.com/bigquery/docs/reference/rest/v2/tables#TableFieldSchema.FIELDS.type).


## Mart Configs

The mart config files defining each DAG, located at the paths specified in `mart_config_paths` in the base config, support the following values:

- `default_schedule` - **(Required)** Schedule for the DAG, in cron format. Overridden by the `{MART_NAME}_SCHEDULE` environment variable.
- `marts` - **(Required)** List of mart definitions. Structure is documented below.
- `starters` - (Optional) List of starter mart definitions. Structure is documented below.
- `default_partition_type` - (Optional) The default [BigQuery time partitioning](https://cloud.google.com/python/docs/reference/bigquery/latest/google.cloud.bigquery.table.TimePartitioning) for partitioned tables (marts). Defaults to [DAY](https://cloud.google.com/python/docs/reference/bigquery/latest/google.cloud.bigquery.table.TimePartitioningType), overridden by the `{MART_NAME}_DEFAULT_PARTITION_TYPE` environment variable.

---

A `mart` definitions for the `marts` list supports:

- `mart` - **(Required)** Name of the mart, used for DAG task naming.
- `file` - **(Required)** Path to SQL file to run for the mart.

- `target_table` - (Optional) The output BigQuery table for the mart. Formatted as `dataset.table`. If not specified then the results of the BIgQuery job will not be saved. If specified, `write_disposition` is **required**.
- `write_disposition` - (Optional) Specifies the action that occurs if the destination table already exists. **Required** if `target_table` is specified. See [this doc](https://cloud.google.com/bigquery/docs/reference/rest/v2/Job) for more info. The following values are supported:
    - `WRITE_TRUNCATE`: If the table already exists, BigQuery overwrites the table data and uses the schema from the query result.
    - `WRITE_APPEND`: If the table already exists, BigQuery appends the data to the table.
    - `WRITE_EMPTY`: If the table already exists and contains data, a 'duplicate' error is returned in the job result.
- `starter` - (Optional) Used to specify another mart as the mart's "starter". A starter is a mart that (usually) runs a SQL file that drops/creates/modified the `target_table` for the mart as a prerequisite for the mart's initial run.
- `partition_field` - (Optional) Field to partition the `target_table` on.
- `partition_type` - (Optional) Overrides `default_partition_type` for the specific mart.
- `depends_on` - (Optional) A list of BigQuery tables the mart is dependent on. Used to build the DAG task dependencies and mermaid charts.

---

A starter `mart` definitions for the `starters` list supports:

> A starter is a mart that (usually) runs a SQL file that drops/creates/modifies a table for another mart as a prerequisite for that mart to run.
> Starters are only needed to run once at initialization or changes are made to the table's schema, fields are added, or calculations/aggregations are altered.

- `mart` - **(Required)** Name of the mart, used for DAG task naming.
- `file` - **(Required)** Path to SQL file to run for the mart.
- `target_dataset` - **(Required)** The BigQuery dataset the starter creates tables in.
- `depends_on` - (Optional) A list of BigQuery tables the mart is dependent on. Used to build the DAG task dependencies and mermaid charts.

# SQL Overrides

The following values if present in any SQL file for a mart get override at runtime by the DAGs:

| overridden string | purpose | value used |
|-|-|-|
| `_WRITE_TO_PROJECT_`| GCP project where the write actions (`CREATE`/`DROP`/`INSERT`/`MODIFY`) should take place. | Defaults to `udp-{tenant}-{env}` unless `write_to_project` is specified in the [base config](#base-config) or the `WRITE_TO_PROJECT` environment variable is set. |
| `_READ_FROM_PROJECT_`| GCP project the BigQuery jobs will read from tables not defined by the mart configs in this repo. Primarily used when defining the `context_store` and `event_store` in SQL. | Defaults to `udp-{tenant}-{env}` unless `read_from_project` is specified in the [base config](#base-config) or the `READ_FROM_PROJECT` environment variable is set. 
| `@run_time`| Represents the intended time of execution. This is a [supported parameter](https://cloud.google.com/bigquery/docs/scheduling-queries#query_string) for traditional BigQuery scheduled queries. | At runtime this will be replaced with a UTC [timestamp](https://cloud.google.com/bigquery/docs/reference/standard-sql/data-types#timestamp_type). |

# Run Airflow locally

By default airflow will use your credentials that are configured on your local machine. You can also auth your GCP credentials via the following commands (run them both):
```sh
gcloud auth login
gcloud auth application-default login
```

### Requirements:
- Docker (`docker`)
- A GCP Service Account json key for a service account with permissions to:
    - Read from `event_store` & `context_store` BQ datasets in the member GCP project (aka `_READ_FROM_PROJECT_`)
    - Create datasets and tables and run query jobs in the target GCP project (aka `_WRITE_TO_PROJECT_`)

## Do the thing

1. Place your GCP service account key at `./sa-key.json`.
    > This path is specifically ignored by `git` so if your key is placed elsewhere or named differently you will need to ensure it is not committed to the repo by accident.

2. Create and populate `./env.yaml` with the desired settings:
    ```sh
    touch ./env.yaml
    ```
    An example of its contents for reading from a tenant project and writing to another project would be:
    ```yaml
    # env.yaml
    env: internal
    tenants:
    - id: unizin
    read_from_project: udp-{tenant}-prod # replace `{tenant}` with a valid shortcode
    write_to_project: unizin-dev-sandbox
    ```
    - Optionally you can set the `DRY_RUN` env variable or `dry_run` in [`config.yaml`](config.yaml) to run the dags without creating any BQ resources. See the [Base Config](#base-config) section for more info.
3. Start the airflow services:
    ```sh
    docker compose up
    ```
    After a little bit you should see similar to the following:
    ```
    airflow-webserver-1  | [2024-07-24 21:29:19 +0000] [13] [INFO] Listening at: http://0.0.0.0:8080 (13)
    ```
    Once you do you should be able to browse to the airflow UI at [`http://0.0.0.0:8080`](http://0.0.0.0:8080).
    >If you are on Linux, you SHOULD follow the instructions below set AIRFLOW_UID environment variable, otherwise files will be owned by root. See https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html#setting-the-right-airflow-user.
    >
    > For other operating systems, you may get a warning that `AIRFLOW_UID` is not set, but you can safely ignore it.

## DAG initial order

DAGss currently should be manually triggered in the following order for their initial run:

1. `load_static_tables` (~1min)
2. `context` (30mins-1.5hrs)
2. `starters` (~30mins)
3. `event_hourly` (~5min)
4. `event_daily` (~5min)
5. `mart_hourly` (~1min) & `mart_daily` (~5min)

After this they can run as scheduled.

## Cleanup and useful commands

### Cleanup

```sh
# cleanup BQ datasets
bq rm -r -f -d ${PROJECT_ID}:mart_helper
bq rm -r -f -d ${PROJECT_ID}:mart_course_section
bq rm -r -f -d ${PROJECT_ID}:mart_course_offering
bq rm -r -f -d ${PROJECT_ID}:mart_general
bq rm -r -f -d ${PROJECT_ID}:mart_taskforce

# temporarily stop the airflow services in docker
docker compose down
# permanently stop and remove the docker resources
docker compose down -v --remove-orphans
# remove ALL docker images/containers/volumes/networks from your machine
docker system prune -a
# remove airflow created files and directories
rm -rf logs/ plugins/ airflow.cfg
```

### Useful commands

If attempting to run Airflow locally outside of docker you may need to install specific python packages to get Airflow to run. To easily fetch the needed packages you can supply the official Airflow constraint file for the specific Airflow+Python version:

```sh
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip

# install requirements including airflow+python version constraints
AIRFLOW_VERSION="2.6.3"
PYTHON_VERSION="3.11"
pip install -r requirements.txt --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-${AIRFLOW_VERSION}/constraints-${PYTHON_VERSION}.txt"
```

# SQL Linting and Validation

### Linting

Linting can be performed with the [`sqlfluff` package](https://sqlfluff.com/) directly or with the `sqllint.py` script employed by the pipeline:

```sh
# find and perform linting on all sql files in directory
python scripts/sqllint.py

# specific file, provides more output
python scripts/sqllint.py -p context/base/academic_career.sql 

# run sqlfluff manually, provides the most verbose output
sqlfluff lint context/base/academic_career.sql --dialect bigquery
```

### Validation

Validate a specific SQL file against BigQuery with the `bq_dryrun.py` script. It takes in a write to and read from project argument and substitutes the proper values in the SQL files as the DAGs would.

```sh
python scripts/bq_dryrun.py {file} {read_from_project} {write_to_project}
```
Output
```sh
Dry running query:

WITH from_cs AS (
  SELECT
{...}

File: {file}
Estimated bytes processed: 9499476081 (8.85GB)
Estimated cost of query: ~$0.054
Referenced tables:
- {project}.context_store_entity.quiz_result
- {project}.context_store_keymap.quiz_result
```

> Note that all of the starter SQL files will return zero estimated bytes processed and cost which is inaccurate.
> ```
> File: event/course_offering/object_interaction_starter.sql
> Estimated bytes processed: 0 (0.0GB)
> Estimated cost of query: ~$0.0
> Referenced tables: []
> ```
---

### Useful links

These helped me during development:
- https://cloud.google.com/bigquery/docs/scheduling-queries
- https://cloud.google.com/bigquery/docs/reference/standard-sql/data-types#timestamp_type
- https://cloud.google.com/bigquery/docs/samples/bigquery-create-job#bigquery_create_job-python
- https://cloud.google.com/python/docs/reference/bigquery/latest/google.cloud.bigquery.job.QueryJobConfig#google_cloud_bigquery_job_QueryJobConfig_time_partitioning
- https://cloud.google.com/python/docs/reference/bigquery/latest/google.cloud.bigquery.table.TimePartitioning
- https://cloud.google.com/python/docs/reference/bigquery/latest/google.cloud.bigquery.job.QueryJob#google_cloud_bigquery_job_QueryJob
- https://cloud.google.com/python/docs/reference/bigquery/latest/google.cloud.bigquery.job.QueryJobConfig

# Things to look at

- https://cloud.google.com/composer/docs/how-to/using/writing-dags
    - https://airflow.apache.org/docs/apache-airflow/stable/_api/airflow/operators/trigger_dagrun/index.html
