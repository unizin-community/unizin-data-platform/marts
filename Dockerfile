FROM apache/airflow:2.6.3-python3.11

ENV AIRFLOW_VERSION "2.6.3"
ENV PYTHON_VERSION "3.11"

COPY requirements.txt .

RUN pip install --upgrade pip && \
    pip install -r requirements.txt
