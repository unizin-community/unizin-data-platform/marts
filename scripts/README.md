# `context_store_entity` and `context_store_keymap` datasets

UDP users often have workflows that span both Postgres and BigQuery. A service that Unizin stands up after the creation of the `context_store` is the creation and refresh schedule of the `context_store_entity` and `context_store_keymap` datasets in BigQuery. These datasets contain BQ-native instances of the same `context_store` data in PostgreSQL.

There are a few benefits to having a refreshed copy exist in BQ. Some very large tables (mostly `learner_activity_result`) take a long time to query from the PostgreSQL DB, but BigQuery handles this data much better. Also, it’s very common for users to join the relational context data with the click stream Caliper event data, and since the Caliper data lives only in BQ, it’s natural to do the joins on the BQ-side.

The `bq_scheduled_queries.py` script can be used to configure `context_store_entity` and `context_store_keymap` datasets, as well as the scheduled queries that refresh the table data within.

> Note: all commands below are assumed to be ran from within the `scripts/` directory

```sh
# prepare the python virtual environment
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
# view parameters
python3 bq_scheduled_queries.py -h
```

Make sure to replace `PROJECT_ID` with a valid GCP project.

```sh
# Create datasets and scheduled queries
python3 bq_scheduled_queries.py PROJECT_ID --create

# Delete scheduled queries
python3 bq_scheduled_queries.py PROJECT_ID --delete

# Manually run scheduled queries
python3 bq_scheduled_queries.py PROJECT_ID --run
```

> Note: any of the [`--create` | `--delete` | `--run`] flags can be combined together for a run. The script will run in the following order: `delete`->`create`->`run`.

By default the script will configure the scheduled queries to run:
- with the [App Engine default service account](https://cloud.google.com/iam/docs/service-account-types#default) (`PROJECT_ID@appspot.gserviceaccount.com`). You can specify a different service account to use with the `--service-account` flag, or have it use your own GCP credentials with the `--self-auth` flag.
- at 16:00 UTC. If changed, with the `--hour` flag, is best to configure run the scheduled queries to run at or after 16:00 UTC to ensure they run post daily `context_store` updates.
