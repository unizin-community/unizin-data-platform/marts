import os, argparse, yaml
from google.cloud import bigquery
from datetime import datetime, UTC

parser = argparse.ArgumentParser()
parser.add_argument("SQL_FILE", help="Path to SQL file.")
parser.add_argument("READ_FROM_PROJECT", help="Project override any instance of `_READ_FROM_PROJECT_` in SQL file.")
parser.add_argument("WRITE_TO_PROJECT", help="Project override any instance of `_WRITE_TO_PROJECT_` in SQL file.")
args = parser.parse_args()
SQL_FILE = args.SQL_FILE
READ_FROM_PROJECT = args.READ_FROM_PROJECT
WRITE_TO_PROJECT = args.WRITE_TO_PROJECT


class colors:
    PASS = "\033[92m"
    FAIL = "\033[91m"
    QUERY = "\033[94m"
    RESET = "\033[0m"


def convert_to_readable(bytes: int) -> str:
    gb = round(bytes / (1024 * 1024 * 1024), 2)
    if gb > 1024:
        return f"{round(gb/1024, 2)}TB"
    else:
        return f"{gb}GB"


if not os.path.isfile(SQL_FILE):
    print(f"{colors.FAIL}ERROR - {SQL_FILE} is not a file.{colors.RESET}")
    exit(1)


with open(SQL_FILE, "r") as file:
    query = file.read()

query = (
    query.replace("_READ_FROM_PROJECT_", READ_FROM_PROJECT)
    .replace("_WRITE_TO_PROJECT_", WRITE_TO_PROJECT)
    .replace("@run_time", f'"{datetime.now(UTC)}"')
)

bq_client = bigquery.Client(project=WRITE_TO_PROJECT)
job_config = bigquery.QueryJobConfig(allow_large_results=True, dry_run=True)
print(f"Dry running query:\n\n{colors.QUERY}{query}{colors.RESET}")
query_job = bq_client.query(query, job_config=job_config)

total_bytes_processed = query_job.total_bytes_processed
readable = convert_to_readable(total_bytes_processed)
referenced_tables = [str(t) for t in query_job.referenced_tables]

# 1 tib = 1024 (gib) * 1024 (mib) * 1024 (kib) * 1024 (bytes)
bytes_per_tib = 1099511627776
# https://cloud.google.com/bigquery/pricing#on_demand_pricing
cost_per_byte = 6.25 / bytes_per_tib

print(f"File: {colors.PASS}{SQL_FILE}{colors.RESET}")
print(f"Estimated bytes processed: {colors.PASS}{total_bytes_processed} ({readable}){colors.RESET}")
print(f"Estimated cost of query: {colors.PASS}~${round(cost_per_byte * total_bytes_processed, 5)}{colors.RESET}")
print(f"Referenced tables:\n{colors.PASS}{yaml.dump(referenced_tables)}{colors.RESET}")
