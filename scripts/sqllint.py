import os, argparse
import sqlfluff
from typing import Iterable

parser = argparse.ArgumentParser()
parser.add_argument("--path", "-p", help="Path to SQL file to lint.", default=None)
args = parser.parse_args()
PATH = args.path


class colors:
    PASS = "\033[92m"
    FAIL = "\033[91m"
    CMD = "\033[93m"
    RESET = "\033[0m"


if PATH:
    if not os.path.isfile(PATH):
        print(f"{colors.FAIL}ERROR - {PATH} is not a file.{colors.RESET}")
        exit(1)


def format_table(
    rows: Iterable[Iterable], headers: bool = False, colsep: str | None = " | "
) -> str:
    """
    Format lists of lists (or other iterables) as the rows and columns of a table.

    When ``headers`` the first row is taken as the value for a headers column.
    Combined with the default ``colsep``, this can produce markdown tables.
    Stolen from: https://gitlab.com/unizin/engineering/apps/unizinlibs/-/blob/master/common/src/unizin/common/format_table.py
    """
    rows = [[str(v) for v in row] for row in rows]
    widths = [max(len(v) for v in col) for col in zip(*rows)]
    fmt = (colsep or "").join(f"{{:{w}}}" for w in widths)

    lines = [fmt.format(*row) for row in rows]

    if headers:
        lines = [
            lines[0],
            # Not bothering to support other divider styles; assuming caller's ``colsep`` makes sense
            "".join(c if c == "|" else "-" for c in lines[0]),  # |---|---| divider
            *lines[1:],
        ]

    return "\n".join(lines)


def lint(file: str):
    with open(file, "r") as f:
        linted = sqlfluff.lint(sql=f.read(), dialect="bigquery")
    if len(linted) == 0:
        return False, linted
    else:
        return True, linted


def pass_fail_msg(failed: bool, file: str, count: int):
    if failed:
        print(f"{colors.FAIL}[FAILED] - {file} ({count} violations){colors.RESET}")
    else:
        print(f"{colors.PASS}[PASSED] - {file} ({count} violations){colors.RESET}")


def process_lint(lint_output: list):
    rules = {r[0]: r[2] for r in sqlfluff.list_rules()}
    findings = {}
    for f in lint_output:
        if f["code"] not in findings:
            findings[f["code"]] = {"count": 0, "description": rules[f["code"]], "lines": []}
        findings[f["code"]]["count"] += 1
        findings[f["code"]]["lines"] += [f["start_line_no"]]

    table = [[v, findings[v]["count"], findings[v]["description"], findings[v]["lines"]] for v in findings]
    table = [["code", "count", "description", "violating lines"]] + table
    print("\n## Violations\n" + format_table(table, headers=True))


if PATH:
    failed, linted = lint(PATH)
    pass_fail_msg(failed, PATH, len(linted))
    if failed:
        process_lint(linted)
        print(f"\nTo view full findings details run: {colors.CMD}sqlfluff lint {PATH} --dialect bigquery{colors.CMD}")
else:
    SQL_FILES = []
    for root, dirs, files in os.walk("./"):
        for file in files:
            if file.endswith(".sql"):
                SQL_FILES.append(os.path.join(root, file))

    failed_files = False
    for file in SQL_FILES:
        failed, linted = lint(file)
        pass_fail_msg(failed, file, len(linted))
        if failed:
            failed_files = True

    if failed_files:
        exit(1)
