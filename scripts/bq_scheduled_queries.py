"""
Create/delete/run bigquery scheduled jobs for the context_store_entity and context_store_keymap datasets

Useful docs:
    https://cloud.google.com/python/docs/reference/bigquerydatatransfer/3.6.0/google.cloud.bigquery_datatransfer_v1.services.data_transfer_service.DataTransferServiceClient
"""
import argparse, datetime
from dataclasses import dataclass
import pathlib
from typing import Dict

import jinja2
from google.cloud import bigquery, bigquery_datatransfer
from google.protobuf.timestamp_pb2 import Timestamp
from progressbar import progressbar


XFER_TEMPLATES_DIR = pathlib.Path(__file__).parent / "xfer_query_templates"
DEFAULT_TEMPLATE = XFER_TEMPLATES_DIR / "default.sql"


def check_hour(value):
    err = argparse.ArgumentTypeError(f"'{value}' is not a valid hour, must be 0-23")
    if value.isdigit():
        value = int(value)
        if (0 > value) or (value > 23):
            raise err
    else:
        raise err
    return value


def check_min(value):
    err = argparse.ArgumentTypeError(f"'{value}' is not a valid minute, must be 0-59")
    if value.isdigit():
        value = int(value)
        if (0 > value) or (value > 59):
            raise err
    else:
        raise err
    return value


DEFAULT_SCHEMAS = ("keymap", "entity")

parser = argparse.ArgumentParser()
parser.add_argument("PROJECT_ID", help="GCP project to create/delete/run scheduled queries in")
parser.add_argument("--create", dest="CREATE", action=argparse.BooleanOptionalAction, help="Create scheduled queries")
parser.add_argument("--run", dest="RUN", action=argparse.BooleanOptionalAction, help="Run scheduled queries")
parser.add_argument("--delete", dest="DELETE", action=argparse.BooleanOptionalAction, help="Delete any jobs this would or has created")
parser.add_argument("--schema", dest="SCHEMAS", choices=DEFAULT_SCHEMAS, help=f"Specific schema to run against")
parser.add_argument("--status", dest="STATUS", action=argparse.BooleanOptionalAction, help="Report Scheduled query status")
parser.add_argument(
    "--service-account",
    dest="SERVICE_ACCOUNT",
    default=None,
    help="Service account to configure jobs to run as, defaults to PROJECT_ID@appspot.gserviceaccount.com",
)
parser.add_argument(
    "--self-auth",
    dest="SELF_AUTH",
    action=argparse.BooleanOptionalAction,
    help="Use current authenticated GCP account to configure jobs to run as",
)
parser.add_argument(
    "--hour",
    dest="HOUR",
    default=16,
    type=check_hour,
    help="Hour 0-23 (UTC) in which to schedule jobs for, default is 16 (UTC). Start date is tomorrow",
)
parser.add_argument(
    "--dist-mins",
    dest="DISTRIBUTE",
    default=15,
    type=check_min,
    help="Minutes 0-59 in which to evenly distribute schedule jobs start times, default is 15. Ex: '15' distributes jobs from minute 0 to 15",
)
args = parser.parse_args()

PROJECT_ID = args.PROJECT_ID
STATUS = args.STATUS
DELETE = args.DELETE
RUN = args.RUN
CREATE = args.CREATE
SELF_AUTH = None if not args.SELF_AUTH else True
DISTRIBUTE = args.DISTRIBUTE + 1 if args.DISTRIBUTE > 0 else args.DISTRIBUTE

if len([a for a in (args.SERVICE_ACCOUNT, SELF_AUTH) if a not in (None, False)]) > 1:
    parser.error("--self-auth and --service-account cannot both be used")

SCHEMAS = (args.SCHEMAS,) if args.SCHEMAS else DEFAULT_SCHEMAS
SERVICE_ACCOUNT = args.SERVICE_ACCOUNT if args.SERVICE_ACCOUNT else f"{PROJECT_ID}@appspot.gserviceaccount.com"
SA_MSG = None if SELF_AUTH else f", service account: {SERVICE_ACCOUNT}"
BQ_CLIENT = bigquery.Client(project=PROJECT_ID)
TRANSFER_CLIENT = bigquery_datatransfer.DataTransferServiceClient()
# returns projects/PROJECT_ID
TRANSFER_CONFIG_PARENT = TRANSFER_CLIENT.common_project_path(PROJECT_ID)
# 16:XX UTC tomorrow
TOMORROW = (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=1)).replace(hour=args.HOUR, minute=0, second=0, microsecond=0)
DATASET_PREFIX = "context_store_"


def dataset_name(schema: str) -> str:
    return f"{DATASET_PREFIX}{schema}"


def load_template(project_id: str, schema: str, table: str) -> str:
    table_template_path = XFER_TEMPLATES_DIR / schema / f"{table}.sql"
    table_template = (
        table_template_path if table_template_path.is_file() else DEFAULT_TEMPLATE
    )
    with open(table_template) as file:
        template = jinja2.Template(file.read(), undefined=jinja2.StrictUndefined)
        return template.render(project_id=project_id, schema=schema, table=table)


@dataclass
class ScheduledQuery:
    schema: str
    table: str
    minute: int

    def __post_init__(self):
        self.name = f"{dataset_name(self.schema)}__{self.table}"
        self.destination_dataset = dataset_name(self.schema)
        self.destination_table = f"{self.table}"

        external_query = load_template(
            project_id=PROJECT_ID, schema=self.schema, table=self.table
        )

        self.transfer_config = bigquery_datatransfer.TransferConfig(
            destination_dataset_id=self.destination_dataset,
            display_name=self.name,
            data_source_id="scheduled_query",
            params={
                "query": external_query,
                "destination_table_name_template": f"{self.destination_table}",
                "write_disposition": "WRITE_TRUNCATE",
            },
            schedule="every 24 hours",
            schedule_options=bigquery_datatransfer.ScheduleOptions(
                start_time=Timestamp(
                    seconds=int(TOMORROW.replace(minute=self.minute).timestamp())
                )
            ),
        )


def get_table_names(schema: str) -> list:
    TABLE_NAMES_QUERY = f"""
        SELECT table_name
        FROM EXTERNAL_QUERY(
            "{PROJECT_ID}.us.context_store",
            "SELECT * FROM INFORMATION_SCHEMA.TABLES;"
        )
        WHERE table_schema='{schema}'
    """
    return [row["table_name"] for row in BQ_CLIENT.query(TABLE_NAMES_QUERY)]


def pad_str(base_string: str, max_len: int) -> str:
    return " " * (max_len - len(base_string))


# TODO: should probably not create jobs that already exist with the same name
def create_scheduled_query(sched_query: ScheduledQuery):
    ctc_request = bigquery_datatransfer.CreateTransferConfigRequest(
        parent=TRANSFER_CONFIG_PARENT,
        transfer_config=sched_query.transfer_config,
    )
    if not SELF_AUTH:
        ctc_request.service_account_name = SERVICE_ACCOUNT

    TRANSFER_CLIENT.create_transfer_config(ctc_request)


def list_transfer_configs():
    return TRANSFER_CLIENT.list_transfer_configs(parent=TRANSFER_CONFIG_PARENT)


def get_transfer_config_name(destination_dataset: str, display_name: str):
    for config in list_transfer_configs():
        if config.destination_dataset_id == destination_dataset and config.display_name == display_name:
            return config.name

    return None


def run_scheduled_query(sched_query: ScheduledQuery):
    now = datetime.datetime.now(datetime.timezone.utc)
    start_time = now - datetime.timedelta(days=3)
    end_time = now - datetime.timedelta(days=2)

    requested_time_range = bigquery_datatransfer.StartManualTransferRunsRequest.TimeRange(
        start_time=start_time,
        end_time=end_time,
    )

    transfer_config_id = get_transfer_config_name(sched_query.destination_dataset, sched_query.name)
    transfer_run_request = bigquery_datatransfer.StartManualTransferRunsRequest(
        parent=transfer_config_id,
        requested_time_range=requested_time_range,
    )

    TRANSFER_CLIENT.start_manual_transfer_runs(transfer_run_request)


def bq_dataset_exists(dataset_id: str) -> bool:
    datasets = list(BQ_CLIENT.list_datasets())
    if datasets:
        datasets = [dataset.dataset_id for dataset in datasets]
        if dataset_id in datasets:
            return True
        else:
            return False
    else:
        return False


def create_bq_dataset(dataset_id: str):
    dataset_fq_id = f"{BQ_CLIENT.project}.{dataset_id}"
    if bq_dataset_exists(dataset_id):
        print(f"Dataset {dataset_fq_id} already exists...")
    else:
        print(f"Creating dataset {dataset_fq_id}...")
        dataset = bigquery.Dataset(f"{dataset_fq_id}")
        dataset.location = "US"
        dataset = BQ_CLIENT.create_dataset(dataset, timeout=30)


def print_column_names(schema: str, max_len: int):
    # im sure there is a better way to do this
    print(f"SCHEDULED QUERY{pad_str('SCHEDULED QUERY', max_len)}{' ' * len(f'{DATASET_PREFIX}{schema}__')}DESTINATION BQ TABLE")


def distribute_jobs(tables: list[str]) -> dict[str, int]:
    """
    Assign values 0-DISTRIBUTE to tables. 

    If DISTRIBUTE is less than the length of tables, will share the limited values evenly,
    assigning the same value to many tables in equal-sized groups.
    """
    if DISTRIBUTE > 0:
        base, extra = divmod(len(tables), DISTRIBUTE)
        distribution = [base + (i < extra) for i in range(DISTRIBUTE)]
    else:
        distribution = [len(tables)]

    trk = 0
    tbl_min = {}
    for min, dist in enumerate(distribution):
        for i in range(trk, trk + dist):
            tbl_min[tables[i]] = min

        trk = trk + dist

    return tbl_min


def create_n_run(schema: str, create: bool = False, run: bool = False):
    if create:
        create_bq_dataset(dataset_name(schema))

    tables = get_table_names(schema)
    max_len = len(max(tables, key=len)) + 4
    job_distribution = distribute_jobs(tables)

    queries = []
    for table in tables:
        query = ScheduledQuery(schema=schema, table=table, minute=job_distribution[table])
        fmt_str = f"{query.name}{pad_str(table, max_len)}{PROJECT_ID}.{query.destination_dataset}.{query.destination_table}"
        queries.append((query, fmt_str))

    doing_msg = f"scheduled queries for project: {PROJECT_ID}, schema: {schema}{SA_MSG}\n"

    if create:
        print(f"Creating {doing_msg}")
        print_column_names(schema, max_len)
        for sched_query in progressbar(queries, redirect_stdout=True):
            print(sched_query[1])
            create_scheduled_query(sched_query[0])
    if run:
        print(f"Running {doing_msg}")
        print_column_names(schema, max_len)
        for sched_query in progressbar(queries, redirect_stdout=True):
            print(sched_query[1])
            run_scheduled_query(sched_query[0])


def delete(schema: str):
    print(f"Fetching scheduled queries to delete for project: {PROJECT_ID}, schema: {schema}\n")

    to_match = [f"{dataset_name(schema)}__{table}" for table in get_table_names(schema)]
    chopping_block = {config.name: config.display_name for config in list_transfer_configs() if config.display_name in to_match}

    if len(chopping_block) > 0:
        print(f"About to delete the following scheduled queries in {PROJECT_ID}:")

        for query in chopping_block:
            print(f"  - {query}: {chopping_block[query]}")
        doit = input("Delete these scheduled queries? [y/n] ")

        if doit.lower() in ["y", "yes"]:
            for query in progressbar(chopping_block, redirect_stdout=True):
                print(f"Deleting scheduled query {query} - {chopping_block[query]}")
                TRANSFER_CLIENT.delete_transfer_config(name=query)
        else:
            print("Aborting.")
    else:
        print(f"Nothing to do, no matching jobs found in {PROJECT_ID}.")


def status(schema: str):
    print(
        f"Fetching scheduled queries status for project: {PROJECT_ID}, schema: {schema}\n"
    )

    def find_duplicates(name__display_name: Dict[str, str]):
        display_names = list(name__display_name.values())
        dupe_display_names = set(
            [c for c in display_names if display_names.count(c) > 1]
        )
        dupes = {
            name: display_name
            for name, display_name in name__display_name.items()
            if display_name in dupe_display_names
        }
        return dupes

    expected_xfer_cfgs = [
        f"{dataset_name(schema)}__{table}" for table in get_table_names(schema)
    ]
    found_xfer_cfgs = {
        config.name: config.display_name
        for config in list_transfer_configs()
        if config.display_name in expected_xfer_cfgs
    }
    duplicates = find_duplicates(found_xfer_cfgs)
    missing = [
        expected
        for expected in expected_xfer_cfgs
        if expected not in found_xfer_cfgs.values()
    ]

    print(f"Existing {schema} queries:")
    for query in found_xfer_cfgs:
        print(f"  - {query}: {found_xfer_cfgs[query]}")

    if duplicates:
        print(f"WARNING: These queries are defined more than once for {schema}:")
        for query in duplicates:
            print(f"  - {query}: {duplicates[query]}")

    if missing:
        print(f"Missing queries for {schema}:")
        for query_name in missing:
            print(f"  - {query_name}")


if STATUS == True:
    for schema in SCHEMAS:
        status(schema)
    if any((DELETE, CREATE, RUN)):
        print("WARNING: extra actions ignored when providing --status, exiting.")
elif True in (DELETE, CREATE, RUN):
    for schema in SCHEMAS:
        if DELETE:
            delete(schema)
        if CREATE or RUN:
            create_n_run(schema, CREATE, RUN)
else:
    print(
        "ERROR: you must supply at least one of [--status | --create | --delete | --run] to do anything.\n"
    )
    parser.print_help()
