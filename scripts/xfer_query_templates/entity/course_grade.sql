SELECT * 
FROM EXTERNAL_QUERY(
    '{{ project_id }}.us.context_store',
    '''
    select *
    from {{ schema }}.{{ table }}
    where (
        (grade_points BETWEEN -99999999999999999999999999999.999999999 AND 99999999999999999999999999999.999999999)
        OR (grade_points IS NULL)
    ) and (
        (le_current_score BETWEEN -99999999999999999999999999999.999999999 AND 99999999999999999999999999999.999999999)
        OR (le_current_score IS NULL)
    ) and (
        (le_final_score BETWEEN -99999999999999999999999999999.999999999 AND 99999999999999999999999999999.999999999)
        OR (le_final_score IS NULL)
    ) and (
        (le_hidden_current_score BETWEEN -99999999999999999999999999999.999999999 AND 99999999999999999999999999999.999999999)
        OR (le_hidden_current_score IS NULL)
    ) and (
        (le_hidden_final_score BETWEEN -99999999999999999999999999999.999999999 AND 99999999999999999999999999999.999999999)
        OR (le_hidden_final_score IS NULL)
    )
    '''
)
