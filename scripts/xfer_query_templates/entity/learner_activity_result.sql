SELECT * 
FROM EXTERNAL_QUERY(
    '{{ project_id }}.us.context_store',
    '''
    select *
    from {{ schema }}.{{ table }}
    where (
        (published_score BETWEEN -99999999999999999999999999999.999999999 AND 99999999999999999999999999999.999999999)
        OR (published_score IS NULL)
    )
    '''
)
