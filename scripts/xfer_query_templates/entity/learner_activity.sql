SELECT * 
FROM EXTERNAL_QUERY(
    '{{ project_id }}.us.context_store',
    '''
    select *
    from {{ schema }}.{{ table }}
    where (
        (points_possible BETWEEN -99999999999999999999999999999.999999999 AND 99999999999999999999999999999.999999999)
        or (points_possible IS NULL)
    )
    '''
)
