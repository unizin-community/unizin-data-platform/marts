SELECT * 
FROM EXTERNAL_QUERY(
    '{{ project_id }}.us.context_store',
    '''
    select * from {{ schema }}.{{ table }}
    '''
)
