from os import environ, write
from datetime import datetime, timedelta, UTC
from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.decorators import dag, task, task_group
from google.cloud import bigquery
import dag_base
import os
import sys

script_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(script_dir, "..")
sys.path.append(parent_dir)
from distribution.distributions import EntityConfig, set_config, render_sql_file
from load_static_tables import get_bq_client


config = set_config()

def template_entity_operator(entity: str, dist_type: str, dest_table: str):
    op = EmptyOperator(task_id=f"{dest_table}-{dist_type}-{entity}-setup")
    return op


def create_job_config(destination: str, write_disposition="WRITE_APPEND"):
    JobConfig = bigquery.QueryJobConfig(
        destination=destination,
        # create_disposition=bigquery.CreateDisposition.CREATE_IF_NEEDED,
        write_disposition="WRITE_APPEND",
        allow_large_results=True,
        # time_partitioning=time_partitioning,
        dry_run=dag_base.DRY_RUN
    )
    return JobConfig


def rootpath(path: str) -> str:
    """
    Template path pointing to parent directory (..) works locally, but not in deploy where code is moved into one root bucket directory
    adjust template_path if FileNotFound.
    """
    def panic_at_the_distribution():
        return FileNotFoundError(f"Check your base directory running the DAG: {BASE_DIR=}, and your {fullpath=}")

    fullpath = os.path.join(
        dag_base.BASE_DIR,
        path,
    )

    if os.path.isfile(fullpath):
        print(f"current {fullpath =}")
        return fullpath
    else:
        raise panic_at_the_distribution()


def append_distributions(
    job_config,
    entity,
    dataset_id,
    dist_type,
    client,
    filtered_fields,
    with_respect_to=None,
):
    template_path = rootpath(f"distribution/sql/{dist_type}_distributions.jinja.sql")
    e_config = EntityConfig(entity, filtered_fields)
    e_config.set_fields(client.project, dataset_id, client)
    row_count = e_config.get_row_count(source_client.project, dataset_id, client)

    for f in e_config.get_filtered_schema():
        dependent_fields = with_respect_to[f] if with_respect_to is not None else None
        dist_template = render_sql_file(template_path)(
            entity=e_config.entity,
            project=client.project,
            field=f,
            row_count=row_count,
            with_respect_to=dependent_fields,
        )
        print(f"Query Template: \n\n {dist_template}")
        distributions = client.query(dist_template, job_config)
        rows = distributions.result()


def concat_distributions(
    job_config,
    dataset_id,
    client,
    input_tables,
):
    concat_template_path = rootpath(f"distribution/sql/concat_distributions.jinja.sql")
    concat_sql = render_sql_file(concat_template_path)(
        project=client.project,
        dataset_id=dataset_id,
        table_list=input_tables,
    )
    print(concat_sql)
    concat_dist = client.query(concat_sql, job_config)
    rows = concat_dist.result()


for tenant in dag_base.TENANTS:

    shortcode = tenant["id"]
    read_from_project, write_to_project = dag_base.read_write_projects(shortcode)
    dest_client = get_bq_client(write_to_project)
    source_client = get_bq_client(read_from_project)

    schedule = os.environ.get(f"DISTRIBUTIONS_SCHEDULE", None)
    if not schedule:
        if "schedule_overrides" in tenant:
            schedule = tenant["schedule_overrides"]["distributions"]
        else:
            schedule = config["distributions_schedule"]

    @dag(
        dag_id=f"{shortcode}-{dag_base.ENV}-distributions",
        schedule_interval=schedule,
        default_args=dag_base.DEFAULT_ARGS,
        dagrun_timeout=timedelta(minutes=dag_base.DAGRUN_TIMEOUT),
        catchup=False,
    )
    def taskflow():
        dist_dataset_id = config["distribution_dataset"]
        init_dataset = PythonOperator(
            task_id=f"{shortcode}_create_distributions",
            python_callable=dag_base.create_bq_dataset,
            op_kwargs={"client": dest_client, "dataset_id": dist_dataset_id},
        )
        # hardcoding these for now, but should add to config.
        dist_tables = [
            "categorical_distributions",
            "continuous_distributions",
            "categorical_dependent_distributions",
            "context_store_distributions",
        ]
        drop_create_tasks = []
        for table_id in dist_tables:
            drop_table = PythonOperator(
                task_id=f"drop_{table_id}_table",
                python_callable=dag_base.delete_bq_table,
                op_kwargs={
                    "client": dest_client,
                    "dataset_id": dist_dataset_id,
                    "table_id": table_id,
                },
            )
            create_table = PythonOperator(
                task_id=f"create_{table_id}_table",
                python_callable=dag_base.create_bq_table,
                op_kwargs={
                    "client": dest_client,
                    "dataset_id": dist_dataset_id,
                    "table_id": table_id,
                    "schema": config["schema"],
                },
            )
            drop_create = init_dataset >> drop_table >> create_table
            drop_create_tasks.append(drop_create)
        end_init = EmptyOperator(task_id="end_setup")
        drop_create_tasks >> end_init

        ConcatJobConfig = create_job_config(
            f"{write_to_project}.{dist_dataset_id}.context_store_distributions",
            write_disposition="WRITE_TRUNCATE",
        )
        concat_dist_tables = PythonOperator(
            task_id=f"concat_distribution_tables",
            python_callable=concat_distributions,
            op_kwargs={
                "job_config": ConcatJobConfig,
                "dataset_id": dist_dataset_id,
                "client": dest_client,
                "input_tables": dist_tables,
            },
        )

        entities = config["distributions"]["entities"].keys()
        source_dataset_id = config["source_dataset_id"]
        for entity in entities:
            e_task_groups = []
            entity_config = config["distributions"]["entities"][entity]
            dist_types = entity_config.keys()

            @task_group(group_id=f"{entity}_task_group")
            def entity_task_group():
                start_chain = EmptyOperator(task_id=f"{entity}-start")

                for dist_type in dist_types:
                    job_config = create_job_config(
                        f"{write_to_project}.{dist_dataset_id}.{dist_type}_distributions"
                    )
                    ucdm_fields = entity_config[dist_type]["ucdm_fields"]
                    base_dist_task = PythonOperator(
                        task_id=f"{entity}_{dist_type}_context_distributions",
                        python_callable=append_distributions,
                        op_kwargs={
                            "job_config": job_config,
                            "entity": entity,
                            "dataset_id": source_dataset_id,
                            "dist_type": dist_type,
                            "client": source_client,
                            "filtered_fields": ucdm_fields,
                        },
                    )
                    start_chain >> base_dist_task
                    # gather dependent field config
                    e_depends_on = entity_config[dist_type].get(
                        "dependent_fields", None
                    )
                    wrt_job_config = create_job_config(
                        destination=f"{write_to_project}.{dist_dataset_id}.{dist_type}_dependent_distributions"
                    )
                    if e_depends_on is not None:
                        # if config contains dependent fields, loop through and add downstream tasks
                        depends_task = PythonOperator(
                            task_id=f"{entity}_{dist_type}_dependent_distributions",
                            python_callable=append_distributions,
                            op_kwargs={
                                "job_config": wrt_job_config,
                                "entity": entity,
                                "dataset_id": source_dataset_id,
                                "dist_type": dist_type,
                                "client": source_client,
                                "filtered_fields": list(e_depends_on.keys()),
                                "with_respect_to": e_depends_on,  # only additional kwarg from "base_dist_task"
                            },
                        )
                        base_dist_task >> depends_task

            e_task_groups.append(entity_task_group())

            (
                end_init >> e_task_groups >> concat_dist_tables
            )

    taskflow()
