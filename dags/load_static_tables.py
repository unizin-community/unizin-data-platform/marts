from os import environ
from datetime import timedelta
from airflow import DAG
from airflow.decorators import dag, task
from google.cloud import bigquery
import dag_base
import util

LOG = util.LOG

static_tables = dag_base.BASE_CONFIG["static_tables"]
static_tables_schedule = environ.get("STATIC_TABLES_SCHEDULE", None) or dag_base.BASE_CONFIG.get("static_tables_schedule", "0 10 * * 1")


def get_bq_client(project_id: str) -> bigquery.Client:
    if dag_base.IS_CLOUD_COMPOSER:
        return bigquery.Client(project=project_id)
    elif dag_base.SA_KEY_PATH:
        return bigquery.Client(project=project_id, credentials=dag_base.SA_CREDS)
    else:
        return bigquery.Client(project=project_id)


def create_bq_datasets(write_to_project: str):
    bq_client = get_bq_client(write_to_project)

    datasets = [mart["target_table"].split(".")[0] for mart in static_tables if "target_table" in mart]
    datasets = [*set(datasets)]
    for d in datasets:
        dag_base.create_bq_dataset(bq_client, d)


def load_csv_to_bq(file_def: dict, write_to_project: str):
    bq_client = get_bq_client(write_to_project)
    table_fq_id = f"{bq_client.project}.{file_def['target_table']}"

    job_config = bigquery.LoadJobConfig(
        schema=[bigquery.SchemaField(s["field"], s["type"]) for s in file_def["schema"]],
        source_format=bigquery.SourceFormat.CSV,
        write_disposition=dag_base.WRITE_DISPOSITION["WRITE_TRUNCATE"],
    )
    with open(f"{dag_base.BASE_DIR}/{file_def['file']}", "rb") as file:
        if dag_base.DRY_RUN:
            LOG.info(f"DRY RUN - Skipping table load for {table_fq_id} from {file.name}")
        else:
            load_job = bq_client.load_table_from_file(file, table_fq_id, job_config=job_config)
            LOG.info(f"Starting job {load_job.job_id}")
            load_job.result()
            LOG.info("Job finished.")
    file.close()

    if not dag_base.DRY_RUN:
        destination_table = bq_client.get_table(table_fq_id)
        LOG.info(f"Loaded {destination_table.num_rows} rows into {table_fq_id}.")


def load_static_tables(tenant: str):
    read_from_project, write_to_project = dag_base.read_write_projects(tenant)

    task_tracker = []
    for file_def in static_tables:
        task_load_csv_to_bq = task(task_id=f"{file_def['file'].split('/')[1].split('.')[0]}", depends_on_past=False)(load_csv_to_bq)
        load_static_file = task_load_csv_to_bq(file_def, write_to_project)
        task_tracker.append(load_static_file)
        load_static_file

    task_create_bq_datasets = task(task_id="create_bq_datasets", depends_on_past=False)(create_bq_datasets)
    create_datasets = task_create_bq_datasets(write_to_project)
    create_datasets
    create_datasets.set_downstream(task_tracker)


for tenant in dag_base.TENANTS:
    shortcode = tenant["id"]
    @dag(
        dag_id=f"{shortcode}-{dag_base.ENV}-load_static_tables",
        schedule_interval=static_tables_schedule,
        default_args=dag_base.DEFAULT_ARGS,
        dagrun_timeout=timedelta(minutes=dag_base.DAGRUN_TIMEOUT),
        catchup=False,
    )
    def taskflow():
        load_static_tables(shortcode)


    taskflow()
