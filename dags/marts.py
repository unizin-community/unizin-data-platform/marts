import os
from datetime import datetime, timedelta, UTC
from airflow import DAG
from airflow.decorators import dag, task
from google.cloud import bigquery
import dag_base
import util

LOG = util.LOG

mart_configs = dag_base.BASE_CONFIG["mart_config_paths"]
timeout = timedelta(minutes=dag_base.DAGRUN_TIMEOUT)


def get_bq_client(project_id: str) -> bigquery.Client:
    if dag_base.IS_CLOUD_COMPOSER:
        return bigquery.Client(project=project_id)
    elif dag_base.SA_KEY_PATH:
        return bigquery.Client(project=project_id, credentials=dag_base.SA_CREDS)
    else:
        return bigquery.Client(project=project_id)


def create_bq_datasets(marts: list, write_to_project: str):
    bq_client = get_bq_client(write_to_project)
    datasets = [mart["target_table"].split(".")[0] for mart in marts if "target_table" in mart]
    datasets += [mart["target_dataset"] for mart in marts if "target_dataset" in mart]
    datasets = [*set(datasets)]

    for d in datasets:
        dag_base.create_bq_dataset(bq_client, d)

def load_mart_table(read_from_project: str, write_to_project: str, mart_def: dict):
    bq_client = get_bq_client(write_to_project)

    query = dag_base.load_file_to_string(mart_def["file"])
    query = (
        query.replace("_READ_FROM_PROJECT_", read_from_project)
        .replace("_WRITE_TO_PROJECT_", write_to_project)
        .replace("@run_time", f'"{datetime.now(UTC).replace(tzinfo=None)}"')
    )

    if "target_table" in mart_def:
        table_fq_id = f"{bq_client.project}.{mart_def['target_table']}"
        if "partition_field" in mart_def:
            default_partition_type = dag_base.TIME_PARTITIONS["DAY"]
            if "default_partition_type" in mart_config:
                default_partition_type = dag_base.TIME_PARTITIONS[
                    os.environ.get(
                        f"{mart_base.upper()}_DEFAULT_PARTITION_TYPE",
                        mart_config["default_partition_type"],
                    )
                ]
            if "partition_type" in mart_def:
                default_partition_type = dag_base.TIME_PARTITIONS[mart_def["partition_type"]]

            time_partitioning = bigquery.TimePartitioning(
                type_=default_partition_type, field=mart_def["partition_field"]
            )
        else:
            time_partitioning = None

        job_config = bigquery.QueryJobConfig(
            destination=table_fq_id,
            create_disposition=bigquery.CreateDisposition.CREATE_IF_NEEDED,
            write_disposition=dag_base.WRITE_DISPOSITION[mart_def["write_disposition"]],
            allow_large_results=True,
            time_partitioning=time_partitioning,
            dry_run=dag_base.DRY_RUN
        )
    else:
        job_config = bigquery.QueryJobConfig(allow_large_results=True, dry_run=dag_base.DRY_RUN)

    LOG.info(
        f"""Using job config:
        destination: {job_config.destination}
        create_disposition: {job_config.create_disposition}
        write_disposition: {job_config.write_disposition}
        allow_large_results: {job_config.allow_large_results}
        time_partitioning: {job_config.time_partitioning}
        dry_run: {job_config.dry_run}
    """
    )
    LOG.info(f"Running query:\n\n{query}")
    query_job = bq_client.query(query, job_config=job_config)
    LOG.info(f"Started job: {query_job.job_id}")

    dag_base.verify_job_finished(query_job)


def _load_marts(tenant: str, marts: list):
    read_from_project, write_to_project = dag_base.read_write_projects(tenant)

    task_tracker = {}
    for mart_def in marts:
        task_load_mart_table = task(task_id=mart_def["mart"], execution_timeout=timeout, depends_on_past=False)(load_mart_table)
        load_mart = task_load_mart_table(read_from_project, write_to_project, mart_def)
        task_tracker[mart_def["mart"]] = load_mart

    # build dependencies
    top_level = dag_base.build_dependency_tree(marts, task_tracker)

    task_create_bq_datasets = task(task_id="create_bq_datasets", execution_timeout=timeout, depends_on_past=False)(create_bq_datasets)
    create_datasets = task_create_bq_datasets(marts, write_to_project)
    create_datasets
    create_datasets.set_downstream(top_level)


for tenant in dag_base.TENANTS:
    shortcode = tenant["id"]
    starters = []
    for mart_base in mart_configs:
        mart_config_path = mart_configs[mart_base]
        mart_config = dag_base.load_config_file(mart_config_path)

        schedule = os.environ.get(f"{mart_base.upper()}_SCHEDULE", None)
        if not schedule:
            if "schedule_overrides" in tenant.keys():
                if mart_base in tenant["schedule_overrides"].keys():
                    schedule = tenant["schedule_overrides"][mart_base]
                else:
                    schedule = mart_config["schedule"]
            else:
                schedule = mart_config["schedule"]


        marts = mart_config["marts"]

        if "starters" in mart_config:
            starters += mart_config["starters"]

        # marts dag
        @dag(
            dag_id=f"{shortcode}-{dag_base.ENV}-{mart_base}",
            schedule_interval=schedule,
            default_args=dag_base.DEFAULT_ARGS,
            dagrun_timeout=timeout,
            catchup=False,
        )
        def taskflow():
            _load_marts(shortcode, marts)


        taskflow()


    # starters dag
    @dag(
        dag_id=f"{shortcode}-{dag_base.ENV}-starters",
        schedule_interval=None,
        default_args=dag_base.DEFAULT_ARGS,
        dagrun_timeout=timeout,
        catchup=False,
    )
    def starter_tf():
        _load_marts(shortcode, starters)


    starter_tf()
