from os import environ
from datetime import timedelta
import requests
from airflow.models import DagBag, DagRun
from airflow.decorators import dag, task
import dag_base
import util

LOG = util.LOG
SLACK_AUTH_HEADERS = {"Authorization": f"Bearer {environ.get('SLACK_BOT_TOKEN', None)}"}
SLACK_CHANNEL = environ.get("SLACK_CHANNEL", None)
PREVIOUS_DAG_RUNS = 3

monitor_schedule = environ.get("MONITOR_SCHEDULE", None) or dag_base.BASE_CONFIG.get("monitor_schedule", "15 14 * * *")


def get_duration(dag_run: DagRun):
    if dag_run.end_date and dag_run.start_date:
        duration = dag_run.end_date - dag_run.start_date
        return duration - timedelta(microseconds=duration.microseconds)
    else:
        return None


def strip_date(dt: timedelta):
    return str(dt).split(".")[0].split("+")[0]


def fetch_dag_statuses():
    dag_ids = DagBag(include_examples=False).dag_ids
    # exclude airflow monitoring dags, starters and load static tables
    # only keeps jobs that run daily/hourly
    dag_ids = [id for id in dag_ids 
               if id not in ["airflow_unizin_monitor", "airflow_monitoring"]
               and not id.endswith(f"-{dag_base.ENV}-load_static_tables")
               and not id.endswith(f"-{dag_base.ENV}-starters")]

    tldr = {}
    full = {}
    for id in dag_ids:
        tenant = id.split("-")[0]
        if tenant not in tldr:
            tldr[tenant] = []
        if id not in full:
            full[id] = []

        # get list of dag runs in chronological order, returns list or None
        dag_runs = DagRun.find(dag_id=id)
        if dag_runs:
            # reverse the list so times are descending
            dag_runs.reverse()
            # cut list down to only the previous x runs if bigger than PREVIOUS_DAG_RUNS
            if len(dag_runs) > PREVIOUS_DAG_RUNS:
                dag_runs = dag_runs[0:PREVIOUS_DAG_RUNS]

            for run in dag_runs:
                full[id].append([run.state, get_duration(run), strip_date(run.execution_date)])

            tldr[tenant].append([id, dag_runs[0].state, get_duration(dag_runs[0]), strip_date(dag_runs[0].execution_date)])
        else:
            tldr[tenant].append([id, None, None, None])
    
    # sort dags so each list will be in the same order
    for tenant in tldr:
        tldr[tenant].sort()
    
    return full, tldr


def send_to_slack(url: str, data: dict):
    post_msg = requests.post(url, data=data, headers=SLACK_AUTH_HEADERS)
    if post_msg.status_code == 200:
        LOG.info(f"Successful slack post to channel {SLACK_CHANNEL}")
    else:
        LOG.error(f"Slack post returned {post_msg.status_code} instead of expected 200. Payload: {data}")


def send_message(message: str):
    data = {
        "text": message,
        "channel": SLACK_CHANNEL
    }
    send_to_slack("https://slack.com/api/chat.postMessage", data)


def send_file(title: str, contents: str):
    data = {
        "title": title,
        "channels": SLACK_CHANNEL,
        "content": contents,
        "filename": title
    }
    send_to_slack("https://slack.com/api/files.upload", data)


def add_to_content_str(content: str, add_on: str):
    return f"{content}\n{add_on}"


def format_full_message(full: dict):
    full_msg = f"## Composer [{dag_base.ENV}]\n"
    full_headers = ["state", "duration", "execution_date (utc)"]
    ordered_keys = list(full.keys())
    ordered_keys.sort()
    for dag_id in ordered_keys:
        full_msg = add_to_content_str(full_msg, "#")
        full_msg = add_to_content_str(full_msg, f"# {dag_id}")

        if full[dag_id]:
            full_msg = add_to_content_str(full_msg, dag_base.format_table([full_headers] + full[dag_id], headers=True))
        else:
            full_msg = add_to_content_str(full_msg, f"No runs for {dag_id}")

        full_msg = add_to_content_str(full_msg, "\n")

    return full_msg


def report_statuses():
    full, tldr = fetch_dag_statuses()
    for tenant in tldr:
        tldr_headers = ["dag", "state", "duration", "execution_date (utc)"]
        tldr_msg = f"```\n## Composer [{dag_base.ENV}] {tenant} TLDR\n"
        tldr_msg = f"{tldr_msg}{dag_base.format_table([tldr_headers] + tldr[tenant], headers=True)}\n```"
        send_message(tldr_msg)
    
    full_contents = format_full_message(full)
    send_file(f"{dag_base.ENV.upper()} composer dag status FULL", full_contents)


def _check_status():
    task_check_status = task(task_id="slack_report", depends_on_past=False, retries=0)(report_statuses)
    run = task_check_status()
    run


@dag(
    dag_id=f"airflow_unizin_monitor",
    schedule_interval=monitor_schedule,
    default_args=dag_base.DEFAULT_ARGS,
    catchup=False,
)
def taskflow():
    _check_status()


taskflow()
