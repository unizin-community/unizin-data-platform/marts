import logging

LOG_FORMAT='{"severity": "%(levelname)s", "message": "%(message)s"}'
logging.basicConfig(format=LOG_FORMAT, encoding="utf-8", level=logging.INFO)
logger = logging.getLogger(__name__)

class LOG():
    def info(msg):
        return logger.info(msg)

    def warning(msg):
        return logger.warning(msg)

    def error(msg):
        return logger.error(msg)
