import os, yaml, time
import datetime as dt
from google.oauth2 import service_account
from google.cloud import bigquery
from typing import Iterable
import util
from typing import List

LOG = util.LOG

# set this in cloud composer so that it will use cc specific settings
IS_CLOUD_COMPOSER = os.environ.get("IS_CLOUD_COMPOSER", False)

if IS_CLOUD_COMPOSER:
    BASE_DIR = "/home/airflow/gcs/dags"
else:
    BASE_DIR = os.getcwd()

SA_KEY_PATH = os.environ.get("SA_KEY_PATH", None)
if SA_KEY_PATH:
    SA_CREDS = service_account.Credentials.from_service_account_file(
        SA_KEY_PATH, scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )


def load_config_file(path: str) -> dict:
    if not path.startswith(BASE_DIR):
        path = f"{BASE_DIR}/{path}"

    with open(path, "r") as f:
        config = yaml.safe_load(f)

    return config


BASE_CONFIG_PATH = f"{BASE_DIR}/{os.environ.get('BASE_CONFIG_PATH', 'config.yaml')}"
BASE_CONFIG = load_config_file(BASE_CONFIG_PATH)
DAGRUN_TIMEOUT = os.environ.get("DAGRUN_TIMEOUT", 240)

ENV_CONFIG_PATH = f"{BASE_DIR}/{os.environ.get('ENV_CONFIG_PATH', 'env.yaml')}"
ENV_CONFIG_EXISTS = os.path.isfile(ENV_CONFIG_PATH)

DRY_RUN = os.environ.get("DRY_RUN", BASE_CONFIG.get("dry_run", False))
if isinstance(DRY_RUN, str) and DRY_RUN.lower() == "false":
    DRY_RUN = False
else:
    DRY_RUN = bool(DRY_RUN)

if IS_CLOUD_COMPOSER and not ENV_CONFIG_EXISTS:
    LOG.error(f"{BASE_DIR}/{ENV_CONFIG_PATH} does not exists! Composer cannot run without this file, please correct.")
    raise FileNotFoundError(f"{BASE_DIR}/{ENV_CONFIG_PATH}")
elif ENV_CONFIG_EXISTS:
    ENV_CONFIG = load_config_file(ENV_CONFIG_PATH)
    TENANTS = ENV_CONFIG["tenants"]
    ENV = os.environ.get("ENV", ENV_CONFIG["env"])
elif set(["tenants", "env"]).issubset(set(BASE_CONFIG.keys())):
    TENANTS = BASE_CONFIG["tenants"]
    ENV = os.environ.get("ENV", BASE_CONFIG["env"])
else:
    LOG.error(f"Missing {ENV_CONFIG_PATH} file or default tenant/env values in {BASE_CONFIG_PATH}!")
    raise ValueError(
        "Invalid configs! Please see https://gitlab.com/unizin/dss/udp-marts#configs for proper configurations."
    )


DEFAULT_ARGS = {
    # https://airflow.apache.org/faq.html#what-s-the-deal-with-start-date
    "start_date": dt.datetime(2023, 6, 1),
    "concurrency": 0,
    "depends_on_past": False,
}

if IS_CLOUD_COMPOSER:
    DEFAULT_ARGS["retries"] = 1
else:
    DEFAULT_ARGS["owner"] = "airflow"
    DEFAULT_ARGS["retries"] = 0

WRITE_DISPOSITION = {
    "WRITE_TRUNCATE": bigquery.WriteDisposition.WRITE_TRUNCATE,
    "WRITE_APPEND": bigquery.WriteDisposition.WRITE_APPEND,
    "WRITE_EMPTY": bigquery.WriteDisposition.WRITE_EMPTY,
}

TIME_PARTITIONS = {
    "DAY": bigquery.TimePartitioningType.DAY,
    "HOUR": bigquery.TimePartitioningType.HOUR,
    "MONTH": bigquery.TimePartitioningType.MONTH,
    "YEAR": bigquery.TimePartitioningType.YEAR,
}

# 1 tib = 1024 (gib) * 1024 (mib) * 1024 (kib) * 1024 (bytes)
BYTES_PER_TIB = 1099511627776
# https://cloud.google.com/bigquery/pricing#on_demand_pricing
COST_PER_BYTE = 6.25 / BYTES_PER_TIB


def read_write_projects(tenant: str) -> tuple:
    # env var > env config file > base config file > tenant project template
    read_from_project = f"udp-{tenant}-{ENV}"
    write_to_project = f"udp-{tenant}-{ENV}"

    if "read_from_project" in BASE_CONFIG:
        read_from_project = BASE_CONFIG["read_from_project"]
    if "write_to_project" in BASE_CONFIG:
        write_to_project = BASE_CONFIG["write_to_project"]

    if ENV_CONFIG_EXISTS:
        if "read_from_project" in ENV_CONFIG:
            read_from_project = ENV_CONFIG["read_from_project"]
        if "write_to_project" in ENV_CONFIG:
            write_to_project = ENV_CONFIG["write_to_project"]

    read_from_project = os.environ.get("READ_FROM_PROJECT", read_from_project)
    write_to_project = os.environ.get("WRITE_TO_PROJECT", write_to_project)

    return read_from_project, write_to_project


def bq_dataset_exists(client: bigquery.client.Client, dataset_id: str) -> bool:
    datasets = list(client.list_datasets())
    if datasets:
        datasets = [dataset.dataset_id for dataset in datasets]
        if dataset_id in datasets:
            return True
        else:
            return False
    else:
        return False


def create_bq_dataset(client: bigquery.client.Client, dataset_id: str):
    dataset_fq_id = f"{client.project}.{dataset_id}"
    if bq_dataset_exists(client, dataset_id):
        LOG.info(f"Dataset {dataset_fq_id} already exists...")
    else:
        if DRY_RUN:
            LOG.info(f"DRY RUN - Skipping dataset creation for dataset: {dataset_fq_id}")
        else:
            LOG.info(f"Creating dataset {dataset_fq_id}...")
            dataset = bigquery.Dataset(f"{dataset_fq_id}")
            dataset.location = "US"
            dataset = client.create_dataset(dataset, timeout=30)


def bq_table_exists(client: bigquery.client.Client, dataset_id: str, table_id: str) -> bool:
    tables = client.list_tables(dataset_id)
    if tables:
        tables = [f"{table.dataset_id}.{table.table_id}" for table in tables]
        if f"{dataset_id}.{table_id}" in tables:
            return True
        else:
            return False
    else:
        return False


def delete_bq_table(client: bigquery.client.Client, dataset_id: str, table_id: str):
    table_fq_id = f"{client.project}.{dataset_id}.{table_id}"
    if DRY_RUN:
        LOG.info(f"DRY RUN -  Skipping {table_fq_id} deletion.")
    else:
        LOG.info(f"Attempting to delete table {table_fq_id}, if exists...")
        client.delete_table(table_fq_id, not_found_ok=True)
        LOG.info("Delete finished...")


def create_bq_table(client: bigquery.client.Client, dataset_id: str, table_id: str, schema: List[bigquery.SchemaField]):
    table_id = f"{client.project}.{dataset_id}.{table_id}"
    if DRY_RUN:
        LOG.info(f"DRY RUN - Skippin {table_id} creation.")
    else:
        LOG.info(f"Attempting to create table {table_id}")
        table = bigquery.Table(table_id, schema=schema)
        table = client.create_table(table)
        LOG.info(
            f"Created table {table.project}.{table.dataset_id}.{table.table_id}"
        )


def load_file_to_string(file_path: str) -> str:
    with open(f"{BASE_DIR}/{file_path}", "r") as file:
        contents = file.read()
    file.close()
    return contents


def build_dependency_tree(config: dict, tasks: dict) -> list:
    """
    Builds the task dependency tree and declares each non-top level task.
    Returns a list of top level marts that have not yet been declared.
    """

    dependencies = {}
    table_owners = {}

    for mart in config:
        dependencies[mart["mart"]] = {"upstream": [], "downstream": []}

        if "target_table" in mart:
            table_owners[mart["target_table"]] = mart["mart"]

    for mart in config:
        if "depends_on" in mart:
            for depend in mart["depends_on"]:
                if (depend in table_owners) and (depend != mart["target_table"]):
                    dependencies[mart["mart"]]["upstream"].append(table_owners[depend])
                    dependencies[table_owners[depend]]["downstream"].append(tasks[mart["mart"]])

    top_level = []
    for mart in dependencies:
        tasks[mart]
        if not dependencies[mart]["upstream"]:
            top_level.append(tasks[mart])
        if dependencies[mart]["downstream"]:
            tasks[mart].set_downstream(dependencies[mart]["downstream"])

    return top_level


def verify_job_finished(job: bigquery.job.query.QueryJob):
    job_id = job.job_id
    while job.running():
        LOG.info(f"Job {job_id} still running...")
        time.sleep(15)

    e = None
    if job.state != "DONE" or not job.done():
        e = f"not running but state ({job.state} | {job.done()}) is not done"
    elif job.exception():
        e = f"{job.exception()}"
    elif job.errors:
        e = f"encounter the following errors: {yaml.dump(job.errors)}"

    if e:
        raise Exception(f"ERROR: job {job_id} - {e}")
    else:
        data = {
            'total_rows': job.result().total_rows,
            'total_bytes_billed': job.total_bytes_billed,
            'total_bytes_processed': job.total_bytes_processed,
            'estimated_cost': round((job.total_bytes_processed * COST_PER_BYTE), 5),
            'ended': str(job.ended)
        }

        LOG.info(f"Job complete: {data}")


def format_table(
    rows: Iterable[Iterable], headers: bool = False, colsep: str | None = " | "
) -> str:
    """
    Format lists of lists (or other iterables) as the rows and columns of a table.

    When ``headers`` the first row is taken as the value for a headers column.
    Combined with the default ``colsep``, this can produce markdown tables.
    Stolen from: https://gitlab.com/unizin/engineering/apps/unizinlibs/-/blob/master/common/src/unizin/common/format_table.py
    """
    rows = [[str(v) for v in row] for row in rows]
    widths = [max(len(v) for v in col) for col in zip(*rows)]
    fmt = (colsep or "").join(f"{{:{w}}}" for w in widths)

    lines = [fmt.format(*row) for row in rows]

    if headers:
        lines = [
            lines[0],
            # Not bothering to support other divider styles; assuming caller's ``colsep`` makes sense
            "".join(c if c == "|" else "-" for c in lines[0]),  # |---|---| divider
            *lines[1:],
        ]

    return "\n".join(lines)
