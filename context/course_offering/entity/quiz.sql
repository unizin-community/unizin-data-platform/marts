/*
  Context / Course offering / Entity / Quiz

    Computes features about the Quizzes in a
    in a Course offering.

  Properties:

    Export table:  mart_helper.context__course_offering_entity__quiz
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course section
    * Context / Base / Quiz

  To do:
    *
*/

WITH data AS (
  SELECT
    q.course_offering_id AS course_offering_id
    , SUM(CASE WHEN q.quiz_id IS NOT NULL THEN 1 ELSE NULL END) AS count_quizzes

    , AVG(q.length_title) AS avg_length_title
  	, AVG(q.length_description) AS avg_length_description

    /*
      Attempts, points possible: all
    */
  	, AVG(q.allowed_attempts) AS avg_allowed_attempts
  	, AVG(q.points_possible) AS avg_points_possible
  	, AVG(q.quiz_item_count) AS avg_quiz_item_count
  	, AVG(q.unpublished_quiz_item_count) AS avg_unpublished_quiz_item_count

    /*
      Attempts, points possible: published
    */
    , AVG(CASE WHEN q.is_status_published = 1 THEN q.allowed_attempts ELSE NULL END) AS avg_published_allowed_attempts
  	, AVG(CASE WHEN q.is_status_published = 1 THEN q.points_possible ELSE NULL END) AS avg_published_points_possible
  	, AVG(CASE WHEN q.is_status_published = 1 THEN q.quiz_item_count ELSE NULL END) AS avg_published_quiz_item_count

    /*
      TODO: Consider adding the time_limit attribute.
    */

    , SUM(q.is_allow_anonymous_submissions) AS count_is_allow_anonymous_submissions
    , SUM(q.is_allow_go_back_to_previous_question) AS count_is_allow_go_back_to_previous_question
    , SUM(q.is_browser_lockdown_monitor_required) AS count_is_browser_lockdown_monitor_required
    , SUM(q.is_browser_lockdown_required) AS count_is_browser_lockdown_required
    , SUM(q.is_browser_lockdown_required_to_display_results) AS count_is_browser_lockdown_required_to_display_results
    , SUM(q.is_can_be_locked) AS count_is_can_be_locked
    , SUM(q.is_display_multiple_questions) AS count_is_display_multiple_questions
    , SUM(q.is_shuffled_answer_display_order) AS count_is_shuffled_answer_display_order

    /*
  		Correct answers display policy
  	*/
    , SUM(q.is_correct_answers_display_policy_always) AS count_is_correct_answers_display_policy_always
    , SUM(q.is_correct_answers_display_policy_never) AS count_is_correct_answers_display_policy_never
    , SUM(q.is_correct_answers_display_policy_always_after_last_attempt) AS count_is_correct_answers_display_policy_always_after_last_attempt
    , SUM(q.is_correct_answers_display_policy_only_once_after_last_attempt) AS count_is_correct_answers_display_policy_only_once_after_last_attempt

    /*
      Scoring policy.
    */
    , SUM(q.is_scoring_policy_keep_highest) AS count_is_scoring_policy_keep_highest
    , SUM(q.is_scoring_policy_keep_latest) AS count_is_scoring_policy_keep_latest
    , SUM(q.is_scoring_policy_keep_average) AS count_is_scoring_policy_keep_average

    /*
      Status.
    */
    , SUM(q.is_status_published) AS count_is_status_published
    , SUM(q.is_status_unpublished) AS count_is_status_unpublished
    , SUM(q.is_status_deleted) AS count_is_status_deleted

    /*
      Type.
    */
    , SUM(q.is_type_assignment) AS count_is_type_assignment
    , SUM(q.is_type_practice_quiz) AS count_is_type_practice_quiz
    , SUM(q.is_type_survey) AS count_is_type_survey
    , SUM(q.is_type_graded_survey) AS count_is_type_graded_survey

  FROM _WRITE_TO_PROJECT_.mart_helper.context__quiz q

  GROUP BY
    q.course_offering_id
)

SELECT
  co.course_offering_id AS course_offering_id
  , d.count_quizzes AS count_quizzes
  , d.avg_length_title AS avg_length_title
  , d.avg_length_description AS avg_length_description
  , d.avg_allowed_attempts AS avg_allowed_attempts
  , d.avg_points_possible AS avg_points_possible
  , d.avg_quiz_item_count AS avg_quiz_item_count
  , d.avg_unpublished_quiz_item_count AS avg_unpublished_quiz_item_count
  , d.avg_published_allowed_attempts AS avg_published_allowed_attempts
  , d.avg_published_points_possible AS avg_published_points_possible
  , d.avg_published_quiz_item_count AS avg_published_quiz_item_count
  , d.count_is_allow_anonymous_submissions AS count_is_allow_anonymous_submissions
  , d.count_is_allow_go_back_to_previous_question AS count_is_allow_go_back_to_previous_question
  , d.count_is_browser_lockdown_monitor_required AS count_is_browser_lockdown_monitor_required
  , d.count_is_browser_lockdown_required AS count_is_browser_lockdown_required
  , d.count_is_browser_lockdown_required_to_display_results AS count_is_browser_lockdown_required_to_display_results
  , d.count_is_can_be_locked AS count_is_can_be_locked
  , d.count_is_display_multiple_questions AS count_is_display_multiple_questions
  , d.count_is_shuffled_answer_display_order AS count_is_shuffled_answer_display_order
  , d.count_is_correct_answers_display_policy_always AS count_is_correct_answers_display_policy_always
  , d.count_is_correct_answers_display_policy_never AS count_is_correct_answers_display_policy_never
  , d.count_is_correct_answers_display_policy_always_after_last_attempt AS count_is_correct_answers_display_policy_always_after_last_attempt
  , d.count_is_correct_answers_display_policy_only_once_after_last_attempt AS count_is_correct_answers_display_policy_only_once_after_last_attempt
  , d.count_is_scoring_policy_keep_highest AS count_is_scoring_policy_keep_highest
  , d.count_is_scoring_policy_keep_latest AS count_is_scoring_policy_keep_latest
  , d.count_is_scoring_policy_keep_average AS count_is_scoring_policy_keep_average
  , d.count_is_status_published AS count_is_status_published
  , d.count_is_status_unpublished AS count_is_status_unpublished
  , d.count_is_status_deleted AS count_is_status_deleted
  , d.count_is_type_assignment AS count_is_type_assignment
  , d.count_is_type_practice_quiz AS count_is_type_practice_quiz
  , d.count_is_type_survey AS count_is_type_survey
  , d.count_is_type_graded_survey AS count_is_type_graded_survey

FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN data AS d USING (course_offering_id)
;
