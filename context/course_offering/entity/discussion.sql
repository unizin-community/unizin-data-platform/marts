/*
  Warehouse / Course offering / Entity / Discussion

    Computes features about the Discussions in a
    in a Course offering.

  Properties:

    Export table:  warehouse_course_offering_entity.discussion
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Warehouse / Base / Course offering
    * Warehouse / Base / Discussion

  To do:
    *
*/

WITH data AS (
  SELECT
    d.course_offering_id AS course_offering_id
    , SUM(CASE WHEN d.discussion_id IS NOT NULL THEN 1 ELSE NULL END) AS count_discussions

    , AVG(d.length_title) AS avg_length_title
    , AVG(d.length_body) AS avg_length_body

    , SUM(d.is_locked) AS count_is_locked
    , SUM(d.is_pinned) AS count_is_pinned

    /*
      Type
    */
    , SUM(d.is_type_announcement) AS count_is_type_announcement

    /*
      Discussion type
    */
    , SUM(d.is_discussion_type_threaded) AS count_is_discussion_type_threaded
    , SUM(d.is_discussion_type_side_comment) AS count_is_discussion_type_side_comment

    /*
      Status
    */
    , SUM(d.is_status_active) AS count_is_status_active
    , SUM(d.is_status_deleted) AS count_is_status_deleted
    , SUM(d.is_status_unpublished) AS count_is_status_unpublished
    , SUM(d.is_status_post_delayed) AS count_is_status_post_delayed

    /*
      Type
    */
    , SUM(d.is_type_announcement) AS is_type_announcement

  FROM _WRITE_TO_PROJECT_.mart_helper.context__discussion as d

  GROUP BY
    d.course_offering_id
)

SELECT
  co.course_offering_id AS course_offering_id
  , d.count_discussions AS count_discussions
  , d.avg_length_title AS avg_length_title
  , d.avg_length_body AS avg_length_body
  , d.count_is_locked AS count_is_locked
  , d.count_is_pinned AS count_is_pinned
  , d.count_is_discussion_type_threaded AS count_is_discussion_type_threaded
  , d.count_is_discussion_type_side_comment AS count_is_discussion_type_side_comment
  , d.count_is_type_announcement AS count_is_type_announcement
  , d.count_is_status_active AS count_is_status_active
  , d.count_is_status_deleted AS count_is_status_deleted
  , d.count_is_status_unpublished AS count_is_status_unpublished
  , d.count_is_status_post_delayed AS count_is_status_post_delayed
  , d.is_type_announcement AS is_type_announcement

FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN data AS d USING (course_offering_id)
;
