/*
  Context / Course offering / Entity / Quiz result

    Computes features about the Quiz results in a
    in a Course offering.

  Properties:

    Export table:  mart_helper.context__course_offering_entity__quiz_result
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course offering
    * Context / Base / Course section
    * Context / Base / Course section enrollment
    * Context / Base / Quiz
    * Context / Base / Quiz result

  To do:
    *
*/

WITH course_offering_student_enrollments AS (
  SELECT
    cs.course_offering_id AS course_offering_id
    , COUNT(1) AS num_student_enrollments
  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment cse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs USING (course_section_id)
  WHERE
    role = 'Student'
  GROUP BY
    cs.course_offering_id
),

data AS (
  SELECT
    cs.course_offering_id AS course_offering_id
    , SUM(CASE WHEN qr.quiz_result_id IS NOT NULL THEN 1 ELSE NULL END) AS count_quiz_results

    /*
      Attributes
    */
    , AVG(qr.total_attempts) AS avg_total_attempts
    , AVG(qr.extra_attempts) AS avg_extra_attempts
    , AVG(qr.time_taken) AS avg_time_taken
    , AVG(qr.extra_time) AS avg_extra_time

    /*
      Points possible & scores: all
    */
    , AVG(qr.points_possible) AS avg_points_possible
    , AVG(qr.score) AS avg_score
    , AVG(qr.kept_score) AS avg_kept_score
    , AVG(qr.fudge_points) AS avg_fudge_points

    , CASE
      WHEN SUM(qr.points_possible) IS NOT NULL AND SUM(qr.points_possible) != 0 THEN
        SUM(qr.kept_score) / SUM(qr.points_possible)
      ELSE 0
      END AS avg_score_as_percentage

    /*
      Points possible & scores: complete quiz results
    */
    , AVG(CASE WHEN qr.is_status_complete = 1 THEN qr.points_possible ELSE NULL END) AS avg_complete_points_possible
    , AVG(CASE WHEN qr.is_status_complete = 1 THEN qr.score ELSE NULL END) AS avg_complete_score
    , AVG(CASE WHEN qr.is_status_complete = 1 THEN qr.kept_score ELSE NULL END) AS avg_complete_kept_score
    , AVG(CASE WHEN qr.is_status_complete = 1 THEN qr.fudge_points ELSE NULL END) AS avg_complete_fudge_points

    , CASE
      WHEN SUM(CASE WHEN qr.is_status_complete = 1 THEN qr.points_possible ELSE NULL END) IS NOT NULL AND SUM(CASE WHEN qr.is_status_complete = 1 THEN qr.points_possible ELSE NULL END) != 0 THEN
        SUM(CASE WHEN qr.is_status_complete = 1 THEN qr.kept_score ELSE NULL END) / SUM(CASE WHEN qr.is_status_complete = 1 THEN qr.points_possible ELSE NULL END)
      ELSE 0
      END AS avg_complete_score_as_percentage

    /*
      State in submission
    */
    , SUM(qr.is_state_in_submission_never_locked) AS count_is_state_in_submission_never_locked
    , SUM(qr.is_state_in_submission_pending_review) AS count_is_state_in_submission_pending_review

    /*
      Status
    */
    , SUM(qr.is_status_complete) AS count_is_status_complete
    , SUM(qr.is_status_pending_review) AS count_is_status_pending_review
    , SUM(qr.is_status_untaken) AS count_is_status_untaken
    , SUM(qr.is_status_settings_only) AS count_is_status_settings_only

    /*
      Submission scoring policy.
    */
    , SUM(qr.is_submission_scoring_policy_keep_highest) AS count_is_submission_scoring_policy_keep_highest
    , SUM(qr.is_submission_scoring_policy_keep_average) AS count_is_submission_scoring_policy_keep_average
    , SUM(qr.is_submission_scoring_policy_keep_latest) AS count_is_submission_scoring_policy_keep_latest
    , SUM(qr.is_submission_scoring_policy_manually_overridden) AS count_is_submission_scoring_policy_manually_overridden

    /* Total scores, by week, cumulative */
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_1
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_2
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_3
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_4
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_5
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_6
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_7
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_8
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_9
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_10
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_11
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_12
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_13
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_14
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_15
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.score ELSE 0 END) AS total_score_by_week_16
    , SUM(CASE WHEN q.due_date >  DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.score ELSE 0 END) AS total_score_beyond_week_16
    , SUM(qr.kept_score) total_score_for_course

    /* Total published scores, by week, cumulative */
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_1
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_2
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_3
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_4
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_5
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_6
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_7
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_8
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_9
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_10
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_11
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_12
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_13
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_14
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_15
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_by_week_16
    , SUM(CASE WHEN q.due_date >  DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.kept_score ELSE 0 END) AS total_kept_score_beyond_week_16
    , SUM(qr.kept_score) total_kept_score_for_course

    /* Total points possible, by week, cumulative */
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_1
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_2
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_3
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_4
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_5
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_6
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_7
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_8
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_9
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_10
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_11
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_12
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_13
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_14
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_15
    , SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_16
    , SUM(CASE WHEN q.due_date >  DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END) AS total_points_possible_beyond_week_16
    , SUM(qr.points_possible) total_points_possible_for_course

    /* Average score */
    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_1

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_2

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_3

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_4

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_5

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_6

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_7

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_8

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_9

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_10

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_11

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_12

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_13

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_14

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_15

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_by_week_16

    , CASE
      WHEN
        SUM(CASE WHEN q.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
        SUM(CASE WHEN q.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END) > 0
      THEN
        SUM(CASE WHEN q.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.score ELSE 0 END) /
        SUM(CASE WHEN q.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN qr.points_possible ELSE 0 END)
      ELSE NULL
      END AS avg_score_beyond_week_16

    , CASE
      WHEN
        SUM(qr.points_possible) IS NOT NULL AND
        SUM(qr.points_possible) > 0
      THEN
        SUM(qr.kept_score) /
        SUM(qr.points_possible)
      ELSE NULL
      END AS avg_score_for_course

  /*
    Although we roll up all of this data by Course offering, we start
    with Course section and Course section enrollment to filter out
    records based on Course section and Course section enrollment
    attributes.
  */
  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse ON cs.course_section_id=cse.course_section_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON cs.course_offering_id=co.course_offering_id
  LEFT JOIN course_offering_student_enrollments AS cose ON co.course_offering_id=cose.course_offering_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__quiz q ON cs.course_offering_id = q.course_offering_id
  LEFT JOIN
  	_WRITE_TO_PROJECT_.mart_helper.context__quiz_result qr ON
      (q.quiz_id = qr.quiz_id AND cse.person_id = qr.person_id)

  WHERE
    /*
      Course section:
        * Is an active course section
    */
    cs.is_status_active = 1

    /*
      Course section enrollment:
        * Is a student enrollment
        * Is an active enrollment
    */
    AND cse.is_role_student = 1
    AND (
      cse.is_role_status_enrolled       = 1
      OR cse.is_role_status_completed   = 1
      OR cse.is_role_status_registered  = 1
      OR cse.is_role_status_wait_listed = 1
    )

  GROUP BY
    cs.course_offering_id
    , cose.num_student_enrollments
)

SELECT
  co.course_offering_id AS course_offering_id
  , d.count_quiz_results AS count_quiz_results
  , d.avg_total_attempts AS avg_total_attempts
  , d.avg_extra_attempts AS avg_extra_attempts
  , d.avg_time_taken AS avg_time_taken
  , d.avg_extra_time AS avg_extra_time
  , d.avg_points_possible AS avg_points_possible
  , d.avg_score AS avg_score
  , d.avg_kept_score AS avg_kept_score
  , d.avg_fudge_points AS avg_fudge_points
  , d.avg_score_as_percentage AS avg_score_as_percentage
  , d.avg_points_possible AS avg_complete_points_possible
  , d.avg_score AS avg_complete_score
  , d.avg_kept_score AS avg_complete_kept_score
  , d.avg_fudge_points AS avg_complete_fudge_points
  , d.avg_score_as_percentage AS avg_complete_score_as_percentage
  , d.count_is_state_in_submission_never_locked AS count_is_state_in_submission_never_locked
  , d.count_is_state_in_submission_pending_review AS count_is_state_in_submission_pending_review
  , d.count_is_status_complete AS count_is_status_complete
  , d.count_is_status_pending_review AS count_is_status_pending_review
  , d.count_is_status_untaken AS count_is_status_untaken
  , d.count_is_status_settings_only AS count_is_status_settings_only
  , d.count_is_submission_scoring_policy_keep_highest AS count_is_submission_scoring_policy_keep_highest
  , d.count_is_submission_scoring_policy_keep_average AS count_is_submission_scoring_policy_keep_average
  , d.count_is_submission_scoring_policy_keep_latest AS count_is_submission_scoring_policy_keep_latest
  , d.count_is_submission_scoring_policy_manually_overridden AS count_is_submission_scoring_policy_manually_overridden
  , d.total_score_by_week_1 AS total_score_by_week_1
  , d.total_score_by_week_2 AS total_score_by_week_2
  , d.total_score_by_week_3 AS total_score_by_week_3
  , d.total_score_by_week_4 AS total_score_by_week_4
  , d.total_score_by_week_5 AS total_score_by_week_5
  , d.total_score_by_week_6 AS total_score_by_week_6
  , d.total_score_by_week_7 AS total_score_by_week_7
  , d.total_score_by_week_8 AS total_score_by_week_8
  , d.total_score_by_week_9 AS total_score_by_week_9
  , d.total_score_by_week_10 AS total_score_by_week_10
  , d.total_score_by_week_11 AS total_score_by_week_11
  , d.total_score_by_week_12 AS total_score_by_week_12
  , d.total_score_by_week_13 AS total_score_by_week_13
  , d.total_score_by_week_14 AS total_score_by_week_14
  , d.total_score_by_week_15 AS total_score_by_week_15
  , d.total_score_by_week_16 AS total_score_by_week_16
  , d.total_score_for_course AS total_score_for_course
  , d.total_score_beyond_week_16 AS total_score_beyond_week_16
  , d.total_kept_score_by_week_1 AS total_kept_score_by_week_1
  , d.total_kept_score_by_week_2 AS total_kept_score_by_week_2
  , d.total_kept_score_by_week_3 AS total_kept_score_by_week_3
  , d.total_kept_score_by_week_4 AS total_kept_score_by_week_4
  , d.total_kept_score_by_week_5 AS total_kept_score_by_week_5
  , d.total_kept_score_by_week_6 AS total_kept_score_by_week_6
  , d.total_kept_score_by_week_7 AS total_kept_score_by_week_7
  , d.total_kept_score_by_week_8 AS total_kept_score_by_week_8
  , d.total_kept_score_by_week_9 AS total_kept_score_by_week_9
  , d.total_kept_score_by_week_10 AS total_kept_score_by_week_10
  , d.total_kept_score_by_week_11 AS total_kept_score_by_week_11
  , d.total_kept_score_by_week_12 AS total_kept_score_by_week_12
  , d.total_kept_score_by_week_13 AS total_kept_score_by_week_13
  , d.total_kept_score_by_week_14 AS total_kept_score_by_week_14
  , d.total_kept_score_by_week_15 AS total_kept_score_by_week_15
  , d.total_kept_score_by_week_16 AS total_kept_score_by_week_16
  , d.total_kept_score_beyond_week_16 AS total_kept_score_beyond_week_16
  , d.total_kept_score_for_course AS total_kept_score_for_course
  , d.total_points_possible_by_week_1 AS total_points_possible_by_week_1
  , d.total_points_possible_by_week_2 AS total_points_possible_by_week_2
  , d.total_points_possible_by_week_3 AS total_points_possible_by_week_3
  , d.total_points_possible_by_week_4 AS total_points_possible_by_week_4
  , d.total_points_possible_by_week_5 AS total_points_possible_by_week_5
  , d.total_points_possible_by_week_6 AS total_points_possible_by_week_6
  , d.total_points_possible_by_week_7 AS total_points_possible_by_week_7
  , d.total_points_possible_by_week_8 AS total_points_possible_by_week_8
  , d.total_points_possible_by_week_9 AS total_points_possible_by_week_9
  , d.total_points_possible_by_week_10 AS total_points_possible_by_week_10
  , d.total_points_possible_by_week_11 AS total_points_possible_by_week_11
  , d.total_points_possible_by_week_12 AS total_points_possible_by_week_12
  , d.total_points_possible_by_week_13 AS total_points_possible_by_week_13
  , d.total_points_possible_by_week_14 AS total_points_possible_by_week_14
  , d.total_points_possible_by_week_15 AS total_points_possible_by_week_15
  , d.total_points_possible_by_week_16 AS total_points_possible_by_week_16
  , d.total_points_possible_beyond_week_16 AS total_points_possible_beyond_week_16
  , d.total_points_possible_for_course AS total_points_possible_for_course
  , d.avg_score_by_week_16 AS avg_score_by_week_1
  , d.avg_score_by_week_2 AS avg_score_by_week_2
  , d.avg_score_by_week_3 AS avg_score_by_week_3
  , d.avg_score_by_week_4 AS avg_score_by_week_4
  , d.avg_score_by_week_5 AS avg_score_by_week_5
  , d.avg_score_by_week_6 AS avg_score_by_week_6
  , d.avg_score_by_week_7 AS avg_score_by_week_7
  , d.avg_score_by_week_8 AS avg_score_by_week_8
  , d.avg_score_by_week_9 AS avg_score_by_week_9
  , d.avg_score_by_week_10 AS avg_score_by_week_10
  , d.avg_score_by_week_11 AS avg_score_by_week_11
  , d.avg_score_by_week_12 AS avg_score_by_week_12
  , d.avg_score_by_week_13 AS avg_score_by_week_13
  , d.avg_score_by_week_14 AS avg_score_by_week_14
  , d.avg_score_by_week_15 AS avg_score_by_week_15
  , d.avg_score_by_week_16 AS avg_score_by_week_16
  , d.avg_score_beyond_week_16 AS avg_score_beyond_week_16
  , d.avg_score_for_course AS avg_score_for_course

FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN data AS d USING (course_offering_id)
;
