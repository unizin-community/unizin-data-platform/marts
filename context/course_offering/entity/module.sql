/*
  Context / Course offering / Entity / Module

    Computes features about the Modules in a
    in a Course offering.

  Properties:

    Export table:  mart_helper.context__course_offering_entity__module
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course section
    * Context / Base / Module

  To do:
    *
*/

WITH data AS (
  SELECT
    m.course_offering_id AS course_offering_id
    , SUM(CASE WHEN m.module_id IS NOT NULL THEN 1 ELSE NULL END) AS count_modules

    /*
      Attributes
    */
    , AVG(m.length_name) AS avg_length_name
    , AVG(m.position) AS avg_position

    /*
      Status
    */
    , SUM(m.is_status_active) count_is_status_active
    , SUM(m.is_status_deleted) count_is_status_deleted
    , SUM(m.is_status_unpublished) count_is_status_unpublished

  FROM _WRITE_TO_PROJECT_.mart_helper.context__module m

  GROUP BY
    m.course_offering_id
)

SELECT
  co.course_offering_id AS course_offering_id
  , d.count_modules AS count_modules
  , d.avg_length_name AS avg_length_name
  , d.avg_position AS avg_position
  , d.count_is_status_active AS count_is_status_active
  , d.count_is_status_deleted AS count_is_status_deleted
  , d.count_is_status_unpublished AS count_is_status_unpublished

FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN data AS d USING (course_offering_id)
;
