/*
  Context / Course offering / Entity / Learner activity

    Computes features about the Learner activities in a
    in a Course offering.

  Properties:

    Export table:  mart_helper.context__course_offering_entity__learner_activity
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course offering
    * Context / Base / Learner activity

  To do:
    *
*/

WITH data AS (
  SELECT
    la.course_offering_id AS course_offering_id
    , SUM(CASE WHEN la.learner_activity_id IS NOT NULL THEN 1 ELSE NULL END) AS count_learner_activities

    , AVG(la.length_title) AS avg_length_title
    , AVG(la.length_description) AS avg_length_description
    , AVG(la.position) AS avg_position

    /*
  		Points possible
  	*/
    , AVG(la.points_possible) AS avg_points_possible
    , COUNT(la.points_possible) AS count_points_possible

    , AVG(CASE WHEN is_status_published = 1 THEN la.points_possible ELSE NULL END) AS avg_published_points_possible
    , COUNT(CASE WHEN is_status_published = 1 THEN la.points_possible ELSE NULL END) AS count_published_points_possible

    /*
      Attributes
    */
  	, SUM(la.is_hidden) AS count_is_hidden
  	, SUM(la.is_lockable) AS count_is_lockable
  	, SUM(la.is_all_day) AS count_is_all_day

    /*
      Grade type
    */
    , SUM(la.is_grade_type_gpa_scale) AS count_is_grade_type_gpa_scale
    , SUM(la.is_grade_type_letter_grade) AS count_is_grade_type_letter_grade
    , SUM(la.is_grade_type_pass_fail) AS count_is_grade_type_pass_fail
    , SUM(la.is_grade_type_points) AS count_is_grade_type_points
    , SUM(la.is_grade_type_not_graded) AS count_is_grade_type_not_graded
    , SUM(la.is_grade_type_percentage) AS count_is_grade_type_percentage

    /*
  		Peer reviews
  	*/
  	, SUM(la.has_peer_reviews) AS count_has_peer_reviews
  	, SUM(la.has_peer_reviews_assigned) AS count_has_peer_reviews_assigned
  	, SUM(la.is_anonymous_peer_reviews) AS count_is_anonymous_peer_reviews
  	, SUM(la.is_automatic_peer_reviews) AS count_is_automatic_peer_reviews
  	, SUM(la.is_grade_group_students_individually) AS count_is_grade_group_students_individually
  	, AVG(la.peer_review_count) AS avg_peer_review_count

    /*
      Status
    */
    , SUM(la.is_status_failed_to_duplicate) AS count_is_status_failed_to_duplicate
    , SUM(la.is_status_published) AS count_is_status_published
    , SUM(la.is_status_unpublished) AS count_is_status_unpublished
    , SUM(la.is_status_deleted) AS count_is_status_deleted

    /*
      Type
    */
    , SUM(la.is_visibility_everyone) AS count_is_visibility_everyone
    , SUM(la.is_visibility_only_visible_to_overrides) AS count_is_visibility_only_visible_to_overrides

  FROM _WRITE_TO_PROJECT_.mart_helper.context__learner_activity AS la

  GROUP BY
    la.course_offering_id
)

SELECT
  co.course_offering_id AS course_offering_id
  , d.count_learner_activities AS count_learner_activities
  , d.avg_length_title AS avg_length_title
  , d.avg_length_description AS avg_length_description
  , d.avg_position AS avg_position
  , d.avg_points_possible AS avg_points_possible
  , d.count_points_possible AS count_points_possible
  , d.avg_published_points_possible AS avg_published_points_possible
  , d.count_published_points_possible AS count_published_points_possible
  , d.count_is_hidden AS count_is_hidden
  , d.count_is_lockable AS count_is_lockable
  , d.count_is_all_day AS count_is_all_day
  , d.count_is_grade_type_gpa_scale AS count_is_grade_type_gpa_scale
  , d.count_is_grade_type_letter_grade AS count_is_grade_type_letter_grade
  , d.count_is_grade_type_pass_fail AS count_is_grade_type_pass_fail
  , d.count_is_grade_type_points AS count_is_grade_type_points
  , d.count_is_grade_type_not_graded AS count_is_grade_type_not_graded
  , d.count_is_grade_type_percentage AS count_is_grade_type_percentage
  , d.count_has_peer_reviews AS count_has_peer_reviews
  , d.count_has_peer_reviews_assigned AS count_has_peer_reviews_assigned
  , d.count_is_anonymous_peer_reviews AS count_is_anonymous_peer_reviews
  , d.count_is_automatic_peer_reviews AS count_is_automatic_peer_reviews
  , d.count_is_grade_group_students_individually AS count_is_grade_group_students_individually
  , d.avg_peer_review_count AS avg_peer_review_count
  , d.count_is_status_failed_to_duplicate AS count_is_status_failed_to_duplicate
  , d.count_is_status_published AS count_is_status_published
  , d.count_is_status_unpublished AS count_is_status_unpublished
  , d.count_is_status_deleted AS count_is_status_deleted
  , d.count_is_visibility_everyone AS count_is_visibility_everyone
  , d.count_is_visibility_only_visible_to_overrides AS count_is_visibility_only_visible_to_overrides

FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN data AS d USING (course_offering_id)
;
