/*
  Context / Course offering / Entity / Learner activity result

    Computes features about the Learner activity results in a
    in a Course offering.

  Properties:

    Export table:  mart_helper.context__course_offering_entity__learner_activity_result
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course offering
    * Context / Base / Course section
    * Context / Base / Course section enrollment
    * Context / Base / Learner activity
    * Context / Base / Learner activity result

  To do:
    *
*/

WITH course_offering_student_enrollments AS (
  SELECT
    cs.course_offering_id AS course_offering_id
    , COUNT(1) AS num_student_enrollments
  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment cse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs USING (course_section_id)
  WHERE
    role = 'Student'
  GROUP BY
    cs.course_offering_id
),

data AS (
  SELECT
    co.course_offering_id AS course_offering_id
    , SUM(CAST(CASE WHEN lar.learner_activity_result_id IS NOT NULL THEN lar.score ELSE  NULL END AS FLOAT64)) AS count_learner_activity_results

    /*
      Attributes
    */
    , AVG(length_body) AS avg_length_body

    , SUM(CAST(has_admin_comment as FLOAT64)) AS count_has_admin_comment
    , SUM(CAST(has_rubric_assessment as FLOAT64)) AS count_has_rubric_assessment
    , SUM(CAST(is_excused as FLOAT64)) AS count_is_excused
    , SUM(CAST(is_grade_anonymously AS FLOAT64)) AS count_is_grade_anonymously
    , SUM(CAST(is_processed AS FLOAT64)) AS count_is_processed
    , AVG(submission_comments_count) AS avg_submission_comments_count
    , AVG(process_attempts) AS avg_process_attempts

    /*
  		Score and grade data.
  	*/
    , AVG(lar.points_possible) AS avg_points_possible
    , AVG(lar.score) AS avg_score
    , AVG(lar.published_score) AS avg_published_score
    , CASE
  		WHEN SUM(CAST(la.points_possible AS FLOAT64)) > 0 THEN SUM(CAST(lar.score AS FLOAT64)) / SUM(CAST(la.points_possible AS FLOAT64))
      ELSE 0
  		END AS score_percentage

    /*
  		Score and grade data for gradebook-only results.
  	*/
    , AVG(CASE WHEN is_gradebook_status_true = 1 THEN lar.points_possible ELSE NULL END) AS avg_gradebook_points_possible
    , AVG(CASE WHEN is_gradebook_status_true = 1 THEN lar.score ELSE NULL END) AS avg_gradebook_score
    , AVG(CASE WHEN is_gradebook_status_true = 1 THEN lar.published_score ELSE NULL END) AS avg_gradebook_published_score
    , CASE
  		WHEN SUM(CAST(CASE WHEN is_gradebook_status_true = 1 THEN la.points_possible ELSE NULL END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN is_gradebook_status_true = 1 THEN lar.score ELSE NULL END AS FLOAT64)) /
        SUM(CAST(CASE WHEN is_gradebook_status_true = 1 THEN la.points_possible ELSE NULL END AS FLOAT64))
      ELSE 0
  		END AS gradebook_score_percentage

    /*
      Grade state
    */
    , SUM(CAST(grade_state_human_graded as FLOAT64)) AS count_grade_state_human_graded
    , SUM(CAST(grade_state_not_graded AS FLOAT64)) AS count_grade_state_not_graded
    , SUM(CAST(grade_state_auto_graded AS FLOAT64)) AS count_grade_state_auto_graded

    /*
      Type
    */
    , SUM(CAST(is_type_media_recording AS FLOAT64)) AS count_is_type_media_recording
    , SUM(CAST(is_type_online_url AS FLOAT64)) AS count_is_type_online_url
    , SUM(CAST(is_type_online_quiz AS FLOAT64)) AS count_is_type_online_quiz
    , SUM(CAST(is_type_external_tool AS FLOAT64)) AS count_is_type_external_tool
    , SUM(CAST(is_type_online_upload AS FLOAT64)) AS count_is_type_online_upload
    , SUM(CAST(is_type_basic_lti_launch AS FLOAT64)) AS count_is_type_basic_lti_launch
    , SUM(CAST(is_type_discussion_topic AS FLOAT64)) AS count_is_type_discussion_topic
    , SUM(CAST(is_type_online_text_entry AS FLOAT64)) AS count_is_type_online_text_entry

    /*
      Grading status
    */
    , SUM(CAST(is_grading_status_graded AS FLOAT64)) AS count_is_grading_status_graded
    , SUM(CAST(is_grading_status_deleted AS FLOAT64)) AS count_is_grading_status_deleted
    , SUM(CAST(is_grading_status_pending_review AS FLOAT64)) AS count_is_grading_status_pending_review
    , SUM(CAST(is_grading_status_submitted AS FLOAT64)) AS count_is_grading_status_submitted
    , SUM(CAST(is_grading_status_unsubmitted AS FLOAT64)) AS count_is_grading_status_unsubmitted

    /*
      Gradebook status
    */
    , SUM(CAST(is_gradebook_status_true AS FLOAT64)) AS count_is_gradebook_status_true
    , SUM(CAST(is_gradebook_status_false AS FLOAT64)) AS count_is_gradebook_status_false

    /* Total scores, by week, cumulative */
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_1
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_2
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_3
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_4
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_5
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_6
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_7
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_8
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_9
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_10
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_11
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_12
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_13
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_14
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_15
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_by_week_16
    , SUM(CAST(CASE WHEN la.due_date >  DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) AS total_score_beyond_week_16
    , SUM(CAST(lar.published_score AS FLOAT64)) total_score_for_course

    /* Total published scores, by week, cumulative */
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_1
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_2
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_3
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_4
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_5
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_6
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_7
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_8
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_9
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_10
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_11
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_12
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_13
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_14
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_15
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_by_week_16
    , SUM(CAST(CASE WHEN la.due_date >  DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.published_score ELSE 0 END AS FLOAT64)) AS total_published_score_beyond_week_16
    , SUM(CAST(lar.published_score AS FLOAT64)) total_published_score_for_course

    /* Total points possible, by week, cumulative */
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_1
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_2
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_3
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_4
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_5
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_6
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_7
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_8
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_9
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_10
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_11
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_12
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_13
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_14
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_15
    , SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_by_week_16
    , SUM(CAST(CASE WHEN la.due_date >  DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) AS total_points_possible_beyond_week_16
    , SUM(CAST(lar.points_possible AS FLOAT64)) total_points_possible_for_course

    /* Average score */
    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 7 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_1

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 14 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_2

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 21 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_3

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 28 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_4

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 35 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_5

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 42 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_6

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 49 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_7

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 56 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_8

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 63 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_9

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 70 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_10

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 77 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_11

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 84 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_12

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 91 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_13

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 98 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_14

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 105 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_15

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date <= DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_by_week_16

    , CASE
      WHEN
        SUM(CAST(CASE WHEN la.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(CASE WHEN la.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64)) > 0
      THEN
        SUM(CAST(CASE WHEN la.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.score ELSE 0 END AS FLOAT64)) /
        SUM(CAST(CASE WHEN la.due_date > DATETIME_ADD(cs.ss_instruction_begin_date, INTERVAL 112 DAY) THEN lar.points_possible ELSE 0 END AS FLOAT64))
      ELSE NULL
      END AS avg_score_beyond_week_16

    , CASE
      WHEN
        SUM(CAST(lar.points_possible AS FLOAT64)) IS NOT NULL AND
        SUM(CAST(lar.points_possible AS FLOAT64)) > 0
      THEN
        SUM(CAST(lar.published_score AS FLOAT64)) /
        SUM(CAST(lar.points_possible AS FLOAT64))
      ELSE NULL
      END AS avg_score_for_course

  /*
    Although we roll up all of this data by Course offering, we start
    with Course section and Course section enrollment to filter out
    records based on Course section and Course section enrollment
    attributes.
  */
  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse ON cs.course_section_id=cse.course_section_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON cs.course_offering_id=co.course_offering_id
  LEFT JOIN course_offering_student_enrollments AS cose ON co.course_offering_id=cose.course_offering_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity la ON cs.course_offering_id = la.course_offering_id
  LEFT JOIN
  	_WRITE_TO_PROJECT_.mart_helper.context__learner_activity_result lar ON
      (la.learner_activity_id = lar.learner_activity_id AND cse.person_id = lar.person_id)

  WHERE
    /*
      Course section:
        * Is an active course section
    */
    cs.is_status_active = 1

    /*
      Course section enrollment:
        * Is a student enrollment
        * Is an active enrollment
    */
    AND cse.is_role_student = 1
    AND (
      cse.is_role_status_enrolled       = 1
      OR cse.is_role_status_completed   = 1
      OR cse.is_role_status_registered  = 1
      OR cse.is_role_status_wait_listed = 1
    )

  GROUP BY
    co.course_offering_id
    , cose.num_student_enrollments
)

SELECT
  co.course_offering_id AS course_offering_id
  , d.count_learner_activity_results AS count_learner_activity_results
  , d.avg_length_body AS avg_length_body
  , d.count_has_admin_comment AS count_has_admin_comment
  , d.count_has_rubric_assessment AS count_has_rubric_assessment
  , d.count_is_excused AS count_is_excused
  , d.count_is_grade_anonymously AS count_is_grade_anonymously
  , d.count_is_processed AS count_is_processed
  , d.avg_submission_comments_count AS avg_submission_comments_count
  , d.avg_process_attempts AS avg_process_attempts
  , d.avg_points_possible AS avg_points_possible
  , d.avg_score AS avg_score
  , d.avg_published_score AS avg_published_score
  , d.score_percentage AS score_percentage
  , d.count_grade_state_human_graded AS count_grade_state_human_graded
  , d.count_grade_state_not_graded AS count_grade_state_not_graded
  , d.count_grade_state_auto_graded AS count_grade_state_auto_graded
  , d.count_is_type_media_recording AS count_is_type_media_recording
  , d.count_is_type_online_url AS count_is_type_online_url
  , d.count_is_type_online_quiz AS count_is_type_online_quiz
  , d.count_is_type_external_tool AS count_is_type_external_tool
  , d.count_is_type_online_upload AS count_is_type_online_upload
  , d.count_is_type_basic_lti_launch AS count_is_type_basic_lti_launch
  , d.count_is_type_discussion_topic AS count_is_type_discussion_topic
  , d.count_is_type_online_text_entry AS count_is_type_online_text_entry
  , d.count_is_grading_status_graded AS count_is_grading_status_graded
  , d.count_is_grading_status_deleted AS count_is_grading_status_deleted
  , d.count_is_grading_status_pending_review AS count_is_grading_status_pending_review
  , d.count_is_grading_status_submitted AS count_is_grading_status_submitted
  , d.count_is_grading_status_unsubmitted AS count_is_grading_status_unsubmitted
  , d.count_is_gradebook_status_true AS count_is_gradebook_status_true
  , d.count_is_gradebook_status_false AS count_is_gradebook_status_false
  , d.total_score_by_week_1 AS total_score_by_week_1
  , d.total_score_by_week_2 AS total_score_by_week_2
  , d.total_score_by_week_3 AS total_score_by_week_3
  , d.total_score_by_week_4 AS total_score_by_week_4
  , d.total_score_by_week_5 AS total_score_by_week_5
  , d.total_score_by_week_6 AS total_score_by_week_6
  , d.total_score_by_week_7 AS total_score_by_week_7
  , d.total_score_by_week_8 AS total_score_by_week_8
  , d.total_score_by_week_9 AS total_score_by_week_9
  , d.total_score_by_week_10 AS total_score_by_week_10
  , d.total_score_by_week_11 AS total_score_by_week_11
  , d.total_score_by_week_12 AS total_score_by_week_12
  , d.total_score_by_week_13 AS total_score_by_week_13
  , d.total_score_by_week_14 AS total_score_by_week_14
  , d.total_score_by_week_15 AS total_score_by_week_15
  , d.total_score_by_week_16 AS total_score_by_week_16
  , d.total_score_for_course AS total_score_for_course
  , d.total_score_beyond_week_16 AS total_score_beyond_week_16
  , d.total_published_score_by_week_1 AS total_published_score_by_week_1
  , d.total_published_score_by_week_2 AS total_published_score_by_week_2
  , d.total_published_score_by_week_3 AS total_published_score_by_week_3
  , d.total_published_score_by_week_4 AS total_published_score_by_week_4
  , d.total_published_score_by_week_5 AS total_published_score_by_week_5
  , d.total_published_score_by_week_6 AS total_published_score_by_week_6
  , d.total_published_score_by_week_7 AS total_published_score_by_week_7
  , d.total_published_score_by_week_8 AS total_published_score_by_week_8
  , d.total_published_score_by_week_9 AS total_published_score_by_week_9
  , d.total_published_score_by_week_10 AS total_published_score_by_week_10
  , d.total_published_score_by_week_11 AS total_published_score_by_week_11
  , d.total_published_score_by_week_12 AS total_published_score_by_week_12
  , d.total_published_score_by_week_13 AS total_published_score_by_week_13
  , d.total_published_score_by_week_14 AS total_published_score_by_week_14
  , d.total_published_score_by_week_15 AS total_published_score_by_week_15
  , d.total_published_score_by_week_16 AS total_published_score_by_week_16
  , d.total_published_score_beyond_week_16 AS total_published_score_beyond_week_16
  , d.total_published_score_for_course AS total_published_score_for_course
  , d.total_points_possible_by_week_1 AS total_points_possible_by_week_1
  , d.total_points_possible_by_week_2 AS total_points_possible_by_week_2
  , d.total_points_possible_by_week_3 AS total_points_possible_by_week_3
  , d.total_points_possible_by_week_4 AS total_points_possible_by_week_4
  , d.total_points_possible_by_week_5 AS total_points_possible_by_week_5
  , d.total_points_possible_by_week_6 AS total_points_possible_by_week_6
  , d.total_points_possible_by_week_7 AS total_points_possible_by_week_7
  , d.total_points_possible_by_week_8 AS total_points_possible_by_week_8
  , d.total_points_possible_by_week_9 AS total_points_possible_by_week_9
  , d.total_points_possible_by_week_10 AS total_points_possible_by_week_10
  , d.total_points_possible_by_week_11 AS total_points_possible_by_week_11
  , d.total_points_possible_by_week_12 AS total_points_possible_by_week_12
  , d.total_points_possible_by_week_13 AS total_points_possible_by_week_13
  , d.total_points_possible_by_week_14 AS total_points_possible_by_week_14
  , d.total_points_possible_by_week_15 AS total_points_possible_by_week_15
  , d.total_points_possible_by_week_16 AS total_points_possible_by_week_16
  , d.total_points_possible_beyond_week_16 AS total_points_possible_beyond_week_16
  , d.total_points_possible_for_course AS total_points_possible_for_course
  , d.avg_score_by_week_16 AS avg_score_by_week_1
  , d.avg_score_by_week_2 AS avg_score_by_week_2
  , d.avg_score_by_week_3 AS avg_score_by_week_3
  , d.avg_score_by_week_4 AS avg_score_by_week_4
  , d.avg_score_by_week_5 AS avg_score_by_week_5
  , d.avg_score_by_week_6 AS avg_score_by_week_6
  , d.avg_score_by_week_7 AS avg_score_by_week_7
  , d.avg_score_by_week_8 AS avg_score_by_week_8
  , d.avg_score_by_week_9 AS avg_score_by_week_9
  , d.avg_score_by_week_10 AS avg_score_by_week_10
  , d.avg_score_by_week_11 AS avg_score_by_week_11
  , d.avg_score_by_week_12 AS avg_score_by_week_12
  , d.avg_score_by_week_13 AS avg_score_by_week_13
  , d.avg_score_by_week_14 AS avg_score_by_week_14
  , d.avg_score_by_week_15 AS avg_score_by_week_15
  , d.avg_score_by_week_16 AS avg_score_by_week_16
  , d.avg_score_beyond_week_16 AS avg_score_beyond_week_16
  , d.avg_score_for_course AS avg_score_for_course

FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN data AS d USING (course_offering_id)
;
