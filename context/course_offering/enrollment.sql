/*
  Context / Course offering / Enrollment

    Aggregates the instructors and students in a
    Course offering and prepares a set of metadata
    that is often used in marts for presentation.

  Properties:

    Export table:  mart_helper.context__course_offering__enrollment
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course offering section enrollment
    * Context / Base / Course offering

  To do:
    *
*/

WITH instructors AS (
  SELECT
    cose.udp_course_offering_id AS udp_course_offering_id
    , ARRAY_AGG(
        CASE
        WHEN p.preferred_first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.preferred_first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.preferred_first_name)
        WHEN p.first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.first_name)
        WHEN ARRAY_LENGTH(SPLIT(p.name,' ')) > 1 
          THEN CONCAT(
                (
                  SELECT STRING_AGG(name, ' ' ORDER BY OFFSET) 
                  FROM UNNEST(SPLIT(p.name, ' ')) name WITH OFFSET 
                  WHERE OFFSET > 0
                )
                ,', '
                ,SPLIT(p.name,' ')[OFFSET(0)])
        ELSE
          'No name'
        END
      ) AS name_array
    , ARRAY_AGG(p.person_id IGNORE NULLS) AS udp_id_array
    , ARRAY_AGG(p.lms_id IGNORE NULLS) AS lms_id_array
    , ARRAY_AGG(p.sis_id IGNORE NULLS) AS sis_id_array
    , ARRAY_AGG(
        CASE
        WHEN p.email_address IS NOT NULL and p.email_address <> '' 
          THEN p.email_address
        ELSE
          'No email'
        END
      ) AS email_address_array

  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_section_enrollment AS cose
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON cose.udp_person_id=p.person_id
  WHERE
    cose.role IN ('Teacher', 'Faculty')
    AND cose.role_status NOT IN ('Not-enrolled', 'Dropped', 'Withdrawn')
  GROUP BY
    cose.udp_course_offering_id
),

students AS (
  SELECT
    cose.udp_course_offering_id AS udp_course_offering_id
    , ARRAY_AGG(CASE
        WHEN p.preferred_first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.preferred_first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.preferred_first_name)
        WHEN p.first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.first_name)
        WHEN ARRAY_LENGTH(SPLIT(p.name,' ')) > 1 
          THEN CONCAT(
                (
                  SELECT STRING_AGG(name, ' ' ORDER BY OFFSET) 
                  FROM UNNEST(SPLIT(p.name, ' ')) name WITH OFFSET 
                  WHERE OFFSET > 0
                )
                ,', '
                ,SPLIT(p.name,' ')[OFFSET(0)])
      ELSE
      'No name'
    END
      ) AS name_array
    , ARRAY_AGG(p.person_id IGNORE NULLS) AS udp_id_array
    , ARRAY_AGG(p.lms_id IGNORE NULLS) AS lms_id_array
    , ARRAY_AGG(p.sis_id IGNORE NULLS) AS sis_id_array
  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_section_enrollment AS cose
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON cose.udp_person_id=p.person_id
  WHERE
    cose.role IN ('Student', 'Observer','Generic')
    AND cose.role_status NOT IN ('Not-enrolled', 'Dropped', 'Withdrawn')
  GROUP BY
    cose.udp_course_offering_id
)

SELECT
  co.course_offering_id AS course_offering_id
  , co.lms_id AS lms_course_offering_id

  , COALESCE(ARRAY_LENGTH(i.name_array),0) AS num_instructors
  , i.name_array AS instructor_name_array
  , i.udp_id_array AS instructor_udp_id_array
  , i.lms_id_array AS instructor_lms_id_array
  , i.sis_id_array AS instructor_sis_id_array
  , i.email_address_array as instructor_email_address_array
  , ARRAY_TO_STRING(i.name_array, ';\n') AS instructor_display
  , ARRAY_TO_STRING(i.email_address_array, ';\n') AS instructor_email_address_display

  , COALESCE(ARRAY_LENGTH(s.name_array),0) AS num_students
  , s.name_array AS student_name_array
  , s.udp_id_array AS student_udp_id_array
  , s.lms_id_array AS student_lms_id_array
  , s.sis_id_array AS student_sis_id_array
  , ARRAY_TO_STRING(s.name_array, ';\n') AS student_display

FROM
  _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN instructors i ON co.course_offering_id=i.udp_course_offering_id
LEFT JOIN students s ON co.course_offering_id=s.udp_course_offering_id
