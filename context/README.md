# Context

The SQL files in this directory capture all mart building blocks that depend solely on the UDP `context_store` data.

## base

The tables in `base` are the foundational layer of `batch-features`. Each of the entities in the UDP `context_store` has a corresponding file in the `base` directory, and this is the only time a UDP instance is hit directly from the `context` tables.

We leverage the federated query functionality to hit each entity, so you'll see `SELECT * FROM EXTERNAL_QUERY()` in every file. The outputs have some aggregations, but nothing too crazy transformation-wise happens here. We mainly want to get all meaningful IDs and fields from the `context_store` in a form that's easy to join and aggregate based on use case in other SQL scripts.

## course_offering

The tables here start performing basic aggregations and joins. The `enrollment.sql` file is a common one we use for a lot of use cases as many many marts need to know how many students are enrolled in courses. 

The files in the `entity` folder looks at some UCDM entities that have `course_offering_id` as a foreign key. Using `course_offering_id` as a primary pivot, these tables do aggregations and counts within the scope of the corresponding entity. For example, the `entity/learner_activity.sql` file calculates the average `points_possible` values for assignments grouped by `course_offering_id`.

## course_section

This folder only has an `enrollment.sql` file, but the idea is similar to the `course_offering` folder. The only difference is that the main pivot is by `course_section_id` instead of `course_offering_id`.

Not many entities have `course_section_id` as a foreign key, so it doesn't make sense to have an `entity` folder here.

## taskforce

We decided to section off the `taskforce` mart tables in their own directory. One could argue that these could fit in `base` or `course_offering`, but since the taskforces were such targeted efforts, we made a separate directory.

This is also reflected in the `event` and `mart` directories.

## config.yaml

This file has the definitions and dependency order that these SQL files should execute in the Jupyter Notebook. Each file has the following config attributes in the YAML:
- file_path - relative path to the SQL file
- refresh_frequency - feeds into BQ scheduled query; how often to run it
- refresh_write - what to do with results; often `overwrite` for the `context` tables
- scheduled_query_name - what BQ should name the scheduled query upon creation
- bq_table_name - destination table for data
- bq_dataset - destination dataset for data
- partitioned - True or False; often applicable to `event` tables
- partition_field - if True for `partitioned`, this is the name of the field. Otherwise, it takes a value of `None`.