/*
  Context / Course section / Enrollment

    Aggregates the instructors and students in a
    Course section and prepares a set of metadata
    that is often used in marts for presentation.

  Properties:

    Export table:  mart_helper.context__course_section__enrollment
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course section enrollment
    * Context / Base / Course section
    * Context / Base / Person
    
  To do:
    *
*/

WITH course_section_enrollment AS (
  SELECT
    cse.course_section_id AS udp_course_section_id
    , COALESCE(cse.le_current_course_offering_id,cse.course_offering_id) AS udp_course_offering_id
    , cse.person_id AS udp_person_id
    , cse.role AS role
    , cse.role_status AS role_status
    , ROW_NUMBER() OVER (PARTITION BY cse.person_id, cse.course_section_id,COALESCE(cse.le_current_course_offering_id,cse.course_offering_id) ORDER BY cse.created_date ASC) AS row_number
  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  WHERE
    cse.role_status NOT IN ('Not-enrolled', 'Dropped', 'Withdrawn')
),

updated_course_section_enrollment AS (
SELECT
  cse.udp_course_section_id AS udp_course_section_id
  , cse.udp_course_offering_id AS udp_course_offering_id
  , cse.udp_person_id AS udp_person_id
  , cse.role AS role
  , cse.role_status AS role_status
FROM
  course_section_enrollment AS cse
WHERE
  row_number = 1 -- Only use one enrollment; no dupes.
),

instructors AS (
  SELECT
    ucse.udp_course_section_id AS udp_course_section_id
    , ucse.udp_course_offering_id as udp_course_offering_id
    , ARRAY_AGG(
        CASE
        WHEN p.preferred_first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.preferred_first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.preferred_first_name)
        WHEN p.first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.first_name)
        WHEN ARRAY_LENGTH(SPLIT(p.name,' ')) > 1 
          THEN CONCAT(
                (
                  SELECT STRING_AGG(name, ' ' ORDER BY OFFSET) 
                  FROM UNNEST(SPLIT(p.name, ' ')) name WITH OFFSET 
                  WHERE OFFSET > 0
                )
                ,', '
                ,SPLIT(p.name,' ')[OFFSET(0)])
        ELSE
          'No name'
        END
      ) AS name_array
    , ARRAY_AGG(p.person_id IGNORE NULLS) AS udp_id_array
    , ARRAY_AGG(p.lms_id IGNORE NULLS) AS lms_id_array
    , ARRAY_AGG(p.sis_id IGNORE NULLS) AS sis_id_array
    , ARRAY_AGG(
        CASE
        WHEN p.email_address IS NOT NULL and p.email_address <> ''
          THEN p.email_address
        ELSE
          'No email'
        END
      ) AS email_address_array

  FROM
    updated_course_section_enrollment AS ucse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON ucse.udp_person_id=p.person_id
  WHERE
    ucse.role IN ('Teacher', 'Faculty')
    AND ucse.role_status NOT IN ('Not-enrolled', 'Dropped', 'Withdrawn')
  GROUP BY
    ucse.udp_course_section_id,
    ucse.udp_course_offering_id
),

students AS (
  SELECT
    ucse.udp_course_section_id AS udp_course_section_id
    , ucse.udp_course_offering_id as udp_course_offering_id
    , ARRAY_AGG(CASE
        WHEN p.preferred_first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.preferred_first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.preferred_first_name)
        WHEN p.first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.first_name <> '' AND p.last_name <> ''
          THEN CONCAT(p.last_name, ', ', p.first_name)
        WHEN ARRAY_LENGTH(SPLIT(p.name,' ')) > 1 
          THEN CONCAT(
                (
                  SELECT STRING_AGG(name, ' ' ORDER BY OFFSET) 
                  FROM UNNEST(SPLIT(p.name, ' ')) name WITH OFFSET 
                  WHERE OFFSET > 0
                )
                ,', '
                ,SPLIT(p.name,' ')[OFFSET(0)])
      ELSE
      'No name'
    END
      ) AS name_array
    , ARRAY_AGG(p.person_id IGNORE NULLS) AS udp_id_array
    , ARRAY_AGG(p.lms_id IGNORE NULLS) AS lms_id_array
    , ARRAY_AGG(p.sis_id IGNORE NULLS) AS sis_id_array
  FROM
    updated_course_section_enrollment AS ucse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON ucse.udp_person_id=p.person_id
  WHERE
    ucse.role IN ('Student', 'Observer','Generic')
    AND ucse.role_status NOT IN ('Not-enrolled', 'Dropped', 'Withdrawn')
  GROUP BY
    ucse.udp_course_section_id,
    ucse.udp_course_offering_id
)

SELECT
  cs.course_section_id AS course_section_id
  , COALESCE(cs.le_current_course_offering_id,cs.course_offering_id) as course_offering_id

  , COALESCE(ARRAY_LENGTH(i.name_array),0) AS num_instructors
  , i.name_array AS instructor_name_array
  , i.udp_id_array AS instructor_udp_id_array
  , i.lms_id_array AS instructor_lms_id_array
  , i.sis_id_array AS instructor_sis_id_array
  , i.email_address_array as instructor_email_address_array
  , ARRAY_TO_STRING(i.name_array, ';\n') AS instructor_display
  , ARRAY_TO_STRING(i.email_address_array, ';\n') as instructor_email_address_display

  , COALESCE(ARRAY_LENGTH(s.name_array),0) AS num_students
  , s.name_array AS student_name_array
  , s.udp_id_array AS student_udp_id_array
  , s.lms_id_array AS student_lms_id_array
  , s.sis_id_array AS student_sis_id_array
  , ARRAY_TO_STRING(s.name_array, ';\n') AS student_display

FROM
  _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs
LEFT JOIN instructors i ON cs.course_section_id=i.udp_course_section_id
LEFT JOIN students s ON cs.course_section_id=s.udp_course_section_id
