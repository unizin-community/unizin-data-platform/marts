/*
  Context / Base / Learner group membership

    Imports Learner group membership data from the context store.

  Properties:

    Export table:  mart_helper.context__learner_group_membership
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learner_group_membership AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_group_membership AS k ON e.learner_group_membership_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learner_group_membership_id AS learner_group_membership_id
  , from_cs.learner_group_id AS learner_group_id
  , from_cs.person_id AS person_id

  /*
  Attributes
  */
  , CASE from_cs.is_moderator WHEN TRUE THEN 1 ELSE NULL END AS is_moderator

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
