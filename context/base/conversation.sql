/*
  Context / Base / Conversation

    Imports Conversation data from the context store.

  Properties:

    Export table:  mart_helper.context__conversation
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.conversation AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.conversation AS k ON e.conversation_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.conversation_id AS conversation_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.learner_group_id AS learner_group_id

  /*
  Attributes
  */
  , from_cs.subject AS subject
  , LENGTH(from_cs.subject) AS length_subject

  , CASE from_cs.has_attachments WHEN TRUE THEN 1 ELSE NULL END AS has_attachments
  , CASE from_cs.has_media_objects WHEN TRUE THEN 1 ELSE NULL END AS has_media_objects

FROM from_cs
;
