/*
  Context / Base / Wiki page

    Imports Wiki page data from the context store.

  Properties:

    Export table:  mart_helper.context__wiki_page
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.wiki_page AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.wiki_page AS k ON e.wiki_page_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.wiki_page_id AS wiki_page_id
  , from_cs.person_id AS person_id
  , from_cs.wiki_id AS wiki_id

  /*
    Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title
  , from_cs.body AS body
  , LENGTH(from_cs.body) AS length_body

  , from_cs.editing_roles AS editing_roles

  , CASE from_cs.could_be_locked WHEN TRUE THEN 1 ELSE NULL END AS could_be_locked
  , CASE from_cs.is_protected_editing WHEN TRUE THEN 1 ELSE NULL END AS is_protected_editing


  , from_cs.url AS url

  , from_cs.view_count AS view_count
  , from_cs.comments_count AS comments_count

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , CASE from_cs.status WHEN 'active' THEN 1 ELSE NULL END AS is_status_active
  , CASE from_cs.status WHEN 'deleted' THEN 1 ELSE NULL END AS is_status_deleted
  , CASE from_cs.status WHEN 'unpublished' THEN 1 ELSE NULL END AS is_status_unpublished

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.revised_date AS revised_date

FROM from_cs
;
