/*
  Context / Base / Academic session

    Imports Academic session data from the context store.

  Properties:

    Export table:  mart_helper.context__academic_session
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.academic_session AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_session AS k ON e.academic_session_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.academic_session_id AS academic_session_id
  , from_cs.academic_term_id AS academic_term_id

  /*
  Attributes
  */
  , from_cs.name AS name
  , LENGTH(from_cs.name) AS length_name

  /*
  Instructional begin and end dates.
  */
  , from_cs.instruction_begin_date AS instruction_begin_date
  , from_cs.instruction_end_date AS instruction_end_date
  , DATE_DIFF(from_cs.instruction_end_date, from_cs.instruction_begin_date, DAY) AS days_instruction_length

  /*
  Used for course materials billing.
  */
  , from_cs.course_materials_bill_after_date AS course_materials_bill_after_date

FROM from_cs
;
