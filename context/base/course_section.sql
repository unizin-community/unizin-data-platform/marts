/*
  Context / Base / Course section

    Imports Course section data from the context store.

  Properties:

    Export table:  mart_helper.context__course_section
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Campus
    * Academic organization
    * Academic term
    * Academic session
    * Course offering
    * Course offering / Learner activity
    * Course offering / Quiz
    * Course offering / Module

  To do:
    *
*/

/*
  Using course section enrollments, calculate actual student enrollments in each
  section, along with the earliest entry and latest exit dates.
*/
WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.course_section AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.course_section AS k ON e.course_section_id=k.id
)

/*
teaching_assignments AS (
  SELECT
    cse.course_section_id
    , p.last_name || ', ' || p.first_name AS instructor_name
  FROM
  _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON cse.person_id = p.person_id
  WHERE
    cse.role = 'Teacher'
    AND p.last_name || ', ' || p.first_name IS NOT NULL
  GROUP BY
    cse.person_id
    , cse.course_section_id
    , p.first_name
    , p.last_name
),

teaching_assignments_array AS (
  SELECT
    course_section_id
    , ARRAY_AGG(DISTINCT instructor_name) AS instructor_array
  FROM
    teaching_assignments
  GROUP BY
    course_section_id
),

student_enrollments AS (
  SELECT
    cs.course_section_id
    , COUNT(DISTINCT cse.person_id) AS count_student_enrollments
  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON cse.course_section_id = cs.course_section_id
  WHERE
    cse.role = 'Student'
    AND cse.role_status IN
      ( 'Enrolled',
        'Completed',
        'Registered',
        'Pre-registered'
      )
  GROUP BY
    cs.course_section_id
)
/

/*
  Now generate the features table for Course sections.
*/
SELECT
  from_cs.sis_id AS sis_id
  , from_cs.lms_id AS lms_id
  , from_cs.course_section_id AS course_section_id
  , tt.academic_term_id AS academic_term_id
  , tt.sis_id AS sis_academic_term_id
  , tt.lms_id AS lms_academic_term_id
  , ss.academic_session_id AS academic_session_id
  , ss.sis_id AS sis_academic_session_id
  , co.course_offering_id AS course_offering_id
  , co.sis_id AS sis_course_offering_id
  , co.lms_id AS lms_course_offering_id
  , from_cs.academic_organization_id AS academic_organization_id

  /*
  Culled from the beyond
  */
  , c.name AS c_name

  , ao.institutional_code AS ao_institutional_code
  , ao.name AS ao_name

  , tt.name AS tt_name
  , tt.term_year AS tt_term_year
  , tt.term_type AS tt_term_type

  , ss.name AS ss_name
  , ss.instruction_begin_date AS ss_instruction_begin_date
  , ss.instruction_end_date AS ss_instruction_end_date

  , co.title AS co_title
  , co.subject AS co_subject
  , co.number AS co_number

  /*
  Attributes
  */
  , from_cs.section_number AS section_number
  , from_cs.class_number AS class_number

  , from_cs.le_current_course_offering_id AS le_current_course_offering_id
  , from_cs.le_original_course_offering_id AS le_original_course_offering_id
  , from_cs.combined_section_id AS combined_section_id

  /*
  Binary attributes
  */
  , CASE from_cs.is_accepting_enrollments WHEN TRUE THEN 1 ELSE NULL END AS is_accepting_enrollments
  , CASE from_cs.is_combined_section_parent WHEN TRUE THEN 1 ELSE NULL END AS is_combined_section_parent
  , CASE from_cs.is_default WHEN TRUE THEN 1 ELSE NULL END AS is_default
  , CASE from_cs.is_graded WHEN TRUE THEN 1 ELSE NULL END AS is_graded
  , CASE from_cs.is_honors WHEN TRUE THEN 1 ELSE NULL END AS is_honors

  /*
  Combined section basis
  */
  -- https://docs.udp.unizin.org/tables/ref_combined_section_basis.html
  , from_cs.combined_section_basis AS combined_section_basis
  , CASE from_cs.combined_section_basis WHEN 'Crosslisted' THEN 1 ELSE NULL END AS is_combined_section_basis_crosslisted
  , CASE from_cs.combined_section_basis WHEN 'MeetTogether' THEN 1 ELSE NULL END AS is_combined_section_basis_meet_together
  , CASE from_cs.combined_section_basis WHEN 'UgGrad' THEN 1 ELSE NULL END AS is_combined_section_basis_ugrad
  , CASE from_cs.combined_section_basis WHEN 'NoData' THEN 1 ELSE NULL END AS is_combined_section_basis_no_data
  , CASE from_cs.combined_section_basis WHEN 'Other' THEN 1 ELSE NULL END AS is_combined_section_basis_other

  /*
  Delivery mode
  */
  -- https://docs.udp.unizin.org/tables/ref_course_section_delivery_mode.html
  , from_cs.delivery_mode AS delivery_mode
  , CASE from_cs.delivery_mode WHEN 'AudioVideo' THEN 1 ELSE NULL END AS is_delivery_mode_audio_video
  , CASE from_cs.delivery_mode WHEN 'BlendedLearning' THEN 1 ELSE NULL END AS is_delivery_mode_blended_learning
  , CASE from_cs.delivery_mode WHEN 'Broadcast' THEN 1 ELSE NULL END AS is_delivery_mode_broadcast
  , CASE from_cs.delivery_mode WHEN 'Correspondence' THEN 1 ELSE NULL END AS is_delivery_mode_correspondence
  , CASE from_cs.delivery_mode WHEN 'EarlyCollege' THEN 1 ELSE NULL END AS is_delivery_mode_early_college
  , CASE from_cs.delivery_mode WHEN 'FaceToFace' THEN 1 ELSE NULL END AS is_delivery_mode_face_to_face
  , CASE from_cs.delivery_mode WHEN 'IndependentStudy' THEN 1 ELSE NULL END AS is_delivery_mode_independent_study
  , CASE from_cs.delivery_mode WHEN 'NoData' THEN 1 ELSE NULL END AS is_delivery_mode_no_data
  , CASE from_cs.delivery_mode WHEN 'Online' THEN 1 ELSE NULL END AS is_delivery_mode_online

  /*
  Status
  */
  , from_cs.status AS status
  , CASE from_cs.status WHEN 'active' THEN 1 ELSE NULL END AS is_status_active
  , CASE from_cs.status WHEN NULL THEN 1 ELSE NULL END AS is_status_inactive

  /*
  Type
  */
  -- https://docs.udp.unizin.org/tables/ref_course_section_type.html
  , from_cs.type AS type
  , CASE from_cs.type WHEN 'Clinical' THEN 1 ELSE NULL END AS is_type_clinical
  , CASE from_cs.type WHEN 'Discussion' THEN 1 ELSE NULL END AS is_type_discussion
  , CASE from_cs.type WHEN 'Dissertation' THEN 1 ELSE NULL END AS is_type_dissertation
  , CASE from_cs.type WHEN 'Drill' THEN 1 ELSE NULL END AS is_type_drill
  , CASE from_cs.type WHEN 'IndependentStudy' THEN 1 ELSE NULL END AS is_type_indepedendent_study
  , CASE from_cs.type WHEN 'Laboratory' THEN 1 ELSE NULL END AS is_type_laboratory
  , CASE from_cs.type WHEN 'Lecture' THEN 1 ELSE NULL END AS is_type_lecture
  , CASE from_cs.type WHEN 'Recitation' THEN 1 ELSE NULL END AS is_type_recitation
  , CASE from_cs.type WHEN 'Research' THEN 1 ELSE NULL END AS is_type_research
  , CASE from_cs.type WHEN 'Seminar' THEN 1 ELSE NULL END AS is_type_seminar
  , CASE from_cs.type WHEN 'StudyAbroad' THEN 1 ELSE NULL END AS is_type_study_abroad
  , CASE from_cs.type WHEN 'Thesis' THEN 1 ELSE NULL END AS is_type_thesis
  , CASE from_cs.type WHEN 'Workshop' THEN 1 ELSE NULL END AS is_type_workshop
  , CASE from_cs.type WHEN 'NoData' THEN 1 ELSE NULL END AS is_type_nodata
  , CASE from_cs.type WHEN 'Other' THEN 1 ELSE NULL END AS is_type_other

  /*
    About total enrollments for this course section.
  */
  , COALESCE(from_cs.max_enrollment, NULL) max_enrollment
  /*
  , CASE
      WHEN se.count_student_enrollments IS NULL THEN 0
    ELSE
      se.count_student_enrollments
    END AS count_student_enrollments
  */
  /*
    Teaching assignment data.
  */
  /*
  , ARRAY_TO_STRING(taa.instructor_array, '\n') AS instructor_display
  , 'missing data' AS instructor_email_array
  */

  /*
    Details about the course section design.
  */
  /*
  , CASE
      WHEN la.count_learner_activities IS NULL THEN 0
    ELSE
      la.count_learner_activities
    END AS published_la
  -- Fix to actually be unpublished LA
  , CASE
      WHEN la.count_learner_activities IS NULL THEN 0
    ELSE
      la.count_learner_activities
    END AS unpublished_la
  , CASE
      WHEN q.count_quizzes IS NULL THEN 0
    ELSE
      q.count_quizzes
    END
    AS published_quiz
  -- Fix to actually be unpublished Quiz
  , CASE
      WHEN q.count_quizzes IS NULL THEN 0
    ELSE
      q.count_quizzes
    END AS unpublished_quiz
  , CASE
      WHEN m.count_modules IS NULL THEN 0
    ELSE
      m.count_modules
    END AS active_module
  -- Fix to actually be unpublished Quiz
  , CASE
      WHEN m.count_modules IS NULL THEN 0
    ELSE
      m.count_modules
    END AS unpublished_module
  */
  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON from_cs.course_offering_id=co.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session AS ss ON co.academic_session_id=ss.academic_session_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_organization AS ao ON from_cs.academic_organization_id=ao.academic_organization_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__campus AS c ON ao.campus_id=c.campus_id
/*
LEFT JOIN teaching_assignments_array AS taa ON from_cs.course_section_id = taa.course_section_id
LEFT JOIN student_enrollments AS se ON from_cs.course_section_id = se.course_section_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity AS la ON co.course_offering_id = la.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__quiz AS q ON co.course_offering_id = q.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__module AS m ON co.course_offering_id = m.course_offering_id
*/
;
