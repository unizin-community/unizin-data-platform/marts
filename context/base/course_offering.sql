/*
  Context / Base / Course offering

    Imports Course offering data from the context store.

  Properties:

    Export table:  mart_helper.context__course_offering
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.course_offering AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.course_offering AS k ON e.course_offering_id=k.id
),

/*
  Get the unique subset of Course offerings
  by subject and number, giving each unique
  combination an index.
*/
course_subject_combinations AS (
  SELECT
    ROW_NUMBER() OVER() AS row_number
    , from_cs.subject AS subject
    , from_cs.number AS number
  FROM
    from_cs
  GROUP BY
    from_cs.subject
    , from_cs.number
),

/*
  Create fake course titles and given each a
  unique index.
*/
course_titles AS (
  SELECT
    ROW_NUMBER() OVER() AS row_number
    , CONCAT(prefix, ' ', cc.title) AS title
    , cc.description AS description
    , cc.description AS syllabus_content
  FROM
    _WRITE_TO_PROJECT_.mart_helper.utility__cip_codes AS cc
  CROSS JOIN UNNEST([
    'Intro to'
    , 'Advanced study of'
    , 'Masters in'
    , 'Seminar in'
    , 'Senior seminar in'
    , 'Self-directed study:'
    , 'Remedial for'
    , 'The art of'
    , 'The science of'
    , 'Modern advances in'
    , 'History of'
    , 'Philisphy of'
    , 'Continuing education in'
    , 'Capstone in'
    , 'Orientation to'
    , 'Advances in'
    , 'The poltiics of'
    , 'The art of'

  ]) AS prefix
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.lms_id AS lms_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.academic_career_id AS academic_career_id
  , from_cs.academic_session_id AS academic_session_id
  , from_cs.academic_term_id as academic_term_id
  , from_cs.campus_id AS campus_id
  , from_cs.learning_environment_organization_id AS learning_environment_organization_id
  /*
  Attributes
  */
  , from_cs.cip_code2k AS cip_code2k
  , from_cs.subject AS subject
  , from_cs.number AS number

  /*
    To undo the fake Course titles and use real ones
    instead, replace the 'ct' table alias with 'from_cs'.
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS character_length_title

  , from_cs.description AS description
  , LENGTH(from_cs.description) AS character_length_description

  , from_cs.syllabus_content AS syllabus_content

  /*
  Administrative attributes
  */
  , CASE from_cs.is_public WHEN TRUE THEN 1 ELSE NULL END AS is_public
  , from_cs.multiple_meeting_patterns AS multiple_meeting_patterns
  , from_cs.available_credits AS available_credits

  /*
  Learning environment status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.le_status AS le_status
  , CASE from_cs.le_status WHEN 'Active' THEN 1 ELSE 0 END AS is_le_status_active

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status
  , CASE from_cs.status WHEN 'Active' THEN 1 ELSE 0 END AS is_status_active

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , COALESCE(from_cs.start_date,from_cs.le_start_date) AS start_date
  , COALESCE(from_cs.end_date,from_cs.le_end_date) AS end_date

FROM from_cs

/*
  Give each Course offering a unique index based on its
  Course subject and number.
*/
LEFT JOIN
  course_subject_combinations AS csc ON from_cs.subject=csc.subject AND from_cs.number=csc.number

/*
  Now give each course subject/number combination its
  appropriate title, ensuring consistency in course
  titles across historical data.
*/
LEFT JOIN
  course_titles AS ct ON csc.row_number=ct.row_number
;
