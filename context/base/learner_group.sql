/*
  Context / Base / Learner group

    Imports Learner group data from the context store.

  Properties:

    Export table:  mart_helper.context__learner_group
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learner_group AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_group AS k ON e.learner_group_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learner_group_id AS learner_group_id
  , from_cs.course_offering_id AS course_offering_id

  /*
  Attributes
  */
  , from_cs.name AS name
  , LENGTH(from_cs.name) AS length_name
  , from_cs.description AS description
  , LENGTH(from_cs.description) AS length_description

  , from_cs.category AS category
  , from_cs.context_type AS context_type

  , from_cs.default_view AS default_view
  , CASE from_cs.is_public_contents WHEN TRUE THEN 1 ELSE NULL END AS is_public_contents

  , from_cs.max_membership AS max_membership

  , from_cs.join_level AS join_level
  , from_cs.storage_quota AS storage_quota

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.deleted_date AS deleted_date

FROM from_cs
;
