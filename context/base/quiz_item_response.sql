/*
  Context / Base / Quiz item response

    Imports Quiz item response data from the context store.

  Properties:

    Export table:  mart_helper.context__quiz_item_response
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.quiz_item_response AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.quiz_item_response AS k ON e.quiz_item_response_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.quiz_item_response_id AS quiz_item_response_id
  , from_cs.quiz_item_id AS quiz_item_id

  /*
    Attributes
  */
  , from_cs.body AS body
  , LENGTH(from_cs.body) AS length_body

  , from_cs.comments AS comments
  , LENGTH(from_cs.comments) AS length_comments

  /*
  Correctness
  */
  , from_cs.weight AS weight
  , from_cs.exact AS exact
  , from_cs.numerical_answer_type AS numerical_answer_type
  , from_cs.incorrect_matches AS incorrect_matches
  , from_cs.margin AS margin

  /*
  Quiz response features
  */
  , from_cs.blank AS blank
  , from_cs.answer_match_left AS answer_match_left
  , from_cs.answer_match_right AS answer_match_right
  , from_cs.starting_range AS starting_range
  , from_cs.ending_range AS ending_range
  , from_cs.html AS html
  , from_cs.text_after_answers AS text_after_answers

FROM from_cs
;
