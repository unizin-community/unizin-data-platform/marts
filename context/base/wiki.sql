/*
  Context / Base / Wiki

    Imports Wiki data from the context store.

  Properties:

    Export table:  mart_helper.context__wiki
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.wiki AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.wiki AS k ON e.wiki_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.wiki_id AS wiki_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.learner_group_id AS learner_group_id

  /*
    Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title

  , CASE from_cs.has_no_front_page WHEN TRUE THEN 1 ELSE NULL END AS has_no_front_page

  , from_cs.front_page_url AS front_page_url

  /*
  Parent entity type
  */
  -- No option set.
  , CASE from_cs.parent_entity_type WHEN 'Course' THEN 1 ELSE NULL END AS is_parent_entity_type_course_offering
  , CASE from_cs.parent_entity_type WHEN 'Group' THEN 1 ELSE NULL END AS is_parent_entity_type_learner_group

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
