/*
  Context / Base / Learner activity result

    Imports Learner activity result data from the context store.

  Properties:

    Export table:  mart_helper.context__learner_activity_result
    Run frequency: Every day
    Run operation: Overwrite


  Dependencies:

    * Academic session
    * Course offering
    * Learner activity

  To do:
    *
*/

WITH from_cs AS (
SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM
    _READ_FROM_PROJECT_.context_store_entity.learner_activity_result AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_activity_result AS k ON e.learner_activity_result_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , COALESCE(ss.academic_term_id, act.academic_term_id) AS academic_term_id
  , co.course_offering_id AS course_offering_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.learner_activity_result_id AS learner_activity_result_id
  , from_cs.learner_group_id AS learner_group_id
  , from_cs.person_id AS person_id
  , from_cs.grader_id AS grader_id

  /*
  Attributes
  */
  , from_cs.body AS body
  , LENGTH(from_cs.body) AS length_body

  , CASE WHEN from_cs.has_admin_comment IS TRUE THEN 1 ELSE NULL END AS has_admin_comment
  , CASE WHEN from_cs.has_rubric_assessment IS TRUE THEN 1 ELSE NULL END AS has_rubric_assessment
  , CASE WHEN from_cs.is_excused IS TRUE THEN 1 ELSE NULL END AS is_excused
  , CASE WHEN from_cs.is_grade_anonymously IS TRUE THEN 1 ELSE NULL END AS is_grade_anonymously
  , CASE WHEN from_cs.is_processed IS TRUE THEN 1 ELSE NULL END AS is_processed

  , from_cs.submission_comments_count AS submission_comments_count
  , from_cs.what_if_score AS what_if_score
  , from_cs.attempt as attempt
  , from_cs.process_attempts AS process_attempts

  /*
  Score and grade data.
  */
  , la.points_possible AS points_possible
  , from_cs.score AS score
  , from_cs.published_score AS published_score
  , from_cs.grade AS grade
  , from_cs.published_grade AS published_grade
  , CASE
  WHEN la.points_possible > 0 THEN from_cs.score / la.points_possible
    ELSE 0
  END AS score_percentage

  /*
    Grade state
  */
  , CASE WHEN from_cs.grade_state = 'human_graded' THEN 1 ELSE NULL END AS grade_state_human_graded
  , CASE WHEN from_cs.grade_state = 'not_graded' THEN 1 ELSE NULL END AS grade_state_not_graded
  , CASE WHEN from_cs.grade_state = 'auto_graded' THEN 1 ELSE NULL END AS grade_state_auto_graded

  /*
  Type
  */
  -- https://docs.udp.unizin.org/tables/ref_learner_activity_result_type.html
  , from_cs.type AS type
  , CASE WHEN from_cs.type = 'media_recording' THEN 1 ELSE NULL END AS is_type_media_recording
  , CASE WHEN from_cs.type = 'online_url' THEN 1 ELSE NULL END AS is_type_online_url
  , CASE WHEN from_cs.type = 'online_quiz' THEN 1 ELSE NULL END AS is_type_online_quiz
  , CASE WHEN from_cs.type = 'external_tool' THEN 1 ELSE NULL END AS is_type_external_tool
  , CASE WHEN from_cs.type = 'online_upload' THEN 1 ELSE NULL END AS is_type_online_upload
  , CASE WHEN from_cs.type = 'basic_lti_launch' THEN 1 ELSE NULL END AS is_type_basic_lti_launch
  , CASE WHEN from_cs.type = 'discussion_topic' THEN 1 ELSE NULL END AS is_type_discussion_topic
  , CASE WHEN from_cs.type = 'online_text_entry' THEN 1 ELSE NULL END AS is_type_online_text_entry

  /*
  Grading status
  */
  , CASE from_cs.grading_status WHEN 'graded' THEN 1 ELSE 0 END AS is_grading_status_graded
  , CASE from_cs.grading_status WHEN 'deleted' THEN 1 ELSE 0 END AS is_grading_status_deleted
  , CASE from_cs.grading_status WHEN 'pending_review' THEN 1 ELSE 0 END AS is_grading_status_pending_review
  , CASE from_cs.grading_status WHEN 'submitted' THEN 1 ELSE 0 END AS is_grading_status_submitted
  , CASE from_cs.grading_status WHEN 'unsubmitted' THEN 1 ELSE 0 END AS is_grading_status_unsubmitted

  /*
  Gradebook status
  */
  -- https://docs.udp.unizin.org/tables/ref_gradebook_status.html
  , CASE from_cs.gradebook_status WHEN 'true' THEN 1 ELSE 0 END AS is_gradebook_status_true
  , CASE from_cs.gradebook_status WHEN 'false' THEN 1 ELSE 0 END AS is_gradebook_status_false
  , CASE from_cs.gradebook_status WHEN NULL THEN 1 ELSE 0 END AS is_gradebook_status_none

  /*
  We may need to revist the accuracy of the claim that this captures
  the amount of time the student had to complete the learner activity.
  */
  , DATE_DIFF(CAST(la.due_date AS DATE), CAST(la.unlocked_date AS DATE), DAY) AS days_to_complete

  /*
  When does this learner activity result rank in the student's
  academic experience overall and in the term?
  */
  , DENSE_RANK() OVER (PARTITION BY from_cs.person_id ORDER BY la.unlocked_date, la.due_date ASC) AS person_la_rank
  , DENSE_RANK() OVER (PARTITION BY from_cs.person_id, la.course_offering_id, ss.academic_term_id ORDER BY la.unlocked_date, la.due_date ASC) AS person_course_term_la_rank

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.posted_at AS posted_at
  , la.unlocked_date AS unlocked_date
  , la.due_date AS due_date
  , from_cs.response_date AS response_date
  , from_cs.graded_date AS graded_date

  /*
  Temporal features about the learner activity result.
  */
  , DATE_DIFF(CAST(from_cs.updated_date AS DATE), CAST(from_cs.created_date AS DATE), DAY) AS days_created_to_updated
  , DATE_DIFF(CAST(from_cs.response_date AS DATE), CAST(from_cs.created_date AS DATE), DAY) AS days_created_to_response
  , DATE_DIFF(CAST(from_cs.graded_date AS DATE), CAST(from_cs.created_date AS DATE), DAY) AS days_created_to_graded
  , DATE_DIFF(CAST(from_cs.response_date AS DATE), CAST(from_cs.updated_date AS DATE), DAY) AS days_updated_to_response
  , DATE_DIFF(CAST(from_cs.graded_date AS DATE), CAST(from_cs.updated_date AS DATE), DAY) AS days_updated_to_graded
  , DATE_DIFF(CAST(from_cs.graded_date AS DATE), CAST(from_cs.response_date AS DATE), DAY) AS days_response_to_graded

FROM from_cs

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity la ON from_cs.learner_activity_id=la.learner_activity_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering  co ON la.course_offering_id=co.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session ss ON co.academic_session_id=ss.academic_session_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act on act.academic_term_id = co.academic_term_id

;
