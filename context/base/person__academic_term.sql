/*
  Context / Base / Person - Acacdemic term

    Imports Person - Acacdemic term data from the context store.

  Properties:

    Export table:  mart_helper.context__person__academic_term
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    kp.sis_ext_id AS sis_person_id
    , kat.sis_ext_id as sis_academic_term_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.person__academic_term AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.person AS kp ON e.person_id=kp.id
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_term AS kat ON e.academic_term_id=kat.id
)

SELECT
  from_cs.sis_person_id AS sis_person_id
  , from_cs.sis_academic_term_id AS sis_academic_term_id
  , from_cs.academic_career_id AS academic_career_id
  , from_cs.academic_term_id AS academic_term_id
  , from_cs.person_id AS person_id

  /*
  Attributes
  */
  , CASE from_cs.in_honors_program WHEN TRUE THEN 1 ELSE NULL END AS in_honors_program
  , CASE from_cs.is_in_supplemental_study_plans WHEN TRUE THEN 1 ELSE NULL END AS is_in_supplemental_study_plans
  , CASE from_cs.is_in_teaching_certificate_plan WHEN TRUE THEN 1 ELSE NULL END AS is_in_teaching_certificate_plan
  , CASE from_cs.is_active_duty WHEN TRUE THEN 1 ELSE NULL END AS is_active_duty
  , CASE from_cs.is_veteran WHEN TRUE THEN 1 ELSE NULL END AS is_veteran

  /*
  GPA
  */
  , from_cs.gpa_credits_units_hours AS gpa_credits_units_hours
  , from_cs.non_gpa_credits_units_hours AS non_gpa_credits_units_hours
  , from_cs.grade_points_earned AS grade_points_earned
  , from_cs.gpa_academic_term AS gpa_academic_term
  , from_cs.gpa_cumulative AS gpa_cumulative

  /*
  Census date academic level
  */
  -- https://docs.udp.unizin.org/tables/ref_academic_level.html
  -- Too many to list
  , from_cs.cen_academic_level AS cen_academic_level

  /*
  Census date academic load
  */
  -- https://docs.udp.unizin.org/tables/ref_academic_load.html
  , from_cs.cen_academic_load AS cen_academic_load
  , CASE from_cs.cen_academic_load WHEN 'FullTime' THEN 1 ELSE NULL END AS cen_academic_load_full_time
  , CASE from_cs.cen_academic_load WHEN 'HalfTime' THEN 1 ELSE NULL END AS cen_academic_load_half_time
  , CASE from_cs.cen_academic_load WHEN 'LessThanHalfTime' THEN 1 ELSE NULL END AS cen_academic_load_less_than_half_time
  , CASE from_cs.cen_academic_load WHEN 'NoCredits' THEN 1 ELSE NULL END AS cen_academic_load_no_credits
  , CASE from_cs.cen_academic_load WHEN 'NoData' THEN 1 ELSE NULL END AS cen_academic_load_no_data
  , CASE from_cs.cen_academic_load WHEN 'Other' THEN 1 ELSE NULL END AS cen_academic_load_other
  , CASE from_cs.cen_academic_load WHEN 'PartTime' THEN 1 ELSE NULL END AS cen_academic_load_part_time
  , CASE from_cs.cen_academic_load WHEN 'ThreeQuartersTime' THEN 1 ELSE NULL END AS cen_academic_load_three_quarters_time

  /*
  End of term academic load
  */
  -- https://docs.udp.unizin.org/tables/ref_academic_load.html
  , from_cs.eot_academic_load AS eot_academic_load
  , CASE from_cs.eot_academic_load WHEN 'FullTime' THEN 1 ELSE NULL END AS eot_academic_load_full_time
  , CASE from_cs.eot_academic_load WHEN 'HalfTime' THEN 1 ELSE NULL END AS eot_academic_load_half_time
  , CASE from_cs.eot_academic_load WHEN 'LessThanHalfTime' THEN 1 ELSE NULL END AS eot_academic_load_less_than_half_time
  , CASE from_cs.eot_academic_load WHEN 'NoCredits' THEN 1 ELSE NULL END AS eot_academic_load_no_credits
  , CASE from_cs.eot_academic_load WHEN 'NoData' THEN 1 ELSE NULL END AS eot_academic_load_no_data
  , CASE from_cs.eot_academic_load WHEN 'Other' THEN 1 ELSE NULL END AS eot_academic_load_other
  , CASE from_cs.eot_academic_load WHEN 'PartTime' THEN 1 ELSE NULL END AS eot_academic_load_part_time
  , CASE from_cs.eot_academic_load WHEN 'ThreeQuartersTime' THEN 1 ELSE NULL END AS eot_academic_load_three_quarters_time

  /*
  Registration status
  */
  -- https://docs.udp.unizin.org/tables/ref_academic_term_registration_status.html
  , from_cs.registration_status AS registration_status
  , CASE from_cs.registration_status WHEN 'Canceled' THEN 1 ELSE NULL END AS registration_status_canceled
  , CASE from_cs.registration_status WHEN 'NoData' THEN 1 ELSE NULL END AS registration_status_no_data
  , CASE from_cs.registration_status WHEN 'NotRegistered' THEN 1 ELSE NULL END AS registration_status_not_registered
  , CASE from_cs.registration_status WHEN 'Other' THEN 1 ELSE NULL END AS registration_status_other
  , CASE from_cs.registration_status WHEN 'Registered' THEN 1 ELSE NULL END AS registration_status_registered
  , CASE from_cs.registration_status WHEN 'Withdrawn' THEN 1 ELSE NULL END AS registration_status_withdrawn

  /*
  US Residency
  */
  -- https://docs.udp.unizin.org/tables/ref_usc_itizenship_status.html
  -- Too many to list.
  , from_cs.us_residency AS us_residency

  /*
  US State residency
  */
  -- https://docs.udp.unizin.org/tables/ref_state_residency_status.html
  -- Too many to list.
  , from_cs.us_state_residency AS us_state_residency

  /*
  Athletic NCAA sport
  */
  -- https://docs.udp.unizin.org/tables/ref_ncaa_sports.html
  -- Too many to list.
  , from_cs.athletic_participant_sport AS athletic_participant_sport

FROM from_cs
;
