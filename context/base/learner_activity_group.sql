/*
  Context / Base / Learner activity group

    Imports Learner activity group data from the context store.

  Properties:

    Export table:  mart_helper.context__learner_activity_group
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learner_activity_group AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_activity_group AS k ON e.learner_activity_group_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learner_activity_group_id AS learner_activity_group_id
  , from_cs.course_offering_id AS course_offering_id

  /*
  Attributes
  */
  , from_cs.position AS position

  , from_cs.name AS name
  , LENGTH(from_cs.name) AS length_name

  , from_cs.default_learner_activity_name AS default_learner_activity_name
  , LENGTH(from_cs.default_learner_activity_name) AS length_default_learner_activity_name

  /*
  Grading behaviors
  */
  , from_cs.drop_lowest_amount AS drop_lowest_amount
  , from_cs.drop_highest_amount AS drop_highest_amount
  , from_cs.group_weight AS group_weight

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status
  , CASE from_cs.status WHEN 'NoData' THEN 1 ELSE 0 END AS is_no_data
  , CASE from_cs.status WHEN 'Available' THEN 1 ELSE 0 END AS is_available
  , CASE from_cs.status WHEN 'Deleted' THEN 1 ELSE 0 END AS is_deleted

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
