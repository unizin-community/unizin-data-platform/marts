/*
  Context / Base / Discussion

    Imports Discussion data from the context store.

  Properties:

    Export table:  mart_helper.context__discussion
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.discussion AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.discussion AS k ON e.discussion_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.discussion_id AS discussion_id
  , from_cs.author_id AS author_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.editor_id AS editor_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.learner_group_id AS learner_group_id

  /*
  Attributes
  */
  , from_cs.title
  , LENGTH(from_cs.title) AS length_title

  , from_cs.body
  , LENGTH(from_cs.body) AS length_body

  , CASE from_cs.is_locked WHEN TRUE THEN 1 ELSE NULL END AS is_locked
  , CASE from_cs.is_pinned WHEN TRUE THEN 1 ELSE NULL END AS is_pinned

  /*
  Type
  */
  -- https://docs.udp.unizin.org/tables/ref_discussion_type.html
  , from_cs.type AS type
  , CASE from_cs.type WHEN 'Announcement' THEN 1 ELSE NULL END AS is_type_announcement

  /*
  Discussion type
  */
  -- https://docs.udp.unizin.org/tables/ref_discussion_type.html
  , from_cs.discussion_type AS discussion_type
  , CASE from_cs.discussion_type WHEN 'threaded' THEN 1 ELSE NULL END AS is_discussion_type_threaded
  , CASE from_cs.discussion_type WHEN 'side_comment' THEN 1 ELSE NULL END AS is_discussion_type_side_comment

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , CASE from_cs.status WHEN 'active' THEN 1 ELSE NULL END AS is_status_active
  , CASE from_cs.status WHEN 'deleted' THEN 1 ELSE NULL END AS is_status_deleted
  , CASE from_cs.status WHEN 'unpublished' THEN 1 ELSE NULL END AS is_status_unpublished
  , CASE from_cs.status WHEN 'post_delayed' THEN 1 ELSE NULL END AS is_status_post_delayed

  /*
  Dates
  */
  , from_cs.created_date
  , from_cs.updated_date
  , from_cs.posted_date
  , from_cs.delayed_post_date
  , from_cs.last_reply_date
  , from_cs.deleted_date

FROM from_cs
;
