/*
  Context / Base / File

    Imports File data from the context store.

  Properties:

    Export table:  mart_helper.context__file
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Learner activity
    * Quiz

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_ext_id
    , k.lms_int_id AS lms_int_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.file AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.file AS k ON e.file_id=k.id
)

SELECT
  from_cs.lms_int_id AS lms_int_id
  , from_cs.lms_ext_id AS lms_ext_id
  , from_cs.file_id AS file_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.conversation_id AS conversation_id
  , from_cs.conversation_message_id AS conversation_message_id
  , from_cs.conversation_person_id AS conversation_person_id
  , from_cs.learner_activity_group_id AS learner_activity_group_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.learner_activity_result_id AS learner_activity_result_id
  , la.lms_id AS lms_learner_activity_result_id
  , from_cs.learner_group_id AS learner_group_id
  , from_cs.owner_id AS owner_id
  , from_cs.quiz_id AS quiz_id
  , q.lms_id AS lms_quiz_id
  , from_cs.quiz_result_id AS quiz_result_id
  , from_cs.replacement_file_id AS replacement_file_id
  , from_cs.root_file_id AS root_file_id
  , from_cs.uploader_id AS uploader_id
  , from_cs.wiki_id AS wiki_id


  /*
    Learner activity.
  */
  , la.title AS learner_activity_title
  , la.due_date AS learner_activity_due_date

  /*
    Quiz
  */
  , q.title AS quiz_title
  , q.due_date AS quiz_due_date

  /*
  Attributes
  */
  , from_cs.display_name AS display_name
  , LENGTH(from_cs.display_name) AS length_display_name
  , from_cs.size AS size
  , CASE from_cs.can_be_locked WHEN TRUE THEN 1 ELSE NULL END AS can_be_locked

  /*
  Content type
  */
  -- No option set.
  , from_cs.content_type AS content_type
  -- Fix me: split_part to split?
--  , split_part(f.content_type, '/', 1) as content_type
--	, split_part(f.content_type, '/', 2) as content_sub_type

  /*
  Owner entity type
  */
  -- No option set
  , from_cs.owner_entity_type AS owner_entity_type
  , CASE from_cs.owner_entity_type WHEN 'quiz_submission' THEN 1 ELSE NULL END AS is_owned_by_quiz_result
  , CASE from_cs.owner_entity_type WHEN 'account' THEN 1 ELSE NULL END AS is_owned_by_account
  , CASE from_cs.owner_entity_type WHEN 'submission' THEN 1 ELSE NULL END AS is_owned_by_learner_activity_result
  , CASE from_cs.owner_entity_type WHEN 'quiz' THEN 1 ELSE NULL END AS is_owned_by_quiz
  , CASE from_cs.owner_entity_type WHEN 'user' THEN 1 ELSE NULL END AS is_owned_by_person
  , CASE from_cs.owner_entity_type WHEN 'group' THEN 1 ELSE NULL END AS is_owned_by_learner_group
  , CASE from_cs.owner_entity_type WHEN 'course' THEN 1 ELSE NULL END AS is_owned_by_course_offering
  , CASE from_cs.owner_entity_type WHEN 'assignment' THEN 1 ELSE NULL END AS is_owned_by_learner_activity

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , CASE from_cs.status WHEN 'available' THEN 1 ELSE NULL END AS is_status_available
  , CASE from_cs.status WHEN 'deleted' THEN 1 ELSE NULL END AS is_status_deleted
  , CASE from_cs.status WHEN 'errored' THEN 1 ELSE NULL END AS is_status_errored
  , CASE from_cs.status WHEN 'hidden' THEN 1 ELSE NULL END AS is_status_hidden

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.viewed_date AS viewed_date
  , from_cs.unlocked_date AS unlocked_date
  , from_cs.locked_date AS locked_date
  , from_cs.deleted_date AS deleted_date
  , CASE
		WHEN from_cs.unlocked_date IS NULL THEN from_cs.created_date
		ELSE from_cs.unlocked_date
    END AS accessible_date
	, CASE
		WHEN from_cs.updated_date IS NULL THEN from_cs.created_Date
		ELSE from_cs.updated_date
    END AS most_recent_version_date

FROM from_cs
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity AS la ON from_cs.learner_activity_id=la.learner_activity_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__quiz AS q ON from_cs.quiz_id=q.quiz_id
;
