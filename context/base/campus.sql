/*
  Context / Base / Campus

    Imports Campus data from the context store.

  Properties:

    Export table:  mart_helper.context__campus
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.campus AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.campus AS k ON e.campus_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.campus_id AS campus_id

  /*
  Attributes
  */
  , from_cs.abbreviation AS abbreviation
  , from_cs.name AS name

FROM from_cs
;
