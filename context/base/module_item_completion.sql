/*
  Context / Base / Module item completion

    Imports Module item completion data from the context store.

  Properties:

    Export table:  mart_helper.context__module_item_completion
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.module_item_completion AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.module_item_completion AS k ON e.module_item_completion_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.module_item_completion_id AS module_item_completion_id
  , from_cs.module_item_id AS module_item_id
  , from_cs.module_progression_id AS module_progression_id

  /*
  Scores
  */
  , from_cs.score AS score
  , from_cs.min_score AS min_score

  /*
  Requirement type
  */
  -- No option set.
  , from_cs.requirement_type AS requirement_type

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.completion_status AS completion_status

FROM from_cs
;
