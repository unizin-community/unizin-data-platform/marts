/*
  Context / Base / Facility

    Imports Facility data from the context store.

  Properties:

    Export table:  mart_helper.context__facility
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.facility AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.facility AS k ON e.facility_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.facility_id AS facility_id
  , from_cs.campus_id AS campus_id

  /*
  Attributes
  */
  , from_cs.abbreviation AS abbreviation
  , from_cs.name AS name

  /*
  Facility type
  */
  -- https://docs.udp.unizin.org/tables/ref_facility_type.html
  , from_cs.facility_type_id AS facility_type_id
  , CASE from_cs.facility_type_id WHEN 'Laboratory' THEN 1 ELSE NULL END AS facility_type_id_laboratory
  , CASE from_cs.facility_type_id WHEN 'LectureHall' THEN 1 ELSE NULL END AS facility_type_id_lecture_hall
  , CASE from_cs.facility_type_id WHEN 'NoData' THEN 1 ELSE NULL END AS facility_type_id_no_data
  , CASE from_cs.facility_type_id WHEN 'Other' THEN 1 ELSE NULL END AS facility_type_id_other

  /*
  Address
  */
  , from_cs.street_name AS street_name
  , from_cs.street_number AS street_number
  , from_cs.city AS city
  , from_cs.state AS state
  , from_cs.zip_code AS zip_code

  /*
  GPS Location
  */
  , from_cs.gps_coordinates AS gps_coordinates

FROM from_cs
;
