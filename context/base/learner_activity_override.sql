/*
  Context / Base / Learner activity override

    Imports Learner activity override data from the context store.

  Properties:

    Export table:  mart_helper.context__learner_activity_override
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learner_activity_override AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_activity_override AS k ON e.learner_activity_override_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learner_activity_override_id AS learner_activity_override_id
  , from_cs.course_section_id AS course_section_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.learner_group_id AS learner_group_id
  , from_cs.quiz_id AS quiz_id

  /*
  Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title
  , from_cs.learner_activity_version AS learner_activity_version
  , from_cs.quiz_version AS quiz_version

  /*
  All day type
  */
  -- https://docs.udp.unizin.org/tables/ref_all_day_type.html
  , from_cs.all_day_type AS all_day_type
  , CASE from_cs.all_day_type WHEN 'NewAllDay' THEN 1 ELSE NULL END AS all_day_type_new_all_day
  , CASE from_cs.all_day_type WHEN 'NoData' THEN 1 ELSE NULL END AS all_day_type_no_data
  , CASE from_cs.all_day_type WHEN 'SameAllDay' THEN 1 ELSE NULL END AS all_day_type_same_all_day

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Override type
  */
  -- https://docs.udp.unizin.org/tables/ref_override_type.html
  , from_cs.type AS type
  , CASE from_cs.type WHEN 'AdHoc' THEN 1 ELSE NULL END AS type_ad_hoc
  , CASE from_cs.type WHEN 'CourseOffering' THEN 1 ELSE NULL END AS type_course_offering
  , CASE from_cs.type WHEN 'CourseSection' THEN 1 ELSE NULL END AS type_course_section
  , CASE from_cs.type WHEN 'LearnerGroup' THEN 1 ELSE NULL END AS type_learner_group
  , CASE from_cs.type WHEN 'NoData' THEN 1 ELSE NULL END AS type_no_data

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

  , from_cs.all_day_date AS all_day_date

  , from_cs.due_date AS due_date
  , from_cs.unlocked_date AS unlocked_date
  , from_cs.locked_date AS locked_date

  , from_cs.overridden_due_date AS overridden_due_date
  , from_cs.overridden_unlocked_date AS overridden_unlocked_date
  , from_cs.overridden_locked_date AS overridden_locked_date

FROM from_cs
;
