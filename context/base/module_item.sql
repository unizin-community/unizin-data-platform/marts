/*
  Context / Base / Module item

    Imports Module item data from the context store.

  Properties:

    Export table:  mart_helper.context__module_item
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (

  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.module_item AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.module_item AS k ON e.module_item_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.module_item_id AS module_item_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.discussion_id AS discussion_id
  , from_cs.file_id AS file_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.module_id AS module_id
  , from_cs.person_id AS person_id
  , from_cs.quiz_id AS quiz_id
  , from_cs.wiki_page_id AS wiki_page_id

  /*
    Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title

  , from_cs.position AS position
  , from_cs.min_score AS min_score

  , from_cs.url AS url

  /*
  Completion type
  */
  -- No option set.
  , from_cs.completion_type AS completion_type
  , CASE from_cs.completion_type WHEN 'must_view' THEN 1 ELSE NULL END AS is_completion_type_must_view
  , CASE from_cs.completion_type WHEN 'must_submit' THEN 1 ELSE NULL END AS is_completion_type_must_submit
  , CASE from_cs.completion_type WHEN 'must_contribute' THEN 1 ELSE NULL END AS is_completion_type_must_contribute
  , CASE from_cs.completion_type WHEN 'must_mark_done' THEN 1 ELSE NULL END AS is_completion_type_must_mark_done
  , CASE from_cs.completion_type WHEN 'min_score' THEN 1 ELSE NULL END AS is_completion_type_min_score

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status
  , CASE from_cs.status WHEN 'active' THEN 1 ELSE NULL END AS is_status_active
  , CASE from_cs.status WHEN 'deleted' THEN 1 ELSE NULL END AS is_status_deleted
  , CASE from_cs.status WHEN 'unpublished' THEN 1 ELSE NULL END AS is_status_unpublished

  /*
  Type
  */
  -- https://docs.udp.unizin.org/tables/ref_module_item_type.html
  , from_cs.type AS type
  , CASE from_cs.type WHEN 'ExternalUrl' THEN 1 ELSE NULL END AS is_type_external_url
  , CASE from_cs.type WHEN 'Assignment' THEN 1 ELSE NULL END AS is_type_learner_activity
  , CASE from_cs.type WHEN 'ContextModuleSubHeader' THEN 1 ELSE NULL END AS is_type_module_subheader
  , CASE from_cs.type WHEN 'ContextExternalTool' THEN 1 ELSE NULL END AS is_type_external_tool
  , CASE from_cs.type WHEN 'Attachment' THEN 1 ELSE NULL END AS is_type_file
  , CASE from_cs.type WHEN 'WikiPage' THEN 1 ELSE NULL END AS is_type_wiki_page
  , CASE from_cs.type WHEN 'Quiz' THEN 1 ELSE NULL END AS is_type_quiz
  , CASE from_cs.type WHEN 'DiscussionTopic' THEN 1 ELSE NULL END AS is_type_discussion

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
