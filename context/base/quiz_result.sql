/*
  Context / Base / Quiz result

    Imports Quiz result data from the context store.

  Properties:

    Export table:  mart_helper.context__quiz_result
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.quiz_result AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.quiz_result AS k ON e.quiz_result_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.quiz_result_id AS quiz_result_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.learner_activity_result_id AS learner_activity_result_id
  , from_cs.person_id AS person_id
  , from_cs.quiz_id AS quiz_id

  /*
    Attributes
  */
  , from_cs.submission_source AS submission_source
  , from_cs.total_attempts AS total_attempts
  , from_cs.extra_attempts AS extra_attempts
  , from_cs.time_taken AS time_taken
  , from_cs.extra_time AS extra_time

  , CASE from_cs.is_student_viewed WHEN TRUE THEN 1 ELSE NULL END AS is_student_viewed

  /*
    Grades
  */
  , from_cs.points_possible AS points_possible
  , from_cs.score AS score
  , from_cs.kept_score AS kept_score
  , from_cs.fudge_points AS fudge_points
  , from_cs.score_before_regrade AS score_before_regrade

  /*
  State in submission
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.state_in_submission AS state_in_submission
  , CASE from_cs.state_in_submission WHEN 'never_locked' THEN 1 ELSE NULL END AS is_state_in_submission_never_locked
  , CASE from_cs.state_in_submission WHEN 'pending_review' THEN 1 ELSE NULL END AS is_state_in_submission_pending_review

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status
  , CASE from_cs.status WHEN 'complete' THEN 1 ELSE NULL END AS is_status_complete
  , CASE from_cs.status WHEN 'pending_review' THEN 1 ELSE NULL END AS is_status_pending_review
  , CASE from_cs.status WHEN 'untaken' THEN 1 ELSE NULL END AS is_status_untaken
  , CASE from_cs.status WHEN 'settings_only' THEN 1 ELSE NULL END AS is_status_settings_only

  /*
  Submission scoring policy
  */
  -- https://docs.udp.unizin.org/tables/ref_assessment_scoring_policy.html
  , from_cs.submission_scoring_policy AS submission_scoring_policy
  , CASE from_cs.submission_scoring_policy WHEN 'keep_highest' THEN 1 ELSE NULL END AS is_submission_scoring_policy_keep_highest
  , CASE from_cs.submission_scoring_policy WHEN 'keep_average' THEN 1 ELSE NULL END AS is_submission_scoring_policy_keep_average
  , CASE from_cs.submission_scoring_policy WHEN 'keep_latest' THEN 1 ELSE NULL END AS is_submission_scoring_policy_keep_latest
  , CASE from_cs.submission_scoring_policy WHEN 'manually_overridden' THEN 1 ELSE NULL END AS is_submission_scoring_policy_manually_overridden

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.started_date AS started_date
  , from_cs.finished_date AS finished_date
  , from_cs.due_date AS due_date
  , from_cs.result_date AS result_date

FROM from_cs
;
