/*
  Context / Base / Grading period grade

    Imports Grading period grade data from the context store.

  Properties:

    Export table:  mart_helper.context__grading_period_grade
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.grading_period_grade AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.grading_period_grade AS k ON e.grading_period_grade_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.grading_period_grade_id AS grading_period_grade_id
  , from_cs.course_section_id AS course_section_id
  , from_cs.grading_period_group_id AS grading_period_group_id
  , from_cs.grading_period_id AS grading_period_id
  , from_cs.person_id AS person_id

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Scores
  */
  , from_cs.le_current_score AS le_current_score
  , from_cs.le_hidden_current_score AS le_hidden_current_score
  , from_cs.le_final_score AS le_final_score
  , from_cs.le_hidden_final_score AS le_hidden_final_score

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
