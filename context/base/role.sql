/*
  Context / Base / Role

    Imports Role data from the context store.

  Properties:

    Export table:  mart_helper.context__role
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.role AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.role AS k ON e.role_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.role_id AS role_id

  /*
  Attributes
  */
  , from_cs.name AS name

  /*
  Role
  */
  -- https://docs.udp.unizin.org/tables/ref_role.html
  , from_cs.base_role_type
  , CASE from_cs.base_role_type WHEN 'Designer' THEN 1 ELSE NULL END AS is_base_role_type_designer
  , CASE from_cs.base_role_type WHEN 'Faculty' THEN 1 ELSE NULL END AS is_base_role_type_faculty
  , CASE from_cs.base_role_type WHEN 'Generic' THEN 1 ELSE NULL END AS is_base_role_type_generic
  , CASE from_cs.base_role_type WHEN 'Observer' THEN 1 ELSE NULL END AS is_base_role_type_observer
  , CASE from_cs.base_role_type WHEN 'Staff' THEN 1 ELSE NULL END AS is_base_role_type_staff
  , CASE from_cs.base_role_type WHEN 'Student' THEN 1 ELSE NULL END AS is_base_role_type_student
  , CASE from_cs.base_role_type WHEN 'Teacher' THEN 1 ELSE NULL END AS is_base_role_type_teacher
  , CASE from_cs.base_role_type WHEN 'TeachingAssistant' THEN 1 ELSE NULL END AS is_base_role_type_teaching_assistant
  , CASE from_cs.base_role_type WHEN 'NoData' THEN 1 ELSE NULL END AS is_base_role_type_no_data
  , CASE from_cs.base_role_type WHEN NULL THEN 1 ELSE NULL END AS is_base_role_type_none

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.deleted_date AS deleted_date


FROM from_cs

;
