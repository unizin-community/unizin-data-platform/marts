/*
  Context / Base / Quiz item group

    Imports Quiz item group data from the context store.

  Properties:

    Export table:  mart_helper.context__quiz_item_group
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.quiz_item_group AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.quiz_item_group AS k ON e.quiz_item_group_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.quiz_item_group_id AS quiz_item_group_id
  , from_cs.quiz_id AS quiz_id

  /*
  Attributes
  */
  , from_cs.position AS position
  , from_cs.name AS name
  , LENGTH(from_cs.name) AS length_name

  , from_cs.pick_count AS pick_count
  , from_cs.quiz_item_points AS quiz_item_points

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
