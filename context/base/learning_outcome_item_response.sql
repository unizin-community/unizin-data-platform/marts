/*
  Context / Base / Learning outcome item response

    Imports Learning outcome item response data from the context store.

  Properties:

    Export table:  mart_helper.context__learning_outcome_item_response
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learning_outcome_item_response AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learning_outcome_item_response AS k ON e.learning_outcome_item_response_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learning_outcome_item_response_id AS learning_outcome_item_response_id
  , from_cs.academic_term_id AS academic_term_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.learning_outcome_id AS learning_outcome_id
  , from_cs.person_id AS person_id
  , from_cs.quiz_id AS quiz_id
  , from_cs.quiz_item_id AS quiz_item_id

  /*
  Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title

  , from_cs.number_attempts AS number_attempts

  /*
  Scores
  */
  , from_cs.points_possible AS points_possible
  , from_cs.original_score AS original_score
  , from_cs.score AS score
  , from_cs.percent AS percent

  /*
  Mastery
  */
  , CASE from_cs.has_mastery WHEN TRUE THEN 1 ELSE NULL END AS has_mastery
  , CASE from_cs.has_original_mastery WHEN TRUE THEN 1 ELSE NULL END AS has_original_mastery

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.submitted_date AS submitted_date
  , from_cs.assessed_date AS assessed_date

FROM from_cs
;
