/*
  Context / Base / Academic minor

    Imports Academic minor data from the context store.

  Properties:

    Export table:  mart_helper.context__academic_minor
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.academic_minor AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_minor AS k ON e.academic_minor_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.academic_minor_id AS academic_minor_id
  , from_cs.academic_program_id AS academic_program_id

  /*
  Attributes
  */
  , from_cs.cip_code2k AS cip_code2k
  , from_cs.code AS code
  , from_cs.description AS description

FROM from_cs
;
