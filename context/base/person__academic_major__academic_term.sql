/*
  Context / Base / Person - Academic major - Academic term

    Imports Person - Academic major - Academic term data from the context store.

  Properties:

    Export table:  mart_helper.context__person__academic_major__academic_term
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    kp.sis_ext_id AS sis_person_id
    , kam.sis_ext_id as sis_academic_major_id
    , kat.sis_ext_id as sis_academic_term_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.person__academic_major__academic_term AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.person AS kp ON e.person_id=kp.id
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_major AS kam ON e.academic_major_id=kam.id
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_term AS kat ON e.academic_term_id=kat.id
)

SELECT
  from_cs.sis_person_id AS sis_person_id
  , from_cs.sis_academic_major_id AS sis_academic_major_id
  , from_cs.sis_academic_term_id AS sis_academic_term_id
  , from_cs.academic_major_id AS academic_major_id
  , from_cs.academic_term_id AS academic_term_id
  , from_cs.person_id AS person_id

FROM from_cs
;
