/*
  Context / Base / Grading period group

    Imports Grading period group data from the context store.

  Properties:

    Export table:  mart_helper.context__grading_period_group
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.grading_period_group AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.grading_period_group AS k ON e.grading_period_group_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.grading_period_group_id AS grading_period_group_id
  , from_cs.course_offering_id AS course_offering_id

  /*
  Attributes
  */
  , from_cs.title AS title

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
