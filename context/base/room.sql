/*
  Context / Base / Room

    Imports Room data from the context store.

  Properties:

    Export table:  mart_helper.context__room
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.room AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.room AS k ON e.room_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.room_id AS room_id
  , from_cs.facility_id AS facility_id

  /*
  Attributes
  */
  , from_cs.code AS code
  , from_cs.max_occupancy AS max_occupancy
  , from_cs.number AS number

  /*
  Room type
  */
  -- https://docs.udp.unizin.org/tables/ref_room_type.html
  , from_cs.ref_type_id AS ref_type_id
  , CASE from_cs.ref_type_id WHEN 'Assembly' THEN 1 ELSE NULL END AS ref_type_id_assembly
  , CASE from_cs.ref_type_id WHEN 'Auditorium' THEN 1 ELSE NULL END AS ref_type_id_auditorium
  , CASE from_cs.ref_type_id WHEN 'Classroom' THEN 1 ELSE NULL END AS ref_type_id_classroom
  , CASE from_cs.ref_type_id WHEN 'ConferenceRoom' THEN 1 ELSE NULL END AS ref_type_id_conference_room
  , CASE from_cs.ref_type_id WHEN 'DanceStudio' THEN 1 ELSE NULL END AS ref_type_id_dance_studio
  , CASE from_cs.ref_type_id WHEN 'DemoSchool' THEN 1 ELSE NULL END AS ref_type_id_demo_school
  , CASE from_cs.ref_type_id WHEN 'DistEdClassroom' THEN 1 ELSE NULL END AS ref_type_id_dist_ed_classroom
  , CASE from_cs.ref_type_id WHEN 'Greenhouse' THEN 1 ELSE NULL END AS ref_type_id_greenhouse
  , CASE from_cs.ref_type_id WHEN 'Gymnasium' THEN 1 ELSE NULL END AS ref_type_id_gymnasium
  , CASE from_cs.ref_type_id WHEN 'Laboratory' THEN 1 ELSE NULL END AS ref_type_id_laboratory
  , CASE from_cs.ref_type_id WHEN 'LectureHall' THEN 1 ELSE NULL END AS ref_type_id_lecture_hall
  , CASE from_cs.ref_type_id WHEN 'Museum' THEN 1 ELSE NULL END AS ref_type_id_museum
  , CASE from_cs.ref_type_id WHEN 'NoData' THEN 1 ELSE NULL END AS ref_type_id_no_data
  , CASE from_cs.ref_type_id WHEN 'NonClassPublic' THEN 1 ELSE NULL END AS ref_type_id_non_class_public
  , CASE from_cs.ref_type_id WHEN 'Other' THEN 1 ELSE NULL END AS ref_type_id_other
  , CASE from_cs.ref_type_id WHEN 'OutpatientClinic' THEN 1 ELSE NULL END AS ref_type_id_outpatient_clinic
  , CASE from_cs.ref_type_id WHEN 'Recreation' THEN 1 ELSE NULL END AS ref_type_id_recreation
  , CASE from_cs.ref_type_id WHEN 'ResearchLab' THEN 1 ELSE NULL END AS ref_type_id_research_lab
  , CASE from_cs.ref_type_id WHEN 'ResearchSite' THEN 1 ELSE NULL END AS ref_type_id_research_site
  , CASE from_cs.ref_type_id WHEN 'SeminarRoom' THEN 1 ELSE NULL END AS ref_type_id_seminar_room
  , CASE from_cs.ref_type_id WHEN 'SportsRoom' THEN 1 ELSE NULL END AS ref_type_id_sports_room
  , CASE from_cs.ref_type_id WHEN 'StudyRoom' THEN 1 ELSE NULL END AS ref_type_id_study_room
  , CASE from_cs.ref_type_id WHEN 'TeachingClinic' THEN 1 ELSE NULL END AS ref_type_id_teaching_clinic

FROM from_cs
;
