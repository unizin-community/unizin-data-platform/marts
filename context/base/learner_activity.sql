/*
  Context / Base / Learner activity

    Imports Learner activity data from the context store.

  Properties:

    Export table:  mart_helper.context__learner_activity
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Academic session
    * Course offering

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM
    _READ_FROM_PROJECT_.context_store_entity.learner_activity AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_activity AS k ON e.learner_activity_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learner_activity_id AS learner_activity_id
  , COALESCE(ss.academic_term_id, act.academic_term_id) AS academic_term_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.learner_activity_group_id AS learner_activity_group_id

  /*
  Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title
  , from_cs.description AS description
  , LENGTH(from_cs.description) AS length_description

  , from_cs.position AS position
  , from_cs.points_possible AS points_possible

  , CASE WHEN from_cs.is_hidden IS TRUE THEN 1 ELSE 0 END AS is_hidden
  , CASE WHEN from_cs.is_lockable IS TRUE THEN 1 ELSE 0 END AS is_lockable
  , CASE WHEN from_cs.is_all_day IS TRUE THEN 1 ELSE 0 END AS is_all_day

  , from_cs.allowable_submission_types AS allowable_submission_types
  , from_cs.drop_rule AS drop_rule

  /*
  Grade type
  */
  -- https://docs.udp.unizin.org/tables/ref_score_metric_type.html
  , from_cs.grade_type AS grade_type
  , CASE WHEN from_cs.grade_type = 'gpa_scale' THEN 1 ELSE 0 END AS is_grade_type_gpa_scale
  , CASE WHEN from_cs.grade_type = 'letter_grade' THEN 1 ELSE 0 END AS is_grade_type_letter_grade
  , CASE WHEN from_cs.grade_type = 'pass_fail' THEN 1 ELSE 0 END AS is_grade_type_pass_fail
  , CASE WHEN from_cs.grade_type = 'points' THEN 1 ELSE 0 END AS is_grade_type_points
  , CASE WHEN from_cs.grade_type = 'not_graded' THEN 1 ELSE 0 END AS is_grade_type_not_graded
  , CASE WHEN from_cs.grade_type = 'percentage' THEN 1 ELSE 0 END AS is_grade_type_percentage

  /*
  Peer reviews
  */
  , CASE WHEN from_cs.has_peer_reviews IS TRUE THEN 1 ELSE 0 END AS has_peer_reviews
  , CASE WHEN from_cs.has_peer_reviews_assigned IS TRUE THEN 1 ELSE 0 END AS has_peer_reviews_assigned
  , CASE WHEN from_cs.is_anonymous_peer_reviews IS TRUE THEN 1 ELSE 0 END AS is_anonymous_peer_reviews
  , CASE WHEN from_cs.is_automatic_peer_reviews IS TRUE THEN 1 ELSE 0 END AS is_automatic_peer_reviews
  , CASE WHEN from_cs.is_grade_group_students_individually IS TRUE THEN 1 ELSE 0 END AS is_grade_group_students_individually
  , from_cs.peer_review_count AS peer_review_count

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status
  , CASE WHEN from_cs.status = 'failed_to_duplicate' THEN 1 ELSE 0 END AS is_status_failed_to_duplicate
  , CASE WHEN from_cs.status = 'published' THEN 1 ELSE 0 END AS is_status_published
  , CASE WHEN from_cs.status = 'unpublished' THEN 1 ELSE 0 END AS is_status_unpublished
  , CASE WHEN from_cs.status = 'deleted' THEN 1 ELSE 0 END AS is_status_deleted

  /*
  Visibility
  */
  -- https://docs.udp.unizin.org/tables/ref_visibility_type.html
  , from_cs.visibility AS visibility
  , CASE WHEN from_cs.visibility = 'everyone' THEN 1 ELSE 0 END AS is_visibility_everyone
  , CASE WHEN from_cs.visibility = 'only_visible_to_overrides' THEN 1 ELSE 0 END AS is_visibility_only_visible_to_overrides

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.unlocked_date AS unlocked_date
  , from_cs.due_date AS due_date
  , from_cs.locked_date AS locked_date
  , from_cs.all_day_date AS all_day_date
  , from_cs.peer_reviews_due_date AS peer_reviews_due_date

  /*
  Temporal metrics about the learner activity.
  */
  , DATE_DIFF(CAST(from_cs.updated_date AS DATE), CAST(from_cs.created_date AS DATE), DAY) AS days_created_to_last_update
  , DATE_DIFF(CAST(from_cs.unlocked_date AS DATE), CAST(from_cs.created_date AS DATE), DAY) AS days_created_to_unlocked
  , DATE_DIFF(CAST(from_cs.locked_date AS DATE), CAST(from_cs.created_date AS DATE), DAY) AS days_created_to_locked
  , DATE_DIFF(CAST(from_cs.due_date AS DATE), CAST(from_cs.created_date AS DATE), DAY) AS days_created_to_due
  , DATE_DIFF(CAST(from_cs.due_date AS DATE), CAST(from_cs.updated_date AS DATE), DAY) AS days_last_update_to_due
  , DATE_DIFF(CAST(from_cs.locked_date AS DATE), CAST(from_cs.unlocked_date AS DATE), DAY) AS days_unlocked_to_locked

  , COALESCE(from_cs.unlocked_date, from_cs.created_date) AS learner_activity_start_date
  , COALESCE(from_cs.due_date, from_cs.locked_date) AS learner_activity_end_date
  , DATE_DIFF(
  COALESCE(CAST(from_cs.due_date AS DATE), CAST(from_cs.locked_date AS DATE)),
  COALESCE(CAST(from_cs.unlocked_date AS DATE), CAST(from_cs.created_date AS DATE)),
  DAY
  ) AS days_start_to_end

FROM from_cs

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering co ON from_cs.course_offering_id=co.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session ss ON co.academic_session_id=ss.academic_session_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term act on act.academic_term_id = co.academic_term_id

WHERE
  learner_activity_id IS NOT NULL
;
