/*
  Context / Base / Academic organization

    Imports Academic organization data from the context store.

  Properties:

    Export table:  mart_helper.context__academic_organization
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.academic_organization AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_organization AS k ON e.academic_organization_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.academic_organization_id AS academic_organization_id
  , from_cs.campus_id AS campus_id
  , from_cs.parent_academic_organization_id AS parent_academic_organization_id

  /*
  Attributes
  */
  , from_cs.institutional_code AS institutional_code
  , from_cs.name AS name

FROM from_cs
;
