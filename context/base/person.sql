/*
  Context / Base / Person

    Imports Person data from the context store.

  Properties:

    Export table:  mart_helper.context__person
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  -- define the number of email addresses associated with each person ID
  WITH person_email_count AS (
    SELECT person_id,count(*) as num_emails
    FROM _READ_FROM_PROJECT_.context_store_entity.person_email
    WHERE person_id IS NOT NULL
    GROUP BY person_id
  ),

  -- define the rows in person_email to keep 
  person_email_keep AS (
    SELECT 
      DISTINCT
      pe.person_id
      , pe.email_address
      , pe.is_primary
      , pec.num_emails
      -- keep row if the person only has one email address, or if they have more than one and the email address is the primary email address
      , CASE 
        WHEN pec.num_emails > 1 AND is_primary IS TRUE 
          THEN 1
        WHEN pec.num_emails > 1 AND is_primary IS FALSE 
          THEN 0 
        ELSE 1
        END AS keep_email 
    FROM _READ_FROM_PROJECT_.context_store_entity.person_email pe 
    LEFT JOIN person_email_count pec ON pe.person_id = pec.person_id 
  ),

  -- only include rows in person_email with keep_email = 1
  person_email AS (
    SELECT *
    FROM person_email_keep
    WHERE keep_email = 1 
  )

  SELECT
    row_number() over() AS row_number
    , k.sis_ext_id AS sis_id
    , k.lms_ext_id AS lms_id
    , e.*
    ,pe.email_address
  FROM _READ_FROM_PROJECT_.context_store_entity.person AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.person AS k ON e.person_id=k.id
  LEFT JOIN person_email as pe
    on pe.person_id = e.person_id
),

/*
  Randomly sample a table of US names.
*/
first_name AS (
  SELECT
    ROW_NUMBER() OVER() AS row_number
    , names.name AS name
  FROM
  `bigquery-public-data.usa_names.usa_1910_current` AS names
  WHERE
    RAND() < 0.8
),

middle_name AS (
  SELECT
    ROW_NUMBER() OVER() AS row_number
    , names.name AS name
  FROM
  `bigquery-public-data.usa_names.usa_1910_current` AS names
  WHERE
    RAND() < 0.8
),

last_name AS (
  SELECT
    ROW_NUMBER() OVER() AS row_number
    , names.name AS name
  FROM
  `bigquery-public-data.usa_names.usa_1910_current` AS names
  WHERE
    RAND() < 0.8
),

/*
  Create fake identities.
*/
compiled_name AS (
  SELECT
    ROW_NUMBER() OVER() AS row_number
    , f.name AS first_name
    , f.name AS preferred_first_name
    , m.name AS middle_name
    , l.name AS last_name
    , CONCAT(LOWER(f.name), '.', LOWER(SUBSTR(m.name, 1, 1)), '.', LOWER(l.name), '@university.edu') AS fed_identity
  FROM
    first_name AS f
  INNER JOIN middle_name AS m USING (row_number)
  INNER JOIN last_name AS l USING (row_number)
)

SELECT
  /*
    Masking SIS ID, since it can contain PII data. To use the
    originaly value, remove the call to MD5 (hash function).
  */
  from_cs.sis_id AS sis_id

  /*
    Other identifies; don't change.
  */
  , from_cs.lms_id AS lms_id
  , from_cs.person_id AS person_id

  /*
  Identity

    Anonynmized by default. To use real names,
    change the table alias in the rows below
    from `cn` to `from_cs`.
  */
  , from_cs.first_name AS first_name
  , from_cs.preferred_first_name AS preferred_first_name
  , from_cs.middle_name AS middle_name
  , from_cs.last_name AS last_name
  , from_cs.name AS name
  , from_cs.suffix AS suffix
  , from_cs.fed_identity AS fed_identity
  , from_cs.email_address as email_address

  /*
  Citizenship
  */
  -- https://docs.udp.unizin.org/tables/ref_country.html
  -- Too many to list
  , from_cs.citizenship_one AS citizenship_one
  , from_cs.citizenship_two AS citizenship_two
  , from_cs.citizenship_three AS citizenship_three

  /*
  Residence
  */
  -- https://docs.udp.unizin.org/tables/ref_country.html
  -- Too many to list
  , from_cs.country_first_foreign_permanent_residence AS country_first_foreign_permanent_residence

  /*
  Parent education
  */
  -- https://docs.udp.unizin.org/tables/ref_education_level.html
  -- Too many to list
  , from_cs.edu_level_maternal AS edu_level_maternal
  , from_cs.edu_level_paternal AS edu_level_paternal
  , from_cs.edu_level_parental AS edu_level_parental

  /*
  High school
  */
  , from_cs.hs_city_name AS hs_city_name
  , from_cs.hs_state_code AS hs_state_code
  , from_cs.hs_zip_code AS hs_zip_code
  , from_cs.hs_cb_cluster_code AS hs_cb_cluster_code
  , from_cs.hs_ceeb_code AS hs_ceeb_code

  , from_cs.hs_gpa AS hs_gpa
  , from_cs.hs_graduating_month AS hs_graduating_month
  , from_cs.hs_graduating_year AS hs_graduating_year
  , from_cs.hs_percentile_rank AS hs_percentile_rank

  /*
  Demograpics
  */
  -- https://docs.udp.unizin.org/tables/ref_sex.html
  , from_cs.sex
  , CASE from_cs.sex WHEN 'NoData' THEN 1 ELSE 0 END AS is_sex_no_data
  , CASE from_cs.sex WHEN 'Female' THEN 1 ELSE 0 END AS is_sex_female
  , CASE from_cs.sex WHEN 'Male' THEN 1 ELSE 0 END AS is_sex_male
  , CASE from_cs.sex WHEN 'NotSelected' THEN 1 ELSE 0 END AS is_sex_not_selected
  , CASE from_cs.sex WHEN NULL THEN 1 ELSE 0 END AS is_sex_none

  /*
  Gender
  */
  -- https://docs.udp.unizin.org/tables/ref_gender.html
  , from_cs.gender AS gender
  , from_cs.gender_pronoun AS gender_pronoun

  /*
  Race
  */
  , CASE from_cs.is_american_indian_alaska_native WHEN TRUE THEN 1 ELSE NULL END AS is_american_indian_alaska_native
  , CASE from_cs.is_asian WHEN TRUE THEN 1 ELSE NULL END AS is_asian
  , CASE from_cs.is_black_african_american WHEN TRUE THEN 1 ELSE NULL END AS is_black_african_american
  , CASE from_cs.is_hawaiian_pacific_islander WHEN TRUE THEN 1 ELSE NULL END AS is_hawaiian_pacific_islander
  , CASE from_cs.is_hispanic_latino WHEN TRUE THEN 1 ELSE NULL END AS is_hispanic_latino
  , CASE from_cs.is_no_race_indicated WHEN TRUE THEN 1 ELSE NULL END AS is_no_race_indicated
  , CASE from_cs.is_other_american WHEN TRUE THEN 1 ELSE NULL END AS is_other_american
  , CASE from_cs.is_white WHEN TRUE THEN 1 ELSE NULL END AS is_white

  /*
  English
  */
  , CASE from_cs.is_english_first_language WHEN TRUE THEN 1 ELSE NULL END AS is_english_first_language
  , CASE from_cs.is_first_generation_hed_student WHEN TRUE THEN 1 ELSE NULL END AS is_first_generation_hed_student

  /*
  Socio-economic
  */
  , from_cs.zip_code_at_time_of_application AS zip_code_at_time_of_application
  , from_cs.zip_code_first_us_permanent_residence AS zip_code_first_us_permanent_residence
  , from_cs.neighborhood_cb_cluster_code_at_time_of_application AS neighborhood_cb_cluster_code_at_time_of_application
  , from_cs.parent_dependents_count AS parent_dependents_count
  , from_cs.parent_is_single_parent AS parent_is_single_parent

  /*
  Family income
  */
  -- https://docs.udp.unizin.org/tables/ref_estimated_gross_family_income.html
  -- Too many to list
  , from_cs.estimated_gross_family_income AS estimated_gross_family_income

  /*
  Last undergraduate school
  */
  , from_cs.last_undergraduate_school AS last_undergraduate_school
  , from_cs.last_undergraduate_school_ceeb_code AS last_undergraduate_school_ceeb_code
  , from_cs.last_undergraduate_school_end_date AS last_undergraduate_school_end_date
  , from_cs.last_undergraduate_school_institution_code AS last_undergraduate_school_institution_code

  /*
  Last school
  */
  -- https://docs.udp.unizin.org/tables/ref_school_type.html
  , from_cs.last_undergraduate_school_type AS last_undergraduate_school_type
  , CASE from_cs.last_undergraduate_school_type WHEN '2YearCollege' THEN 1 ELSE NULL END AS last_undergraduate_school_type_2_year_college
  , CASE from_cs.last_undergraduate_school_type WHEN '4YearCollege' THEN 1 ELSE NULL END AS last_undergraduate_school_type_4_year_college
  , CASE from_cs.last_undergraduate_school_type WHEN 'Affiliate' THEN 1 ELSE NULL END AS last_undergraduate_school_type_affiliate
  , CASE from_cs.last_undergraduate_school_type WHEN 'CommunityCollege' THEN 1 ELSE NULL END AS last_undergraduate_school_type_community_college
  , CASE from_cs.last_undergraduate_school_type WHEN 'Elementary' THEN 1 ELSE NULL END AS last_undergraduate_school_type_elementary
  , CASE from_cs.last_undergraduate_school_type WHEN 'MiddleSchool' THEN 1 ELSE NULL END AS last_undergraduate_school_type_middle_school
  , CASE from_cs.last_undergraduate_school_type WHEN 'NoData' THEN 1 ELSE NULL END AS last_undergraduate_school_type_no_data
  , CASE from_cs.last_undergraduate_school_type WHEN 'Other' THEN 1 ELSE NULL END AS last_undergraduate_school_type_other
  , CASE from_cs.last_undergraduate_school_type WHEN 'Secondary' THEN 1 ELSE NULL END AS last_undergraduate_school_type_secondary
  , CASE from_cs.last_undergraduate_school_type WHEN 'University' THEN 1 ELSE NULL END AS last_undergraduate_school_type_university
  , CASE from_cs.last_undergraduate_school_type WHEN 'Unknown' THEN 1 ELSE NULL END AS last_undergraduate_school_type_unknown
  , CASE from_cs.last_undergraduate_school_type WHEN 'Vocational' THEN 1 ELSE NULL END AS last_undergraduate_school_type_vocational

  /*
  Military
  */
  , IF(from_cs.was_veteran_at_admissions, 1, 0) AS was_veteran_at_admissions

FROM from_cs

INNER JOIN compiled_name AS cn USING (row_number)
;
