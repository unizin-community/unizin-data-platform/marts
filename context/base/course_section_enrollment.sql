/*
  Context / Base / Course section enrollment

    Imports Course section enrollment data from the context store.

  Properties:

    Export table:  mart_helper.context__course_section_enrollment
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Academic term
    * Academic session
    * Course offering
    * Course section
    * Person

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    kp.sis_ext_id AS sis_person_id
    , kp.lms_ext_id AS lms_person_id
    , kcs.sis_ext_id AS sis_course_section_id
    , kcs.lms_ext_id AS lms_course_section_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.course_section_enrollment AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.person AS kp ON e.person_id=kp.id
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.course_section AS kcs ON e.course_section_id=kcs.id
)

SELECT
  from_cs.sis_person_id AS sis_person_id
  , from_cs.lms_person_id AS lms_person_id
  , from_cs.sis_course_section_id AS sis_course_section_id
  , from_cs.lms_course_section_id AS lms_course_section_id
  , academic_term.academic_term_id AS academic_term_id
  , ss.academic_session_id AS academic_session_id
  , co.course_offering_id AS course_offering_id
  , cs.le_current_course_offering_id AS le_current_course_offering_id
  , cs.course_section_id AS course_section_id
  ,	from_cs.person_id AS person_id

  /* Term metadata */
  , academic_term.term_type AS term_type
  , academic_term.term_year AS term_year

  /*
    IU and PeopleSoft specific variables,
    but acceptable for now.
  */
  , RIGHT(
      REGEXP_EXTRACT(sis_course_section_id, '-[0-9]*$'),
      LENGTH(REGEXP_EXTRACT(sis_course_section_id, '-[0-9]*$')) - 1
    ) AS sis_course_section_code
  , CASE
      WHEN term_type = 'Fall' THEN CONCAT('4', RIGHT(CAST(term_year AS STRING),2), '8')
      WHEN term_type = 'Spring' THEN CONCAT('4', RIGHT(CAST(term_year AS STRING),2), '2')
      WHEN term_type = 'Winter' THEN CONCAT('4', RIGHT(CAST(term_year AS STRING),2), '9')
      WHEN term_type = 'Summer' THEN CONCAT('4', RIGHT(CAST(term_year AS STRING),2), '5')
    ELSE
      'Unknown'
    END AS sis_term_code

  /*
  Create dummy variables to identify students with related kinds
  of role status values over a conceptually identical lifecycle.
  */
  , CASE
    	WHEN ( from_cs.role = 'Student' )
    	THEN 1
    	ELSE 0
    END AS is_student

  , CASE
    	WHEN ( from_cs.role = 'Student' AND (from_cs.role_status = 'Registered' OR from_cs.role_status = 'Enrolled' OR from_cs.role_status = 'Completed'))
    	THEN 1
    	ELSE 0
    END AS is_student_registered_or_enrolled_or_completed

  , CASE
    	WHEN ( from_cs.role = 'Student' AND (from_cs.role_status = 'Dropped' OR from_cs.role_status = 'Withdrawn'))
    	THEN 1
    	ELSE 0
    END AS is_student_dropped_or_withdrawn

  /*
  Role
  */
  -- https://docs.udp.unizin.org/tables/ref_role.html
  , from_cs.role
  , CASE from_cs.role WHEN 'Designer' THEN 1 ELSE NULL END AS is_role_designer
  , CASE from_cs.role WHEN 'Faculty' THEN 1 ELSE NULL END AS is_role_faculty
  , CASE from_cs.role WHEN 'Generic' THEN 1 ELSE NULL END AS is_role_generic
  , CASE from_cs.role WHEN 'Observer' THEN 1 ELSE NULL END AS is_role_observer
  , CASE from_cs.role WHEN 'Staff' THEN 1 ELSE NULL END AS is_role_staff
  , CASE from_cs.role WHEN 'Student' THEN 1 ELSE NULL END AS is_role_student
  , CASE from_cs.role WHEN 'Teacher' THEN 1 ELSE NULL END AS is_role_teacher
  , CASE from_cs.role WHEN 'TeachingAssistant' THEN 1 ELSE NULL END AS is_role_teaching_assistant
  , CASE from_cs.role WHEN 'NoData' THEN 1 ELSE NULL END AS is_role_no_data
  , CASE from_cs.role WHEN NULL THEN 1 ELSE NULL END AS is_role_none

  /*
  Role status
  */
  -- https://docs.udp.unizin.org/tables/ref_role_status.html
  , from_cs.role_status
  , CASE from_cs.role_status WHEN 'Assigned' THEN 1 ELSE NULL END AS is_role_status_assigned
  , CASE from_cs.role_status WHEN 'Completed' THEN 1 ELSE NULL END AS is_role_status_completed
  , CASE from_cs.role_status WHEN 'Dropped' THEN 1 ELSE NULL END AS is_role_status_dropped
  , CASE from_cs.role_status WHEN 'Enrolled' THEN 1 ELSE NULL END AS is_role_status_enrolled
  , CASE from_cs.role_status WHEN 'Not-enrolled' THEN 1 ELSE NULL END AS is_role_status_not_enrolled
  , CASE from_cs.role_status WHEN 'Pre-registered' THEN 1 ELSE NULL END AS is_role_status_pre_registered
  , CASE from_cs.role_status WHEN 'Registered' THEN 1 ELSE NULL END AS is_role_status_registered
  , CASE from_cs.role_status WHEN 'WaitListed' THEN 1 ELSE NULL END AS is_role_status_wait_listed
  , CASE from_cs.role_status WHEN 'Withdrawn' THEN 1 ELSE NULL END AS is_role_status_withdrawn
  , CASE from_cs.role_status WHEN 'NoData' THEN 1 ELSE NULL END AS is_role_status_no_data
  , CASE from_cs.role_status WHEN NULL THEN 1 ELSE NULL END AS is_role_status_none

  /*
  Enrollment status
  */
  -- https://docs.udp.unizin.org/tables/ref_enrollment_status.html
  , from_cs.enrollment_status
  , CASE from_cs.enrollment_status WHEN 'Active' THEN 1 ELSE NULL END AS is_enrollment_status_active
  , CASE from_cs.enrollment_status WHEN 'Completed' THEN 1 ELSE NULL END AS is_enrollment_status_completed
  , CASE from_cs.enrollment_status WHEN 'Inactive' THEN 1 ELSE NULL END AS is_enrollment_status_inactive
  , CASE from_cs.enrollment_status WHEN 'Not-enrolled' THEN 1 ELSE NULL END AS is_enrollment_status_not_enrolled
  , CASE from_cs.enrollment_status WHEN 'NoData' THEN 1 ELSE NULL END AS is_enrollment_status_no_data
  , CASE from_cs.enrollment_status WHEN NULL THEN 1 ELSE NULL END AS is_enrollment_status_none

  /*
  Repetition basis
  */
  -- https://docs.udp.unizin.org/tables/ref_enrollment_repetition_basis.html
  , from_cs.repetition_basis
  , CASE from_cs.repetition_basis WHEN 'Approved' THEN 1 ELSE NULL END AS is_repetition_basis_approved
  , CASE from_cs.repetition_basis WHEN 'FailedCourse' THEN 1 ELSE NULL END AS is_repetition_basis_failed_course
  , CASE from_cs.repetition_basis WHEN 'NotForCredit' THEN 1 ELSE NULL END AS is_repetition_basis_not_for_credit
  , CASE from_cs.repetition_basis WHEN 'Repetition' THEN 1 ELSE NULL END AS is_repetition_basis_repetition
  , CASE from_cs.repetition_basis WHEN 'Transfer' THEN 1 ELSE NULL END AS is_repetition_basis_transfer
  , CASE from_cs.repetition_basis WHEN 'NoData' THEN 1 ELSE NULL END AS is_repetition_basis_no_data
  , CASE from_cs.repetition_basis WHEN 'Other' THEN 1 ELSE NULL END AS is_repetition_basis_other
  , CASE from_cs.repetition_basis WHEN NULL THEN 1 ELSE NULL END AS is_repetition_basis_none

  /*
  Credits taken and earned.
  */
  , COALESCE(CAST(NULLIF(credits_taken,0) AS NUMERIC), NULL) AS credits_taken
  , COALESCE(CAST(NULLIF(credits_earned,0) AS NUMERIC), NULL) AS credits_earned

  /*
  Enrollment
  */
  , CASE from_cs.le_is_self_enrolled WHEN TRUE THEN 1 ELSE NULL END AS le_is_self_enrolled

  /*
  Assuming the student experiences a sequence of Academic terms, in which
  Academic term (from their point of view) was this Course section?
  */
  , DENSE_RANK() OVER (PARTITION BY from_cs.person_id ORDER BY academic_term.term_begin_date ASC) AS rank_person_academic_term_order_asc

  /*
  Days enrolled
  */
  , DATE_DIFF(EXTRACT(DATE FROM from_cs.exit_date), EXTRACT(DATE FROM from_cs.entry_date), DAY) AS days_enrollment_length

  /*
  Dates
  */
  , from_cs.entry_date AS entry_date
  , from_cs.exit_date AS exit_date
  , from_cs.completed_date AS completed_date
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section cs ON from_cs.course_section_id=cs.course_section_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering co ON cs.course_offering_id=co.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session ss ON co.academic_session_id=ss.academic_session_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term academic_term ON co.academic_term_id=academic_term.academic_term_id
;
