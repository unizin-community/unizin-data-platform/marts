/*
  Context / Base / Learning outcome group

    Imports Learning outcome group data from the context store.

  Properties:

    Export table:  mart_helper.context__learning_outcome_group
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learning_outcome_group AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learning_outcome_group AS k ON e.learning_outcome_group_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learning_outcome_group_id AS learning_outcome_group_id
  , from_cs.academic_term_id AS academic_term_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.parent_group_id AS parent_group_id
  , from_cs.root_group_id AS root_group_id

  /*
  Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title
  , from_cs.description AS description
  , LENGTH(from_cs.description) AS length_description

  , from_cs.vendor_guid AS vendor_guid

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
