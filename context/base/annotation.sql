/*
  Context / Base / Annotation

    Imports Annotation data from the context store.

  Properties:

    Export table:  mart_helper.context__annotation
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.annotation AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.annotation AS k ON e.annotation_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.annotation_id AS annotation_id
  , la.course_offering_id AS course_offering_id
  , lar.learner_activity_result_id AS learner_activity_result_id
  , from_cs.author_id AS author_id

  /*
  Attributes
  */
  , from_cs.author_name AS author_name

  , from_cs.body_value AS message
  , LENGTH(from_cs.body_value) AS length_message

  , from_cs.message_line_count AS count_message_lines
  , from_cs.message_word_count AS count_message_words
  , from_cs.message_character_count AS count_message_characters
  , from_cs.message_size_bytes AS count_message_size_bytes

  , CASE from_cs.is_hidden WHEN TRUE THEN 1 ELSE NULL END AS is_hidden
  , CASE from_cs.is_anonymous WHEN TRUE THEN 1 ELSE NULL END AS is_anonymous
  , CASE from_cs.has_teacher_only_comments WHEN TRUE THEN 1 ELSE NULL END AS has_teacher_only_comments

  /*
  Dates
  */
  , from_cs.updated_date AS updated_date
  , from_cs.created_date AS created_date

FROM from_cs

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity_result AS lar ON from_cs.learner_activity_result_id=lar.learner_activity_result_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity AS la ON lar.learner_activity_id=la.learner_activity_id
;

