/*
  Context / Base / Learner environment organization expanded

    Imports Learner environment organization data from the context store.

  Properties:

    Export table:  mart_helper.context__learning_environment_organization_expanded
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learning_environment_organization AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learning_environment_organization AS k ON e.learning_environment_organization_id=k.id
)

,
/*
Expand the learning_environment_organization entity to define the multiple subaccounts associated with the given account. 
*/
leo_expanded as (
select 
  /* 
  Define the name and ID associated with each account 
  */
  leo.learning_environment_organization_id as learning_environment_organization_id_0,
  leo.name as name_0,
  leo.status as status_0,
  leo.lms_id as lms_id_0,
  leo1.learning_environment_organization_id as learning_environment_organization_id_1,
  leo1.name as name_1,
  leo2.learning_environment_organization_id as learning_environment_organization_id_2,
  leo2.name as name_2,
  leo3.learning_environment_organization_id as learning_environment_organization_id_3,
  leo3.name as name_3,
  leo4.learning_environment_organization_id as learning_environment_organization_id_4,
  leo4.name as name_4,
  leo5.learning_environment_organization_id as learning_environment_organization_id_5,
  leo5.name as name_5,
  leo6.learning_environment_organization_id as learning_environment_organization_id_6,
  leo6.name as name_6,
  leo7.learning_environment_organization_id as learning_environment_organization_id_7,
  leo7.name as name_7,
  leo8.learning_environment_organization_id as learning_environment_organization_id_8,
  leo8.name as name_8,
  leo9.learning_environment_organization_id as learning_environment_organization_id_9,
  leo9.name as name_9,
  leo10.learning_environment_organization_id as learning_environment_organization_id_10,
  leo10.name as name_10,
  
  /*
  Define the depth of the account
  */
  case 
  when (leo1.learning_environment_organization_id is null)
    then 0
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is null)
    then 1 
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is null)
    then 2
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is null)
    then 3
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is not null and leo5.learning_environment_organization_id is null)
    then 4
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is not null and leo5.learning_environment_organization_id is not null and leo6.learning_environment_organization_id is null)
    then 5
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is not null and leo5.learning_environment_organization_id is not null and leo6.learning_environment_organization_id is not null and leo7.learning_environment_organization_id is null)
    then 6
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is not null and leo5.learning_environment_organization_id is not null and leo6.learning_environment_organization_id is not null and leo7.learning_environment_organization_id is not null and leo8.learning_environment_organization_id is null)
    then 7
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is not null and leo5.learning_environment_organization_id is not null and leo6.learning_environment_organization_id is not null and leo7.learning_environment_organization_id is not null and leo8.learning_environment_organization_id is not null and leo9.learning_environment_organization_id is null)
    then 8
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is not null and leo5.learning_environment_organization_id is not null and leo6.learning_environment_organization_id is not null and leo7.learning_environment_organization_id is not null and leo8.learning_environment_organization_id is not null and leo9.learning_environment_organization_id is not null and leo10.learning_environment_organization_id is null)
    then 9
  when (leo1.learning_environment_organization_id is not null and leo2.learning_environment_organization_id is not null and leo3.learning_environment_organization_id is not null and leo4.learning_environment_organization_id is not null and leo5.learning_environment_organization_id is not null and leo6.learning_environment_organization_id is not null and leo7.learning_environment_organization_id is not null and leo8.learning_environment_organization_id is not null and leo9.learning_environment_organization_id is not null and leo10.learning_environment_organization_id is not null)
    then 10
  else null   
  end as depth

from from_cs leo
left join from_cs leo1 on leo.parent_learning_environment_organization_id = leo1.learning_environment_organization_id
left join from_cs leo2 on leo1.parent_learning_environment_organization_id = leo2.learning_environment_organization_id
left join from_cs leo3 on leo2.parent_learning_environment_organization_id = leo3.learning_environment_organization_id
left join from_cs leo4 on leo3.parent_learning_environment_organization_id = leo4.learning_environment_organization_id
left join from_cs leo5 on leo4.parent_learning_environment_organization_id = leo5.learning_environment_organization_id
left join from_cs leo6 on leo5.parent_learning_environment_organization_id = leo6.learning_environment_organization_id
left join from_cs leo7 on leo6.parent_learning_environment_organization_id = leo7.learning_environment_organization_id
left join from_cs leo8 on leo7.parent_learning_environment_organization_id = leo8.learning_environment_organization_id
left join from_cs leo9 on leo8.parent_learning_environment_organization_id = leo9.learning_environment_organization_id
left join from_cs leo10 on leo9.parent_learning_environment_organization_id = leo10.learning_environment_organization_id
)

select 
/*
Define fields for the given learning environment organization / account
*/
leo_expanded.learning_environment_organization_id_0 as learning_environment_organization_id,
leo_expanded.lms_id_0 as lms_id,
leo_expanded.name_0 as name,
leo_expanded.depth as depth,
leo_expanded.status_0 as status,

/* 
Define parent and grandparent learning_environment_organizations 
*/
learning_environment_organization_id_1 as parent_learning_environment_organization_id,
name_1 as parent_learning_environment_organization,

learning_environment_organization_id_2 as grandparent_learning_environment_organization_id,
name_2 as grandparent_learning_environment_organization,

/*
Define root learning environment organization ID and name 
*/
case 
when depth = 0
  then learning_environment_organization_id_0
when depth = 1 
  then learning_environment_organization_id_1 
when depth = 2 
  then learning_environment_organization_id_2
when depth = 3 
  then learning_environment_organization_id_3
when depth = 4
  then learning_environment_organization_id_4
when depth = 5
  then learning_environment_organization_id_5
when depth = 6
  then learning_environment_organization_id_6
when depth = 7
  then learning_environment_organization_id_7
when depth = 8
  then learning_environment_organization_id_8
when depth = 9
  then learning_environment_organization_id_9
when depth = 10
  then learning_environment_organization_id_10
else null
end as root_learning_environment_organization_id,

case 
when depth = 0
  then name_0
when depth = 1 
  then name_1 
when depth = 2 
  then name_2
when depth = 3 
  then name_3
when depth = 4
  then name_4
when depth = 5
  then name_5
when depth = 6
  then name_6
when depth = 7
  then name_7
when depth = 8
  then name_8
when depth = 9
  then name_9
when depth = 10
  then name_10
else null
end as root_learning_environment_organization,

/* 
Define all subaccount IDs and names 
*/

case 
when depth = 1 
  then learning_environment_organization_id_0
when depth = 2 
  then learning_environment_organization_id_1
when depth = 3 
  then learning_environment_organization_id_2
when depth = 4
  then learning_environment_organization_id_3
when depth = 5
  then learning_environment_organization_id_4
when depth = 6
  then learning_environment_organization_id_5
when depth = 7
  then learning_environment_organization_id_6
when depth = 8
  then learning_environment_organization_id_7
when depth = 9
  then learning_environment_organization_id_8
when depth = 10
  then learning_environment_organization_id_9
else null   
end as sub_learning_environment_organization_id_1,

case 
when depth = 1 
  then name_0 
when depth = 2 
  then name_1
when depth = 3 
  then name_2
when depth = 4
  then name_3
when depth = 5
  then name_4
when depth = 6
  then name_5
when depth = 7
  then name_6
when depth = 8
  then name_7
when depth = 9
  then name_8
when depth = 10
  then name_9
else null
end as sub_learning_environment_organization_1,

case 
when depth = 2 
  then learning_environment_organization_id_0
when depth = 3 
  then learning_environment_organization_id_1
when depth = 4
  then learning_environment_organization_id_2
when depth = 5
  then learning_environment_organization_id_3
when depth = 6
  then learning_environment_organization_id_4
when depth = 7
  then learning_environment_organization_id_5
when depth = 8
  then learning_environment_organization_id_6
when depth = 9
  then learning_environment_organization_id_7
when depth = 10
  then learning_environment_organization_id_8
else null   
end as sub_learning_environment_organization_id_2,

case 
when depth = 2 
  then name_0
when depth = 3 
  then name_1
when depth = 4
  then name_2
when depth = 5
  then name_3
when depth = 6
  then name_4
when depth = 7
  then name_5
when depth = 8
  then name_6
when depth = 9
  then name_5
when depth = 10
  then name_8
else null
end as sub_learning_environment_organization_2,

case 
when depth = 3 
  then learning_environment_organization_id_0
when depth = 4
  then learning_environment_organization_id_1
when depth = 5
  then learning_environment_organization_id_2
when depth = 6
  then learning_environment_organization_id_3
when depth = 7
  then learning_environment_organization_id_4
when depth = 8
  then learning_environment_organization_id_5
when depth = 9
  then learning_environment_organization_id_6
when depth = 10
  then learning_environment_organization_id_7

else null   
end as sub_learning_environment_organization_id_3,

case 
when depth = 3 
  then name_0
when depth = 4
  then name_1
when depth = 5
  then name_2
when depth = 6
  then name_3
when depth = 7
  then name_4
when depth = 8
  then name_5
when depth = 9
  then name_6
when depth = 10
  then name_7
else null
end as sub_learning_environment_organization_3,

case 
when depth = 4
  then learning_environment_organization_id_0
when depth = 5
  then learning_environment_organization_id_1
when depth = 6
  then learning_environment_organization_id_2
when depth = 7
  then learning_environment_organization_id_3
when depth = 8
  then learning_environment_organization_id_4
when depth = 9
  then learning_environment_organization_id_5
when depth = 10
  then learning_environment_organization_id_6
else null   
end as sub_learning_environment_organization_id_4,

case 
when depth = 4
  then name_0
when depth = 5
  then name_1
when depth = 6
  then name_2
when depth = 7
  then name_3
when depth = 8
  then name_4
when depth = 9
  then name_5
when depth = 10
  then name_6

else null
end as sub_learning_environment_organization_4,

case 
when depth = 5
  then learning_environment_organization_id_0
when depth = 6
  then learning_environment_organization_id_1
when depth = 7
  then learning_environment_organization_id_2
when depth = 8
  then learning_environment_organization_id_3
when depth = 9
  then learning_environment_organization_id_4
when depth = 10
  then learning_environment_organization_id_5
else null   
end as sub_learning_environment_organization_id_5,

case 
when depth = 5
  then name_0
when depth = 6
  then name_1
when depth = 7
  then name_2
when depth = 8
  then name_3
when depth = 9
  then name_4
when depth = 10
  then name_5
else null
end as sub_learning_environment_organization_5,

case 
when depth = 6
  then learning_environment_organization_id_0
when depth = 7
  then learning_environment_organization_id_1
when depth = 8
  then learning_environment_organization_id_2
when depth = 9
  then learning_environment_organization_id_3
when depth = 10
  then learning_environment_organization_id_4
else null   
end as sub_learning_environment_organization_id_6,

case 
when depth = 6
  then name_0
when depth = 7
  then name_1
when depth = 8
  then name_2
when depth = 9
  then name_3
when depth = 10
  then name_4
else null
end as sub_learning_environment_organization_6,

case 
when depth = 7
  then learning_environment_organization_id_0
when depth = 8
  then learning_environment_organization_id_1
when depth = 9
  then learning_environment_organization_id_2
when depth = 10
  then learning_environment_organization_id_3
else null   
end as sub_learning_environment_organization_id_7,

case 
when depth = 7
  then name_0
when depth = 8
  then name_1
when depth = 9
  then name_2
when depth = 10
  then name_3
else null
end as sub_learning_environment_organization_7,

case 
when depth = 8
  then learning_environment_organization_id_0
when depth = 9
  then learning_environment_organization_id_1
when depth = 10
  then learning_environment_organization_id_2
else null   
end as sub_learning_environment_organization_id_8,

case 
when depth = 8
  then name_0
when depth = 9
  then name_1
when depth = 10
  then name_2
else null
end as sub_learning_environment_organization_8,

case 
when depth = 9
  then learning_environment_organization_id_0
when depth = 10
  then learning_environment_organization_id_1
else null   
end as sub_learning_environment_organization_id_9,

case 
when depth = 9
  then name_0
when depth = 10
  then name_1
else null
end as sub_learning_environment_organization_9,

case 
when depth = 10
  then learning_environment_organization_id_0
else null   
end as sub_learning_environment_organization_id_10,

case 
when depth = 10
  then name_0
else null
end as sub_learning_environment_organization_10

from leo_expanded    
