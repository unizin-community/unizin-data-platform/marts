/*
  Context / Base / Person - Academic degree

    Imports Person - Academic degree data from the context store.

  Properties:

    Export table:  mart_helper.context__person__academic_degree
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    kp.sis_ext_id AS sis_person_id
    , kad.sis_ext_id as sis_academic_degree_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.person__academic_degree AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.person AS kp ON e.person_id=kp.id
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_degree AS kad ON e.academic_degree_id=kad.id
)

SELECT
  from_cs.sis_person_id AS sis_person_id
  , from_cs.sis_academic_degree_id AS sis_academic_degree_id
  , from_cs.person__academic_degree_id AS person__academic_degree_id
  , from_cs.academic_degree_id AS academic_degree_id
  , from_cs.academic_term_id AS academic_term_id
  , from_cs.person_id AS person_id

  /*
  Attributes
  */
  , from_cs.academic_major_1 AS academic_major_1
  , from_cs.academic_major_2 AS academic_major_2
  , from_cs.academic_major_3 AS academic_major_3

  , from_cs.academic_major_specialization_1 AS academic_major_specialization_1
  , from_cs.academic_major_specialization_2 AS academic_major_specialization_2
  , from_cs.academic_major_specialization_3 AS academic_major_specialization_3

  , from_cs.academic_minor_1 AS academic_minor_1
  , from_cs.academic_minor_2 AS academic_minor_2
  , from_cs.academic_minor_3 AS academic_minor_3

  /*
  Attributes
  */
  -- https://docs.udp.unizin.org/tables/ref_academic_excellence.html
  , from_cs.academic_excellence AS academic_excellence
  , CASE from_cs.academic_excellence WHEN 'CumLaude' THEN 1 ELSE NULL END AS academic_excellence_cum_laude
  , CASE from_cs.academic_excellence WHEN 'Distinction' THEN 1 ELSE NULL END AS academic_excellence_distinction
  , CASE from_cs.academic_excellence WHEN 'HighDistinction' THEN 1 ELSE NULL END AS academic_excellence_high_distinction
  , CASE from_cs.academic_excellence WHEN 'HighHonor' THEN 1 ELSE NULL END AS academic_excellence_high_honor
  , CASE from_cs.academic_excellence WHEN 'HighestDistinction' THEN 1 ELSE NULL END AS academic_excellence_highest_distinction
  , CASE from_cs.academic_excellence WHEN 'HighestHonor' THEN 1 ELSE NULL END AS academic_excellence_highest_honor
  , CASE from_cs.academic_excellence WHEN 'Honors' THEN 1 ELSE NULL END AS academic_excellence_honors
  , CASE from_cs.academic_excellence WHEN 'MagnaCumLaude' THEN 1 ELSE NULL END AS academic_excellence_magna_cum_laude
  , CASE from_cs.academic_excellence WHEN 'NoData' THEN 1 ELSE NULL END AS academic_excellence_no_data
  , CASE from_cs.academic_excellence WHEN 'Other' THEN 1 ELSE NULL END AS academic_excellence_other
  , CASE from_cs.academic_excellence WHEN 'SummaCumLaude' THEN 1 ELSE NULL END AS academic_excellence_summa_cum_laude

  /*
  Dates
  */
  , from_cs.conferred_date AS conferred_date

FROM from_cs
;
