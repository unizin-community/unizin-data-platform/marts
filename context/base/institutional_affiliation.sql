/*
  Context / Base / Institutional affiliation

    Imports Institutional affiliation data from the context store.

  Properties:

    Export table:  mart_helper.context__institutional_affiliation
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    kp.sis_ext_id AS sis_person_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.institutional_affiliation AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.person AS kp ON e.person_id=kp.id
)

SELECT
  from_cs.sis_person_id AS sis_person_id
  , from_cs.person_id AS person_id
  , from_cs.directory_block AS directory_block
FROM from_cs
;
