/*
  Context / Base / Conversation message

    Imports Conversation message data from the context store.

  Properties:

    Export table:  mart_helper.context__conversation_message
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.conversation_message AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.conversation_message AS k ON e.conversation_message_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.conversation_message_id AS conversation_message_id
  , from_cs.author_id AS author_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.learner_group_id AS learner_group_id
  , from_cs.conversation_id AS conversation_id

  /*
  Attributes
  */
  , from_cs.body AS body
  , LENGTH(from_cs.body) AS length_message

  , from_cs.message_line_count AS count_message_lines
  , from_cs.message_word_count AS count_message_words
  , from_cs.message_character_count AS count_message_characters
  , from_cs.message_size_bytes AS count_message_size_bytes

  , CASE from_cs.has_attachments WHEN TRUE THEN 1 ELSE NULL END AS has_attachments
  , CASE from_cs.has_media_objects WHEN 'true' THEN 1 ELSE NULL END AS has_media_objects
  , CASE from_cs.is_system_generated WHEN TRUE THEN 1 ELSE NULL END AS is_system_generated

  /*
  Dates
  */
  , from_cs.created_date AS created_date

FROM from_cs
;
