/*
  Context / Base / Learner activity group grade

    Imports Learner activity group grade data from the context store.

  Properties:

    Export table:  mart_helper.context__learner_activity_group_grade
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learner_activity_group_grade AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_activity_group_grade AS k ON e.learner_activity_group_grade_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learner_activity_group_grade_id AS learner_activity_group_grade_id
  , from_cs.learner_activity_group_id AS learner_activity_group_id

  /*
  Scores
  */
  , from_cs.le_current_score AS le_current_score
  , from_cs.le_hidden_current_score AS le_hidden_current_score
  , from_cs.le_final_score AS le_final_score
  , from_cs.le_hidden_final_score AS le_hidden_final_score

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
