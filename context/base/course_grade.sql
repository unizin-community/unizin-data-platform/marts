/*
  Context / Base / Course grade

    Imports Course grade data from the context store.

  Properties:

    Export table:  mart_helper.context__course_grade
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    kp.sis_ext_id AS sis_person_id
    , kp.lms_ext_id AS lms_person_id
    , kcs.sis_ext_id AS sis_course_section_id
    , kcs.lms_ext_id AS lms_course_section_id
    , e.*

  FROM _READ_FROM_PROJECT_.context_store_entity.course_grade AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.person AS kp ON e.person_id=kp.id
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.course_section AS kcs ON e.course_section_id=kcs.id
)

SELECT
  from_cs.sis_person_id AS sis_person_id
  , from_cs.lms_person_id AS lms_person_id
  , from_cs.sis_course_section_id AS sis_course_section_id
  , from_cs.lms_course_section_id AS lms_course_section_id
  , from_cs.course_section_id AS course_section_id
  , from_cs.person_id AS person_id

  /*
  Attributes
  */
  , from_cs.gpa_cumulative_excluding_course_grade AS gpa_cumulative_excluding_course_grade
  , from_cs.gpa_current_excluding_course_grade AS gpa_current_excluding_course_grade

  , CASE from_cs.is_course_grade_in_gpa WHEN TRUE THEN 1 ELSE NULL END AS is_course_grade_in_gpa

  /*
  Grade
  */
  -- https://docs.udp.unizin.org/tables/ref_grading_basis.html
  , from_cs.grading_basis AS grading_basis
  , CASE from_cs.grading_basis WHEN 'Audit' THEN 1 ELSE NULL END AS grading_basis_audit
  , CASE from_cs.grading_basis WHEN 'ContinuingCourse' THEN 1 ELSE NULL END AS grading_basis_continuing_course
  , CASE from_cs.grading_basis WHEN 'Credit' THEN 1 ELSE NULL END AS grading_basis_credit
  , CASE from_cs.grading_basis WHEN 'Graded' THEN 1 ELSE NULL END AS grading_basis_graded
  , CASE from_cs.grading_basis WHEN 'NoCredit' THEN 1 ELSE NULL END AS grading_basis_no_credit
  , CASE from_cs.grading_basis WHEN 'NoData' THEN 1 ELSE NULL END AS grading_basis_no_data
  , CASE from_cs.grading_basis WHEN 'NotGraded' THEN 1 ELSE NULL END AS grading_basis_not_graded
  , CASE from_cs.grading_basis WHEN 'Other' THEN 1 ELSE NULL END AS grading_basis_other
  , CASE from_cs.grading_basis WHEN 'PassFail' THEN 1 ELSE NULL END AS grading_basis_pass_fail
  , CASE from_cs.grading_basis WHEN 'Withdrawn' THEN 1 ELSE NULL END AS grading_basis_withdrawn

  , from_cs.grade_inputted_by_instructor AS grade_inputted_by_instructor
  , from_cs.grade_on_official_transcript AS grade_on_official_transcript
  , from_cs.grade_points AS grade_points
  , from_cs.grade_points_per_credit AS grade_points_per_credit

  /*
  Scores
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.le_final_score_status AS le_final_score_status

  , from_cs.le_hidden_current_score AS le_hidden_current_score
  , from_cs.le_hidden_final_score AS le_hidden_final_score

  , from_cs.le_current_score AS le_current_score
  , from_cs.le_final_score AS le_final_score

  /*
  Dates
  */
  , from_cs.updated_date AS updated_date
  , from_cs.created_date AS created_date

FROM from_cs
;
