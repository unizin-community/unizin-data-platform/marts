/*
  Context / Base / Discussion entry

    Imports Discussion entry data from the context store.

  Properties:

    Export table:  mart_helper.context__discussion_entry
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Discussion

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.discussion_entry AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.discussion_entry AS k ON e.discussion_entry_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.discussion_entry_id AS discussion_entry_id
  , from_cs.person_id AS person_id
  , d.course_offering_id AS course_offering_id
  , from_cs.discussion_id AS discussion_id

  /*
  Attributes
  */
  , from_cs.body AS body
  , LENGTH(from_cs.body) AS length_body

  , from_cs.position AS position
  , from_cs.parent_discussion_entry_id AS parent_discussion_entry_id

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , CASE from_cs.status WHEN 'active' THEN 1 ELSE NULL END AS is_status_active
  , CASE from_cs.status WHEN 'deleted' THEN 1 ELSE NULL END AS is_status_deleted

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.deleted_date AS deleted_date

FROM from_cs

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__discussion AS d ON from_cs.discussion_id=d.discussion_id
;
