/*
  Context / Base / Quiz

    Imports Quiz data from the context store.

  Properties:

    Export table:  mart_helper.context__quiz
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM
    _READ_FROM_PROJECT_.context_store_entity.quiz AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.quiz AS k ON e.quiz_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.quiz_id AS quiz_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.course_offering_id AS course_offering_id

  /*
    Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title
  , from_cs.description AS description
  , LENGTH(from_cs.description) AS length_description

  , from_cs.allowed_attempts AS allowed_attempts
  , from_cs.points_possible AS points_possible
  , from_cs.quiz_item_count AS quiz_item_count
  , from_cs.unpublished_quiz_item_count AS unpublished_quiz_item_count
  , from_cs.time_limit AS time_limit

  , CASE WHEN from_cs.is_allow_anonymous_submissions IS TRUE THEN 1 ELSE NULL END AS is_allow_anonymous_submissions
  , CASE WHEN from_cs.is_allow_go_back_to_previous_question IS TRUE THEN 1 ELSE NULL END AS is_allow_go_back_to_previous_question
  , CASE WHEN from_cs.is_browser_lockdown_monitor_required IS TRUE THEN 1 ELSE NULL END AS is_browser_lockdown_monitor_required
  , CASE WHEN from_cs.is_browser_lockdown_required IS TRUE THEN 1 ELSE NULL END AS is_browser_lockdown_required
  , CASE WHEN from_cs.is_browser_lockdown_required_to_display_results IS TRUE THEN 1 ELSE NULL END AS is_browser_lockdown_required_to_display_results
  , CASE WHEN from_cs.is_can_be_locked IS TRUE THEN 1 ELSE NULL END AS is_can_be_locked
  , CASE WHEN from_cs.is_display_multiple_questions IS TRUE THEN 1 ELSE NULL END AS is_display_multiple_questions
  , CASE WHEN from_cs.is_shuffled_answer_display_order IS TRUE THEN 1 ELSE NULL END AS is_shuffled_answer_display_order

  , from_cs.show_results AS show_results
  , from_cs.ip_filter AS ip_filter

  /*
  Correct answers display policy
  */
  -- https://docs.udp.unizin.org/tables/ref_learning_activity_display_policy.html
  , from_cs.correct_answers_display_policy AS correct_answers_display_policy
  , CASE WHEN from_cs.correct_answers_display_policy = 'always' THEN 1 ELSE NULL END AS is_correct_answers_display_policy_always
  , CASE WHEN from_cs.correct_answers_display_policy = 'never' THEN 1 ELSE NULL END AS is_correct_answers_display_policy_never
  , CASE WHEN from_cs.correct_answers_display_policy = 'always_after_last_attempt' THEN 1 ELSE NULL END AS is_correct_answers_display_policy_always_after_last_attempt
  , CASE WHEN from_cs.correct_answers_display_policy = 'only_once_after_last_attempt' THEN 1 ELSE NULL END AS is_correct_answers_display_policy_only_once_after_last_attempt

  /*
  Scoring policy
  */
  -- https://docs.udp.unizin.org/tables/ref_assessment_scoring_policy.html
  , from_cs.scoring_policy AS scoring_policy
  , CASE WHEN from_cs.scoring_policy = 'keep_highest' THEN 1 ELSE NULL END AS is_scoring_policy_keep_highest
  , CASE WHEN from_cs.scoring_policy = 'keep_latest' THEN 1 ELSE NULL END AS is_scoring_policy_keep_latest
  , CASE WHEN from_cs.scoring_policy = 'keep_average' THEN 1 ELSE NULL END AS is_scoring_policy_keep_average

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status
  , CASE WHEN from_cs.status = 'published' THEN 1 ELSE NULL END AS is_status_published
  , CASE WHEN from_cs.status = 'unpublished' THEN 1 ELSE NULL END AS is_status_unpublished
  , CASE WHEN from_cs.status = 'deleted' THEN 1 ELSE NULL END AS is_status_deleted

  /*
  Type
  */
  -- https://docs.udp.unizin.org/tables/ref_assessment_type.html
  , from_cs.type AS type
  , CASE WHEN from_cs.type = 'assignment' THEN 1 ELSE NULL END AS is_type_assignment
  , CASE WHEN from_cs.type = 'practice_quiz' THEN 1 ELSE NULL END AS is_type_practice_quiz
  , CASE WHEN from_cs.type = 'survey' THEN 1 ELSE NULL END AS is_type_survey
  , CASE WHEN from_cs.type = 'graded_survey' THEN 1 ELSE NULL END AS is_type_graded_survey

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.deleted_date AS deleted_date
  , from_cs.updated_date AS updated_date
  , from_cs.due_date AS due_date
  , from_cs.published_date AS published_date
  , from_cs.show_correct_answers_date AS show_correct_answers_date
  , from_cs.hide_correct_answers_date AS hide_correct_answers_date
  , from_cs.locked_date AS locked_date
  , from_cs.unlocked_date AS unlocked_date

FROM from_cs
;
