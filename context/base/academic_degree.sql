/*
  Context / Base / Academic degree

    Imports Academic degree data from the context store.

  Properties:

    Export table:  mart_helper.context__academic_degree
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.academic_degree AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_degree AS k ON e.academic_degree_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.academic_degree_id AS academic_degree_id

  /*
  Attributes
  */
  , from_cs.name AS name

  -- https://docs.udp.unizin.org/tables/ref_degree_educational_level.html
  , from_cs.educational_level AS educational_level
  , CASE from_cs.educational_level WHEN 'Associates' THEN 1 ELSE NULL END AS educational_level_associates
  , CASE from_cs.educational_level WHEN 'Bachelors' THEN 1 ELSE NULL END AS educational_level_bachelors
  , CASE from_cs.educational_level WHEN 'Certificate' THEN 1 ELSE NULL END AS educational_level_certificate
  , CASE from_cs.educational_level WHEN 'DoctorJuridicalScience' THEN 1 ELSE NULL END AS educational_level_doctor_juridical_sciences
  , CASE from_cs.educational_level WHEN 'DoctorVeterinaryMedicine' THEN 1 ELSE NULL END AS educational_level_doctor_veterinary_medicine
  , CASE from_cs.educational_level WHEN 'Doctorate' THEN 1 ELSE NULL END AS educational_level_doctorate
  , CASE from_cs.educational_level WHEN 'Graduate Professor' THEN 1 ELSE NULL END AS educational_level_graduate_professor
  , CASE from_cs.educational_level WHEN 'Intermediate' THEN 1 ELSE NULL END AS educational_level_intermediate
  , CASE from_cs.educational_level WHEN 'JurisDoctorate' THEN 1 ELSE NULL END AS educational_level_juris_doctorate
  , CASE from_cs.educational_level WHEN 'Masters' THEN 1 ELSE NULL END AS educational_level_masters
  , CASE from_cs.educational_level WHEN 'MedicalDoctorate' THEN 1 ELSE NULL END AS educational_level_medical_doctorate
  , CASE from_cs.educational_level WHEN 'NoData' THEN 1 ELSE NULL END AS educational_level_no_data
  , CASE from_cs.educational_level WHEN 'Other' THEN 1 ELSE NULL END AS educational_level_other
  , CASE from_cs.educational_level WHEN 'OtherProfessionalDegree' THEN 1 ELSE NULL END AS educational_level_other_professional_degree
  , CASE from_cs.educational_level WHEN 'Pharmacology' THEN 1 ELSE NULL END AS educational_level_pharmacology
  , CASE from_cs.educational_level WHEN 'Teaching Certificate' THEN 1 ELSE NULL END AS educational_level_teaching_certificate

FROM from_cs
;
