/*
  Context / Base / Quiz item

    Imports Quiz item data from the context store.

  Properties:

    Export table:  mart_helper.context__quiz_item
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.quiz_item AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.quiz_item AS k ON e.quiz_item_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.quiz_item_id AS quiz_item_id
  , from_cs.learner_activity_id AS learner_activity_id
  , from_cs.quiz_id AS quiz_id
  , from_cs.quiz_item_group_id AS quiz_item_group_id

  /*
    Attributes
  */
  , from_cs.name AS name
  , LENGTH(from_cs.name) AS length_name
  , from_cs.body AS body
  , LENGTH(from_cs.body) AS length_body

  , from_cs.position AS position
  , from_cs.points_possible AS points_possible
  , CASE WHEN from_cs.is_regrade_option_available IS TRUE THEN 1 ELSE NULL END AS is_regrade_option_available

  /*
  Display after grade
  */
  , from_cs.neutral_comments AS neutral_comments
  , from_cs.correct_comments AS correct_comments
  , from_cs.incorrect_comments AS incorrect_comments

  /*
  Quiz item type
  */
  -- https://docs.udp.unizin.org/tables/ref_assessment_item_type.html
  , CASE from_cs.quiz_item_type WHEN 'calculated_question' THEN 1 ELSE NULL END AS is_quiz_item_type_calculated_question
  , CASE from_cs.quiz_item_type WHEN 'error' THEN 1 ELSE NULL END AS is_quiz_item_type_error
  , CASE from_cs.quiz_item_type WHEN 'file_upload_question' THEN 1 ELSE NULL END AS is_quiz_item_type_file_upload_question
  , CASE from_cs.quiz_item_type WHEN 'essay_question' THEN 1 ELSE NULL END AS is_quiz_item_type_essay_question
  , CASE from_cs.quiz_item_type WHEN 'matching_question' THEN 1 ELSE NULL END AS is_quiz_item_type_matching_question
  , CASE from_cs.quiz_item_type WHEN 'numerical_question' THEN 1 ELSE NULL END AS is_quiz_item_type_numerical_question
  , CASE from_cs.quiz_item_type WHEN 'text_only_question' THEN 1 ELSE NULL END AS is_quiz_item_type_text_only_question
  , CASE from_cs.quiz_item_type WHEN 'true_false_question' THEN 1 ELSE NULL END AS is_quiz_item_type_true_false_question
  , CASE from_cs.quiz_item_type WHEN 'short_answer_question' THEN 1 ELSE NULL END AS is_quiz_item_type_short_answer_question
  , CASE from_cs.quiz_item_type WHEN 'multiple_choice_question' THEN 1 ELSE NULL END AS is_quiz_item_type_multiple_choice_question
  , CASE from_cs.quiz_item_type WHEN 'multiple_answers_question' THEN 1 ELSE NULL END AS is_quiz_item_type_multiple_answers_question
  , CASE from_cs.quiz_item_type WHEN 'multiple_dropdowns_question' THEN 1 ELSE NULL END AS is_quiz_item_type_multiple_dropdowns_question
  , CASE from_cs.quiz_item_type WHEN 'fill_in_multiple_blanks_question' THEN 1 ELSE NULL END AS is_quiz_item_type_fill_in_multiple_blanks_question

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , CASE from_cs.status WHEN 'published' THEN 1 ELSE NULL END AS is_status_published
  , CASE from_cs.status WHEN 'unpublished' THEN 1 ELSE NULL END AS is_status_unpublished
  , CASE from_cs.status WHEN 'deleted' THEN 1 ELSE NULL END AS is_status_deleted

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
