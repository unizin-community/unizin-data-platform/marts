/*
  Context / Base / Module progression

    Imports Module progression data from the context store.

  Properties:

    Export table:  mart_helper.context__module_progression
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.module_progression AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.module_progression AS k ON e.module_progression_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.module_progression_id AS module_progression_id
  , from_cs.module_id AS module_id
  , from_cs.person_id AS person_id

  /*
  Attributes
  */
  , from_cs.current_position AS current_position
  , from_cs.lock_version AS lock_version

  , CASE from_cs.is_collapsed WHEN TRUE THEN 1 ELSE NULL END AS is_collapsed
  , CASE from_cs.is_current WHEN TRUE THEN 1 ELSE NULL END AS is_current

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.completed_date AS completed_date
  , from_cs.evaluated_date AS evaluated_date

FROM from_cs
;
