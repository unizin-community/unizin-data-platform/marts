/*
  Context / Base / Grading period

    Imports Grading period data from the context store.

  Properties:

    Export table:  mart_helper.context__grading_period
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.grading_period AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.grading_period AS k ON e.grading_period_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.grading_period_id AS grading_period_id
  , from_cs.grading_period_group_id AS grading_period_group_id

  /*
  Attributes
  */
  , from_cs.title AS title
  , LENGTH(from_cs.title) AS length_title
  , from_cs.weight AS weight

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.start_date AS start_date
  , from_cs.close_date AS close_date
  , from_cs.end_date AS end_date

FROM from_cs
;
