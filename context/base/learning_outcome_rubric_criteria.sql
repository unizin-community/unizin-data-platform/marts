/*
  Context / Base / Learning outcome rubric criteria

    Imports Learning outcome rubric criteria data from the context store.

  Properties:

    Export table:  mart_helper.context__learning_outcome_rubric_criteria
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learning_outcome_rubric_criteria AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learning_outcome_rubric_criteria AS k ON e.learning_outcome_rubric_criteria_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learning_outcome_rubric_criteria_id AS learning_outcome_rubric_criteria_id
  , from_cs.academic_term_id AS academic_term_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.learning_outcome_id AS learning_outcome_id

  /*
  Attributes
  */
  , from_cs.description AS description
  , LENGTH(from_cs.description) AS length_description

  /*
  Score
  */
  , from_cs.points_possible AS points_possible

FROM from_cs
;
