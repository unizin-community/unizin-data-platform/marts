/*
  Context / Base / Course offering section enrollment

    Attempts to generate the best approximiation of
    the Course section that is most relevant to a
    Course offering-Person pair.

  Properties:

    Export table:  mart_helper.context__course_offering_section_enrollment
    Run frequency: Daily
    Run operation: Overwrite

  Dependencies:

    * Base / Course section enrollemnt

  To do:
    *
*/

WITH course_section_enrollment AS (
  SELECT
    COALESCE(cse.le_current_course_offering_id,cse.course_offering_id) AS udp_course_offering_id
    , cse.course_section_id AS udp_course_section_id
    , cse.person_id AS udp_person_id
    , cse.role AS role
    , cse.role_status AS role_status
    , ROW_NUMBER() OVER (PARTITION BY cse.person_id, COALESCE(cse.le_current_course_offering_id,cse.course_offering_id) ORDER BY cse.created_date ASC) AS row_number
  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  WHERE
    cse.role_status NOT IN ('Not-enrolled', 'Dropped', 'Withdrawn')
)

SELECT
  cse.udp_course_offering_id AS udp_course_offering_id
  , cse.udp_course_section_id AS udp_course_section_id
  , cse.udp_person_id AS udp_person_id
  , cse.role AS role
  , cse.role_status AS role_status
FROM
  course_section_enrollment AS cse
WHERE
  row_number = 1 -- Only use one course section; no dupes.
