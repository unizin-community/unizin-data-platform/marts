/*
  Context / Base / Participant session role

    Imports Participant session role data from the context store.

  Properties:

    Export table:  mart_helper.context__participant_session role
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.participant_session_role AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.participant_session_role AS k ON e.participant_session_role_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.participant_session_role_id AS participant_session_role_id
  , from_cs.participant_session_id AS participant_session_id
  , from_cs.person_id AS person_id

  /*
  Role
  */
  -- https://docs.udp.unizin.org/tables/ref_role.html
  , from_cs.role AS role
  , CASE from_cs.role WHEN 'Auditor' THEN 1 ELSE NULL END AS role_auditor
  , CASE from_cs.role WHEN 'Designer' THEN 1 ELSE NULL END AS role_designer
  , CASE from_cs.role WHEN 'Faculty' THEN 1 ELSE NULL END AS role_faculty
  , CASE from_cs.role WHEN 'Generic' THEN 1 ELSE NULL END AS role_generic
  , CASE from_cs.role WHEN 'NoData' THEN 1 ELSE NULL END AS role_no_data
  , CASE from_cs.role WHEN 'Observer' THEN 1 ELSE NULL END AS role_observer
  , CASE from_cs.role WHEN 'Staff' THEN 1 ELSE NULL END AS role_staff
  , CASE from_cs.role WHEN 'Student' THEN 1 ELSE NULL END AS role_student
  , CASE from_cs.role WHEN 'Teacher' THEN 1 ELSE NULL END AS role_teacher
  , CASE from_cs.role WHEN 'TeachingAssistant' THEN 1 ELSE NULL END AS role_teaching_assistant

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
