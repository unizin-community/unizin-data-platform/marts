/*
  Context / Base / Academic career

    Imports Academic career data from the context store.

  Properties:

    Export table:  mart_helper.context__academic_career
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.academic_career AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_career AS k ON e.academic_career_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.academic_career_id AS academic_career_id

  /*
  Attributes
  */
  , from_cs.code AS code
  , from_cs.description AS description

FROM from_cs
;
