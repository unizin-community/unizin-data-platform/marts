/*
  Context / Base / Admission test score

    Imports Admission test score data from the context store.

  Properties:

    Export table:  mart_helper.context__admission_test_score
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.admission_test_score AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.admission_test_score AS k ON e.admission_test_score_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.admission_test_score_id AS admission_test_score_id
  , from_cs.person_id AS person_id

  /*
  Attributes
  */
  , from_cs.admission_test_date AS admission_test_date

  /*
  Admission test type
  */
  -- https://docs.udp.unizin.org/tables/ref_admission_test_type.html
  , from_cs.admission_test_type AS admission_test_type
  , CASE from_cs.admission_test_type WHEN 'Institutional' THEN 1 ELSE NULL END AS admission_test_type_institutional
  , CASE from_cs.admission_test_type WHEN 'NoData' THEN 1 ELSE NULL END AS admission_test_type_no_data
  , CASE from_cs.admission_test_type WHEN 'Standardized' THEN 1 ELSE NULL END AS admission_test_type_standardized

  /*
  Standardized test
  */
  -- https://docs.udp.unizin.org/tables/ref_standardized_test.html
  , from_cs.standardized_test AS standardized_test
  , CASE from_cs.standardized_test WHEN 'ACT' THEN 1 ELSE NULL END AS standardized_test_act
  , CASE from_cs.standardized_test WHEN 'DAT' THEN 1 ELSE NULL END AS standardized_test_dat
  , CASE from_cs.standardized_test WHEN 'GED' THEN 1 ELSE NULL END AS standardized_test_ged
  , CASE from_cs.standardized_test WHEN 'GMAT' THEN 1 ELSE NULL END AS standardized_test_gmat
  , CASE from_cs.standardized_test WHEN 'GRE' THEN 1 ELSE NULL END AS standardized_test_gre
  , CASE from_cs.standardized_test WHEN 'IELTS' THEN 1 ELSE NULL END AS standardized_test_ielts
  , CASE from_cs.standardized_test WHEN 'LSAT' THEN 1 ELSE NULL END AS standardized_test_lsat
  , CASE from_cs.standardized_test WHEN 'MCAT' THEN 1 ELSE NULL END AS standardized_test_mcat
  , CASE from_cs.standardized_test WHEN 'NoData' THEN 1 ELSE NULL END AS standardized_test_no_data
  , CASE from_cs.standardized_test WHEN 'PRAXIS' THEN 1 ELSE NULL END AS standardized_test_praxis
  , CASE from_cs.standardized_test WHEN 'SAT' THEN 1 ELSE NULL END AS standardized_test_sat
  , CASE from_cs.standardized_test WHEN 'SATSubjectTest' THEN 1 ELSE NULL END AS standardized_test_sat_subject_test
  , CASE from_cs.standardized_test WHEN 'TOEFL' THEN 1 ELSE NULL END AS standardized_test_toefl

  /*
  Standardized test component
  */
  -- https://docs.udp.unizin.org/tables/ref_standardized_test_component.html
  -- Too long to count.
  , from_cs.standardized_test_component AS standardized_test_component

  /*
  Standardized test version
  */
  -- https://docs.udp.unizin.org/tables/ref_standardized_test_version.html
  , from_cs.standardized_test_version AS standardized_test_version
  , CASE from_cs.standardized_test_version WHEN 'ACT2015To2016' THEN 1 ELSE NULL END AS standardized_test_version_act_2015_to_2016
  , CASE from_cs.standardized_test_version WHEN 'ACTPost2016' THEN 1 ELSE NULL END AS standardized_test_version_act_post_2016
  , CASE from_cs.standardized_test_version WHEN 'ACTPre2015' THEN 1 ELSE NULL END AS standardized_test_version_act_pre_2016
  , CASE from_cs.standardized_test_version WHEN 'NoData' THEN 1 ELSE NULL END AS standardized_test_version_no_data
  , CASE from_cs.standardized_test_version WHEN 'SATPost2016' THEN 1 ELSE NULL END AS standardized_test_version_sat_post_2016
  , CASE from_cs.standardized_test_version WHEN 'SATPre2016' THEN 1 ELSE NULL END AS standardized_test_version_sat_pre_2016

  , from_cs.institutional_test AS institutional_test
  , from_cs.institutional_test_component AS institutional_test_component
  , from_cs.institutional_test_version AS institutional_test_version

  /*
  Score type
  */
  -- https://docs.udp.unizin.org/tables/ref_admission_test_value_type.html
  , from_cs.score_type AS score_type
  , CASE from_cs.score_type WHEN 'NoData' THEN 1 ELSE NULL END AS score_type_no_data
  , CASE from_cs.score_type WHEN 'Percentile' THEN 1 ELSE NULL END AS score_type_percentile
  , CASE from_cs.score_type WHEN 'Score' THEN 1 ELSE NULL END AS score_type_score

  /*
  Score
  */
  , from_cs.score AS score

FROM from_cs
;
