/*
  Context / Base / Module

    Imports Module data from the context store.

  Properties:

    Export table:  mart_helper.context__module
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.module AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.module AS k ON e.module_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.module_id AS module_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.prerequisite_module_id AS prerequisite_module_id

  /*
    Attributes
  */
  , from_cs.sequential AS sequential
  , from_cs.name AS name
  , LENGTH(from_cs.name) AS length_name
  , from_cs.position AS position

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , CASE from_cs.status WHEN 'active' THEN 1 ELSE NULL END AS is_status_active
  , CASE from_cs.status WHEN 'deleted' THEN 1 ELSE NULL END AS is_status_deleted
  , CASE from_cs.status WHEN 'unpublished' THEN 1 ELSE NULL END AS is_status_unpublished

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date
  , from_cs.unlocked_date AS unlocked_date
  , from_cs.deleted_date AS deleted_date

FROM from_cs

;
