/*
  Context / Base / Course offering academic organization

    Attempts to generate the best approximiation of
    the Academic organization that is most relevant
    to a Course offering.

  Properties:

    Export table:  mart_helper.context__course_offering_academic_organization
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Base / Academic organization
    * Base / Course offering
    * Base / Course section
    * Base / Course section enrollment

  To do:
    *
*/

WITH course_offering_course_section_academic_organization AS (
  SELECT
    COALESCE(cse.le_current_course_offering_id,cse.course_offering_id) AS udp_course_offering_id
    , cse.course_section_id AS udp_course_section_id
    , cs.academic_organization_id AS udp_academic_organization_id
    , CASE WHEN cs.ao_name = '' 
        THEN NULL
      ELSE cs.ao_name 
      END AS academic_organization_name
    , COUNT(1) AS num_sections
  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  INNER JOIN
    _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON cse.course_section_id=cs.course_section_id
  GROUP BY
    udp_course_offering_id
    , udp_course_section_id
    , udp_academic_organization_id
    , academic_organization_name
),

/* Comment out code that uses course section 
ordered_list AS (
  SELECT
    cocsao.udp_course_offering_id AS udp_course_offering_id
    , cocsao.udp_course_section_id AS udp_course_section_id
    , cocsao.udp_academic_organization_id AS udp_academic_organization_id
    , ROW_NUMBER() OVER (
        PARTITION BY
          cocsao.udp_course_offering_id
          , cocsao.udp_academic_organization_id
        ORDER BY num_sections DESC
      ) AS row_number
  FROM
    course_offering_course_section_academic_organization AS cocsao
),

known_available AS (
  SELECT
    ol.udp_course_offering_id AS udp_course_offering_id
    , ol.udp_course_section_id AS udp_course_section_id
    , ol.udp_academic_organization_id AS udp_academic_organization_id
  FROM
    ordered_list AS ol
  WHERE
    row_number = 1 -- Only use one course section; no dupes.
)
*/

  course_offering_academic_organization as (
      SELECT 
        cocsao.udp_course_offering_id AS udp_course_offering_id 
          , array_agg(distinct cocsao.academic_organization_name IGNORE NULLS ORDER BY cocsao.academic_organization_name)
               as academic_organization_array
      FROM 
        course_offering_course_section_academic_organization AS cocsao 
      GROUP BY 
        udp_course_offering_id
)



SELECT
  co.course_offering_id AS udp_course_offering_id
    , coao.academic_organization_array AS academic_organization_array
    , ARRAY_TO_STRING(coao.academic_organization_array, ';\n') as academic_organization_display
  /* comment out course section code
  , ka.udp_course_section_id AS udp_course_section_id
  , ka.udp_academic_organization_id AS udp_academic_organization_id
  */
FROM
  _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co
LEFT JOIN 
  course_offering_academic_organization AS coao ON co.course_offering_id = coao.udp_course_offering_id
--LEFT JOIN known_available AS ka ON co.course_offering_id=ka.udp_course_offering_id
