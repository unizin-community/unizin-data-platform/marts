/*
  Context / Base / Academic term

    Imports Academic term data from the context store.

  Properties:

    Export table:  mart_helper.context__academic_term
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.sis_ext_id AS sis_id
    , k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.academic_term AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.academic_term AS k ON e.academic_term_id=k.id
)

SELECT
  from_cs.sis_id AS sis_id
  , from_cs.lms_id AS lms_id
  , from_cs.academic_term_id AS academic_term_id

  /*
  Attributes
  */
  , from_cs.name AS name
  , LENGTH(from_cs.name) AS length_name
  , from_cs.term_year AS term_year

  /*
  Term type
  */
  -- https://docs.udp.unizin.org/tables/ref_term_type.html
  , from_cs.term_type AS term_type
  , CASE from_cs.term_type WHEN 'NoData' THEN 1 ELSE 0 END AS is_term_type_no_data
  , CASE from_cs.term_type WHEN 'Fall' THEN 1 ELSE 0 END AS is_term_type_fall
  , CASE from_cs.term_type WHEN 'Winter' THEN 1 ELSE 0 END AS is_term_type_winter
  , CASE from_cs.term_type WHEN 'WinterIntersession' THEN 1 ELSE 0 END AS is_term_type_winter_intercession
  , CASE from_cs.term_type WHEN 'Spring' THEN 1 ELSE 0 END AS is_term_type_spring
  , CASE from_cs.term_type WHEN 'SpringIntersession' THEN 1 ELSE 0 END AS is_term_type_spring_intercession
  , CASE from_cs.term_type WHEN 'Summer' THEN 1 ELSE 0 END AS is_term_type_summer
  , CASE from_cs.term_type WHEN 'Summer1' THEN 1 ELSE 0 END AS is_term_type_summer_1
  , CASE from_cs.term_type WHEN 'Summer2' THEN 1 ELSE 0 END AS is_term_type_summer_2
  , CASE from_cs.term_type WHEN 'Other' THEN 1 ELSE 0 END AS is_term_type_other

  /*
  Term begin and end dates.
  */
  , COALESCE(from_cs.term_begin_date,from_cs.le_term_begin_date) AS term_begin_date
  , COALESCE(from_cs.term_end_date,from_cs.le_term_end_date) AS term_end_date
  , DATE_DIFF(COALESCE(from_cs.term_end_date,from_cs.le_term_end_date), COALESCE(from_cs.term_begin_date,from_cs.le_term_begin_date), DAY) AS days_term_length

FROM from_cs
;
