/*
  Context / Base / Participant session

    Imports Participant session data from the context store.

  Properties:

    Export table:  mart_helper.context__participant_session
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.participant_session AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.participant_session AS k ON e.participant_session_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.participant_session_id AS participant_session_id
  , from_cs.course_offering_id AS course_offering_id
  , from_cs.learner_group_id AS learner_group_id

  /*
  Attributes
  */
  , from_cs.title AS title

  , from_cs.actual_start_time AS actual_start_time
  , from_cs.actual_end_time AS actual_end_time
  , from_cs.scheduled_start_time AS scheduled_start_time
  , from_cs.scheduled_end_time AS scheduled_end_time

  , from_cs.duration AS duration

  /*
  Type
  */
  , from_cs.type AS type

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
