/*
  Context / Base / Learning outcome

    Imports Learning outcome data from the context store.

  Properties:

    Export table:  mart_helper.context__learning_outcome
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    None

  To do:
    *
*/

WITH from_cs AS (
  SELECT
    k.lms_ext_id AS lms_id
    , e.*
  FROM _READ_FROM_PROJECT_.context_store_entity.learning_outcome AS e
  INNER JOIN _READ_FROM_PROJECT_.context_store_keymap.learning_outcome AS k ON e.learning_outcome_id=k.id
)

SELECT
  from_cs.lms_id AS lms_id
  , from_cs.learning_outcome_id AS learning_outcome_id
  , from_cs.academic_term_id AS academic_term_id
  , from_cs.course_offering_id AS course_offering_id

  /*
  Attributes
  */
  , from_cs.display_name AS display_name
  , LENGTH(from_cs.display_name) AS length_display_name
  , from_cs.description AS description
  , LENGTH(from_cs.description) AS length_description
  , from_cs.short_desc AS short_desc
  , LENGTH(from_cs.short_desc) AS length_short_desc

  , from_cs.calculation_int AS calculation_int
  , from_cs.calculation_method AS calculation_method

  , from_cs.vendor_guid AS vendor_guid

  /*
  Score
  */
  , from_cs.mastery_points AS mastery_points
  , from_cs.points_possible AS points_possible

  /*
  Status
  */
  -- https://docs.udp.unizin.org/tables/ref_workflow_state.html
  , from_cs.status AS status

  /*
  Dates
  */
  , from_cs.created_date AS created_date
  , from_cs.updated_date AS updated_date

FROM from_cs
;
