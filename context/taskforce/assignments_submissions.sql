/*
  Context / Taskforce / Level1 Assignments Submissions
    Tracks information about weekly assignments and submissions. 
    
  Properties:

    Export table:  mart_helper.context__taskforce__level1_assignments_submissions
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Learner activity result 
    * Context / Base / Learner activity 
    * Context / Base / Learner activity group 
    * Context / Base / Learner activity override   
    * Context / Taskforce / Course profile 
  To do:
    *
*/

WITH initial_data AS (
 SELECT
   DISTINCT
   lar.person_id 
   , lar.learner_activity_result_id 
   , la.learner_activity_id
   , la.course_offering_id 

   /* Learner activity result */
    , lar.type AS submission_type 
    , lar.response_date AS submission_date 
    , lar.graded_date as graded_date
    , lar.posted_at as posted_at_date
    , lar.updated_date AS updated_date
    , CASE 
        WHEN lar.is_grading_status_graded = 1 THEN 'graded'
        WHEN lar.is_grading_status_submitted = 1 THEN 'submitted'
        WHEN lar.is_grading_status_unsubmitted = 1 THEN 'unsubmitted'
        WHEN lar.is_grading_status_pending_review = 1 THEN 'pending_review'
        ELSE NULL 
     END AS grading_status
    , CASE 
        WHEN lar.grade_state_human_graded = 1 THEN 'human_graded'
        WHEN lar.grade_state_not_graded = 1 THEN 'not_graded'
        WHEN lar.grade_state_auto_graded = 1 THEN 'auto_graded'
        ELSE NULL 
     END AS grade_state
    , lar.published_score AS published_score 
    , lar.score AS score
    , CASE 
        WHEN la.points_possible > 0 THEN lar.published_score / la.points_possible 
        ELSE 0 
     END AS published_score_pct
    , lar.score_percentage AS score_pct  

    /* Learner activity */
    , la.title AS learner_activity_title 
    , CASE 
        WHEN laoverride.created_date IS NULL THEN la.due_date
        WHEN lar.response_date < laoverride.created_date THEN la.due_date 
        WHEN lar.response_date >= laoverride.created_date THEN COALESCE(laoverride.due_date,la.due_date)
        ELSE la.due_date 
     END AS due_date 
    , la.points_possible AS points_possible 
    
    /* Learner activity group */
    , lagroup.name AS assignment_group_name 
    , lagroup.group_weight AS assignment_group_weight
    , CASE 
        WHEN (lagroup.group_weight IS NULL OR lagroup.group_weight = 0) THEN 'Unweighted'
        WHEN lagroup.group_weight <= 2 THEN 'Tiny'
        WHEN lagroup.group_weight <= 5 THEN 'Small'
        WHEN lagroup.group_weight <= 10 THEN 'Medium'
        WHEN lagroup.group_weight <= 25 THEN 'Large'
        ELSE 'Major'
     END AS weight_grouping 
     , laoverride.created_date AS override_created_date
   , CASE 
      WHEN lar.response_date IS NOT NULL THEN 1
      WHEN lar.is_grading_status_submitted = 1 THEN 1
      WHEN lar.is_grading_status_unsubmitted = 1 THEN 0
      WHEN lar.graded_date IS NOT NULL AND lar.score > 0 THEN 1 
      ELSE 0
    END AS is_submitted

    /* Quiz */
    , CASE 
        WHEN q.quiz_id IS NOT NULL THEN 1
        ELSE 0
        END AS is_quiz
 FROM
   _WRITE_TO_PROJECT_.mart_helper.context__learner_activity_result AS lar 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__learner_activity AS la ON lar.learner_activity_id = la.learner_activity_id 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON la.course_offering_id = COALESCE(cs.le_current_course_offering_id,cs.course_offering_id)
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse ON lar.person_id = cse.person_id AND cs.course_section_id = cse.course_section_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__learner_activity_group AS lagroup ON la.learner_activity_group_id = lagroup.learner_activity_group_id AND la.course_offering_id = lagroup.course_offering_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__learner_activity_override AS laoverride ON la.learner_activity_id = laoverride.learner_activity_id AND cse.course_section_id = laoverride.course_section_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__quiz AS q ON la.learner_activity_id = q.learner_activity_id

 WHERE
    ((lar.is_grading_status_unsubmitted = 1) OR (lar.is_grading_status_submitted = 1) OR (lar.is_grading_status_graded = 1) OR (lar.is_grading_status_pending_review = 1))
    AND lar.person_id IS NOT NULL
    AND lar.is_excused IS NULL 
    AND la.course_offering_id IS NOT NULL 
    AND la.status = 'published'
    AND la.points_possible > 0 
 )
 
,
initial_data_ordered AS (
  SELECT 
    * EXCEPT(override_created_date)
    , row_number() OVER(PARTITION BY learner_activity_result_id,learner_activity_id,person_id,course_offering_id ORDER BY override_created_date DESC) AS override_created_rank
  FROM initial_data
)


 SELECT
   DISTINCT
   id.* EXCEPT(override_created_rank)
   , CASE 
        WHEN id.is_submitted = 1 AND id.due_date < COALESCE(id.submission_date,id.graded_date,id.posted_at_date) THEN 1 
        ELSE 0
     END AS is_late 
    , CASE 
        WHEN id.is_submitted = 0 AND id.due_date < CURRENT_DATE() THEN 1 
        ELSE 0
     END AS is_missing 
    , CASE 
        WHEN id.is_submitted = 1 THEN DATE_DIFF(id.due_date,COALESCE(id.submission_date,id.graded_date,id.posted_at_date),HOUR) 
        ELSE NULL 
      END AS submission_time_buffer
    , CASE 
        WHEN cp.instruction_begin_date IS NOT NULL THEN DATE_DIFF(COALESCE(id.due_date,id.submission_date,id.graded_date,id.posted_at_date,id.updated_date),cp.instruction_begin_date,WEEK) + 1 
        WHEN cp.term_begin_date IS NOT NULL THEN DATE_DIFF(COALESCE(id.due_date,id.submission_date,id.graded_date,id.posted_at_date,id.updated_date),cp.term_begin_date,WEEK) + 1 
        WHEN cp.course_start_date IS NOT NULL THEN DATE_DIFF(COALESCE(id.due_date,id.submission_date,id.graded_date,id.posted_at_date,id.updated_date),cp.course_start_date,WEEK) + 1
        ELSE NULL 
     END AS week_in_term
    , CASE 
        WHEN cp.instruction_begin_date IS NOT NULL THEN DATE_DIFF(CURRENT_DATE(),cp.instruction_begin_date,WEEK) + 1 
        WHEN cp.term_begin_date IS NOT NULL THEN DATE_DIFF(CURRENT_DATE(),cp.term_begin_date,WEEK) + 1 
        WHEN cp.course_start_date IS NOT NULL THEN DATE_DIFF(CURRENT_DATE(),cp.course_start_date,WEEK) + 1
        ELSE NULL 
    END AS current_week_in_term

 FROM 
    initial_data_ordered AS id 
 INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__course_profile AS cp ON id.course_offering_id = cp.course_offering_id 
WHERE 
    id.override_created_rank = 1
    AND DATE(COALESCE(id.due_date,id.submission_date,id.graded_date,id.posted_at_date,id.updated_date)) >= COALESCE(cp.instruction_begin_date,cp.term_begin_date,cp.course_start_date)
    AND DATE(COALESCE(id.due_date,id.submission_date,id.graded_date,id.posted_at_date,id.updated_date)) <= DATE_ADD(COALESCE(cp.instruction_end_date,cp.term_end_date,cp.course_end_date),INTERVAL 1 WEEK)
