/*
  Context / Taskforce / Weekly discussions
    Tracks information about weekly assignments and submissions. 
    
  Properties:

    Export table:  mart_helper.context__taskforce__level1_assignments_submissions
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Discussion entry  
    * Context / Base / Discussion 
    * Context / Taskforce / Course profile 
  To do:
    *
*/

WITH initial_data AS (
 SELECT
  DISTINCT 
   de.discussion_entry_id 
   , d.discussion_id 
   , de.person_id
   , d.learner_activity_id 
   , d.course_offering_id 
   , cp.academic_term_id 

   /* Week in term */
   , CASE 
        WHEN cp.instruction_begin_date IS NOT NULL THEN DATE_DIFF(de.created_date,cp.instruction_begin_date,WEEK) + 1 
        WHEN cp.term_begin_date IS NOT NULL THEN DATE_DIFF(de.created_date,cp.term_begin_date,WEEK) + 1 
        WHEN cp.course_start_date IS NOT NULL THEN DATE_DIFF(de.created_date,cp.course_start_date,WEEK) + 1
        ELSE NULL 
     END AS week_in_term 

    /* Discussion entry */
    , de.created_date 
    , de.length_body AS message_length 
    , de.position
    
    /* Discussion */ 
    , d.created_date AS discussion_created_date 
    , d.deleted_date AS discussion_deleted_date 
    , d.discussion_type 
    , d.is_locked 
    , d.is_pinned 
    , d.posted_date AS discussion_posted_date
    , d.length_body AS discussion_message_length 
    , d.updated_date AS discussion_updated_date

 FROM
   _WRITE_TO_PROJECT_.mart_helper.context__discussion_entry AS de 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__discussion AS d ON de.discussion_id = d.discussion_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__course_profile AS cp ON d.course_offering_id = cp.course_offering_id 
 WHERE
    d.is_status_active = 1

 )
 
,
discussion_count AS (
 SELECT
   course_offering_id 
   , academic_term_id 
   , week_in_term 

   /* Total discussion counts */
   , COUNT(DISTINCT discussion_id) AS total_discussion_count 
   , COUNT(DISTINCT CASE WHEN learner_activity_id IS NOT NULL THEN discussion_id ELSE NULL END) AS total_assignment_discussion_count 
   , COUNT(DISTINCT CASE WHEN discussion_type = 'threaded' THEN discussion_id ELSE NULL END) AS total_threaded_discussion_count 
   , COUNT(DISTINCT CASE WHEN discussion_type = 'side_comment' THEN discussion_id ELSE NULL END) AS total_side_comment_discussion_count 
 FROM 
    initial_data 
GROUP BY 
    course_offering_id 
    , academic_term_id
    , week_in_term 
)

,
discussion_entry_count AS (
 SELECT
   person_id
   , course_offering_id 
   , academic_term_id 
   , week_in_term 
   /* Discussion entry counts */
   , COUNT(DISTINCT discussion_entry_id) AS discussion_entry_count 
   , COUNTIF(position = 1) AS discussion_post_count 
   , COUNTIF(position > 1) AS discussion_reply_count
   
   /* Discussion counts */
   , COUNT(DISTINCT discussion_id) AS  discussion_count
   , COUNT(DISTINCT CASE WHEN learner_activity_id IS NOT NULL THEN discussion_id ELSE NULL END) AS assignment_discussion_count 
   , COUNT(DISTINCT CASE WHEN discussion_type = 'threaded' THEN discussion_id ELSE NULL END) AS threaded_discussion_count 
   , COUNT(DISTINCT CASE WHEN discussion_type = 'side_comment' THEN discussion_id ELSE NULL END) AS side_comment_discussion_count 

   /* Discussion entry lengths */
   , AVG(message_length) AS avg_discussion_entry_length
   , AVG(CASE WHEN position = 1 THEN message_length ELSE NULL END) AS avg_discussion_post_length
   , AVG(CASE WHEN position > 1 THEN message_length ELSE NULL END) AS avg_discussion_reply_length 
  FROM 
    initial_data 
GROUP BY 
    person_id
    , course_offering_id 
    , academic_term_id
    , week_in_term 
)

 
 SELECT 
    dec.person_id AS person_id 
    , dec.course_offering_id AS course_offering_id
    , dec.academic_term_id AS academic_term_id
    , dec.week_in_term AS week_in_term 
    
    /* Discussion entry counts */
    , dec.discussion_entry_count AS discussion_entry_count 
    , dec.discussion_post_count AS discussion_post_count 
    , dec.discussion_reply_count AS discussion_reply_count 

    /* Discussion counts */
    , dec.discussion_count AS discussion_count 
    , dec.assignment_discussion_count AS assignment_discussion_count 
    , dec.threaded_discussion_count AS threaded_discussion_count 
    , dec.side_comment_discussion_count AS side_comment_discussion_count 

    /* Total discussion counts */
    , dc.total_discussion_count AS total_discussion_count 
    , dc.total_assignment_discussion_count AS total_assignment_discussion_count
    , dc.total_threaded_discussion_count AS total_threaded_discussion_count
    , dc.total_side_comment_discussion_count AS total_side_comment_discussion_count

    /* Discussion entry lengths */
    , dec.avg_discussion_entry_length AS avg_discussion_entry_length
    , dec.avg_discussion_post_length AS avg_discussion_post_length 
    , dec.avg_discussion_reply_length AS avg_discussion_reply_length
 FROM
    discussion_entry_count AS dec 
 INNER JOIN 
    discussion_count AS dc ON dec.course_offering_id = dc.course_offering_id AND dec.academic_term_id = dc.academic_term_id AND dec.week_in_term = dc.week_in_term 
