/*
  Context / Taskforce / Level1 Assignments Submissions
    Tracks information about weekly assignments and submissions. 
    
  Properties:

    Export table:  mart_helper.context__taskforce__level1_assignments_submissions
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Learner activity result 
    * Context / Base / Learner activity 
    * Context / Base / Learner activity group 
    * Context / Base / Learner activity override   
    * Context / Taskforce / Course profile 
  To do:
    *
*/

WITH 
enrollments_grades_courses_updated AS (
SELECT 
    *
    , COALESCE(
      DATE_DIFF(DATE_ADD(instruction_end_date,INTERVAL 1 WEEK),instruction_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(term_end_date,INTERVAL 1 WEEK),term_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(course_end_date,INTERVAL 1 WEEK),course_start_date,WEEK)) + 1 
    AS term_length_weeks 
FROM 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__enrollments_grades_courses
)

,
student_course_weeks as (
SELECT 
  person_id,
  course_offering_id,
  week_in_term
FROM 
    enrollments_grades_courses_updated en, UNNEST(GENERATE_ARRAY(1,COALESCE(term_length_weeks,17))) AS week_in_term
)

,
weekly_cumulative_scores as (
select 
  scw.person_id
  , scw.course_offering_id
  , scw.week_in_term
  , SUM(CASE WHEN weight_grouping = "Tiny" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Tiny" THEN assignment_group_weight ELSE NULL END) AS avg_published_score_pct_tiny_cumulative
  , SUM(CASE WHEN weight_grouping = "Small" THEN  assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Small" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_small_cumulative
   , SUM(CASE WHEN weight_grouping = "Medium" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Medium" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_medium_cumulative
   , SUM(CASE WHEN weight_grouping = "Large" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Large" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_large_cumulative
   , SUM(CASE WHEN weight_grouping = "Major" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Major" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_major_cumulative
   , SUM(CASE WHEN weight_grouping = "Unweighted" THEN published_score ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Unweighted" THEN points_possible ELSE NULL END) AS avg_score_pct_unweighted_cumulative
   , SUM(CASE WHEN weight_grouping <> 'Unweighted' THEN assignment_group_weight*published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping <> 'Unweighted' THEN assignment_group_weight ELSE NULL END) AS avg_published_score_pct_weighted_cumulative
   , CASE 
      WHEN SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight END) = 0 
        THEN SUM(CASE WHEN due_date IS NULL THEN published_score END)/SUM(CASE WHEN due_date IS NULL THEN points_possible END) 
      WHEN SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight END) > 0 
        THEN SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight*published_score_pct END)/SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight END) 
      END AS avg_published_score_pct_without_due_date_cumulative
   , CASE 
      WHEN SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight END) = 0 
        THEN SUM(CASE WHEN due_date IS NOT NULL THEN published_score END)/SUM(CASE WHEN due_date IS NOT NULL THEN points_possible END) 
      WHEN SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight END) > 0 
        THEN SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight*published_score_pct END)/SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight END) 
      END AS avg_published_score_pct_with_due_date_cumulative
   , CASE 
      WHEN SUM(assignment_group_weight) = 0 
        THEN SUM(published_score)/SUM(points_possible) 
      WHEN SUM(assignment_group_weight) > 0 
        THEN SUM(assignment_group_weight*published_score_pct)/SUM(assignment_group_weight) 
      END AS avg_published_score_cumulative
from student_course_weeks scw 
left join _WRITE_TO_PROJECT_.mart_helper.context__taskforce__assignments_submissions tas on scw.person_id = tas.person_id and scw.course_offering_id = tas.course_offering_id and scw.week_in_term >= tas.week_in_term
group by scw.person_id,scw.course_offering_id,scw.week_in_term
order by scw.person_id,scw.course_offering_id,scw.week_in_term
)


,
weekly_metrics AS (

 SELECT 
    person_id AS person_id 
    , course_offering_id AS course_offering_id
    , week_in_term AS week_in_term 

   /* Submissions */
   , COUNTIF(weight_grouping = "Tiny" AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_tiny_submissions
   , COUNTIF(weight_grouping = "Small" AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_small_submissions
   , COUNTIF(weight_grouping = "Medium" AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_medium_submissions
   , COUNTIF(weight_grouping = "Large" AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_large_submissions
   , COUNTIF(weight_grouping = "Major" AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_major_submissions
   , COUNTIF(weight_grouping = 'Unweighted' AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_unweighted_submissions
   , COUNTIF(weight_grouping <> 'Unweighted' AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_weighted_submissions
   , COUNTIF(due_date IS NULL AND is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_submissions_without_due_date 
   , COUNTIF(due_date IS NOT NULL AND is_submitted = 1 AND week_in_term <= current_week_in_term) num_submissions_with_due_date
   , COUNTIF(is_submitted = 1 AND week_in_term <= current_week_in_term) AS num_submissions

   /* Assignments due */
   , COUNTIF(weight_grouping = "Tiny") AS num_tiny_assignments
   , COUNTIF(weight_grouping = "Small") AS num_small_assignments
   , COUNTIF(weight_grouping = "Medium") AS num_medium_assignments
   , COUNTIF(weight_grouping = "Large") AS num_large_assignments
   , COUNTIF(weight_grouping = "Major") AS num_major_assignments
   , COUNTIF(weight_grouping = 'Unweighted') AS num_unweighted_assignments
   , COUNTIF(weight_grouping <> 'Unweighted') AS num_weighted_assignments
   , COUNTIF(due_date IS NULL) AS num_assignments_without_due_date 
   , COUNTIF(due_date IS NOT NULL) num_assignments_with_due_date
   , COUNT(*) AS num_assignments

   /* Missing assignments */
   , COUNTIF(weight_grouping = "Tiny" AND is_missing = 1) AS num_tiny_missing_submissions
   , COUNTIF(weight_grouping = "Small" and is_missing = 1) AS num_small_missing_submissions
   , COUNTIF(weight_grouping = "Medium" and is_missing = 1) AS num_medium_missing_submissions
   , COUNTIF(weight_grouping = "Large" and is_missing = 1) AS num_large_missing_submissions
   , COUNTIF(weight_grouping = "Major" and is_missing = 1) AS num_major_missing_submissions
   , COUNTIF(weight_grouping = 'Unweighted' AND is_missing = 1) AS num_unweighted_missing_submissions
   , COUNTIF(weight_grouping <> 'Unweighted' AND is_missing = 1) AS num_weighted_missing_submissions
   , COUNTIF(is_missing = 1) AS num_missing_submissions

   /* Late assignments */
   , COUNTIF(weight_grouping = "Tiny" AND is_late = 1) AS num_tiny_late_submissions
   , COUNTIF(weight_grouping = "Small" AND is_late = 1) AS num_small_late_submissions
   , COUNTIF(weight_grouping = "Medium" AND is_late = 1) AS num_medium_late_submissions
   , COUNTIF(weight_grouping = "Large" AND is_late = 1) AS num_large_late_submissions
   , COUNTIF(weight_grouping = "Major" AND is_late = 1) AS num_major_late_submissions
   , COUNTIF(weight_grouping = 'Unweighted' AND is_late = 1) AS num_unweighted_late_submissions
   , COUNTIF(weight_grouping <> 'Unweighted' AND is_late = 1) AS num_weighted_late_submissions
   , COUNTIF(is_late = 1) AS num_late_submissions

   /* Average assignment submission buffer */
   , AVG(CASE WHEN weight_grouping = "Tiny" THEN submission_time_buffer ELSE NULL END) AS avg_time_buffer_hrs_tiny
   , AVG(CASE WHEN weight_grouping = "Small" THEN submission_time_buffer ELSE NULL END) AS avg_time_buffer_hrs_small
   , AVG(CASE WHEN weight_grouping = "Medium" THEN submission_time_buffer ELSE NULL END) AS avg_time_buffer_hrs_medium
   , AVG(CASE WHEN weight_grouping = "Large" THEN submission_time_buffer ELSE NULL END) AS avg_time_buffer_hrs_large
   , AVG(CASE WHEN weight_grouping = "Major" THEN submission_time_buffer ELSE NULL END) AS avg_time_buffer_hrs_major
   , AVG(CASE WHEN weight_grouping = 'Unweighted' THEN submission_time_buffer ELSE NULL END) AS avg_time_buffer_hrs_unweighted
   , AVG(CASE WHEN weight_grouping <> 'Unweighted' THEN submission_time_buffer ELSE NULL END) AS avg_time_buffer_hrs_weighted
   , AVG(submission_time_buffer) AS avg_time_buffer_hrs

   /* Learner activity score */
   , SUM(CASE WHEN weight_grouping = "Tiny" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Tiny" THEN assignment_group_weight ELSE NULL END) AS avg_published_score_pct_tiny
   , SUM(CASE WHEN weight_grouping = "Small" THEN  assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Small" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_small
   , SUM(CASE WHEN weight_grouping = "Medium" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Medium" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_medium
   , SUM(CASE WHEN weight_grouping = "Large" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Large" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_large
   , SUM(CASE WHEN weight_grouping = "Major" THEN assignment_group_weight * published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Major" THEN  assignment_group_weight ELSE NULL END) AS avg_published_score_pct_major
   , SUM(CASE WHEN weight_grouping = "Unweighted" THEN published_score ELSE NULL END) / SUM(CASE WHEN weight_grouping = "Unweighted" THEN points_possible ELSE NULL END) AS avg_score_pct_unweighted
   , SUM(CASE WHEN weight_grouping <> 'Unweighted' THEN assignment_group_weight*published_score_pct ELSE NULL END) / SUM(CASE WHEN weight_grouping <> 'Unweighted' THEN assignment_group_weight ELSE NULL END) AS avg_published_score_pct_weighted
   , CASE 
      WHEN SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight END) = 0 
        THEN SUM(CASE WHEN due_date IS NULL THEN published_score END)/SUM(CASE WHEN due_date IS NULL THEN points_possible END) 
      WHEN SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight END) > 0 
        THEN SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight*published_score_pct END)/SUM(CASE WHEN due_date IS NULL THEN assignment_group_weight END) 
      END AS avg_published_score_pct_without_due_date
   , CASE 
      WHEN SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight END) = 0 
        THEN SUM(CASE WHEN due_date IS NOT NULL THEN published_score END)/SUM(CASE WHEN due_date IS NOT NULL THEN points_possible END) 
      WHEN SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight END) > 0 
        THEN SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight*published_score_pct END)/SUM(CASE WHEN due_date IS NOT NULL THEN assignment_group_weight END) 
      END AS avg_published_score_pct_with_due_date
   ,  CASE 
      WHEN SUM(assignment_group_weight) = 0 
        THEN SUM(published_score)/SUM(points_possible) 
      WHEN SUM(assignment_group_weight) > 0 
        THEN SUM(assignment_group_weight*published_score_pct)/SUM(assignment_group_weight) 
      END AS avg_published_score

 FROM
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__assignments_submissions
GROUP BY 
    person_id 
    , course_offering_id 
    , week_in_term 
)

SELECT 
  wcs.person_id,
  wcs.course_offering_id,
  wcs.week_in_term,
  wm.* except(person_id,course_offering_id,week_in_term),
  wcs.* except(person_id,course_offering_id,week_in_term)
FROM weekly_cumulative_scores wcs 
LEFT JOIN weekly_metrics wm ON wcs.person_id = wm.person_id AND wcs.course_offering_id = wm.course_offering_id AND wcs.week_in_term = wm.week_in_term