/*
  Context / Taskforce / Student term profile
    Describes the profile of a student in an academic term. 
    
  Properties:

    Export table:  mart_helper.context__taskforce__student_term_profile
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Person academic term
    * Context / Base / Person  
    * Context / Base / Academic term 
    * Context / Base / Person academic major academic term 
    * Context / Base / Person academic minor academic term 
    * Context / Base / Academic major 
    * Context / Base / Academic minor
    * Context / Base / Academic career 
    * Context / Base / Institutional affiliation 
    
  To do:
    *
*/

SELECT  
    pat.person_id AS person_id
    , pat.academic_term_id AS academic_term_id

    /* Student academic term */
    , pat.athletic_participant_sport AS athletic_participant_sport
    , pat.cen_academic_level AS cen_academic_level
    , pat.eot_academic_load AS eot_academic_load
    , pat.gpa_academic_term AS gpa_academic_term
    , pat.gpa_cumulative AS gpa_cumulative
    , pat.in_honors_program AS in_honors_program
    , pat.registration_status AS registration_status
    , pat.us_residency AS us_residency 
    
    /* Student */
    , p.is_english_first_language AS is_english_first_language
    , p.is_first_generation_hed_student AS is_first_generation_hed_student
    , CASE 
        WHEN p.is_american_indian_alaska_native = 1 THEN 'Alaskan / American Native'
        WHEN p.is_asian = 1 THEN 'Asian'
        WHEN p.is_black_african_american = 1 THEN 'Black / African American'
        WHEN p.is_hawaiian_pacific_islander = 1 THEN 'Hawaiian / Pacific Islander'
        WHEN p.is_hispanic_latino = 1 THEN 'Hispanic / Latino'
        WHEN p.is_no_race_indicated = 1 THEN 'No Race Indicated'
        WHEN p.is_other_american = 1 THEN 'Some Other Race'
        WHEN p.is_white = 1 THEN 'White'
        ELSE 'Unknown'
     END AS race_indication 
    , CASE 
        WHEN p.is_white = 1 THEN 0
        ELSE 1 
     END AS is_minority 
    , p.sex AS sex 

    /* Academic term */
    , act.name AS term_name
    , act.term_type AS term_type
    , act.term_year AS term_year
    , act.term_begin_date AS term_begin_date
    , act.term_end_date AS term_end_date

    /* Academic major */
    , am.description AS academic_major_name 

    /* Academic minor */
    , am2.description AS academic_minor_name 

    /* Academic career */
    , ac.description AS academic_career_name 
    
    /* Institutional affiliation */
    , ia.directory_block AS directory_block

FROM 
    _WRITE_TO_PROJECT_.mart_helper.context__person__academic_term AS pat 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON pat.person_id = p.person_id 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS act ON pat.academic_term_id = act.academic_term_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__person__academic_major__academic_term AS pamat ON pat.person_id = pamat.person_id AND pat.academic_term_id = pamat.academic_term_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__person__academic_minor__academic_term AS pamat2 ON pat.person_id = pamat2.person_id AND pat.academic_term_id = pamat2.academic_term_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__academic_major AS am ON pamat.academic_major_id = am.academic_major_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__academic_minor AS am2 ON pamat2.academic_minor_id = am2.academic_minor_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__academic_career AS ac ON pat.academic_career_id = ac.academic_career_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__institutional_affiliation AS ia ON pat.person_id = ia.person_id 
