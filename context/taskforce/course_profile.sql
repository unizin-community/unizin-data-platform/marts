/*
  Context / Taskforce / Course profile
    Describes the profile of Course sections. 
    
  Properties:

    Export table:  mart_helper.context__taskforce__course_profile
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course section
    * Context / Base / Course offering 
    * Context / Base / Academic term 
    * Context / Base / Academic career 
    * Context / Course section / Enrollment 
    * Context / Course offering / Entity / Learner activity 
    * Context / Course offering / Entity / Module 
    * Context / Course offering / Entity / Quiz 
    * Context / Course offering / Entity / Discussion 
    * Context / Course offering / Entity / Learner activity result 
    * Context / Course offering / Entity / Quiz result 

  To do:
    *
*/

SELECT  
    cs.course_section_id AS course_section_id
    , co.course_offering_id AS course_offering_id
    , acs.academic_session_id AS academic_session_id
    , act.academic_term_id AS academic_term_id

    /* Course offering */
    , co.title AS course_title
    , co.subject AS course_subject
    , co.number AS course_number
    , CONCAT(co.subject,' ',co.number) AS course_code
    , CASE 
        WHEN SAFE_CAST(LEFT(co.number,1) AS INT64) IS NOT NULL THEN LEFT(co.number,1)
        ELSE 'Unknown'
      END AS course_level
    , co.start_date AS course_start_date
    , co.end_date AS course_end_date
    , co.available_credits

    /* Course section */
    , cs.delivery_mode AS delivery_mode
    , cs.is_graded AS is_graded
    , cs.is_honors AS is_honors
    , cs.max_enrollment AS max_enrollment
    , cs.type AS course_type 

    /* Academic session */
    , acs.name AS session_name
    , acs.instruction_begin_date AS instruction_begin_date
    , acs.instruction_end_date AS instruction_end_date

    /* Academic term */
    , act.name AS term_name
    , act.term_type AS term_type
    , act.term_year AS term_year
    , act.term_begin_date AS term_begin_date
    , act.term_end_date AS term_end_date

    /* Academic organization */
    , cs.ao_name AS program_name

    /* Academic career information */
    , ac.description AS academic_career_name

    /* Instructor enrollments */
    , cse.instructor_name_array AS instructor_name_array
    , cse.num_instructors AS num_instructors

    /* Student enrollments */
    , cse.num_students AS num_students

    /* Assignments */
    , la.count_is_status_published AS num_published_assignments
    , la.count_is_status_unpublished AS num_unpublished_assignments
    , lar.score_percentage AS avg_assignment_score

    /* Modules */
    , m.count_is_status_active AS num_active_modules
    , m.count_is_status_unpublished AS num_unpublished_modules

    /* Quizzes */
    , q.count_is_status_published AS num_published_quizzes
    , q.count_is_status_unpublished AS num_unpublished_quizzes
    , qr.avg_score_as_percentage AS avg_quiz_score

    /* Discussions */
    , d.count_is_status_active AS num_active_discussions    
    , d.count_is_status_unpublished AS num_unpublished_discussions

FROM 
    _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON COALESCE(cs.le_current_course_offering_id,cs.course_offering_id) = co.course_offering_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__academic_session AS acs ON co.academic_session_id = acs.academic_session_id 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS act ON COALESCE(co.academic_term_id,acs.academic_term_id) = act.academic_term_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__academic_career AS ac ON co.academic_career_id = ac.academic_career_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_section__enrollment AS cse ON cs.course_section_id = cse.course_section_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__learner_activity AS la ON co.course_offering_id = la.course_offering_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__module AS m ON co.course_offering_id = m.course_offering_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__quiz AS q ON co.course_offering_id = q.course_offering_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__discussion AS d ON co.course_offering_id = d.course_offering_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__learner_activity_result AS lar ON co.course_offering_id = lar.course_offering_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__quiz_result AS qr ON co.course_offering_id = qr.course_offering_id
