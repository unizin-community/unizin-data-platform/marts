/*
  Context / Taskforce / Enrollments grades courses
    Tracks information about student enrollments and grades in courses. 
    
  Properties:

    Export table:  mart_helper.context__taskforce__enrollments_grades_courses
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    * Context / Base / Course section enrollment 
    * Context / Base / Course grade  
    * Context / Taskforce / Course profile 
    * Context / Taskforce / Student term profile     
  To do:
    *
*/

WITH student_enrollments AS (
 SELECT
   cse.person_id 
   , cse.course_section_id 
   , COALESCE(cse.le_current_course_offering_id,cse.course_offering_id) as course_offering_id
   , cse.academic_term_id
   , cse.created_date 
   , cse.entry_date 
   , cse.updated_date  
   , cse.exit_date 
   , cse.credits_taken 
   , cse.role_status
   , ARRAY_AGG(cse.role_status) OVER(PARTITION BY cse.person_id,cse.course_section_id) AS role_status_array
 FROM
   _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
 WHERE
   cse.role = 'Student'
 )
 
,
/* Define the new field enrollment_status based on the role_status_array */
student_enrollments_status AS (
 SELECT
   DISTINCT
     se.* EXCEPT(role_status_array)
   /* If there is only one role_status for the enrollment, define enrollment_status as
      the role_status */
     , CASE
         WHEN ARRAY_LENGTH(se.role_status_array) = 1
           THEN se.role_status_array[ORDINAL(1)]
         /* Define enrollment_status based on role_status_array */
         WHEN 'Completed' IN UNNEST(se.role_status_array)
           THEN 'Completed'
         WHEN 'Withdrawn' IN UNNEST(se.role_status_array)
           THEN 'Withdrawn'
         WHEN 'Dropped' IN UNNEST(se.role_status_array)
           THEN 'Dropped'
         WHEN 'Not-enrolled' IN UNNEST(se.role_status_array)
           THEN 'Not-enrolled'
         WHEN 'Enrolled' IN UNNEST(se.role_status_array)
           THEN 'Enrolled'
         ELSE NULL
       END AS enrollment_status
 FROM 
    student_enrollments AS se
)
 
,

student_enrollments_updated AS (
 SELECT
   ses.* EXCEPT(role_status)
 FROM
   student_enrollments_status AS ses
 WHERE
   ses.role_status = ses.enrollment_status
)


SELECT  
    seu.person_id AS person_id
    , seu.course_section_id AS course_section_id 
    , seu.course_offering_id AS course_offering_id
    , seu.academic_term_id AS academic_term_id 
    , cp.academic_session_id AS academic_session_id

    /* Student enrollment */
    , seu.enrollment_status AS enrollment_status 
    , seu.credits_taken AS credits_taken

    /* Course offering */
    , cp.course_title AS course_title 
    , cp.course_subject AS course_subject 
    , cp.course_start_date AS course_start_date 
    , cp.course_end_date AS course_end_date 
    , cp.available_credits AS course_available_credits 
    
    /* Academic session */
    , cp.session_name AS session_name 
    , cp.instruction_begin_date AS instruction_begin_date
    , cp.instruction_end_date AS instruction_end_date

    /* Academic term */
    , cp.term_name AS term_name 
    , cp.term_begin_date AS term_begin_date 
    , cp.term_end_date AS term_end_date

    /* Student academic term */
    , stp.registration_status AS registration_status 
    , stp.cen_academic_level AS cen_academic_level 
    , stp.eot_academic_load AS eot_academic_load 
    , stp.gpa_cumulative AS gpa_cumulative 

    /* Course grade */
    , cg.grade_on_official_transcript AS grade_on_official_transcript 
    , cg.grade_points_per_credit AS grade_points_per_credit 
    , 

FROM 
    student_enrollments_updated AS seu 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__course_profile AS cp ON seu.course_offering_id = cp.course_offering_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__student_term_profile AS stp ON seu.person_id = stp.person_id AND seu.academic_term_id = stp.academic_term_id 
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_grade AS cg ON seu.person_id = cg.person_id AND seu.course_section_id = cg.course_section_id 
