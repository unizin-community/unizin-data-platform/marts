/*
  Context / Student activity score / Student term academic level

    Defines the academic level for a student in the academic term.

  Properties:

    Export table:  mart_helper.context__student_activity_score__student_term_academic_level
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    - mart_helper.context__person__academic_term
    - mart_helper.context__person
    - mart_helper.context__academic_term 
    - mart_helper.context__academic_career

  To do:
    *
*/


WITH student_term_academic_level AS (
SELECT 
  p.sis_id AS university_id,
  act.name AS term_name,
  acc.code AS academic_level_code,
  acc.description AS academic_level_description,
FROM _WRITE_TO_PROJECT_.mart_helper.context__person__academic_term AS pat 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person as p on pat.person_id = p.person_id 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS act ON pat.academic_term_id = act.academic_term_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_career AS acc ON pat.academic_career_id = acc.academic_career_id
)

SELECT 
  DISTINCT 
  stac.university_id
  , stac.term_name 
  , stac.academic_level_code
  , stac.academic_level_description
FROM student_term_academic_level AS stac
;
