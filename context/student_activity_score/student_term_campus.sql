/*
  Context / Student activity score / Student term campus

    Defines the campus for a student in the academic term.

  Properties:

    Export table:  mart_helper.context__student_activity_score__student_term_campus
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    - mart_helper.context__person__academic_term
    - mart_helper.context__person
    - mart_helper.context__academic_term 
    - mart_helper.context__academic_career
    - mart_helper.context__course_section_enrollment
    - mart_helper.context__course_section
    - mart_helper.context__course_offering
    - mart_helper.context__campus
  To do:
    *
*/

WITH student_term_campus as (
  SELECT 
    DISTINCT 
    p.sis_id AS university_id,
    act.name AS term_name,
    /* For IU campuses */
    CASE 
      WHEN acc.code LIKE 'IUSEA%' THEN 'Southeast'
      WHEN acc.code LIKE 'IUINA%' THEN 'IUPUI'
      WHEN acc.code LIKE 'IUNWA%' THEN 'Northwest'
      WHEN acc.code LIKE 'IUFWA%' THEN 'IPFW'
      WHEN acc.code LIKE 'IUBLA%' THEN 'Bloomington'
      WHEN acc.code LIKE 'IUFTW%' THEN 'Fort Wayne'
      WHEN acc.code LIKE 'PUFWA%' THEN 'Purdue U Indiana U Fort Wayne'
      WHEN acc.code LIKE 'IUCSA%' THEN 'Continuing Study'
      WHEN acc.code LIKE 'IUEAA%' THEN 'East'
      WHEN acc.code LIKE 'IUCOA%' THEN 'IUPUC'
      WHEN acc.code LIKE 'IUKOA%' THEN 'Kokomo'
      WHEN acc.code LIKE 'IUSBA%' THEN 'South Bend'
    END AS campus_name
  FROM _WRITE_TO_PROJECT_.mart_helper.context__person__academic_term AS pat
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS act ON pat.academic_term_id = act.academic_term_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_career AS acc ON pat.academic_career_id = acc.academic_career_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person as p on pat.person_id = p.person_id 
)

,
student_term_campus_counts AS (
  SELECT 
    p.sis_id as university_id,
    act.name as term_name,
    cam.name as campus_name,
    COUNT(*),
    ROW_NUMBER() OVER (PARTITION BY p.sis_id,act.name,cam.name ORDER BY COUNT(*) DESC) as seq_num
  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment cse 
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section cs on cse.course_section_id = cs.course_section_id 
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering co on cs.course_offering_id = co.course_offering_id 
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term act on co.academic_term_id = act.academic_term_id 
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__campus cam on co.campus_id = cam.campus_id 
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person p on cse.person_id = p.person_id 
  WHERE 
    role = 'Student'
    AND cse.role_status = 'Enrolled'
    AND p.sis_id is not null 
  GROUP BY 
    p.sis_id,
    act.name,
    cam.name
  ORDER BY 
    p.sis_id,
    act.name
)

,
student_term_first_campus AS (
  SELECT 
    university_id,
    term_name,
    campus_name
  FROM student_term_campus_counts
  WHERE seq_num = 1
)

,
new_student_term_campus as (
  SELECT  
    DISTINCT 
    stfc.university_id,
    stfc.term_name,
    stfc.campus_name
  FROM student_term_first_campus stfc
  LEFT JOIN student_term_campus stc ON stc.university_id = stfc.university_id AND stc.term_name = stfc.term_name 
  WHERE stc.university_id IS NULL OR stc.term_name IS NULL
  ORDER BY 
    stfc.university_id,
    stfc.term_name
)

SELECT *
FROM student_term_campus 
UNION ALL 
SELECT *
FROM new_student_term_campus
