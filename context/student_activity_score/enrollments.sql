/*
  Context / Student activity score / Enrollments

    Defines the enrollments of students in a course.

  Properties:

    Export table:  mart_helper.context__student_activity_score__enrollments
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    - mart_helper.context__course_section_enrollment
    - mart_helper.context__course_section
    - mart_helper.context__course_offering
    - mart_helper.context__academic_term
    - mart_helper.context__person

  To do:
    *
*/


SELECT 
  DISTINCT
  p.sis_id AS university_id
  , p.person_id AS global_user_id
  , p.lms_id AS canvas_user_id
  , co.lms_id AS course_canvas_id
  , co.course_offering_id AS course_global_id
  , co.sis_id AS course_code
  , cs.lms_id as course_section_canvas_id
  , cs.course_section_id as course_section_global_id
  , cs.sis_id as course_section_code
  , act.name AS term_name
  , acs.name AS session_name
  , COALESCE(DATE_DIFF(instruction_end_date,instruction_begin_date,WEEK),DATE_DIFF(term_end_date,term_begin_date,WEEK),DATE_DIFF(co.end_date,co.start_date,WEEK),16) + 1 AS term_length
FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment cse 
JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section cs ON cse.course_section_id = cs.course_section_id 
JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering co ON cs.le_current_course_offering_id = co.course_offering_id 
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session acs on co.academic_session_id = acs.academic_session_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term act on co.academic_term_id = act.academic_term_id
JOIN _WRITE_TO_PROJECT_.mart_helper.context__person p ON cse.person_id = p.person_id 
WHERE cse.role = 'Student'
  AND cse.role_status = 'Enrolled'
  --AND p.sis_id IS NOT NULL 
  AND co.sis_id IS NOT NULL 
ORDER BY 
  p.sis_id,
  co.sis_id
