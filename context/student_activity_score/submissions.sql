/*
  Context / Student activity score / Submissions

    Defines the submissions for a student in a course.

  Properties:

    Export table:  mart_helper.context__student_activity_score__submissions
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:
    - mart_helper.context__course_offering
    - mart_helper.context__learner_activity
    - mart_helper.context__academic_term
    - mart_helper.context__learner_activity_result

  To do:
    *
*/

WITH assignments as (
  SELECT 
    co.course_offering_id AS course_global_id,
    co.sis_id AS course_code,
    la.learner_activity_id AS assignment_id,
    la.title AS assignment_title,
    la.due_date AS assignment_due_date,
    CASE 
      WHEN acs.instruction_begin_date IS NOT NULL THEN DATE_DIFF(la.due_date,acs.instruction_begin_date,WEEK) + 1
      WHEN act.term_begin_date IS NOT NULL THEN DATE_DIFF(la.due_date,act.term_begin_date,WEEK) + 1 
      WHEN co.start_date IS NOT NULL THEN DATE_DIFF(la.due_date,co.start_date,WEEK) + 1
        ELSE NULL 
    END AS week_number

  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co 
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity AS la ON co.course_offering_id = la.course_offering_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session AS acs ON co.academic_session_id = acs.academic_session_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS act ON co.academic_term_id = act.academic_term_id
  WHERE la.due_date IS NOT NULL
    AND la.allowable_submission_types NOT IN ('on_paper','','Assignments','not_graded','none', 'external_tool')
    AND la.points_possible > 0
    AND la.points_possible IS NOT NULL
    AND la.status = 'published'
    AND la.due_date <= @run_time
    AND la.due_date >= COALESCE(acs.instruction_begin_date,act.term_begin_date,co.start_date)
    AND la.due_date <= COALESCE(acs.instruction_end_date,act.term_end_date,co.end_date)

  ) 

, submissions as (
    SELECT 
      assn.course_global_id,
      assn.course_code,
      assn.assignment_id,
      assn.assignment_title,
      assn.assignment_due_date,
      assn.week_number,
      lar.person_id AS user_id,
      lar.learner_activity_result_id AS submission_id,
      lar.response_date AS submitted_at
    FROM assignments AS assn 
    INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity_result AS lar ON assn.assignment_id = lar.learner_activity_id
    WHERE 
      lar.is_grading_status_unsubmitted IS DISTINCT FROM 1
      AND lar.is_grading_status_deleted IS DISTINCT FROM 1
      AND lar.response_date IS NOT NULL
      AND lar.response_date <= @run_time
  )

SELECT
  sub.course_global_id,
  sub.course_code,
  sub.user_id,
  sub.week_number,
  COUNT(DISTINCT sub.submission_id) AS submissions,
  COUNT(DISTINCT prev_sub.submission_id) AS submissions_cumulative
FROM
  submissions sub 
LEFT JOIN 
  submissions prev_sub ON sub.course_global_id = prev_sub.course_global_id AND sub.user_id = prev_sub.user_id and sub.week_number >= prev_sub.week_number
GROUP BY
  course_global_id,
  course_code,
  user_id,
  week_number
