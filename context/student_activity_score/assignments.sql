/*
  Context / Student activity score / Assignments

    Defines the assignments for a course.

  Properties:

    Export table:  mart_helper.context__student_activity_score__assignments
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:
    - mart_helper.context__course_offering
    - mart_helper.context__learner_activity
    - mart_helper.context__academic_term

  To do:
    *
*/

WITH assignments AS (
  SELECT 
  co.course_offering_id AS course_global_id,
  co.sis_id AS course_code,
  act.term_begin_date as term_begin_date,
  la.learner_activity_id AS assignment_id,
  la.title AS assignment_title,
  la.due_date AS assignment_due_date,
  -- Define the week number of the course
  CASE 
    WHEN acs.instruction_begin_date IS NOT NULL THEN DATE_DIFF(la.due_date,acs.instruction_begin_date,WEEK) + 1
    WHEN act.term_begin_date IS NOT NULL THEN DATE_DIFF(la.due_date,act.term_begin_date,WEEK) + 1 
    WHEN co.start_date IS NOT NULL THEN DATE_DIFF(la.due_date,co.start_date,WEEK) + 1
    ELSE NULL 
  END AS week_number

  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co 
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__learner_activity AS la ON co.course_offering_id = la.course_offering_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session AS acs ON co.academic_session_id = acs.academic_session_id 
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS act ON co.academic_term_id = act.academic_term_id
  WHERE la.due_date IS NOT NULL
    AND la.points_possible > 0
    AND la.points_possible IS NOT NULL
    AND la.status = 'published'
    AND la.due_date <= @run_time
    AND la.due_date >= COALESCE(acs.instruction_begin_date,act.term_begin_date,co.start_date)
    AND la.due_date <= COALESCE(acs.instruction_end_date,act.term_end_date,co.end_date)
)

SELECT
  assn.course_global_id,
  assn.course_code,
  assn.week_number,
  COUNT(DISTINCT assn.assignment_id) AS assignments_due,
  COUNT(DISTINCT prev_assn.assignment_id) AS assignments_due_cumulative
FROM
  assignments assn 
LEFT JOIN assignments prev_assn ON assn.course_global_id = prev_assn.course_global_id AND assn.week_number >= prev_assn.week_number 
GROUP BY
  course_global_id,
  course_code,
  week_number
