/*
  Context / Student activity score / Student term program

    Defines the academic program for a student in the academic term.

  Properties:

    Export table:  mart_helper.context__student_activity_score__student_term_program
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:

    - mart_helper.context__person__academic_major__academic_term
    - mart_helper.context__academic_major
    - mart_helper.context__academic_program
    - mart_helper.context__academic_term
    - mart_helper.context__person

  To do:
    *
*/

with student_term_program AS (
SELECT 
  p.sis_id AS university_id,
  act.name AS term_name,
  ARRAY_AGG(DISTINCT ap.description ORDER BY ap.description) AS academic_program_array
FROM _WRITE_TO_PROJECT_.mart_helper.context__person__academic_major__academic_term AS pamat
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_major AS am ON pamat.academic_major_id = am.academic_major_id 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_program AS ap ON am.academic_program_id = ap.academic_program_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS act ON pamat.academic_term_id = act.academic_term_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person as p on pamat.person_id = p.person_id 
GROUP BY 
  p.sis_id,
  act.name
)

SELECT 
  DISTINCT 
  stp.university_id
  , stp.term_name 
  , academic_program_array[ordinal(1)] AS academic_program
FROM student_term_program AS stp
