import os, argparse, yaml, subprocess
from progressbar import progressbar

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--mart", help="Specific mart or table to generate chart for.")
args = parser.parse_args()

_MART = args.mart

URL_PREFIX="https://gitlab.com/unizin-community/unizin-data-platform/marts/-/tree/master/"

REPO_ROOT = os.path.abspath(os.path.join(__file__, os.pardir, os.pardir))
MERMAID_CONFIG = f"{REPO_ROOT}/mermaid/config.json"

def load_config_file(path: str) -> dict:
    if not path.startswith(REPO_ROOT):
        path = f"{REPO_ROOT}/{path}"

    with open(path, "r") as f:
        config = yaml.safe_load(f)

    return config


def get_mart_def_from_table(all_marts, table):
    matched = None
    for mart in all_marts:
        if "target_table" in all_marts[mart]:
            if table == all_marts[mart]["target_table"]:
                matched = all_marts[mart]
    return matched


def get_click_link_from_table(all_marts, table):
    mart_def = get_mart_def_from_table(all_marts, table)
    if mart_def:
        return mart_def["file"]
    else:
        return None


BASE_CONFIG_PATH = f"{REPO_ROOT}/{os.environ.get('BASE_CONFIG_PATH', 'config.yaml')}"
BASE_CONFIG = load_config_file(BASE_CONFIG_PATH)

MART_CONFIGS = BASE_CONFIG["mart_config_paths"]
STATIC_TABLES = BASE_CONFIG["static_tables"]

MERMAID_DIR = f"{REPO_ROOT}/mermaid"
if not os.path.exists(MERMAID_DIR):
    os.mkdir(MERMAID_DIR)

static_tables_urls = {}
for st in STATIC_TABLES:
    table = st["target_table"]
    static_tables_urls[table] = URL_PREFIX + st["file"]

all_marts = {}
for mart_base in MART_CONFIGS:
    mart_config_path = MART_CONFIGS[mart_base]
    mart_dir = mart_config_path.split("/")[0]
    mart_config = load_config_file(mart_config_path)
    marts = mart_config["marts"]
    if "starters" in mart_config:
        marts += mart_config["starters"]

    for mart in marts:
        mart["file"] = URL_PREFIX + mart["file"]
        mart["dir"] = mart_dir
        all_marts[mart["mart"]] = mart

all_depends = {}
for mart in all_marts:
    if "target_table" in all_marts[mart]:
        target_table = all_marts[mart]["target_table"]
        all_depends[target_table] = []
        if "depends_on" in all_marts[mart]:
            all_depends[target_table] = all_marts[mart]["depends_on"]


def add_to_dep_trackers(tracker, table, depends):
    for d in depends:
        tracker.append((d, table))
    return list(set(tracker))


def determine_color(table: str):
    def format_style_str(table, color):
        return f"style {table} fill:{color},color:#000000"

    STYLES = {
        "mart_helper.context__": "#48bbfd", # BLUE
        "mart_helper.event__": "#ff4d4d", # RED
        "mart_helper.utility__": "#84fa8a", # GREEN
        "mart_": "#c869ee", # PURPLE
        "context_store": "#9ddafd", # LIGHT_BLUE
        "event_store": "#ff9f9f" # LIGHT_RED
    }
    for prefix, color in STYLES.items():
        if table.startswith(prefix):
            return format_style_str(table, color)


def add_to_color_defs(color_defs: list, table: str):
    color_str = determine_color(table)
    if color_str:
        color_defs.append(color_str)
    return color_defs


def generate_svgs(mermaid_files):
    def svg_path(mmd_path):
        return os.path.splitext(mmd_path)[0] + '.svg'
    
    print(f"Generating {len(mermaid_files)} SVG file(s).")
    for mmd in progressbar(mermaid_files):
        svg_file = svg_path(svg_path(mmd))
        subprocess.run(["mmdc", "-i", mmd, "-o", svg_file, "-c", MERMAID_CONFIG], stdout = subprocess.DEVNULL)


svgs_to_generate = []
for mart in all_marts:
    mart_def = all_marts[mart]
    mart_mermaid_dir = f"{MERMAID_DIR}/{mart_def['dir']}"
    path = f"{mart_mermaid_dir}/{mart}.mmd"

    if _MART:
        if "target_table" in mart_def:
            if mart != _MART and _MART != mart_def["target_table"]:
                continue
        else:
            if mart != _MART:
                continue

    if not os.path.exists(mart_mermaid_dir):
        os.mkdir(mart_mermaid_dir)

    main_defs = []
    click_defs = []
    dep_defs = []
    color_defs = []
    full_mart_deps = []
    addressed_deps = []

    if "target_table" in mart_def:
        main_defs.append(mart_def['target_table'])
        color_defs = add_to_color_defs(color_defs, mart_def['target_table'])
        click_defs.append(f"click {mart_def['target_table']} \"{mart_def['file']}\"")
        full_mart_deps = add_to_dep_trackers(full_mart_deps, mart_def["target_table"], mart_def["depends_on"])

        while not all(item in addressed_deps for item in full_mart_deps):
            for d in full_mart_deps:
                if d not in addressed_deps:
                    depend = d[0]
                    table = d[1]
                    main_defs.append(depend)
                    color_defs = add_to_color_defs(color_defs, depend)
                    clk = get_click_link_from_table(all_marts, depend)
                    if clk:
                        click_defs.append(f"click {depend} \"{clk}\"")
                    elif depend in static_tables_urls:
                        click_defs.append(f"click {depend} \"{static_tables_urls[depend]}\"")

                    dep_defs.append(f"{depend} --> {table}")
                    depend_def = get_mart_def_from_table(all_marts, depend)
                    if depend_def:
                        if "starter" in depend_def:
                            main_defs.append(depend_def["starter"])
                            click_defs.append(f"click {depend_def['starter']} \"{all_marts[depend_def['starter']]['file']}\"")
                            dep_defs.append(f"{depend_def['starter']} --> {depend}")

                    if depend in all_depends:
                        full_mart_deps = add_to_dep_trackers(full_mart_deps, depend, all_depends[depend])
                    
                    addressed_deps.append((depend, table))

        if "starter" in mart_def:
            main_defs.append(mart_def['starter'])
            click_defs.append(f"click {mart_def['starter']} \"{all_marts[mart_def['starter']]['file']}\"")
            dep_defs.append(f"{mart_def['starter']} --> {mart_def['target_table']}")


        with open(path, "w") as f:
            # f.write(f"# {mart}\n\n")
            # f.write(f"Big Query table id: `{mart_def['target_table']}`\n\n")
            # f.write("```mermaid\n")
            # f.write("%%{init: { \"flowchart\": { \"useMaxWidth\": 400 } } }%%\n")
            f.write("flowchart LR\n")

            for md in main_defs:
                f.write(f"\t{md}\n")
            for cd in color_defs:
                f.write(f"\t{cd}\n")
            for cld in click_defs:
                f.write(f"\t{cld}\n")
            for dd in dep_defs:
                f.write(f"\t{dd}\n")
            
            svgs_to_generate.append(path)
            # f.write("```\n")
        
        print(path)

generate_svgs(svgs_to_generate)
