DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_helper.event__general__lti_tool;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_helper.event__general__lti_tool(
    	udp_course_offering_id INT64
    ,	lms_course_offering_id STRING
    ,	udp_person_id INT64
    ,	lms_person_id STRING
    ,	role STRING
    ,	event_time DATETIME
    ,	event_day DATE
    ,	event_hour INT64
    , week_in_term INT64
    ,   launch_app_url STRING
    ,   launch_app_domain STRING
    ,   launch_app_name STRING
    ,   is_lti_tool BOOLEAN
    ,   is_redirect_tool BOOLEAN
    )
PARTITION BY
  DATE(event_time);


INSERT INTO _WRITE_TO_PROJECT_.mart_helper.event__general__lti_tool(
    	udp_course_offering_id
    ,	lms_course_offering_id
    ,	udp_person_id
    ,	lms_person_id
    ,	role
    ,	event_time
    ,	event_day
    ,	event_hour
    , week_in_term
    ,   launch_app_url
    ,   launch_app_domain
    ,   launch_app_name
    ,   is_lti_tool
    ,   is_redirect_tool
    )

SELECT
  /* Identifiers */
    coalesce(r.course_offering.udp_id,lms_co.course_offering_id) AS udp_course_offering_id
    , coalesce(r.course_offering.canvas_id,udp_co.lms_id) AS lms_course_offering_id
    , coalesce(r.person.udp_id,p.person_id) AS udp_person_id
    , r.person.canvas_id AS lms_person_id

  , CASE
    WHEN ARRAY_LENGTH(r.person.roles) > 0 THEN
      r.person.roles[ORDINAL(1)]
    ELSE
      NULL
    END AS role

  , CAST(r.event_time AS DATETIME) AS event_time
  , EXTRACT(DATE FROM DATETIME(r.event_time)) AS event_day
  , EXTRACT(HOUR FROM DATETIME(r.event_time)) AS event_hour
  , CASE
       WHEN acs.instruction_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(r.event_time)), acs.instruction_begin_date, WEEK) + 1
       WHEN act.term_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(r.event_time)), act.term_begin_date, WEEK) + 1
       WHEN coalesce(udp_co.start_date,lms_co.start_date) is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(r.event_time)), coalesce(udp_co.start_date,lms_co.start_date), WEEK) + 1
    ELSE null end as week_in_term

  /*
    Use the Launch App Domain as a backup in case Launch App URL
    is missing.
  */
  , COALESCE(
      JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][url]"),
      JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][domain]")
    ) AS launch_app_url
  , JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][domain]") AS launch_app_domain

  , CASE
    WHEN
      (r.ed_app.id LIKE "%canvas%" OR r.ed_app.id LIKE "%instructure%")
      AND (
        'context_external_tool' = JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]")
        OR 'lti/tool_proxy' = JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]")
      )
      THEN JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_name]")
    ELSE NULL
    END launch_app_name

  /*
    This is a check to determine that we have a proper LTI tool.
  */
  , CASE
    WHEN
      JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][domain]") != ''
      OR
        (
          JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][url]") != ''
          AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][url]") != 'https://www.edu-apps.org/redirect'
        )
      OR 
        (
          JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]") = 'lti/tool_proxy'
        )
      THEN TRUE
      ELSE FALSE
    END is_lti_tool

  /*
    This is a check to determine if we have use of the redirect tool.
  */
  , CASE
    WHEN
      JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][domain]") IS NULL
      AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][url]") = 'https://www.edu-apps.org/redirect'
      THEN TRUE
      ELSE FALSE
    END is_redirect_tool

FROM
  `_READ_FROM_PROJECT_.event_store.expanded` AS r
  left join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as udp_co
      on udp_co.course_offering_id = r.course_offering.udp_id
  left join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as lms_co
      on lms_co.lms_id = r.course_offering.canvas_id
	left join _WRITE_TO_PROJECT_.mart_helper.context__academic_session as acs
	    on COALESCE(udp_co.academic_session_id,lms_co.academic_session_id) = acs.academic_session_id
  left join _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act
      on act.academic_term_id = coalesce(udp_co.academic_term_id,lms_co.academic_term_id,acs.academic_term_id)
  left join _WRITE_TO_PROJECT_.mart_helper.context__person as p
      on p.lms_id = r.person.canvas_id

WHERE
  /*
    Because Instruture is so fucking stupid and, for some reason,
    includes the Course section ID in the Event events.
  */
  type = 'NavigationEvent'

  /*
    Filter out the bad values, in theory.
  */

  /*
  AND r.course_offering.canvas_id IS NOT NULL
  AND r.course_offering.canvas_id != ''
  AND REGEXP_CONTAINS(r.course_offering.canvas_id, r"^\d+$") IS TRUE
  */

  AND r.person.canvas_id IS NOT NULL
  AND r.person.canvas_id != ''
  AND REGEXP_CONTAINS(r.person.canvas_id, r"^\d+$") IS TRUE

  AND (
    r.ed_app.id LIKE "%canvas%"
    OR r.ed_app.id LIKE "%instructure%"
  )

  /*
    This checks that it's a launch event.
  */
  AND (
    r.object.name = 'context_external_tool'
    OR r.object.name = 'lti/tool_proxy'
  )
  AND r.event_time >= '2021-01-01'
;
