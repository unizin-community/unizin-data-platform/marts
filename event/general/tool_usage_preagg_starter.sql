/*
  Event ETL / General / Tool usage preagg starter

  Starter query to do a historical preaggragation from teh tool_usage_metrics helper table

  Properties:

    Export table:  mart_helper.event__general__tool_usage_preagg
    Run frequency: Every hour
    Run operation: Append

  To do:
    *

*/

DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_preagg;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_preagg(
      ed_app_id STRING,
      week_number INT64,
      event_date DATE,
      event_hour INT64,
      earliest_event DATETIME,
      latest_event DATETIME,
      num_events INT64
    )
PARTITION BY
  event_date;

INSERT INTO _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_preagg(
      ed_app_id,
      week_number,
      event_date,
      event_hour,
      earliest_event,
      latest_event,
      num_events
)

SELECT
  ed_app_id,
  week_number,
  event_date,
  event_hour,
  min(event_time) as earliest_event,
  max(event_time) as latest_event,
  COUNT(id) AS num_events
FROM
  _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_metrics
WHERE event_time <= DATETIME_TRUNC(CURRENT_DATETIME(), HOUR) --FLOOR FOR CURRENT HOUR
GROUP BY
  ed_app_id,
  week_number,
  event_date,
  event_hour;