/*
  Event ETL / General / Tool usage Preagg

  Pre-aggregates events by tool, day, and hour to optimize usage metrics calculations

  Properties:

    Export table:  mart_helper.event__general__tool_usage_preagg
    Run frequency: Every hour
    Run operation: Append

  To do:
    *

*/

SELECT
  ed_app_id,
  week_number,
  event_date,
  event_hour,
  min(event_time) as earliest_event,
  max(event_time) as latest_event,
  COUNT(id) AS num_events
FROM
  _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_metrics
WHERE event_time >= DATETIME_TRUNC(DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR), HOUR) --FLOOR FOR PREVIOUS HOUR
and event_time <= DATETIME_TRUNC(CURRENT_DATETIME(), HOUR) --FLOOR FOR CURRENT HOUR
GROUP BY
  ed_app_id,
  week_number,
  event_date,
  event_hour