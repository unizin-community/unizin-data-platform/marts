DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_helper.event__general__lms_tool;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_helper.event__general__lms_tool(
    	udp_course_offering_id INT64
    ,	lms_course_offering_id STRING
    ,	udp_person_id INT64
    ,	lms_person_id STRING
    ,	role STRING
    ,	event_time DATETIME
    ,	event_day DATE
    ,	event_hour INT64
    ,	canvas_tool STRING
    ,	asset_type STRING
    ,	asset_type_id STRING
    ,	asset_subtype STRING
    ,	asset_subtype_id STRING
    ,	module_item_id STRING
    ,	learner_activity_id STRING
    ,	student_id STRING
    )
PARTITION BY
  DATE(event_time);


INSERT INTO _WRITE_TO_PROJECT_.mart_helper.event__general__lms_tool(
    	udp_course_offering_id
    ,	lms_course_offering_id
    ,	udp_person_id
    ,	lms_person_id
    ,	role
    ,	event_time
    ,	event_day
    ,	event_hour
    ,	canvas_tool
    ,	asset_type
    ,	asset_type_id
    ,	asset_subtype
    ,	asset_subtype_id
    ,	module_item_id
    ,	learner_activity_id
    ,	student_id
    )
WITH basic_events AS (
  SELECT
    coalesce(r.course_offering.udp_id,co.course_offering_id) AS udp_course_offering_id
    , r.course_offering.canvas_id AS lms_course_offering_id
    , coalesce(r.person.udp_id,p.person_id) AS udp_person_id
    , r.person.canvas_id AS lms_person_id

    , CASE
      WHEN ARRAY_LENGTH(r.person.roles) > 0 THEN
        r.person.roles[ORDINAL(1)]
      ELSE
        NULL
      END AS role

    , r.event_time AS event_time
    , EXTRACT(DATE FROM DATETIME(r.event_time)) AS event_day
    , EXTRACT(HOUR FROM DATETIME(r.event_time)) AS event_hour

    /*
      I'm keeping here the code to extract object_name and object_id
      from the event. It turns out that, given the logic below to set
      asset_subtye, keeping track of object_name and object_id is not
      needed.

      The reason is that asset_subtype is only ever provided when the
      asset_type is 'course'. In these cases, the object_name is the
      same as asset_subtype.

    , r.object_name AS object_name
    , r.object.id AS object_id
    */

    , CASE
      WHEN JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]") = 'course'
      AND REGEXP_CONTAINS(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"courses/\d+/grades/\d+$")
      THEN 'gradebook'
      ELSE JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]")
      END AS asset_type

    , COALESCE(
        JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][entity_id]")
        , REGEXP_EXTRACT(r.object.id, r"urn:instructure:canvas:[_:\w]+:(\d+)")
      ) AS asset_type_id

    , CASE
      WHEN JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]") = 'course'
      THEN
        CASE
        WHEN REGEXP_CONTAINS(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"courses/\d+/grades/\d+$") THEN 'user'
        ELSE JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_subtype]")
        END
      WHEN JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]") = 'enrollment'
      THEN 'user'
      ELSE
        JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_subtype]")
      END AS asset_subtype

    , CASE
      WHEN  JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_subtype]") IS NOT NULL
            AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]") != 'course'
      THEN  COALESCE(
              JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][entity_id]"),
              REGEXP_EXTRACT(r.object.id, r"urn:instructure:canvas:[_:\w]+:(\d+)")
            )
      WHEN  JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]") = 'enrollment'
      THEN  REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"users/(\d+)$")
      WHEN  JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][asset_type]") = 'course'
            AND REGEXP_CONTAINS(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"courses/\d+/grades/\d+$")
      THEN
        REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"courses/\d+/grades/(\d+)$")
      ELSE NULL
      END AS asset_subtype_id

    /*
      Sometimes, a resource is access as a module item. This fact only appears
      in the event's request_url value.
    */
    , CASE
      WHEN REGEXP_CONTAINS(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"module_item_id=(\d+)")
      THEN
        REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"module_item_id=(\d+)")
      ELSE NULL
      END AS module_item_id

    /*
      Sometimes, an assignment is referenced in the speed grader. Let's get it.
    */
    , CASE
      WHEN REGEXP_CONTAINS(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"assignment_id=(\d+)")
      THEN
        REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"assignment_id=(\d+)")
      ELSE NULL
      END AS learner_activity_id

      /*
        Sometimes, a student is referenced in the speed grader. Let's get it.
      */
    , CASE
      WHEN REGEXP_CONTAINS(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"student_id=(\d+)")
      THEN
        REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]"), r"student_id=(\d+)")
      ELSE NULL
      END AS student_id

    , JSON_EXTRACT_SCALAR(r.extensions_json, "$['com.instructure.canvas'][request_url]") AS request_url

  FROM
    `_READ_FROM_PROJECT_.event_store.expanded` AS r
	left join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as co
	    on co.lms_id = r.course_offering.canvas_id
  left join _WRITE_TO_PROJECT_.mart_helper.context__person as p 
        on p.lms_id = r.person.canvas_id 

  WHERE

    r.type = 'NavigationEvent'
    AND r.action = 'NavigatedTo'

    /*
      Filter out the bad values, in theory.
    */
    AND r.course_offering.canvas_id IS NOT NULL
    AND r.course_offering.canvas_id != ''
    AND REGEXP_CONTAINS(r.course_offering.canvas_id, r"^\d+$") IS TRUE

    AND r.person.canvas_id IS NOT NULL
    AND r.person.canvas_id != ''
    AND REGEXP_CONTAINS(r.person.canvas_id, r"^\d+$") IS TRUE

    AND (
      r.ed_app.id LIKE "%canvas%"
      OR r.ed_app.id LIKE "%instructure%"
    )

    AND r.event_time >= '2021-01-01'
)

-- What Canvas tool was used?
--
-- Un checked cases:
--    	https://uiowa.instructure.com/courses/1616793/files/folder/unfiled
--      https://canvas.uiowa.edu/courses/1818102/gradebook/speed_grader?assignment_id=8176407&student_id=6312752 ; object_name = 'speed_grader'
--      what is a content_tag object_name? e.g.: 	https://canvas.uiowa.edu/courses/1807224/modules/items/19833644
--      enrollment object name means visiing a user page: https://canvas.uiowa.edu/courses/1803035/users/6221979
--

SELECT
  be.udp_course_offering_id AS udp_course_offering_id
  , be.lms_course_offering_id AS lms_course_offering_id
  , be.udp_person_id AS udp_person_id
  , be.lms_person_id AS lms_person_id

  , be.role AS role

  , CAST(be.event_time AS DATETIME) AS event_time
  , be.event_day AS event_day
  , be.event_hour AS event_hour

  , CASE
    WHEN be.asset_type = 'course' THEN
      CASE
      WHEN be.asset_subtype = 'assignments' THEN 'Learner activities'
      WHEN be.asset_subtype = 'modules' THEN 'Modules'
      WHEN be.asset_subtype = 'home' THEN 'Homepage'
      WHEN be.asset_subtype = 'topics' THEN 'Discussions'
      WHEN be.asset_subtype = 'grades' THEN 'Gradebook'
      WHEN be.asset_subtype = 'announcements' THEN 'Announcements'
      WHEN be.asset_subtype = 'quizzes' THEN 'Quizzes'
      WHEN be.asset_subtype = 'syllabus' THEN 'Syllabus'
      WHEN be.asset_subtype = 'files' THEN 'Files'
      WHEN be.asset_subtype = 'roster' THEN 'Roster'
      WHEN be.asset_subtype = 'collaborations' THEN 'Collaborations'
      WHEN be.asset_subtype = 'speed_grader' THEN 'Speed grader'
      WHEN be.asset_subtype = 'pages' THEN 'Wiki pages'
      WHEN be.asset_subtype = 'outcomes' THEN 'Learning outcomes'
      WHEN be.asset_subtype = 'conferences' THEN 'Conferences'
      WHEN be.asset_subtype = 'quizzes.next' THEN 'Quizzes'
      WHEN be.asset_subtype = 'calendar_feed' THEN 'Calendar'
      WHEN be.asset_subtype = 'user' THEN 'Person'
      ELSE 'Unknown subtype'
      END
    WHEN be.asset_type = 'web_conference' THEN 'Web conference'
    WHEN be.asset_type = 'discussion_topic' THEN 'Discussion'
    WHEN be.asset_type = 'context_external_tool' THEN 'LTI Tool'
    WHEN be.asset_type = 'assignment' THEN 'Learner activity'
    WHEN be.asset_type = 'attachment' THEN 'File'
    WHEN be.asset_type = 'quizzes:quiz' THEN 'Quiz'
    WHEN be.asset_type = 'wiki_page' THEN 'Wiki page'
    WHEN be.asset_type = 'lti/tool_proxy' THEN 'LTI Tool'
    WHEN be.asset_type = 'content_tag' THEN 'Module item'
    WHEN be.asset_type = 'enrollment' THEN 'User'
    WHEN be.asset_type = 'gradebook' THEN 'Gradebook'
    WHEN be.asset_type = 'calendar_event' THEN 'Calendar event'
    WHEN be.asset_type = 'collaboration' THEN 'Collaboration'
    ELSE 'Unknown canvas tool'
    END AS canvas_tool

  , be.asset_type AS asset_type

  , CASE
    WHEN be.asset_type_id IS NOT NULL THEN be.asset_type_id
    ELSE NULL
    END AS asset_type_id

  , be.asset_subtype AS asset_subtype
  , CASE
    WHEN be.asset_subtype_id IS NOT NULL THEN be.asset_subtype_id
    ELSE NULL
    END AS asset_subtype_id

  , be.module_item_id AS module_item_id
  , be.learner_activity_id AS learner_activity_id
  , be.student_id AS student_id

FROM
  basic_events AS be
