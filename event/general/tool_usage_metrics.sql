/*
  Event ETL / General / Tool usage metrics

  Defines events for all tools included in the event store

  Properties:

    Export table:  mart_helper.event__general__tool_usage_metrics
    Run frequency: Every hour
    Run operation: Append

  To do:
    *

*/

SELECT 
    id,
    ed_app.id as ed_app_id,
    event_time,
    event_date,
    EXTRACT(WEEK FROM event_date) as week_number,
    event_hour,
    type,
    action
FROM `_READ_FROM_PROJECT_.event_store.expanded` 
WHERE event_time >= CAST(TIMESTAMP_SUB(@run_time, INTERVAL 1 HOUR) AS DATETIME)
