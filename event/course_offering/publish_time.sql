/*
  Event ETL / Course offering / Publish time

    Captures the first event emitted in a course,
    thereby signaling the time when it was published.

  Properties:

    Export table:  mart_helper.event__course_offering__publish_time
    Run frequency: Every hour
    Run operation: Overwrite

  To do:
    *

*/

WITH course_offering_latest AS (
  SELECT
    r.course_offering.canvas_id AS lms_course_offering_id
    , MIN(r.event_time) AS earliest_time

  FROM
    `_READ_FROM_PROJECT_.event_store.expanded` AS r

  WHERE
    r.type = 'Event'

    AND r.action = 'Modified'

    AND r.course_offering.canvas_id IS NOT NULL
    AND r.course_offering.canvas_id != ''

    AND r.event_time >= CAST(TIMESTAMP_SUB(@run_time, INTERVAL 1 HOUR) AS DATETIME)

    AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") IS NOT NULL
    AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") IN
      (
        'available'
      )

  GROUP BY
    lms_course_offering_id
),

/*
  Get the existing Course offering publish
  times.
*/
course_offering_existing AS (
  SELECT
    pt.lms_course_offering_id AS lms_course_offering_id
    , pt.publish_time AS publish_time

  FROM
    _WRITE_TO_PROJECT_.mart_helper.event__course_offering__publish_time AS pt
),

/*
  Filter out the existing Course offerings
  from our new batch of events.
*/
course_offering_new AS (
  SELECT
    col.lms_course_offering_id AS lms_course_offering_id
    , col.earliest_time AS earliest_time

  FROM
    course_offering_latest AS col

  LEFT JOIN course_offering_existing AS coe USING (lms_course_offering_id)

  -- This does the filtering magic go eliminate exisitng course offerings.
  WHERE
    coe.lms_course_offering_id IS NULL
)

/*
  Combine existing and new together.
*/
SELECT
  coe.lms_course_offering_id AS lms_course_offering_id
  , coe.publish_time AS publish_time
FROM
  course_offering_existing AS coe
UNION ALL
SELECT
  con.lms_course_offering_id AS lms_course_offering_id
  , con.earliest_time AS publish_time
FROM
  course_offering_new AS con
