DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_helper.event__course_offering__publish_time;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_helper.event__course_offering__publish_time(
    	lms_course_offering_id STRING
    ,	publish_time DATETIME 
    )
PARTITION BY
  DATE(publish_time);

INSERT INTO _WRITE_TO_PROJECT_.mart_helper.event__course_offering__publish_time
(
        lms_course_offering_id
    ,   publish_time
)
SELECT
    r.course_offering.canvas_id AS lms_course_offering_id
    , MIN(r.event_time) AS publish_time

  FROM
    `_READ_FROM_PROJECT_.event_store.expanded` AS r

  WHERE
    r.type = 'Event'

    AND r.action = 'Modified'

    AND r.course_offering.canvas_id IS NOT NULL
    AND r.course_offering.canvas_id != ''

    AND r.event_time >= '2019-01-01'

    AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") IS NOT NULL
    AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") IN
      (
        'available'
      )

  GROUP BY
    lms_course_offering_id
  
