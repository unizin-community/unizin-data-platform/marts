/*
  Event ETL / Course offering / Last activity

    Captures the last activity for every Course offering / Person
    pair in the last hour.

  Properties:

    Export table:  mart_helper.event__course_offering__last_activity
    Run frequency: Every hour
    Run operation: Overwrite

  To do:
    *

*/

/*
  Get all events for the last hour, grouping by Course offering
  and Person to get counts of events by type.
*/
WITH new_activity AS (
  SELECT
    /* Identifiers */
    course_offering.canvas_id AS lms_course_offering_id
    , person.canvas_id AS lms_person_id

    /* Distinguish last activities */
    , MAX(event_time) AS last_activity
    , MAX(CASE WHEN type='NavigationEvent' THEN event_time ELSE NULL END) AS last_navigation_activity
    , MAX(CASE WHEN type='MediaEvent' THEN event_time ELSE NULL END) AS last_media_activity
    , MAX(CASE WHEN type='GradeEvent' THEN event_time ELSE NULL END) AS last_grade_activity
    , MAX(CASE WHEN type='AssessmentEvent' THEN event_time ELSE NULL END) AS last_assessment_activity
    , MAX(CASE WHEN type='AssignableEvent' THEN event_time ELSE NULL END) AS last_assignment_activity

    /* Count activites by type */
    , COUNT(1) as num_events
    , SUM(CASE WHEN type='NavigationEvent' THEN 1 ELSE 0 END) as num_navigation_events
    , SUM(CASE WHEN type='MediaEvent' THEN 1 ELSE 0 END) as num_media_events
    , SUM(CASE WHEN type='GradeEvent' THEN 1 ELSE 0 END) as num_grade_events
    , SUM(CASE WHEN type='AssessmentEvent' THEN 1 ELSE 0 END) as num_assessment_events
    , SUM(CASE WHEN type='AssignableEvent' THEN 1 ELSE 0 END) as num_assignment_events

  FROM
    `_READ_FROM_PROJECT_.event_store.expanded`

  WHERE

    -- Generic events don't count.
    type != 'Event'

    -- Filter out records that do not have an LMS ID.
    AND course_offering.canvas_id IS NOT NULL
    AND REGEXP_CONTAINS(course_offering.canvas_id, r"^\d+$") IS TRUE

    AND person.canvas_id IS NOT NULL
    AND REGEXP_CONTAINS(person.canvas_id, r"^\d+$") IS TRUE

    AND event_time >= CAST(TIMESTAMP_SUB(@run_time, INTERVAL 1 HOUR) AS DATETIME)

  GROUP BY
    lms_course_offering_id
    , lms_person_id
),

/*
  Update the existing base last activity table to reflect
  the new last activity stats for institution.
*/
updated_last_activity AS (
  SELECT
    mla.lms_course_offering_id AS lms_course_offering_id
    , mla.lms_person_id AS lms_person_id

    /* Distinct last activities */
    , COALESCE(na.last_activity, CAST(mla.last_activity AS DATETIME)) AS last_activity
    , COALESCE(na.last_navigation_activity, CAST(mla.last_navigation_activity AS DATETIME)) AS last_navigation_activity
    , COALESCE(na.last_media_activity, CAST(mla.last_media_activity AS DATETIME)) AS last_media_activity
    , COALESCE(na.last_grade_activity, CAST(mla.last_grade_activity AS DATETIME)) AS last_grade_activity
    , COALESCE(na.last_assessment_activity, CAST(mla.last_assessment_activity AS DATETIME)) AS last_assessment_activity
    , COALESCE(na.last_assignment_activity, CAST(mla.last_assignment_activity AS DATETIME)) AS last_assignment_activity

    /* Conts activities by type */
    , (mla.num_events + COALESCE(na.num_events,0)) AS num_events
    , (mla.num_navigation_events + COALESCE(na.num_navigation_events,0)) AS num_navigation_events
    , (mla.num_media_events + COALESCE(na.num_media_events,0)) AS num_media_events
    , (mla.num_grade_events + COALESCE(na.num_grade_events,0)) AS num_grade_events
    , (mla.num_assessment_events + COALESCE(na.num_assessment_events,0)) AS num_assessment_events
    , (mla.num_assignment_events + COALESCE(na.num_assignment_events,0)) AS num_assignment_events

  FROM
    `_WRITE_TO_PROJECT_.mart_helper.event__course_offering__last_activity` AS mla

  LEFT JOIN
     new_activity AS na ON
      na.lms_course_offering_id=mla.lms_course_offering_id AND
      na.lms_person_id=mla.lms_person_id
),

/*
  Determine which of the course offering/person pairs in the last
  hour are net-new.
*/
net_new_last_activity AS (
  SELECT
    na.lms_course_offering_id AS lms_course_offering_id
    , na.lms_person_id AS lms_person_id

    , na.last_activity AS last_activity
    , na.last_navigation_activity AS last_navigation_activity
    , na.last_media_activity AS last_media_activity
    , na.last_grade_activity AS last_grade_activity
    , na.last_assessment_activity AS last_assessment_activity
    , na.last_assignment_activity AS last_assignment_activity

    , na.num_events AS num_events
    , na.num_navigation_events AS num_navigation_events
    , na.num_media_events AS num_media_events
    , na.num_grade_events AS num_grade_events
    , na.num_assessment_events AS num_assessment_events
    , na.num_assignment_events AS num_assignment_events
  FROM
    new_activity AS na
  LEFT JOIN 
    updated_last_activity AS ula ON
      na.lms_course_offering_id = ula.lms_course_offering_id AND 
      na.lms_person_id = ula.lms_person_id
  WHERE
    ula.lms_course_offering_id IS NULL OR 
      ula.lms_person_id IS NULL
)

/*
  Union the updated base lactivity table and the net new
  last activity data.
*/
SELECT
  lms_course_offering_id
  , lms_person_id
  , last_activity
  , last_navigation_activity
  , last_media_activity
  , last_grade_activity
  , last_assessment_activity
  , last_assignment_activity
  , num_events
  , num_navigation_events
  , num_media_events
  , num_grade_events
  , num_assessment_events
  , num_assignment_events
FROM updated_last_activity
UNION ALL
SELECT
  lms_course_offering_id
  , lms_person_id
  , last_activity
  , last_navigation_activity
  , last_media_activity
  , last_grade_activity
  , last_assessment_activity
  , last_assignment_activity
  , num_events
  , num_navigation_events
  , num_media_events
  , num_grade_events
  , num_assessment_events
  , num_assignment_events
FROM net_new_last_activity
;
