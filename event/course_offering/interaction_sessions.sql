/*
  Event ETL / Interaction Sessions

    Captures the amount of time a student spends in the learning environment
    Based on 10, 20, and 30 minute cutoff options

  Properties:

    Export table:  mart_helper.event__course_offering__interaction_sessions
    Run frequency: Every Day
    Run operation: Append

  To do:
    *

*/

with filtered_events as (
   select distinct
       coalesce(e.person.udp_id,p.person_id) as udp_person_id,
       coalesce(e.course_offering.udp_id,lms_co.course_offering_id) as udp_course_offering_id
    	, CASE
        WHEN acs.instruction_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(e.event_time)), acs.instruction_begin_date, WEEK) + 1
        WHEN act.term_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(e.event_time)), act.term_begin_date, WEEK) + 1
        WHEN coalesce(udp_co.start_date,lms_co.start_date) is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(e.event_time)), coalesce(udp_co.start_date,lms_co.start_date), WEEK) + 1
      ELSE null end as week_in_term,
      event_time,
      DATE_DIFF(term_end_date,term_begin_date,WEEK) + 1 AS term_length_weeks 
   from
       `_READ_FROM_PROJECT_.event_store.expanded` as e
  left join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as udp_co 
      on udp_co.course_offering_id = e.course_offering.udp_id 
	left join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as lms_co
	    on lms_co.lms_id = e.course_offering.canvas_id
	left join _WRITE_TO_PROJECT_.mart_helper.context__academic_session as acs
	    on COALESCE(udp_co.academic_session_id,lms_co.academic_session_id) = acs.academic_session_id
	inner join _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act
	    on act.academic_term_id = coalesce(udp_co.academic_term_id,lms_co.academic_term_id)
  left join _WRITE_TO_PROJECT_.mart_helper.context__person as p 
        on p.lms_id = e.person.canvas_id 
   where
       event_time >=  CAST(TIMESTAMP_SUB(@run_time, INTERVAL 1 DAY) AS DATETIME)
       and event_time >= COALESCE(acs.instruction_begin_date,act.term_begin_date,coalesce(udp_co.start_date,lms_co.start_date))
       and event_time <= COALESCE(acs.instruction_end_date,act.term_end_date,coalesce(udp_co.end_date,lms_co.end_date))
),

lagged_events as (
   select
       f.*,
       ifnull(lag(f.event_time, 1) over (partition by udp_person_id, udp_course_offering_id order by event_time asc), event_time) as previous_event_time
   from
       filtered_events as f ),

durations as (
   select
       *,

       case
           when TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) >= 600 then 0
           else TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) end as dwell_duration_seconds_10min,
       sum(case
           when TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) >= 600 then 1 else 0 end) over (partition by udp_person_id, udp_course_offering_id order by event_time asc) as session_num_10min,

       case
           when TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) >= 1200 then 0
           else TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) end as dwell_duration_seconds_20min,
       sum(case
           when TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) >= 1200 then 1 else 0 end) over (partition by udp_person_id, udp_course_offering_id order by event_time asc) as session_num_20min,

       case
           when TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) >= 1800 then 0
           else TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) end as dwell_duration_seconds_30min,
       sum(case
           when TIMESTAMP_DIFF(event_time,
           previous_event_time,
           second) >= 1800 then 1 else 0 end) over (partition by udp_person_id, udp_course_offering_id order by event_time asc) as session_num_30min,

       from lagged_events
       order by udp_person_id, udp_course_offering_id, event_time
),
 
sessions_10min as (
select
   udp_person_id,
   udp_course_offering_id,
   week_in_term,
   term_length_weeks,
   EXTRACT(DATE FROM min(event_time)) as session_date,
   session_num_10min,
   min(event_time) as begins,
   max(event_time) as ends,
   count(*) as actions,
   sum(dwell_duration_seconds_10min) as `length`
from durations
group by udp_person_id, udp_course_offering_id, week_in_term,term_length_weeks,session_num_10min
),

session_10min_agg as (
    select  udp_person_id
       ,   udp_course_offering_id
       ,   week_in_term
       ,   session_date
       ,   sum(CASE WHEN length > 0 THEN 1 ELSE 0 END) as num_sessions
       ,   sum(`length`) as total_time_seconds
       ,   sum(CASE WHEN length > 0 THEN actions ELSE 0 END) as total_actions
       ,    avg(CASE WHEN length > 0 THEN `length` ELSE NULL END) as avg_time_seconds
       ,    avg(CASE WHEN length > 0 THEN actions ELSE NULL END) as avg_actions
    from sessions_10min
    where week_in_term > 0 and week_in_term <= COALESCE(term_length_weeks,17)
    group by  udp_person_id
       ,   udp_course_offering_id
       ,   week_in_term
       ,   session_date
),

sessions_20min as (
select
   udp_person_id,
   udp_course_offering_id,
   week_in_term,
   term_length_weeks,
   EXTRACT(DATE FROM min(event_time)) as session_date,
   session_num_20min,
   min(event_time) as begins,
   max(event_time) as ends,
   count(*) as actions,
   sum(dwell_duration_seconds_20min) as `length`
from durations
group by udp_person_id, udp_course_offering_id, week_in_term, term_length_weeks,session_num_20min
),

session_20min_agg as (
    select  udp_person_id
       ,   udp_course_offering_id
       ,   week_in_term
       ,   session_date
       ,   sum(CASE WHEN length > 0 THEN 1 ELSE 0 END) as num_sessions
       ,   sum(`length`) as total_time_seconds
       ,   sum(CASE WHEN length > 0 THEN actions ELSE 0 END) as total_actions
       ,    avg(CASE WHEN length > 0 THEN `length` ELSE NULL END) as avg_time_seconds
       ,    avg(CASE WHEN length > 0 THEN actions ELSE NULL END) as avg_actions
    from sessions_20min
    where week_in_term > 0 and week_in_term <= COALESCE(term_length_weeks,17)
    group by  udp_person_id
       ,   udp_course_offering_id
       ,   week_in_term
       ,   session_date
),

sessions_30min as (
select
   udp_person_id,
   udp_course_offering_id,
   week_in_term,
   term_length_weeks,
   EXTRACT(DATE FROM min(event_time)) as session_date,
   session_num_30min,
   min(event_time) as begins,
   max(event_time) as ends,
   count(*) as actions,
   sum(dwell_duration_seconds_30min) as `length`
from durations
group by udp_person_id, udp_course_offering_id, week_in_term, term_length_weeks, session_num_30min
),

session_30min_agg as (
    select  udp_person_id
       ,   udp_course_offering_id
       ,   week_in_term
       ,   session_date
       ,   sum(CASE WHEN length > 0 THEN 1 ELSE 0 END) as num_sessions
       ,   sum(`length`) as total_time_seconds
       ,   sum(CASE WHEN length > 0 THEN actions ELSE 0 END) as total_actions
       ,    avg(CASE WHEN length > 0 THEN `length` ELSE NULL END) as avg_time_seconds
       ,    avg(CASE WHEN length > 0 THEN actions ELSE NULL END) as avg_actions
    from sessions_30min
    where week_in_term > 0 and week_in_term <= COALESCE(term_length_weeks,17)
    group by  udp_person_id
       ,   udp_course_offering_id
       ,   week_in_term
       ,   session_date
)


select  s10.udp_person_id
            ,   s10.udp_course_offering_id
            ,   s10.week_in_term
      , s10.session_date
            ,   s10.num_sessions as num_sessions_10min
            ,   s10.total_time_seconds as total_time_seconds_10min
      , s10.total_actions as total_actions_10min
      , s10.avg_time_seconds as avg_time_seconds_10min
      , s10.avg_actions as avg_actions_10min
            ,   s20.num_sessions as num_sessions_20min
            ,   s20.total_time_seconds as total_time_seconds_20min
      , s20.total_actions as total_actions_20min
      , s20.avg_time_seconds as avg_time_seconds_20min
      , s20.avg_actions as avg_actions_20min
            ,   s30.num_sessions as num_sessions_30min
            ,   s30.total_time_seconds as total_time_seconds_30min
      , s30.total_actions as total_actions_30min
      , s30.avg_time_seconds as avg_time_seconds_30min
      , s30.avg_actions as avg_actions_30min
from session_10min_agg as s10
inner join session_20min_agg as s20
    on s20.udp_person_id = s10.udp_person_id
    and s20.udp_course_offering_id = s10.udp_course_offering_id
    and s20.week_in_term = s10.week_in_term
    and s20.session_date = s10.session_date
inner join session_30min_agg as s30
    on s30.udp_person_id = s10.udp_person_id
    and s30.udp_course_offering_id = s10.udp_course_offering_id
    and s30.week_in_term = s10.week_in_term
    and s30.session_date = s10.session_date
