/*
  Event ETL / Course offering / Status

    Captures the latest status for a course offering
    from the event data.

  Properties:

    Export table:  mart_helper.event__course_offering__status
    Run frequency: Every hour
    Run operation: Overwrite

  To do:
    *

*/

/*
  Get all events where the course offering
  workflow state is updated.
*/
WITH course_offering_latest AS (
  SELECT
    r.course_offering.canvas_id as lms_course_offering_id

    , JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") AS status

    /*
      We assign a row number based on timestamp so that, in a future step
      we take the latest event's status.
    */
    , ROW_NUMBER() OVER (PARTITION BY r.course_offering.canvas_id ORDER BY r.event_time DESC) AS row_number

  FROM
    `_READ_FROM_PROJECT_.event_store.expanded` AS r

  WHERE

    r.type = 'Event'

    AND r.action = 'Modified'

    AND r.course_offering.canvas_id IS NOT NULL
    AND r.course_offering.canvas_id != ''
    AND REGEXP_CONTAINS(r.course_offering.canvas_id, r"^\d+$") IS TRUE

    AND r.object.type = 'CourseOffering'
    AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") IS NOT NULL
    AND r.event_time >= CAST(TIMESTAMP_SUB(@run_time, INTERVAL 1 HOUR) AS DATETIME)
),

/*
  Pare down all our new events to a unique set of
  Course offerings that  privilege the last updated
  event over others.
*/
course_offering_latest_aggregated AS (
  SELECT
    col.lms_course_offering_id AS lms_course_offering_id

    , col.status AS status

    , col.row_number AS row_number
  FROM
    course_offering_latest AS col
  WHERE
    row_number = 1 -- Only use the last update to the course offering.
    AND col.lms_course_offering_id IS NOT NULL
),

/*
  Get the existing Course offering status and update
  their status values, if needed, with the latest
  event data that we just collected.
*/
course_offering_existing_updated AS (
  SELECT
    s.lms_course_offering_id AS lms_course_offering_id

    -- Privilege the latest event status.
    , COALESCE(cola.status, s.status) AS status
  FROM
    _WRITE_TO_PROJECT_.mart_helper.event__course_offering__status AS s

  LEFT JOIN
    course_offering_latest_aggregated AS cola USING (lms_course_offering_id)
),

/*
  Pluck from our new event data the subset of Course offering
  status values that correspond to a Course offering that we
  have not yet seen.
*/
course_offering_new AS (
  SELECT
    cola.lms_course_offering_id AS lms_course_offering_id

    , cola.status AS status
  FROM
    course_offering_latest_aggregated AS cola

  LEFT JOIN course_offering_existing_updated AS coea USING (lms_course_offering_id)

  /*
    This does the filtering magic to eliminate exisitng course offerings
    from our new events.
  */
  WHERE
    coea.lms_course_offering_id IS NULL
)

/*
  Combine updated and new data together.
*/
SELECT
  coeu.lms_course_offering_id AS lms_course_offering_id
  , coeu.status AS status
FROM
  course_offering_existing_updated AS coeu
UNION ALL
SELECT
  con.lms_course_offering_id AS lms_course_offering_id
  , con.status AS status
FROM
  course_offering_new AS con
