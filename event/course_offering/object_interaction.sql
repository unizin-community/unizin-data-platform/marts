/*
  Event ETL / Object interaction

    Captures the subset of events where an interaction
    with an object is involved, per course offering.

  Properties:

    Export table:  mart_helper.event__course_offering__object_interaction
    Run frequency: Every hour
    Run operation: Append

  To do:
    *

*/

WITH raw_events AS (
  SELECT
    coalesce(r.course_offering.udp_id,lms_co.course_offering_id) AS udp_course_offering_id
    , r.course_offering.canvas_id AS lms_course_offering_id
    , coalesce(r.person.udp_id,p.person_id) AS udp_person_id
    , r.person.canvas_id AS lms_person_id

  , CASE
    WHEN ARRAY_LENGTH(r.person.roles) > 0 THEN
      r.person.roles[ORDINAL(1)]
    ELSE
      NULL
    END AS role

  , r.event_time AS event_time
  , EXTRACT(DATE FROM DATETIME(r.event_time)) AS event_day
  , EXTRACT(HOUR FROM DATETIME(r.event_time)) AS event_hour
  , CASE
       WHEN acs.instruction_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(r.event_time)), acs.instruction_begin_date, WEEK) + 1
       WHEN act.term_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(r.event_time)), act.term_begin_date, WEEK) + 1
       WHEN coalesce(udp_co.start_date,lms_co.start_date) is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(r.event_time)), coalesce(udp_co.start_date,lms_co.start_date), WEEK) + 1
    ELSE null end as week_in_term
  , CASE
      WHEN (r.ed_app.id LIKE "%canvas%" OR r.ed_app.id LIKE "%instructure%") THEN REGEXP_EXTRACT(r.object.id, r"(\d+)$")
      WHEN r.ed_app.id LIKE "%kaltura%" THEN REGEXP_EXTRACT(r.object.id, r"media/(\w+)$")
      WHEN r.ed_app.id = "https://inscribe.education" THEN r.object.id
      WHEN r.ed_app.id = "https://engage.unizin.org" THEN REGEXP_EXTRACT(r.object.id, r"page:(\d+)$")
      WHEN r.ed_app.id = "https://app.tophat.com" THEN r.object.id
      WHEN r.ed_app.id = "https://quickcheck.eds.iu.edu" THEN REGEXP_EXTRACT(r.object.id, r"https://quickcheck.eds.iu.edu/api/caliper/student/(\d+)$")
    ELSE NULL
  END AS object_id
  , object.type AS object_type

  FROM
    `_READ_FROM_PROJECT_.event_store.expanded` AS r
  left join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as udp_co 
      on udp_co.course_offering_id = r.course_offering.udp_id 
	left join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as lms_co
	    on lms_co.lms_id = r.course_offering.canvas_id
	left join _WRITE_TO_PROJECT_.mart_helper.context__academic_session as acs
	    on COALESCE(udp_co.academic_session_id,lms_co.academic_session_id) = acs.academic_session_id
	inner join _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act
	    on act.academic_term_id = coalesce(udp_co.academic_term_id,lms_co.academic_term_id,acs.academic_term_id)
  left join _WRITE_TO_PROJECT_.mart_helper.context__person as p 
        on p.lms_id = r.person.canvas_id 

  WHERE
    r.event_time >= CAST(TIMESTAMP_SUB(@run_time, INTERVAL 1 HOUR) AS DATETIME)
)

SELECT
  r.udp_course_offering_id AS udp_course_offering_id
  , r.lms_course_offering_id AS lms_course_offering_id
  , r.udp_person_id AS udp_person_id
  , r.lms_person_id AS lms_person_id
  , r.role AS role
  , r.event_time AS event_time
  , r.event_day AS event_day
  , r.event_hour AS event_hour
  , r.week_in_term as week_in_term
  , r.object_id AS object_id
  , r.object_type AS object_type
FROM
  raw_events AS r
