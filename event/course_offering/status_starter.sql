DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_helper.event__course_offering__status;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_helper.event__course_offering__status(
    	lms_course_offering_id STRING
    ,	status STRING
    );

INSERT INTO _WRITE_TO_PROJECT_.mart_helper.event__course_offering__status
(
		lms_course_offering_id
	,	status
)
with course_offering_latest as (

SELECT
r.course_offering.canvas_id as lms_course_offering_id

, JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") AS status

/*
  We assign a row number based on timestamp so that, in a future step
  we take the latest event's status.
*/
, ROW_NUMBER() OVER (PARTITION BY r.course_offering.canvas_id ORDER BY r.event_time DESC) AS row_number

FROM
`_READ_FROM_PROJECT_.event_store.expanded` AS r

WHERE

r.type = 'Event'

AND r.action = 'Modified'

AND r.course_offering.canvas_id IS NOT NULL
AND r.course_offering.canvas_id != ''
AND REGEXP_CONTAINS(r.course_offering.canvas_id, r"^\d+$") IS TRUE

AND r.object.type = 'CourseOffering'
AND JSON_EXTRACT_SCALAR(r.object.extensions, "$['com.instructure.canvas'][workflow_state]") IS NOT NULL
AND r.event_time >= '2019-01-01'
)

  SELECT
    col.lms_course_offering_id AS lms_course_offering_id

    , col.status AS status
  FROM
    course_offering_latest AS col
  WHERE
    row_number = 1 -- Only use the last update to the course offering.
    AND col.lms_course_offering_id IS NOT NULL

