DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_helper.event__course_offering__last_activity;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_helper.event__course_offering__last_activity (
    	lms_course_offering_id STRING
	  , lms_person_id STRING
	  , last_activity DATETIME
	  , last_navigation_activity DATETIME
	  , last_media_activity DATETIME
	  , last_grade_activity DATETIME
	  , last_assessment_activity DATETIME
	  , last_assignment_activity DATETIME
	  , num_events INT64
	  , num_navigation_events INT64
	  , num_media_events INT64
	  , num_grade_events INT64
	  , num_assessment_events INT64
	  , num_assignment_events INT64
    )
PARTITION BY
  DATE(last_activity);


INSERT INTO _WRITE_TO_PROJECT_.mart_helper.event__course_offering__last_activity
(
	lms_course_offering_id
  , lms_person_id
  , last_activity
  , last_navigation_activity
  , last_media_activity
  , last_grade_activity
  , last_assessment_activity
  , last_assignment_activity
  , num_events
  , num_navigation_events
  , num_media_events
  , num_grade_events
  , num_assessment_events
  , num_assignment_events
)
SELECT
/* Identifiers */
course_offering.canvas_id AS lms_course_offering_id
, person.canvas_id AS lms_person_id

/* Distinguish last activities */
, MAX(event_time) AS last_activity
, MAX(CASE WHEN type='NavigationEvent' THEN event_time ELSE NULL END) AS last_navigation_activity
, MAX(CASE WHEN type='MediaEvent' THEN event_time ELSE NULL END) AS last_media_activity
, MAX(CASE WHEN type='GradeEvent' THEN event_time ELSE NULL END) AS last_grade_activity
, MAX(CASE WHEN type='AssessmentEvent' THEN event_time ELSE NULL END) AS last_assessment_activity
, MAX(CASE WHEN type='AssignableEvent' THEN event_time ELSE NULL END) AS last_assignment_activity

/* Count activites by type */
, COUNT(1) as num_events
, SUM(CASE WHEN type='NavigationEvent' THEN 1 ELSE 0 END) as num_navigation_events
, SUM(CASE WHEN type='MediaEvent' THEN 1 ELSE 0 END) as num_media_events
, SUM(CASE WHEN type='GradeEvent' THEN 1 ELSE 0 END) as num_grade_events
, SUM(CASE WHEN type='AssessmentEvent' THEN 1 ELSE 0 END) as num_assessment_events
, SUM(CASE WHEN type='AssignableEvent' THEN 1 ELSE 0 END) as num_assignment_events

FROM
`_READ_FROM_PROJECT_.event_store.expanded`

WHERE

-- Generic events don't count.
type != 'Event'

-- Filter out records that do not have an LMS ID.
AND course_offering.canvas_id IS NOT NULL
AND REGEXP_CONTAINS(course_offering.canvas_id, r"^\d+$") IS TRUE

AND person.canvas_id IS NOT NULL
AND REGEXP_CONTAINS(person.canvas_id, r"^\d+$") IS TRUE

AND event_time >= '2021-01-01'

GROUP BY
lms_course_offering_id
, lms_person_id
