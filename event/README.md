# Event

The SQL files in this directory capture all mart building blocks that depend solely on the UDP Caliper `event_store` data.

## Starter SQL and Logic SQL

A quirk of the `event` directory is that use cases often have 2 files instead of one. For example `publish_time` has both `publish_time.sql` and `publish_time_starter.sql`

We have this structure due to the nature of the Caliper data in BigQuery: it's very expensive to do full scans of tables. The refresh frequency of these tables is often `hourly` and we usually `append`. This means we can leverage the partitioning of the `expanded` table to only ever scan 1-hour of data per refresh. 

To make this happen while also having historical views of data for these tables, the `*_starter.sql` scripts will be run once at the creation of the table and performs the following:
1. Define the schema of the table and field partitioning
2. Perform a one-time historical load of data up to the current timestamp.
It should be noted that the `*_starter.sql` queries ARE expensive. When/if we need to recreate an `event` table, this must be run and a full scan of `expanded` table data may be required.

The corresponding SQL file not suffixed by `_starter.sql` is the cheaper, hourly-running logic that is scheduled for refresh in BigQuery. The Jupyter notebook knows to look for both of these files, and the attributes in the `config.yaml` file help the script know if `_starter.sql` needs to run.

## course_offering

Similarly to the `course_offering` folder within `context`, the SQL in this directory uses `course_offering_id` as a primary pivot for aggregation and calculations. Use cases include:
- interaction_session - organizes a stream of clicks by a student in a course into sessions of learning.
- last_activity - tracks the latest timestamps of clicks by a student in a course
- obejct_interaction - records clicks by students on assets (files, discussions, etc.) for a course in scope
- publish_time - captures the timestamps for when courses get published in Canvas
- status - reports the latest status of a course. We look at `Event - Modified` Caliper events here to know how courses are changing.

## general

The `general` folder has both the `lti_tool` and `lms_tool` building blocks. We put these in `general` instead of `course_offering` because, even though a `course_offering_id` can be resolved, it's not necessarily a primary reporting pivot. The data in these tables have applications to look across multiple courses too.

- lms_tool - looks at sequences of clicks across Canvas-native tools (e.g. homepage, calendar, discussion forums, etc.)
- lti_tool - tracks the volume of LTI tool launches from Canvas

## taskforce

Again, we section off the `taskforce` components of `event` here. The use cases are the following:
- file_accesses - similar to `object_interaction` but more specific to just `files`. Object interaction is more general and contains many types of assets in courses.
- time_spent_view_days - has `interaction_sessions` as a dependency, but we play with different functional cutoffs for time deltas in events. The idea is that we present multiple flavors of time spent to fit different downstream use cases.
- tool_launches - has `lti_tool` as a dependency, and we perform tool launch aggregations and arrays of tool names launched. Helps with downstream reporting.

## config.yaml

The format of the `config.yaml` file here is the same as in `context`.
- file_path - relative path to the SQL file
- refresh_frequency - feeds into BQ scheduled query; how often to run it
- refresh_write - what to do with results; often `overwrite` for the `context` tables
- scheduled_query_name - what BQ should name the scheduled query upon creation
- bq_table_name - destination table for data
- bq_dataset - destination dataset for data
- partitioned - True or False; often applicable to `event` tables
- partition_field - if True for `partitioned`, this is the name of the field. Otherwise, it takes a value of `None`.

A main difference here is that we often see the `partitioned` flag set to True, `refresh_frequency` is hourly, and `refresh_write` is append. Again, this is to make refreshing from the `expanded` store cost efficient.

