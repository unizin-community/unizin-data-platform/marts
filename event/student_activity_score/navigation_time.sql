/*
  Event / Student activity score / Navigation time

    Captures navigation time for students in a course. 

  Properties:

    Export table:  mart_helper.event__student_activity_score__navigation_time
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:
    - mart_helper.event__student_activity_score__navigation_events 
    - mart_helper.context__student_activity_score__enrollments

  To do:
    *

*/

/* Define week numbers for enrollments */
WITH week_enrollments AS (
SELECT 
    DISTINCT  
    university_id,
    canvas_user_id,
    global_user_id,
    course_code,
    course_canvas_id,
    course_global_id,
    week_number
FROM 
   _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__enrollments, UNNEST(GENERATE_ARRAY(1,COALESCE(term_length,17))) AS week_number
)

,
/* Define time between events for navigation events */
sessionNavigationEvents AS (
  SELECT
    ne.*,
    TIMESTAMP_DIFF(LEAD(ne.event_time) OVER (PARTITION BY ne.session_id ORDER BY ne.event_time), ne.event_time, second) AS timebetweenEvents
  FROM
    _WRITE_TO_PROJECT_.mart_helper.event__student_activity_score__navigation_events ne 
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__enrollments en
    ON ne.course_id = en.course_canvas_id AND ne.user_id = en.canvas_user_id 
  -- Only include weeks in the term
  WHERE week_number BETWEEN 1 AND COALESCE(en.term_length,17)
  )
  ,
  /* Define the latest event time for a week in a course */
  course_week_metrics as (
  select     
    course_id,
    week_number,
    -- define the latest event time
    MAX(event_time) AS max_event_time,
    -- two weeks before the latest event time 
    CAST(TIMESTAMP_SUB(MAX(event_time) ,INTERVAL 2 WEEK) AS DATETIME) as two_week_diff
  from sessionNavigationEvents
  group by 
    course_id,
    week_number
  )
  ,
  /* Define all the user's events in the 2 week range for a given week in the course */ 
  events_per_week as (
  select 
    sne.* except(week_number),
    cwm.week_number
  from sessionNavigationEvents sne 
  -- define the events for a course that occured in the 2 week range for a week in the course
  JOIN course_week_metrics cwm on sne.course_id = cwm.course_id and sne.event_time <= cwm.max_event_time and sne.event_time >= cwm.two_week_diff 
  )

  ,
  /* Define navigation durations for sessions */
  sessionNavigationDurations as (
  SELECT
    course_id,
    user_id,
    week_number,
    session_id,
    SUM(timebetweenEvents) AS navigationDuration
  FROM
    events_per_week
  WHERE
    timebetweenEvents <= 1500 --ignore periods of inactivity > 25 minutes
  GROUP BY
    course_id,
    user_id,
    week_number,
    session_id 
  )

  /* Define the number of sessions and navigation time per user, course, and week in term */
  select 
    we.university_id,
    we.course_code,
    we.course_global_id,
    we.week_number,
    COUNT(DISTINCT snd.session_id) AS num_sessions,
    COALESCE(ROUND(SUM(snd.navigationDuration)/60,2), 0) AS navigationTime
  FROM sessionNavigationDurations snd
  RIGHT JOIN week_enrollments we 
    ON snd.course_id = we.course_canvas_id AND snd.user_id = we.canvas_user_id AND snd.week_number = we.week_number
where we.week_number >=1 and we.week_number <= 17
  GROUP BY 
    we.university_id,
    we.course_code,
    we.course_global_id,
    we.week_number

