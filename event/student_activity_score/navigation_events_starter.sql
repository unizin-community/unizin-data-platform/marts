/*
  Event / Student activity score / Navigation events

    Captures navigation events for students in a course. 

  Properties:

    Export table:  mart_helper.event__student_activity_score__navigation_events
    Run frequency: Every hour
    Run operation: Append

  Dependencies:
    - event_store.expanded 
    - mart_helper.context__course_offering
    - mart_helper.context__academic_term

  To do:
    *

*/

DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_helper.event__student_activity_score__navigation_events;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_helper.event__student_activity_score__navigation_events(
    	course_id STRING
    ,	course_udp_id INT64
    ,	user_id STRING
    ,	user_udp_id INT64
    ,	session_id STRING
    ,	event_time DATETIME
    ,	week_number INT64
    )
PARTITION BY
  DATE(event_time);


INSERT INTO _WRITE_TO_PROJECT_.mart_helper.event__student_activity_score__navigation_events(
    	course_id
    ,	course_udp_id
    ,	user_id
    ,	user_udp_id
    ,	session_id
    ,	event_time
    ,	week_number
)
 
 
  SELECT
    es.course_offering.canvas_id AS course_id,
    es.course_offering.udp_id AS course_udp_id,
    es.person.canvas_id AS user_id,
    es.person.udp_id AS user_udp_id,
    es.session.id AS session_id,
    event_time,
    CASE
      WHEN acs.instruction_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(es.event_time)), acs.instruction_begin_date, WEEK) + 1
      WHEN act.term_begin_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(es.event_time)), act.term_begin_date, WEEK) + 1
      WHEN co.start_date is not null then DATE_DIFF(EXTRACT(DATE FROM DATETIME(es.event_time)), co.start_date, WEEK) + 1
    ELSE null end as week_number
  FROM
    `_READ_FROM_PROJECT_.event_store.expanded` AS es
	LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering as co
	    on co.lms_id = es.course_offering.canvas_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session as acs 
      on acs.academic_session_id = co.academic_session_id
	LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act
	    on act.academic_term_id = co.academic_term_id
  WHERE
    es.course_offering.canvas_id IS NOT NULL
    AND es.person.canvas_id IS NOT NULL
    AND es.session.id IS NOT NULL
    AND es.actor.type = "Person"
    AND es.action = 'NavigatedTo' 
    AND es.event_time >= '2021-01-01'
