/*
  Event ETL / Taskforce / File Accesses

    Captures the subset of events where an interaction
    with an object is involved, per course offering.

  Properties:

    Export table:  mart_helper.event__taskforce__file_accesses
    Run frequency: Every day
    Run operation: Overwrite

  To do:
    *

*/

with initial_data as (

select 
    oi.udp_person_id as person_id
  , oi.udp_course_offering_id as course_offering_id
  , fi.file_id as file_id
  , oi.week_in_term as week_in_term
  , display_name
  , SPLIT(fi.content_type, '/')[OFFSET(0)] as content_type 
  , CASE WHEN ARRAY_LENGTH(SPLIT(fi.content_type, '/')) <= 1 THEN 'None' ELSE SPLIT(fi.content_type, '/')[OFFSET(1)] END as content_sub_type
  , size
  , event_time
from _WRITE_TO_PROJECT_.mart_helper.event__course_offering__object_interaction as oi
  join _WRITE_TO_PROJECT_.mart_helper.context__file as fi
    on fi.lms_int_id = oi.object_id
    and fi.course_offering_id = oi.udp_course_offering_id
),

weekly_file_accesses as (
select  person_id
    ,   course_offering_id
    ,   file_id
    ,   display_name
    ,   content_type
    ,   content_sub_type
    ,   week_in_term
    ,   count(event_time) as num_times_viewed
from initial_data
group by 
        person_id
    ,   course_offering_id
    ,   file_id
    ,   display_name
    ,   content_type
    ,   content_sub_type
    ,   week_in_term
)

select  fa.person_id
    ,   fa.course_offering_id
    ,   fa.week_in_term
    ,   sum(num_times_viewed) as file_views
    ,   count(distinct file_id) as num_files_viewed
    ,   ARRAY_AGG(STRUCT(file_id, display_name, content_type, content_sub_type, num_times_viewed)) as file_access_detail
from weekly_file_accesses fa 
join _WRITE_TO_PROJECT_.mart_helper.context__taskforce__course_profile c
  on fa.course_offering_id = c.course_offering_id
where 
  week_in_term > 0 
  and week_in_term <= COALESCE(
    COALESCE(
      DATE_DIFF(DATE_ADD(instruction_end_date,INTERVAL 1 WEEK),instruction_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(term_end_date,INTERVAL 1 WEEK),term_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(course_end_date,INTERVAL 1 WEEK),course_start_date,WEEK)) + 1, 17)
  and fa.person_id is not null 
  and fa.course_offering_id is not null
group by person_id
    ,    course_offering_id
    ,    week_in_term
