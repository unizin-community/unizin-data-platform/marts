/*
  Event ETL / Taskforce / Time Spent View days

  Captures the weekly quantity and frequency of time spent in learning environment

  Properties:

    Export table:  mart_helper.event__taskforce__time_spent_view_days
    Run frequency: Every day
    Run operation: Overwrite

  To do:
    *

*/

select 
    udp_person_id as person_id
  , udp_course_offering_id as course_offering_id
  , week_in_term as week_in_term
  , COUNT(distinct session_date) as view_days
  , sum(num_sessions_10min) as num_sessions_10min
  , sum(total_time_seconds_10min) as total_time_seconds_10min
  , sum(total_actions_10min) as total_actions_10min
  , SAFE_DIVIDE(sum(total_time_seconds_10min) , sum(num_sessions_10min)) as avg_time_seconds_10min
  , SAFE_DIVIDE(sum(total_actions_10min) , sum(num_sessions_10min)) as avg_actions_10min

  , sum(num_sessions_20min) as num_sessions_20min
  , sum(total_time_seconds_20min) as total_time_seconds_20min
  , sum(total_actions_20min) as total_actions_20min
  , SAFE_DIVIDE(sum(total_time_seconds_20min) , sum(num_sessions_20min)) as avg_time_seconds_20min
  , SAFE_DIVIDE(sum(total_actions_20min) , sum(num_sessions_20min)) as avg_actions_20min

  , sum(num_sessions_30min) as num_sessions_30min
  , sum(total_time_seconds_30min) as total_time_seconds_30min
  , sum(total_actions_30min) as total_actions_30min
  , SAFE_DIVIDE(sum(total_time_seconds_30min) , sum(num_sessions_30min)) as avg_time_seconds_30min
  , SAFE_DIVIDE(sum(total_actions_30min) , sum(num_sessions_30min)) as avg_actions_30min
from _WRITE_TO_PROJECT_.mart_helper.event__course_offering__interaction_sessions i
join _WRITE_TO_PROJECT_.mart_helper.context__taskforce__course_profile c
  on i.udp_course_offering_id = c.course_offering_id
where week_in_term > 0 
  and week_in_term <= COALESCE(
    COALESCE(
      DATE_DIFF(DATE_ADD(instruction_end_date,INTERVAL 1 WEEK),instruction_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(term_end_date,INTERVAL 1 WEEK),term_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(course_end_date,INTERVAL 1 WEEK),course_start_date,WEEK)) + 1, 17)
group by
  udp_person_id,
  udp_course_offering_id,
  week_in_term
