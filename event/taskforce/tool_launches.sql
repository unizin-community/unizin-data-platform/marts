/*
  Event ETL / Taskforce / Tool Launches

  Captures the weekly tool launches and array of tools launched for the taskforce marts

  Properties:

    Export table:  mart_helper.event__taskforce__tool_launches
    Run frequency: Every day
    Run operation: Overwrite

  To do:
    *

*/

with initial_data as (
  select
      udp_person_id as person_id
    , udp_course_offering_id as course_offering_id
    , week_in_term as week_in_term
    , launch_app_name
    , event_time
  from _WRITE_TO_PROJECT_.mart_helper.event__general__lti_tool
  where is_redirect_tool = False
),

launches_per_tool as (
select  id.person_id
    ,   id.course_offering_id
    ,   id.week_in_term
    ,   launch_app_name
    ,   count(event_time) as num_launches
from initial_data id 
join _WRITE_TO_PROJECT_.mart_helper.context__taskforce__course_profile c
  on id.course_offering_id = c.course_offering_id
where 
  week_in_term > 0 
  and week_in_term <= COALESCE(
    COALESCE(
      DATE_DIFF(DATE_ADD(instruction_end_date,INTERVAL 1 WEEK),instruction_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(term_end_date,INTERVAL 1 WEEK),term_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(course_end_date,INTERVAL 1 WEEK),course_start_date,WEEK)) + 1, 17)
group by 
        id.person_id
    ,   id.course_offering_id
    ,   id.week_in_term
    ,   launch_app_name
)

select  person_id
    ,   course_offering_id
    ,   week_in_term
    ,   sum(num_launches) as num_tool_launches
    ,   count(distinct launch_app_name) as num_tools_launched
    ,   ARRAY_AGG(STRUCT(launch_app_name, num_launches)) as tool_launch_detail
from launches_per_tool
group by person_id
    ,    course_offering_id
    ,    week_in_term
