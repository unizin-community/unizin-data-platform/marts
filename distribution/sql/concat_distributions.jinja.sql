{% for tbl in table_list %}
SELECT entity, field_name, field_value, field_source, distribution_type,
avg_field_value, median_field_value,sd_field_value, min_field_value, max_field_value, num_records, pct_records,
with_respect_to_field_name_1, with_respect_to_field_value_1, with_respect_to_field_name_2, with_respect_to_field_value_2,
with_respect_to_field_name_3, with_respect_to_field_value_3, number_of_dependencies
from `{{ project }}.udp_distributions.{{ tbl }}`
{% if not loop.last %}
UNION ALL
{% endif %}
{% endfor %}
;