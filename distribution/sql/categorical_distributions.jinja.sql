{# Define SQL for when custom fields -#}

{% if required_functions -%}

-- Define temporary functions required to define custom fields 

-- function to define the given number of leftmost characters from the given string 
create temp function string_left(string STRING,length INT64) AS (
    LEFT(string,length)
);

-- function to define if given date is null
create temp function is_null_date(field DATETIME) AS (
    IF(field IS NULL, TRUE, FALSE)
);

-- function to define if given integer is null
create temp function is_null_int(field INT64) AS (
    IF(field IS NULL, TRUE, FALSE)
);

-- function to define the difference in weeks between two datetime fields
create temp function date_diff_weeks(field1 DATETIME, field2 DATETIME) AS (
    DATETIME_DIFF(field2,field1,WEEK)
);

-- function to define the difference in days between two datetime fields
create temp function date_diff_days(field1 DATETIME, field2 DATETIME) AS (
    DATETIME_DIFF(field2,field1,DAY)
);

-- function to extract the file extension from the file name
create temp function extract_file_extension(file_name STRING) AS (
    CASE WHEN REGEXP_CONTAINS(file_name,r'\.')
        THEN ARRAY_REVERSE(SPLIT(file_name,'.'))[OFFSET(0)]  
    ELSE NULL END 
);

-- CTE to define custom field 
with custom_field as (
    SELECT 
        {{ entity }}.*,
        -- define custom field using the given function and parameters
        {{ required_functions['function'] }}({% for parameter in required_functions['parameters'] -%} 
            {{ parameter }}{% if loop.last -%} 
            
            {% else -%} 
                , 
            {%- endif %} 
        {%- endfor %}) as {{ field }}    
    FROM `{{ project }}.context_store_entity.{{ entity }}` {{ entity }}
    -- left join with other entities based on current entity
    {% if entity == 'learner_activity' -%}
        LEFT JOIN `{{ project }}.context_store_entity.course_offering` course_offering USING(course_offering_id)
        LEFT JOIN `{{ project }}.context_store_entity.learner_activity_group` learner_activity_group USING(learner_activity_group_id)
    {% elif entity == 'learner_activity_group' -%}
        LEFT JOIN `{{ project }}.context_store_entity.course_offering` course_offering USING(course_offering_id)
        LEFT JOIN `{{ project }}.context_store_entity.learner_activity` learner_activity USING(learner_activity_group_id)
    {% elif entity == 'learner_activity_override' -%}
        LEFT JOIN `{{ project }}.context_store_entity.learner_activity` learner_activity USING(learner_activity_id)
    {% elif entity == 'learner_activity_result' -%}
        LEFT JOIN `{{ project }}.context_store_entity.learner_activity` learner_activity USING(learner_activity_id)
    {% elif entity == 'module'  -%}
        LEFT JOIN `{{ project }}.context_store_entity.course_offering` course_offering USING(course_offering_id)
    {%- endif %}
)

{%- endif %}

{# Define SQL for dependent fields -#}

{% if with_respect_to -%}
    
-- define CTE to define the distribution of the fields related to the current field
with with_respect_to_counts as (
    SELECT 
    -- for each related field, define the field name and value
    {% for f in with_respect_to -%}
        '{{ f }}' as with_respect_to_field_name_{{ loop.index}},
        COALESCE(SAFE_CAST({{ f }} AS STRING),'NULL') as with_respect_to_field_value_{{ loop.index}},
    {%- endfor %}
        COUNT(*) AS num_records
    FROM `{{ project }}.context_store_entity.{{ entity }}`
    -- group by all the related fields
    GROUP BY 
        {% for f in with_respect_to -%}
            COALESCE(SAFE_CAST({{ f }} AS STRING),'NULL')
            {% if loop.index <  loop.length -%}
            , 
            {%- endif %}
        {%- endfor %}
)

{%- endif %}

/* 
Final query to define distributions for all field types
*/
SELECT 
    '{{ entity }}' as entity
    , '{{ field }}' as field_name
    , SAFE_CAST({{ field }} AS STRING) as field_value
    , 'Categorical' AS distribution_type
    -- define all continuous fields as null
    , SAFE_CAST(NULL AS NUMERIC) AS avg_field_value
    , SAFE_CAST(NULL AS NUMERIC) AS median_field_value
    , SAFE_CAST(NULL AS NUMERIC) AS sd_field_value
    , SAFE_CAST(NULL AS NUMERIC) AS min_field_value
    , SAFE_CAST(NULL AS NUMERIC) AS max_field_value
    , count(*) as num_records
{# if a custom field -#}
{% if required_functions %}
    , 'Custom' as field_source
    , COUNT(*)/SUM(COUNT(*)) OVER() AS pct_records
    -- define all with_respect_to fields as null
    {% for n in range(1,4) %}
        , SAFE_CAST(NULL AS STRING) as with_respect_to_field_name_{{ n}}
        , SAFE_CAST(NULL AS STRING) as with_respect_to_field_value_{{ n}}
    {% endfor %}
    , 0 as number_of_dependencies
    FROM custom_field 
    group by 
        SAFE_CAST({{ field }} AS STRING)
{# if a dependent field -#}
{% elif with_respect_to %}
    , 'UCDM' as field_source
    , COUNT(*)/wrt.num_records as pct_records
    -- define with_respect_to fields
    {% for n in range(1,4) %}
        -- if the given with_respect_to field is defined, define the with_respect_to field name and value
        {% if with_respect_to[n-1] %}
            , with_respect_to_field_name_{{ n}} 
            , NULLIF(with_respect_to_field_value_{{ n}},'NULL') AS with_respect_to_field_value_{{ n}}
        -- if the given with_respect_to field is not defined, define with_respect_to field as null
        {% else %}
            , SAFE_CAST(NULL AS STRING) as with_respect_to_field_name_{{ n}}
            , SAFE_CAST(NULL AS STRING) as with_respect_to_field_value_{{ n}}
        {% endif %}
    {% endfor %}
    , {{ with_respect_to | length }} as number_of_dependencies
    from `{{ project }}.context_store_entity.{{ entity }}` e
    -- join with with_respect_to_counts on the with_respect_to fields 
    join with_respect_to_counts wrt on 
        {% for f in with_respect_to %}
            wrt.with_respect_to_field_value_{{ loop.index}} = COALESCE(SAFE_CAST(e.{{f}} AS STRING),'NULL')
            {% if loop.index <  loop.length %}
            and
            {% endif %}
        {% endfor %}
    -- group by the with_respect_to field names and values
    group by 
        {% for f in with_respect_to %}
            wrt.with_respect_to_field_name_{{ loop.index}},
            wrt.with_respect_to_field_value_{{loop.index}},
        {% endfor %}
        wrt.num_records,
        e.{{ field }}
{# for base UCDM fields -#}
{% else %}
    , 'UCDM' as field_source
    -- define the percent of records associated with the field value
    , COUNT(*)/SUM(COUNT(*)) OVER() AS pct_records
    -- define all with_respect_to fields as null
    {% for n in range(1,4) %}
        , SAFE_CAST(NULL AS STRING) as with_respect_to_field_name_{{ n}}
        , SAFE_CAST(NULL AS STRING) as with_respect_to_field_value_{{ n}}
    {% endfor %}
    , 0 as number_of_dependencies
    from `{{ project }}.context_store_entity.{{ entity }}` e
    group by 
        SAFE_CAST(e.{{ field }} AS STRING)
{% endif %}
;