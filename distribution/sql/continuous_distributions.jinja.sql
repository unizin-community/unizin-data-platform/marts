-- define the quartiles for the field values
WITH entity_quartiles AS (
  select 
    percentiles[offset(25)] as q1,
    percentiles[offset(50)] as median,
    percentiles[offset(75)] as q3,
    percentiles[offset(75)] - percentiles[offset(25)] as iqr

  FROM (
    SELECT APPROX_QUANTILES(SAFE_CAST({{ field }} AS NUMERIC),100) AS percentiles 
    FROM `{{ project }}.context_store_entity.{{ entity }}`
    )
),

-- remove outliers based on the IQR
filtered_entity as (
SELECT e.*
FROM `{{ project }}.context_store_entity.{{ entity }}` e
, entity_quartiles
where SAFE_CAST({{ field }} AS NUMERIC) <= (q3+(1.5*iqr))
and SAFE_CAST({{ field }} AS NUMERIC) >= (q1-(1.5*iqr))
),

-- define quartiles for the field values without the outliers
updated_quartiles as (
  select 
    percentiles[offset(25)] as q1,
    percentiles[offset(50)] as median,
    percentiles[offset(75)] as q3
  FROM (SELECT APPROX_QUANTILES(SAFE_CAST({{ field }} AS NUMERIC),100) AS percentiles FROM filtered_entity)
)


-- define summary statistics for the field values without the outliers 
SELECT 
    '{{ entity }}' as entity
    , '{{ field }}' as field_name
    , 'UCDM' as field_source
    , 'Continuous' as distribution_type
    , SAFE_CAST(AVG(SAFE_CAST({{ field }} AS NUMERIC)) AS NUMERIC) AS avg_field_value
    , SAFE_CAST(AVG(SAFE_CAST(median AS NUMERIC)) AS NUMERIC) AS median_field_value
    , SAFE_CAST(STDDEV(SAFE_CAST({{ field }} AS NUMERIC)) AS NUMERIC) AS sd_field_value
    , SAFE_CAST(MIN(SAFE_CAST({{ field }} AS NUMERIC)) AS NUMERIC) as min_field_value
    , SAFE_CAST(MAX(SAFE_CAST({{ field }} AS NUMERIC)) AS NUMERIC) as max_field_value
    , SAFE_CAST(NULL AS STRING) as field_value 
    {% for n in range(1,4) %}
        , SAFE_CAST(NULL AS STRING) as with_respect_to_field_name_{{ n}}
        , SAFE_CAST(NULL AS STRING) as with_respect_to_field_value_{{ n}}
    {% endfor %}
    , SAFE_CAST(NULL AS INT64) as num_records
    , SAFE_CAST(NULL AS FLOAT64) as pct_records 
    , 0 as number_of_dependencies 
    from filtered_entity e
    , updated_quartiles 

;