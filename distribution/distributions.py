from lib2to3.fixes.fix_input import context
from google.cloud import bigquery
from google.cloud.exceptions import NotFound
from pathlib import Path
import pandas as pd
import logging
import os
from jinja2 import Template
from typing import Any, Callable
import contextvars
import yaml
from typing import List

FORMAT = "%(asctime)s %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)
sql_dir = Path(__file__).parent
bq_client = contextvars.ContextVar('bq_client')

class BqClient:
    """allows for global access to BQ client via contextvars."""

    def __init__(self, val):
        self.val = val

    def __enter__(self):
        self.token = bq_client.set(self.val)

    def __exit__(self, *_, **__):
        bq_client.reset(self.token)


class EntityConfig:
    def __init__(self, entity, filter_fields: list):
        self.entity = entity
        self.filter_fields = filter_fields

    def set_fields(
        self,
        project: str,
        dataset: str,
        client: bigquery.Client,
    ):
        get_fields_qry = f"""
                SELECT column_name, data_type
                FROM `{project}.{dataset}.INFORMATION_SCHEMA.COLUMNS`
                WHERE table_name = '{self.entity}'
            """
        logger.debug("Query get_fields: %s", get_fields_qry)
        self.fields = {
            row["column_name"]: row["data_type"] for row in client.query(get_fields_qry)
        }

    def get_row_count(self, project: str, dataset: str, client: bigquery.Client):
        row_count_qry = f"""
        SELECT table_id, row_count
        FROM `{project}.{dataset}.__TABLES__`
        WHERE table_id = '{self.entity}'
        """
        return [row["row_count"] for row in client.query(row_count_qry)][0]

    def get_filtered_schema(self):
        if not self.fields:
            raise Exception(
                f"Fields for {self.entity} not set. Please provide GCP project and dataset."
            )
        return {s: self.fields.get(s) for s in self.fields if s in self.filter_fields}


def normalize_path(file_path: str = os.path.dirname(__file__), *args) -> str:
    """
    Given a file path, and a arguments of subfolders, return the absolute path to the given subfolder in a directory.
    """
    reference_path = os.path.abspath(os.path.join(file_path, *args))
    return reference_path


def set_config(yml: str = normalize_path(os.path.dirname(__file__), "config.yaml")):
    with open(yml, "r") as f:
        config = yaml.safe_load(f)
    return config


def render_sql_file(path: str, **base_kwargs: Any) -> Callable[..., str]:
    """jinja template rendering"""
    with open(path, "r") as fh:
        template = Template(fh.read())

    return lambda **kwargs: template.render(**{**base_kwargs, **kwargs})


def get_tables(client: bigquery.Client, project: str, dataset: str):
    client = bq_client.get()
    qry_template = """
        SELECT table_id, row_count
        FROM `{{ project }}.{{ dataset }}.__TABLES__`
        order by table_id desc;
    """
    tables_query = Template(qry_template).render(project=project, dataset=dataset)
    logger.info("Query get_tables: %s", tables_query)

    logger.debug("Query get_tables: %s", tables_query)

    return list(client.query(tables_query).result())


def main():
    """
    Demonstrates module when run as standalone script, not meant to be an implementation.
    """
    project = "unizin-dev-sandbox"
    dataset_id = "context_store_entity"
    entities = [
        "person",
        "learner_activity",
        "course_offering",
        "learner_activity_result",
    ]


    template_path = os.path.join(os.path.dirname(__file__), "sql/categorical.jinja.sql")
    # client = BqClient(bigquery.Client())
    bq_client.set(bigquery.Client())
    config = set_config()
    entities = config["distributions"]["categorical"]["entities"].keys()
    job_config = bigquery.QueryJobConfig(
            destination="unizin-dev-sandbox.udp_distributions.categorical",
            # create_disposition=bigquery.CreateDisposition.CREATE_IF_NEEDED,
            write_disposition="WRITE_APPEND",
            allow_large_results=True,
            # time_partitioning=time_partitioning,
            dry_run=dag_base.DRY_RUN 
        )
    if os.path.exists(template_path):
        for e in entities:
            e_config = EntityConfig(
                e, ["type", "gender", "status", "subject"]
            )
            e_config.set_fields(project, dataset_id, bigquery.Client())
            row_count = e_config.get_row_count(project, dataset_id, bigquery.Client())
            print(e_config.get_filtered_schema())
            for f in e_config.get_filtered_schema():
                dist_template = render_sql_file(template_path)(
                    entity=e_config.entity,
                    project=project,
                    field=f,
                    row_count=row_count,
                )

                print(f"Query Template: \n\n {dist_template}")
                distributions = bq_client.get().query(dist_template, job_config)
                dist_df = distributions.to_dataframe()
                logger.info(dist_df[dist_df.columns[:-3]])


if __name__ == "__main__":
    main()
