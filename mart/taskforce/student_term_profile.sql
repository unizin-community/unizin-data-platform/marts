/* 
  Mart / Taskforce / Student_Term_Profile
    Describes the profile of a student in an academic term. 
    Passthrough from the warehouse.taskforce.course_profile table
    
  Properties:

    Export table:  mart_taskforce.student_term_profile
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Context / taskforce / student_term_profile

  To do:
    *
*/

select *
from _WRITE_TO_PROJECT_.mart_helper.context__taskforce__student_term_profile
