/* 
  Mart / Taskforce / Student_Term_Profile
    Describes the "average" behavior per week as well as standard deviations for each field
    Also lists all tools and files possible to access based on activity
    Mostly joins from some level2 helper tables + other calculations and aggregations
    
  Properties:

    Export table:  mart_taskforce.student_term_profile
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Mart / taskforce / level2_course_week_files
    * Mart / taskforce / level2_course_week_tools
    * Mart / taskforce / level2_course_week_metrics

  To do:
    *
*/

select l2_cwm.*
  , l2_cwf.file_ids
  , l2_cwf.total_views as total_file_views
  , l2_cwt.launch_app_names
  , l2_cwt.total_launches as total_tool_launches

from _WRITE_TO_PROJECT_.mart_taskforce.level2_course_week_metrics as l2_cwm
--course week files
left join _WRITE_TO_PROJECT_.mart_taskforce.level2_course_week_files as l2_cwf
  on l2_cwf.udp_course_offering_id = l2_cwm.udp_course_offering_id
  and l2_cwf.week_in_term = l2_cwm.week_in_term
--course week tools
left join _WRITE_TO_PROJECT_.mart_taskforce.level2_course_week_tools as l2_cwt
  on l2_cwt.udp_course_offering_id = l2_cwm.udp_course_offering_id
  and l2_cwt.week_in_term = l2_cwm.week_in_term
