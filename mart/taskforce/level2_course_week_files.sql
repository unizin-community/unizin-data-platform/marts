/* 
  Mart / Taskforce / Level 2 Course Week Files
    Aggregates all interactions of students with files for a course in a given week.
    
  Properties:

    Export table:  mart_taskforce.level2_course_week_files
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Mart / Taskforce / Level 1 Aggregated
  To do:
    *
*/

WITH course_week_files as (
select 
    udp_course_offering_id
    ,week_in_term
    , week_start_date
    , week_end_date
    ,file_access_detail.file_id as file_id
    ,file_access_detail.display_name as file_display_name
    ,file_access_detail.content_type as file_content_type,file_access_detail.content_sub_type as file_content_sub_type
    ,file_access_detail.num_times_viewed as file_num_times_viewed
from _WRITE_TO_PROJECT_.mart_taskforce.level1_aggregated,unnest(file_access_detail) as file_access_detail
order by udp_course_offering_id,week_in_term
)
,
course_week_files_aggregated as (
select 
    udp_course_offering_id
    ,week_in_term
    , week_start_date 
    , week_end_date
    ,file_id
    ,sum(file_num_times_viewed) as total_views
from course_week_files
group by udp_course_offering_id,week_in_term,week_start_date,week_end_date,file_id,file_display_name 
)

select 
    udp_course_offering_id
    ,week_in_term
    , week_start_date 
    , week_end_date
    ,array_agg(file_id order by file_id) as file_ids
    ,array_agg(total_views order by file_id) as total_views
from course_week_files_aggregated 
group by udp_course_offering_id,week_in_term,week_start_date,week_end_date

