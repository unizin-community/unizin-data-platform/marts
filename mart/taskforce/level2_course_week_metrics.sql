/* 
  Mart / Taskforce / Level 2 Course Week Metrics
    Computes metrics for all students in a course for a given week. 
    
  Properties:

    Export table:  mart_taskforce.level2_course_week_metrics
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Mart / Taskforce / Level 1 Aggregated
  To do:
    *
*/

select  
  -- PRIMARY PIVOTS
  udp_course_offering_id
  , week_in_term
  , week_start_date 
  , week_end_date
  -- SUBMISSIONS
  , avg(num_tiny_submissions) as avg_num_tiny_submissions
  , stddev(num_tiny_submissions) as sd_num_tiny_submissions
  , sum(num_tiny_submissions) as total_tiny_submissions
  , sum(CASE WHEN num_tiny_submissions > 0 then 1 else 0 end) as num_students_tiny_submissions
  , avg(num_small_submissions) as avg_num_small_submissions
  , stddev(num_small_submissions) as sd_num_small_submissions
  , coalesce(sum(num_small_submissions),0) as total_small_submissions
  , sum(CASE WHEN num_small_submissions > 0 then 1 else 0 end) as num_students_small_submissions
  , avg(num_medium_submissions) as avg_num_medium_submissions
  , stddev(num_medium_submissions) as sd_num_medium_submissions
  , coalesce(sum(num_medium_submissions),0) as total_medium_submissions
  , sum(CASE WHEN num_medium_submissions > 0 then 1 else 0 end) as num_students_medium_submissions
  , avg(num_large_submissions) as avg_num_large_submissions
  , stddev(num_large_submissions) as sd_num_large_submissions
  , coalesce(sum(num_large_submissions),0) as total_large_submissions
  , sum(CASE WHEN num_large_submissions > 0 then 1 else 0 end) as num_students_large_submissions
  , avg(num_major_submissions) as avg_num_major_submissions
  , stddev(num_major_submissions) as sd_num_major_submissions
  , coalesce(sum(num_major_submissions),0) as total_major_submissions
  , sum(CASE WHEN num_major_submissions > 0 then 1 else 0 end) as num_students_major_submissions
  , avg(num_unweighted_submissions) as avg_num_unweighted_submissions
  , stddev(num_unweighted_submissions) as sd_num_unweighted_submissions
  , coalesce(sum(num_unweighted_submissions),0) as total_unweighted_submissions
  , sum(CASE WHEN num_unweighted_submissions > 0 then 1 else 0 end) as num_students_unweighted_submissions
  , avg(num_weighted_submissions) as avg_num_weighted_submissions
  , stddev(num_weighted_submissions) as sd_num_weighted_submissions
  , coalesce(sum(num_weighted_submissions),0) as total_weighted_submissions
  , sum(CASE WHEN num_weighted_submissions > 0 then 1 else 0 end) as num_students_weighted_submissions
  , avg(num_submissions_without_due_date) as avg_num_submissions_without_due_date
  , stddev(num_submissions_without_due_date) as sd_num_submissions_without_due_date
  , coalesce(sum(num_submissions_without_due_date),0) as total_submissions_without_due_date
  , sum(CASE WHEN num_submissions_without_due_date > 0 then 1 else 0 end) as num_students_submissions_without_due_date
  , avg(num_submissions_with_due_date) as avg_num_submissions_with_due_date
  , stddev(num_submissions_with_due_date) as sd_num_submissions_with_due_date
  , coalesce(sum(num_submissions_with_due_date),0) as total_submissions_with_due_date
  , sum(CASE WHEN num_submissions_with_due_date > 0 then 1 else 0 end) as num_students_submissions_with_due_date
  , avg(num_submissions) as avg_num_submissions
  , stddev(num_submissions) as sd_num_submissions
  , coalesce(sum(num_submissions),0) as total_submissions
  , sum(CASE WHEN num_submissions > 0 then 1 else 0 end) as num_students_submissions

  -- ASSIGNMENTS
  , avg(num_tiny_assignments) as avg_num_tiny_assignments
  , stddev(num_tiny_assignments) as sd_num_tiny_assignments
  , avg(num_small_assignments) as avg_num_small_assignments
  , stddev(num_small_assignments) as sd_num_small_assignments
  , avg(num_medium_assignments) as avg_num_medium_assignments
  , stddev(num_medium_assignments) as sd_num_medium_assignments
  , avg(num_large_assignments) as avg_num_large_assignments
  , stddev(num_large_assignments) as sd_num_large_assignments
  , avg(num_major_assignments) as avg_num_major_assignments
  , stddev(num_major_assignments) as sd_num_major_assignments
  , avg(num_unweighted_assignments) as avg_num_unweighted_assignments
  , stddev(num_unweighted_assignments) as sd_num_unweighted_assignments
  , avg(num_weighted_assignments) as avg_num_weighted_assignments
  , stddev(num_weighted_assignments) as sd_num_weighted_assignments
  , avg(num_assignments_without_due_date) as avg_num_assignments_without_due_date
  , stddev(num_assignments_without_due_date) as sd_num_assignments_without_due_date
  , avg(num_assignments_with_due_date) as avg_num_assignments_with_due_date
  , stddev(num_assignments_with_due_date) as sd_num_assignments_with_due_date
  , avg(num_assignments) as avg_num_assignments
  , stddev(num_assignments) as sd_num_assignments

  -- MISSING SUBMISSIONS
  , avg(num_tiny_missing_submissions) as avg_num_tiny_missing_submissions
  , stddev(num_tiny_missing_submissions) as sd_num_tiny_missing_submissions
  , avg(num_small_missing_submissions) as avg_num_small_missing_submissions
  , stddev(num_small_missing_submissions) as sd_num_small_missing_submissions
  , avg(num_medium_missing_submissions) as avg_num_medium_missing_submissions
  , stddev(num_medium_missing_submissions) as sd_num_medium_missing_submissions
  , avg(num_large_missing_submissions) as avg_num_large_missing_submissions
  , stddev(num_large_missing_submissions) as sd_num_large_missing_submissions
  , avg(num_major_missing_submissions) as avg_num_major_missing_submissions
  , stddev(num_major_missing_submissions) as sd_num_major_missing_submissions
  , avg(num_unweighted_missing_submissions) as avg_num_unweighted_missing_submissions
  , stddev(num_unweighted_missing_submissions) as sd_num_unweighted_missing_submissions
  , avg(num_weighted_missing_submissions) as avg_num_weighted_missing_submissions
  , stddev(num_weighted_missing_submissions) as sd_num_weighted_missing_submissions
  , avg(num_missing_submissions) as avg_num_missing_submissions
  , stddev(num_missing_submissions) as sd_num_missing_submissions

  -- LATE SUBMISSIONS
  , avg(num_tiny_late_submissions) as avg_num_tiny_late_submissions
  , stddev(num_tiny_late_submissions) as sd_num_tiny_late_submissions
  , avg(num_small_late_submissions) as avg_num_small_late_submissions
  , stddev(num_small_late_submissions) as sd_num_small_late_submissions
  , avg(num_medium_late_submissions) as avg_num_medium_late_submissions
  , stddev(num_medium_late_submissions) as sd_num_medium_late_submissions
  , avg(num_large_late_submissions) as avg_num_large_late_submissions
  , stddev(num_large_late_submissions) as sd_num_large_late_submissions
  , avg(num_major_late_submissions) as avg_num_major_late_submissions
  , stddev(num_major_late_submissions) as sd_num_major_late_submissions
  , avg(num_unweighted_late_submissions) as avg_num_unweighted_late_submissions
  , stddev(num_unweighted_late_submissions) as sd_num_unweighted_late_submissions
  , avg(num_weighted_late_submissions) as avg_num_weighted_late_submissions
  , stddev(num_weighted_late_submissions) as sd_num_weighted_late_submissions
  , avg(num_late_submissions) as avg_num_late_submissions
  , stddev(num_late_submissions) as sd_num_late_submissions

  -- TIME BUFFERS
  , avg(avg_time_buffer_hrs_tiny) as avg_avg_time_buffer_hrs_tiny
  , stddev(avg_time_buffer_hrs_tiny) as sd_avg_time_buffer_hrs_tiny
  , avg(avg_time_buffer_hrs_small) as avg_avg_time_buffer_hrs_small
  , stddev(avg_time_buffer_hrs_small) as sd_avg_time_buffer_hrs_small
  , avg(avg_time_buffer_hrs_medium) as avg_avg_time_buffer_hrs_medium
  , stddev(avg_time_buffer_hrs_medium) as sd_avg_time_buffer_hrs_medium
  , avg(avg_time_buffer_hrs_large) as avg_avg_time_buffer_hrs_large
  , stddev(avg_time_buffer_hrs_large) as sd_avg_time_buffer_hrs_large
  , avg(avg_time_buffer_hrs_major) as avg_avg_time_buffer_hrs_major
  , stddev(avg_time_buffer_hrs_major) as sd_avg_time_buffer_hrs_major
  , avg(avg_time_buffer_hrs_unweighted) as avg_avg_time_buffer_hrs_unweighted
  , stddev(avg_time_buffer_hrs_unweighted) as sd_avg_time_buffer_hrs_unweighted
  , avg(avg_time_buffer_hrs_weighted) as avg_avg_time_buffer_hrs_weighted
  , stddev(avg_time_buffer_hrs_weighted) as sd_avg_time_buffer_hrs_weighted
  , avg(avg_time_buffer_hrs) as avg_avg_time_buffer_hrs
  , stddev(avg_time_buffer_hrs) as sd_avg_time_buffer_hrs

  -- SCORES
  , avg(avg_published_score_pct_tiny) as avg_avg_published_score_pct_tiny
  , stddev(avg_published_score_pct_tiny) as sd_avg_published_score_pct_tiny
  , avg(avg_published_score_pct_small) as avg_avg_published_score_pct_small
  , stddev(avg_published_score_pct_small) as sd_avg_published_score_pct_small
  , avg(avg_published_score_pct_medium) as avg_avg_published_score_pct_medium
  , stddev(avg_published_score_pct_medium) as sd_avg_published_score_pct_medium
  , avg(avg_published_score_pct_large) as avg_avg_published_score_pct_large
  , stddev(avg_published_score_pct_large) as sd_avg_published_score_pct_large
  , avg(avg_published_score_pct_major) as avg_avg_published_score_pct_major
  , stddev(avg_published_score_pct_major) as sd_avg_published_score_pct_major
  , avg(avg_score_pct_unweighted) as avg_avg_score_pct_unweighted
  , stddev(avg_score_pct_unweighted) as sd_avg_score_pct_unweighted
  , avg(avg_published_score_pct_weighted) as avg_avg_published_score_pct_weighted
  , stddev(avg_published_score_pct_weighted) as sd_avg_published_score_pct_weighted
  , avg(avg_published_score_pct_without_due_date) as avg_avg_published_score_pct_without_due_date
  , stddev(avg_published_score_pct_without_due_date) as sd_avg_published_score_pct_without_due_date
  , avg(avg_published_score_pct_with_due_date) as avg_avg_published_score_pct_with_due_date
  , stddev(avg_published_score_pct_with_due_date) as sd_avg_published_score_pct_with_due_date 
  , avg(avg_published_score) as avg_avg_published_score
  , stddev(avg_published_score) as sd_avg_published_score

  -- CUMULATIVE SCORES
  , avg(avg_published_score_pct_tiny_cumulative) as avg_avg_published_score_pct_tiny_cumulative
  , stddev(avg_published_score_pct_tiny_cumulative) as sd_avg_published_score_pct_tiny_cumulative
  , avg(avg_published_score_pct_small_cumulative) as avg_avg_published_score_pct_small_cumulative
  , stddev(avg_published_score_pct_small_cumulative) as sd_avg_published_score_pct_small_cumulative
  , avg(avg_published_score_pct_medium_cumulative) as avg_avg_published_score_pct_medium_cumulative
  , stddev(avg_published_score_pct_medium_cumulative) as sd_avg_published_score_pct_medium_cumulative
  , avg(avg_published_score_pct_large_cumulative) as avg_avg_published_score_pct_large_cumulative
  , stddev(avg_published_score_pct_large_cumulative) as sd_avg_published_score_pct_large_cumulative
  , avg(avg_published_score_pct_major_cumulative) as avg_avg_published_score_pct_major_cumulative
  , stddev(avg_published_score_pct_major_cumulative) as sd_avg_published_score_pct_major_cumulative
  , avg(avg_score_pct_unweighted_cumulative) as avg_avg_score_pct_unweighted_cumulative
  , stddev(avg_score_pct_unweighted_cumulative) as sd_avg_score_pct_unweighted_cumulative
  , avg(avg_published_score_pct_weighted_cumulative) as avg_avg_published_score_pct_weighted_cumulative
  , stddev(avg_published_score_pct_weighted_cumulative) as sd_avg_published_score_pct_weighted_cumulative
  , avg(avg_published_score_pct_without_due_date_cumulative) as avg_avg_published_score_pct_without_due_date_cumulative
  , stddev(avg_published_score_pct_without_due_date_cumulative) as sd_avg_published_score_pct_without_due_date_cumulative
  , avg(avg_published_score_pct_with_due_date_cumulative) as avg_avg_published_score_pct_with_due_date_cumulative
  , stddev(avg_published_score_pct_with_due_date_cumulative) as sd_avg_published_score_pct_with_due_date_cumulative 
  , avg(avg_published_score_cumulative) as avg_avg_published_score_cumulative
  , stddev(avg_published_score_cumulative) as sd_avg_published_score_cumulative

  -- DISCUSSION ENTRY
  , avg(discussion_entry_count) as avg_discussion_entry_count
  , stddev(discussion_entry_count) as sd_discussion_entry_count
  , avg(discussion_post_count) as avg_discussion_post_count
  , stddev(discussion_post_count) as sd_discussion_post_count
  , avg(discussion_reply_count) as avg_discussion_reply_count
  , stddev(discussion_reply_count) as sd_discussion_reply_count
  , avg(discussion_count) as avg_discussion_count
  , stddev(discussion_count) as sd_discussion_count
  , avg(assignment_discussion_count) as avg_assignment_discussion_count
  , stddev(assignment_discussion_count) as sd_assignment_discussion_count
  , avg(threaded_discussion_count) as avg_threaded_discussion_count
  , stddev(threaded_discussion_count) as sd_threaded_discussion_count
  , avg(side_comment_discussion_count) as avg_side_comment_discussion_count
  , stddev(side_comment_discussion_count) as sd_side_comment_discussion_count
  , avg(total_discussion_count) as avg_total_discussion_count
  , stddev(total_discussion_count) as sd_total_discussion_count
  , avg(total_assignment_discussion_count) as avg_total_assignment_discussion_count
  , stddev(total_assignment_discussion_count) as sd_total_assignment_discussion_count
  , avg(total_threaded_discussion_count) as avg_total_threaded_discussion_count
  , stddev(total_threaded_discussion_count) as sd_total_threaded_discussion_count
  , avg(total_side_comment_discussion_count) as avg_total_side_comment_discussion_count
  , stddev(total_side_comment_discussion_count) as sd_total_side_comment_discussion_count
  , avg(avg_discussion_entry_length) as avg_avg_discussion_entry_length
  , stddev(avg_discussion_entry_length) as sd_avg_discussion_entry_length
  , avg(avg_discussion_post_length) as avg_avg_discussion_post_length
  , stddev(avg_discussion_post_length) as sd_avg_discussion_post_length
  , avg(avg_discussion_reply_length) as avg_avg_discussion_reply_length
  , stddev(avg_discussion_reply_length) as sd_avg_discussion_reply_length

  -- VIEW DAYS
  , avg(view_days) as avg_view_days
  , stddev(view_days) as sd_view_days

  -- INTERACTION SESSIONS
  , avg(num_sessions_10min) as avg_num_sessions_10min
  , stddev(num_sessions_10min) as sd_num_sessions_10min
  , avg(total_time_seconds_10min) as avg_total_time_seconds_10min
  , stddev(total_time_seconds_10min) as sd_total_time_seconds_10min
  , avg(total_actions_10min) as avg_total_actions_10min
  , stddev(total_actions_10min) as sd_total_actions_10min
  , avg(avg_time_seconds_10min) as avg_avg_time_seconds_10min
  , stddev(avg_time_seconds_10min) as sd_avg_time_seconds_10min
  , avg(avg_actions_10min) as avg_avg_actions_10min
  , stddev(avg_actions_10min) as sd_avg_actions_10min
  , avg(num_sessions_20min) as avg_num_sessions_20min
  , stddev(num_sessions_20min) as sd_num_sessions_20min
  , avg(total_time_seconds_20min) as avg_total_time_seconds_20min
  , stddev(total_time_seconds_20min) as sd_total_time_seconds_20min
  , avg(total_actions_20min) as avg_total_actions_20min
  , stddev(total_actions_20min) as sd_total_actions_20min
  , avg(avg_time_seconds_20min) as avg_avg_time_seconds_20min
  , stddev(avg_time_seconds_20min) as sd_avg_time_seconds_20min
  , avg(avg_actions_20min) as avg_avg_actions_20min
  , stddev(avg_actions_20min) as sd_avg_actions_20min
  , avg(num_sessions_30min) as avg_num_sessions_30min
  , stddev(num_sessions_30min) as sd_num_sessions_30min
  , avg(total_time_seconds_30min) as avg_total_time_seconds_30min
  , stddev(total_time_seconds_30min) as sd_total_time_seconds_30min
  , avg(total_actions_30min) as avg_total_actions_30min
  , stddev(total_actions_30min) as sd_total_actions_30min
  , avg(avg_time_seconds_30min) as avg_avg_time_seconds_30min
  , stddev(avg_time_seconds_30min) as sd_avg_time_seconds_30min
  , avg(avg_actions_30min) as avg_avg_actions_30min
  , stddev(avg_actions_30min) as sd_avg_actions_30min
  , avg(num_tool_launches) as avg_num_tool_launches
  , stddev(num_tool_launches) as sd_num_tool_launches
  , avg(num_tools_launched) as avg_num_tools_launched
  , stddev(num_tools_launched) as sd_num_tools_launched

  -- FILE VIEWS
  , avg(file_views) as avg_file_views
  , stddev(file_views) as sd_file_views
  , avg(num_files_viewed) as avg_num_files_viewed
  , stddev(num_files_viewed) as sd_num_files_viewed

from _WRITE_TO_PROJECT_.mart_taskforce.level1_aggregated
group by udp_course_offering_id,week_in_term,week_start_date,week_end_date
