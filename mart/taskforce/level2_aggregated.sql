/* 
  Mart / Taskforce / Level 2 Aggregated
    Compares a individual student's performance and activities to the average student's performance and 
    activities in a course on a weekly basis.
    
  Properties:

    Export table:  mart_taskforce.level2_aggregated
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Mart / Taskforce / Level 1 Aggregated
    * Mart / Taskforce / Level 2 Course Week Metrics
    * Mart / Taskforce / Level 2 Course Week Tools
    * Mart / Taskforce / Level 2 Course Week Files
  To do:
    *
*/

select 
  la.udp_person_id as udp_person_id
  , la.lms_person_id as lms_person_id 
  , la.udp_course_offering_id as udp_course_offering_id
  , la.lms_course_offering_id as lms_course_offering_id 
  , la.week_in_term as week_in_term
  , la.week_start_date as week_start_date 
  , la.week_end_date as week_end_date
  , CASE WHEN sd_num_tiny_submissions <> 0 
      THEN (num_tiny_submissions - avg_num_tiny_submissions) / sd_num_tiny_submissions 
      END AS num_tiny_submissions_z
  , CASE WHEN sd_num_small_submissions <> 0 
      THEN (num_small_submissions - avg_num_small_submissions) / sd_num_small_submissions 
      END AS num_small_submissions_z
  , CASE WHEN sd_num_medium_submissions <> 0 
      THEN (num_medium_submissions - avg_num_medium_submissions) / sd_num_medium_submissions 
      END AS num_medium_submissions_z
  , CASE WHEN sd_num_large_submissions <> 0 
      THEN (num_large_submissions - avg_num_large_submissions) / sd_num_large_submissions 
      END AS num_large_submissions_z
  , CASE WHEN sd_num_major_submissions <> 0 
      THEN (num_major_submissions - avg_num_major_submissions) / sd_num_major_submissions 
      END AS num_major_submissions_z
  , CASE WHEN sd_num_unweighted_submissions <> 0 
      THEN (num_unweighted_submissions - avg_num_unweighted_submissions) / sd_num_unweighted_submissions 
      END AS num_unweighted_submissions_z
  , CASE WHEN sd_num_weighted_submissions <> 0 
      THEN (num_weighted_submissions - avg_num_weighted_submissions) / sd_num_weighted_submissions 
      END AS num_weighted_submissions_z
  , CASE WHEN sd_num_submissions_without_due_date <> 0 
      THEN (num_submissions_without_due_date - avg_num_submissions_without_due_date) / sd_num_submissions_without_due_date 
      END AS num_submissions_without_due_date_z
  , CASE WHEN sd_num_submissions_with_due_date <> 0 
      THEN (num_submissions_with_due_date - avg_num_submissions_with_due_date) / sd_num_submissions_with_due_date 
      END AS num_submissions_with_due_date_z
  , CASE WHEN sd_num_submissions <> 0 
      THEN (num_submissions - avg_num_submissions) / sd_num_submissions 
      END AS num_submissions_z
  , CASE WHEN sd_num_tiny_assignments <> 0 
      THEN (num_tiny_assignments - avg_num_tiny_assignments) / sd_num_tiny_assignments 
      END AS num_tiny_assignments_z
  , CASE WHEN sd_num_small_assignments <> 0 
      THEN (num_small_assignments - avg_num_small_assignments) / sd_num_small_assignments 
      END AS num_small_assignments_z
  , CASE WHEN sd_num_medium_assignments <> 0 
      THEN (num_medium_assignments - avg_num_medium_assignments) / sd_num_medium_assignments 
      END AS num_medium_assignments_z
  , CASE WHEN sd_num_large_assignments <> 0 
      THEN (num_large_assignments - avg_num_large_assignments) / sd_num_large_assignments 
      END AS num_large_assignments_z
  , CASE WHEN sd_num_major_assignments <> 0 
      THEN (num_major_assignments - avg_num_major_assignments) / sd_num_major_assignments 
      END AS num_major_assignments_z
  , CASE WHEN sd_num_unweighted_assignments <> 0 
      THEN (num_unweighted_assignments - avg_num_unweighted_assignments) / sd_num_unweighted_assignments 
      END AS num_unweighted_assignments_z
  , CASE WHEN sd_num_weighted_assignments <> 0 
      THEN (num_weighted_assignments - avg_num_weighted_assignments) / sd_num_weighted_assignments 
      END AS num_weighted_assignments_z
  , CASE WHEN sd_num_assignments_without_due_date <> 0 
      THEN (num_assignments_without_due_date - avg_num_assignments_without_due_date) / sd_num_assignments_without_due_date 
      END AS num_assignments_without_due_date_z
  , CASE WHEN sd_num_assignments_with_due_date <> 0 
      THEN (num_assignments_with_due_date - avg_num_assignments_with_due_date) / sd_num_assignments_with_due_date 
      END AS num_assignments_with_due_date_z
  , CASE WHEN sd_num_assignments <> 0 
      THEN (num_assignments - avg_num_assignments) / sd_num_assignments 
      END AS num_assignments_z
  , CASE WHEN sd_num_tiny_missing_submissions <> 0 
      THEN (num_tiny_missing_submissions - avg_num_tiny_missing_submissions) / sd_num_tiny_missing_submissions 
      END AS num_tiny_missing_submissions_z
  , CASE WHEN sd_num_small_missing_submissions <> 0 
      THEN (num_small_missing_submissions - avg_num_small_missing_submissions) / sd_num_small_missing_submissions 
      END AS num_small_missing_submissions_z
  , CASE WHEN sd_num_medium_missing_submissions <> 0 
      THEN (num_medium_missing_submissions - avg_num_medium_missing_submissions) / sd_num_medium_missing_submissions 
      END AS num_medium_missing_submissions_z
  , CASE WHEN sd_num_large_missing_submissions <> 0 
      THEN (num_large_missing_submissions - avg_num_large_missing_submissions) / sd_num_large_missing_submissions 
      END AS num_large_missing_submissions_z
  , CASE WHEN sd_num_major_missing_submissions <> 0 
      THEN (num_major_missing_submissions - avg_num_major_missing_submissions) / sd_num_major_missing_submissions 
      END AS num_major_missing_submissions_z
  , CASE WHEN sd_num_unweighted_missing_submissions <> 0 
      THEN (num_unweighted_missing_submissions - avg_num_unweighted_missing_submissions) / sd_num_unweighted_missing_submissions 
      END AS num_unweighted_missing_submissions_z
  , CASE WHEN sd_num_weighted_missing_submissions <> 0 
      THEN (num_weighted_missing_submissions - avg_num_weighted_missing_submissions) / sd_num_weighted_missing_submissions 
      END AS num_weighted_missing_submissions_z
  , CASE WHEN sd_num_missing_submissions <> 0 
      THEN (num_missing_submissions - avg_num_missing_submissions) / sd_num_missing_submissions 
      END AS num_missing_submissions_z
  , CASE WHEN sd_num_tiny_late_submissions <> 0 
      THEN (num_tiny_late_submissions - avg_num_tiny_late_submissions) / sd_num_tiny_late_submissions 
      END AS num_tiny_late_submissions_z
  , CASE WHEN sd_num_small_late_submissions <> 0 
      THEN (num_small_late_submissions - avg_num_small_late_submissions) / sd_num_small_late_submissions 
      END AS num_small_late_submissions_z
  , CASE WHEN sd_num_medium_late_submissions <> 0 
      THEN (num_medium_late_submissions - avg_num_medium_late_submissions) / sd_num_medium_late_submissions 
      END AS num_medium_late_submissions_z
  , CASE WHEN sd_num_large_late_submissions <> 0 
      THEN (num_large_late_submissions - avg_num_large_late_submissions) / sd_num_large_late_submissions 
      END AS num_large_late_submissions_z
  , CASE WHEN sd_num_major_late_submissions <> 0 
      THEN (num_major_late_submissions - avg_num_major_late_submissions) / sd_num_major_late_submissions 
      END AS num_major_late_submissions_z
  , CASE WHEN sd_num_unweighted_late_submissions <> 0 
      THEN (num_unweighted_late_submissions - avg_num_unweighted_late_submissions) / sd_num_unweighted_late_submissions 
      END AS num_unweighted_late_submissions_z
  , CASE WHEN sd_num_weighted_late_submissions <> 0 
      THEN (num_weighted_late_submissions - avg_num_weighted_late_submissions) / sd_num_weighted_late_submissions 
      END AS num_weighted_late_submissions_z
  , CASE WHEN sd_num_late_submissions <> 0 
      THEN (num_late_submissions - avg_num_late_submissions) / sd_num_late_submissions 
      END AS num_late_submissions_z
  , CASE WHEN sd_avg_time_buffer_hrs_tiny <> 0 
      THEN (avg_time_buffer_hrs_tiny - avg_avg_time_buffer_hrs_tiny) / sd_avg_time_buffer_hrs_tiny 
      END AS avg_time_buffer_hrs_tiny_z
  , CASE WHEN sd_avg_time_buffer_hrs_small <> 0 
      THEN (avg_time_buffer_hrs_small - avg_avg_time_buffer_hrs_small) / sd_avg_time_buffer_hrs_small 
      END AS avg_time_buffer_hrs_small_z
  , CASE WHEN sd_avg_time_buffer_hrs_medium <> 0 
      THEN (avg_time_buffer_hrs_medium - avg_avg_time_buffer_hrs_medium) / sd_avg_time_buffer_hrs_medium 
      END AS avg_time_buffer_hrs_medium_z
  , CASE WHEN sd_avg_time_buffer_hrs_large <> 0 
      THEN (avg_time_buffer_hrs_large - avg_avg_time_buffer_hrs_large) / sd_avg_time_buffer_hrs_large 
      END AS avg_time_buffer_hrs_large_z
  , CASE WHEN sd_avg_time_buffer_hrs_major <> 0 
      THEN (avg_time_buffer_hrs_major - avg_avg_time_buffer_hrs_major) / sd_avg_time_buffer_hrs_major 
      END AS avg_time_buffer_hrs_major_z
  , CASE WHEN sd_avg_time_buffer_hrs_unweighted <> 0 
      THEN (avg_time_buffer_hrs_unweighted - avg_avg_time_buffer_hrs_unweighted) / sd_avg_time_buffer_hrs_unweighted 
      END AS avg_time_buffer_hrs_unweighted_z
  , CASE WHEN sd_avg_time_buffer_hrs_weighted <> 0 
      THEN (avg_time_buffer_hrs_weighted - avg_avg_time_buffer_hrs_weighted) / sd_avg_time_buffer_hrs_weighted 
      END AS avg_time_buffer_hrs_weighted_z
  , CASE WHEN sd_avg_time_buffer_hrs <> 0 
      THEN (avg_time_buffer_hrs - avg_avg_time_buffer_hrs) / sd_avg_time_buffer_hrs 
      END AS avg_time_buffer_hrs_z
  , CASE WHEN sd_avg_published_score_pct_tiny <> 0 
      THEN (avg_published_score_pct_tiny - avg_avg_published_score_pct_tiny) / sd_avg_published_score_pct_tiny 
      END AS avg_published_score_pct_tiny_z
  , CASE WHEN sd_avg_published_score_pct_small <> 0 
      THEN (avg_published_score_pct_small - avg_avg_published_score_pct_small) / sd_avg_published_score_pct_small 
      END AS avg_published_score_pct_small_z
  , CASE WHEN sd_avg_published_score_pct_medium <> 0 
      THEN (avg_published_score_pct_medium - avg_avg_published_score_pct_medium) / sd_avg_published_score_pct_medium 
      END AS avg_published_score_pct_medium_z
  , CASE WHEN sd_avg_published_score_pct_large <> 0 
      THEN (avg_published_score_pct_large - avg_avg_published_score_pct_large) / sd_avg_published_score_pct_large 
      END AS avg_published_score_pct_large_z
  , CASE WHEN sd_avg_published_score_pct_major <> 0 
      THEN (avg_published_score_pct_major - avg_avg_published_score_pct_major) / sd_avg_published_score_pct_major 
      END AS avg_published_score_pct_major_z
  , CASE WHEN sd_avg_published_score_pct_without_due_date <> 0 
      THEN (avg_published_score_pct_without_due_date - avg_avg_published_score_pct_without_due_date) / sd_avg_published_score_pct_without_due_date
      END AS avg_published_score_pct_without_due_date_z
  , CASE WHEN sd_avg_published_score_pct_with_due_date <> 0 
      THEN (avg_published_score_pct_with_due_date - avg_avg_published_score_pct_with_due_date) / sd_avg_published_score_pct_with_due_date
      END AS avg_published_score_pct_with_due_date_z
  , CASE WHEN sd_avg_score_pct_unweighted <> 0 
      THEN (avg_score_pct_unweighted - avg_avg_score_pct_unweighted) / sd_avg_score_pct_unweighted 
      END AS avg_score_pct_unweighted_z
  , CASE WHEN sd_avg_published_score_pct_weighted <> 0 
      THEN (avg_published_score_pct_weighted - avg_avg_published_score_pct_weighted) / sd_avg_published_score_pct_weighted
      END AS avg_published_score_pct_weighted_z
  , CASE WHEN sd_avg_published_score <> 0 
      THEN (avg_published_score - avg_avg_published_score) / sd_avg_published_score
      END AS avg_published_score_z
  , CASE WHEN sd_avg_published_score_pct_tiny_cumulative <> 0 
      THEN (avg_published_score_pct_tiny_cumulative - avg_avg_published_score_pct_tiny_cumulative) / sd_avg_published_score_pct_tiny_cumulative 
      END AS avg_published_score_pct_tiny_cumulative_z
  , CASE WHEN sd_avg_published_score_pct_small_cumulative <> 0 
      THEN (avg_published_score_pct_small_cumulative - avg_avg_published_score_pct_small_cumulative) / sd_avg_published_score_pct_small_cumulative 
      END AS avg_published_score_pct_small_cumulative_z
  , CASE WHEN sd_avg_published_score_pct_medium_cumulative <> 0 
      THEN (avg_published_score_pct_medium_cumulative - avg_avg_published_score_pct_medium_cumulative) / sd_avg_published_score_pct_medium_cumulative 
      END AS avg_published_score_pct_medium_cumulative_z
  , CASE WHEN sd_avg_published_score_pct_large_cumulative <> 0 
      THEN (avg_published_score_pct_large_cumulative - avg_avg_published_score_pct_large_cumulative) / sd_avg_published_score_pct_large_cumulative 
      END AS avg_published_score_pct_large_cumulative_z
  , CASE WHEN sd_avg_published_score_pct_major_cumulative <> 0 
      THEN (avg_published_score_pct_major_cumulative - avg_avg_published_score_pct_major_cumulative) / sd_avg_published_score_pct_major_cumulative 
      END AS avg_published_score_pct_major_cumulative_z
  , CASE WHEN sd_avg_published_score_pct_without_due_date_cumulative <> 0 
      THEN (avg_published_score_pct_without_due_date_cumulative - avg_avg_published_score_pct_without_due_date_cumulative) / sd_avg_published_score_pct_without_due_date_cumulative
      END AS avg_published_score_pct_without_due_date_cumulative_z
  , CASE WHEN sd_avg_published_score_pct_with_due_date_cumulative <> 0 
      THEN (avg_published_score_pct_with_due_date_cumulative - avg_avg_published_score_pct_with_due_date_cumulative) / sd_avg_published_score_pct_with_due_date_cumulative
      END AS avg_published_score_pct_with_due_date_cumulative_z
  , CASE WHEN sd_avg_score_pct_unweighted_cumulative <> 0 
      THEN (avg_score_pct_unweighted_cumulative - avg_avg_score_pct_unweighted_cumulative) / sd_avg_score_pct_unweighted_cumulative 
      END AS avg_score_pct_unweighted_cumulative_z
  , CASE WHEN sd_avg_published_score_pct_weighted_cumulative <> 0 
      THEN (avg_published_score_pct_weighted_cumulative - avg_avg_published_score_pct_weighted_cumulative) / sd_avg_published_score_pct_weighted_cumulative
      END AS avg_published_score_pct_weighted_cumulative_z
  , CASE WHEN sd_avg_published_score_cumulative <> 0 
      THEN (avg_published_score_cumulative - avg_avg_published_score_cumulative) / sd_avg_published_score_cumulative
      END AS avg_published_score_cumulative_z
  , CASE WHEN sd_discussion_entry_count <> 0 
      THEN (discussion_entry_count - avg_discussion_entry_count) / sd_discussion_entry_count 
      END AS discussion_entry_count_z
  , CASE WHEN sd_discussion_post_count <> 0 
      THEN (discussion_post_count - avg_discussion_post_count) / sd_discussion_post_count 
      END AS discussion_post_count_z
  , CASE WHEN sd_discussion_reply_count <> 0 
      THEN (discussion_reply_count - avg_discussion_reply_count) / sd_discussion_reply_count 
      END AS discussion_reply_count_z  
  , CASE WHEN sd_discussion_count <> 0 
      THEN (discussion_count - avg_discussion_count) / sd_discussion_count 
      END AS discussion_count_z
  , CASE WHEN sd_assignment_discussion_count <> 0 
      THEN (assignment_discussion_count - avg_assignment_discussion_count) / sd_assignment_discussion_count 
      END AS assignment_discussion_count_z
  , CASE WHEN sd_threaded_discussion_count <> 0 
      THEN (threaded_discussion_count - avg_threaded_discussion_count) / sd_threaded_discussion_count 
      END AS threaded_discussion_count_z
  , CASE WHEN sd_side_comment_discussion_count <> 0 
      THEN (side_comment_discussion_count - avg_side_comment_discussion_count) / sd_side_comment_discussion_count 
      END AS side_comment_discussion_count_z
  , CASE WHEN sd_total_discussion_count <> 0 
      THEN (total_discussion_count - avg_total_discussion_count) / sd_total_discussion_count 
      END AS total_discussion_count_z
  , CASE WHEN sd_total_assignment_discussion_count <> 0 
      THEN (total_assignment_discussion_count - avg_total_assignment_discussion_count) / sd_total_assignment_discussion_count 
      END AS total_assignment_discussion_count_z
  , CASE WHEN sd_total_threaded_discussion_count <> 0 
      THEN (total_threaded_discussion_count - avg_total_threaded_discussion_count) / sd_total_threaded_discussion_count 
      END AS total_threaded_discussion_count_z
  , CASE WHEN sd_total_side_comment_discussion_count <> 0 
      THEN (total_side_comment_discussion_count - avg_total_side_comment_discussion_count) / sd_total_side_comment_discussion_count 
      END AS total_side_comment_discussion_count_z
  , CASE WHEN sd_avg_discussion_entry_length <> 0 
      THEN (avg_discussion_entry_length - avg_avg_discussion_entry_length) / sd_avg_discussion_entry_length 
      END AS avg_discussion_entry_length_z
  , CASE WHEN sd_avg_discussion_post_length <> 0 
      THEN (avg_discussion_post_length - avg_avg_discussion_post_length) / sd_avg_discussion_post_length 
      END AS avg_discussion_post_length_z
  , CASE WHEN sd_avg_discussion_reply_length <> 0 
      THEN (avg_discussion_reply_length - avg_avg_discussion_reply_length) / sd_avg_discussion_reply_length 
      END AS avg_discussion_reply_length_z
  , CASE WHEN sd_view_days <> 0 
      THEN (view_days - avg_view_days) / sd_view_days 
      END AS view_days_z
  , CASE WHEN sd_num_sessions_10min <> 0 
      THEN (num_sessions_10min - avg_num_sessions_10min) / sd_num_sessions_10min 
      END AS num_sessions_10min_z
  , CASE WHEN sd_total_time_seconds_10min <> 0 
      THEN (total_time_seconds_10min - avg_total_time_seconds_10min) / sd_total_time_seconds_10min 
      END AS total_time_seconds_10min_z
  , CASE WHEN sd_total_actions_10min <> 0 
      THEN (total_actions_10min - avg_total_actions_10min) / sd_total_actions_10min 
      END AS total_actions_10min_z
  , CASE WHEN sd_avg_time_seconds_10min <> 0 
      THEN (avg_time_seconds_10min - avg_avg_time_seconds_10min) / sd_avg_time_seconds_10min 
      END AS avg_time_seconds_10min_z
  , CASE WHEN sd_avg_actions_10min <> 0 
      THEN (avg_actions_10min - avg_avg_actions_10min) / sd_avg_actions_10min 
      END AS avg_actions_10min_z
  , CASE WHEN sd_num_sessions_20min <> 0 
      THEN (num_sessions_20min - avg_num_sessions_20min) / sd_num_sessions_20min 
      END AS num_sessions_20min_z
  , CASE WHEN sd_total_time_seconds_20min <> 0 
      THEN (total_time_seconds_20min - avg_total_time_seconds_20min) / sd_total_time_seconds_20min 
      END AS total_time_seconds_20min_z
  , CASE WHEN sd_total_actions_20min <> 0 
      THEN (total_actions_20min - avg_total_actions_20min) / sd_total_actions_20min 
      END AS total_actions_20min_z
  , CASE WHEN sd_avg_time_seconds_20min <> 0 
      THEN (avg_time_seconds_20min - avg_avg_time_seconds_20min) / sd_avg_time_seconds_20min 
      END AS avg_time_seconds_20min_z
  , CASE WHEN sd_avg_actions_20min <> 0 
      THEN (avg_actions_20min - avg_avg_actions_20min) / sd_avg_actions_20min 
      END AS avg_actions_20min_z
  , CASE WHEN sd_num_sessions_30min <> 0 
      THEN (num_sessions_30min - avg_num_sessions_30min) / sd_num_sessions_30min 
      END AS num_sessions_30min_z
  , CASE WHEN sd_total_time_seconds_30min <> 0 
      THEN (total_time_seconds_30min - avg_total_time_seconds_30min) / sd_total_time_seconds_30min 
      END AS total_time_seconds_30min_z
  , CASE WHEN sd_total_actions_30min <> 0 
      THEN (total_actions_30min - avg_total_actions_30min) / sd_total_actions_30min 
      END AS total_actions_30min_z
  , CASE WHEN sd_avg_time_seconds_30min <> 0 
      THEN (avg_time_seconds_30min - avg_avg_time_seconds_30min) / sd_avg_time_seconds_30min 
      END AS avg_time_seconds_30min_z
  , CASE WHEN sd_avg_actions_30min <> 0 
      THEN (avg_actions_30min - avg_avg_actions_30min) / sd_avg_actions_30min 
      END AS avg_actions_30min_z
  , CASE WHEN sd_num_tool_launches <> 0 
      THEN (num_tool_launches - avg_num_tool_launches) / sd_num_tool_launches 
      END AS num_tool_launches_z
  , CASE WHEN sd_num_tools_launched <> 0 
      THEN (num_tools_launched - avg_num_tools_launched) / sd_num_tools_launched 
      END AS num_tools_launched_z
  , CASE WHEN sd_file_views <> 0 
      THEN (file_views - avg_file_views) / sd_file_views 
      END AS file_views_z
  , CASE WHEN sd_num_files_viewed <> 0 
      THEN (num_files_viewed - avg_num_files_viewed) / sd_num_files_viewed 
      END AS num_files_viewed_z
  ,  ARRAY(
      SELECT * from UNNEST(cwt.launch_app_names)
      AS a where a in UNNEST(la.tool_launch_detail.launch_app_name)) AS course_tools_launched
  ,  ARRAY(
      SELECT * from UNNEST(cwt.launch_app_names)
      AS a where a not in UNNEST(la.tool_launch_detail.launch_app_name)) AS course_tools_not_launched
  ,  ARRAY(
      SELECT * from UNNEST(cwf.file_ids)
      AS a where a in UNNEST(la.file_access_detail.file_id)) AS course_files_viewed
  ,  ARRAY(
      SELECT * from UNNEST(cwf.file_ids)
      AS a where a not in UNNEST(la.file_access_detail.file_id)) AS course_files_not_viewed

from _WRITE_TO_PROJECT_.mart_taskforce.level1_aggregated la 
left join _WRITE_TO_PROJECT_.mart_taskforce.level2_course_week_metrics cwm 
    on la.udp_course_offering_id = cwm.udp_course_offering_id and la.week_in_term = cwm.week_in_term 
left join _WRITE_TO_PROJECT_.mart_taskforce.level2_course_week_tools cwt 
    on la.udp_course_offering_id = cwt.udp_course_offering_id and la.week_in_term = cwt.week_in_term 
left join _WRITE_TO_PROJECT_.mart_taskforce.level2_course_week_files cwf 
    on la.udp_course_offering_id = cwf.udp_course_offering_id and la.week_in_term = cwf.week_in_term 
