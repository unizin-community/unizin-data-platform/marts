/* 
  Mart / Taskforce / Course_Profile
    Describes the profile of Course sections. 
    Passthrough from the warehouse.taskforce.course_profile table
    
  Properties:

    Export table:  mart_taskforce.course_profile
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Context / taskforce / course_profile

  To do:
    *
*/

select *
from _WRITE_TO_PROJECT_.mart_helper.context__taskforce__course_profile
