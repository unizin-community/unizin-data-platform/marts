/* 
  Mart / Taskforce / Level 1 Aggregated
    Aggregates data about student's performance and activities in courses on a weekly basis. 
    
  Properties:

    Export table:  mart_taskforce.level1_aggregated
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Context / Base / Course offering 
    * Context / Base / Person
    * Context / Taskforce / Enrollments grades courses
    * Context / Taskforce / Level1 assignment submissions
    * Context / Taskforce / Weekly discussions 
    * Event / Taskforce / Time spent view days 
    * Event / Taskforce / Tool launches 
    * Event / Taskforce / File accesses

  To do:
    *
*/
WITH enrollments_grades_courses_updated AS (
SELECT 
    *
    , COALESCE(
      DATE_DIFF(DATE_ADD(instruction_end_date,INTERVAL 1 WEEK),instruction_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(term_end_date,INTERVAL 1 WEEK),term_begin_date,WEEK),
      DATE_DIFF(DATE_ADD(course_end_date,INTERVAL 1 WEEK),course_start_date,WEEK)) + 1 
    AS term_length_weeks 
FROM 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__enrollments_grades_courses

)

,
framework_data AS (
SELECT 
    DISTINCT  
    person_id
    , en.course_offering_id
    , week_in_term
    , CASE
      WHEN acs.instruction_begin_date is not null then DATE_TRUNC(DATE_ADD(acs.instruction_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN act.term_begin_date is not null then DATE_TRUNC(DATE_ADD(act.term_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN co.start_date is not null then DATE_TRUNC(DATE_ADD(co.start_date, INTERVAL week_in_term - 1 WEEK),WEEK)
        ELSE null end as week_start_date
    , CASE
      WHEN acs.instruction_begin_date is not null then LAST_DAY(DATE_ADD(acs.instruction_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN act.term_begin_date is not null then LAST_DAY(DATE_ADD(act.term_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN co.start_date is not null then LAST_DAY(DATE_ADD(co.start_date, INTERVAL week_in_term - 1 WEEK),WEEK)
        ELSE null end as week_end_date
FROM 
    enrollments_grades_courses_updated en, UNNEST(GENERATE_ARRAY(1,COALESCE(term_length_weeks,17))) AS week_in_term
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering as co
	  on en.course_offering_id = co.course_offering_id 
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session as acs 
    on acs.academic_session_id = co.academic_session_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act
	  on act.academic_term_id = co.academic_term_id
)

SELECT
    p.person_id AS udp_person_id
    , p.lms_id AS lms_person_id
    , co.course_offering_id AS udp_course_offering_id
    , co.lms_id AS lms_course_offering_id
    , fd.week_in_term AS week_in_term
    , fd.week_start_date AS week_start_date
    , fd.week_end_date AS week_end_date

    -- Submission and assignment metrics
    , COALESCE(subs.num_tiny_submissions,0) AS num_tiny_submissions
    , COALESCE(subs.num_small_submissions,0) AS num_small_submissions
    , COALESCE(subs.num_medium_submissions,0) AS num_medium_submissions
    , COALESCE(subs.num_large_submissions,0) AS num_large_submissions
    , COALESCE(subs.num_major_submissions,0) AS num_major_submissions
    , COALESCE(subs.num_unweighted_submissions,0) AS num_unweighted_submissions
    , COALESCE(subs.num_weighted_submissions,0) AS num_weighted_submissions
    , COALESCE(subs.num_submissions_without_due_date,0) AS num_submissions_without_due_date
    , COALESCE(subs.num_submissions_with_due_date,0) AS num_submissions_with_due_date
    , COALESCE(subs.num_submissions,0) AS num_submissions
    , COALESCE(subs.num_tiny_assignments,0) AS num_tiny_assignments
    , COALESCE(subs.num_small_assignments,0) AS num_small_assignments
    , COALESCE(subs.num_medium_assignments,0) AS num_medium_assignments
    , COALESCE(subs.num_large_assignments,0) AS num_large_assignments
    , COALESCE(subs.num_major_assignments,0) AS num_major_assignments
    , COALESCE(subs.num_unweighted_assignments,0) AS num_unweighted_assignments
    , COALESCE(subs.num_weighted_assignments,0) AS num_weighted_assignments
    , COALESCE(subs.num_assignments_without_due_date,0) AS num_assignments_without_due_date
    , COALESCE(subs.num_assignments_with_due_date,0) AS num_assignments_with_due_date
    , COALESCE(subs.num_assignments,0) AS num_assignments
    , COALESCE(subs.num_tiny_missing_submissions,0) AS num_tiny_missing_submissions
    , COALESCE(subs.num_small_missing_submissions,0) AS num_small_missing_submissions
    , COALESCE(subs.num_medium_missing_submissions,0) AS num_medium_missing_submissions
    , COALESCE(subs.num_large_missing_submissions,0) AS num_large_missing_submissions
    , COALESCE(subs.num_major_missing_submissions,0) AS num_major_missing_submissions
    , COALESCE(subs.num_unweighted_missing_submissions,0) AS num_unweighted_missing_submissions
    , COALESCE(subs.num_weighted_missing_submissions,0) AS num_weighted_missing_submissions
    , COALESCE(subs.num_missing_submissions,0) AS num_missing_submissions
    , COALESCE(subs.num_tiny_late_submissions,0) AS num_tiny_late_submissions
    , COALESCE(subs.num_small_late_submissions,0) AS num_small_late_submissions
    , COALESCE(subs.num_medium_late_submissions,0) AS num_medium_late_submissions
    , COALESCE(subs.num_large_late_submissions,0) AS num_large_late_submissions
    , COALESCE(subs.num_major_late_submissions,0) AS num_major_late_submissions
    , COALESCE(subs.num_unweighted_late_submissions,0) AS num_unweighted_late_submissions
    , COALESCE(subs.num_weighted_late_submissions,0) AS num_weighted_late_submissions
    , COALESCE(subs.num_late_submissions,0) AS num_late_submissions
    , subs.avg_time_buffer_hrs_tiny 
    , subs.avg_time_buffer_hrs_small 
    , subs.avg_time_buffer_hrs_medium
    , subs.avg_time_buffer_hrs_large
    , subs.avg_time_buffer_hrs_major
    , subs.avg_time_buffer_hrs_unweighted
    , subs.avg_time_buffer_hrs_weighted
    , subs.avg_time_buffer_hrs 
    , subs.avg_published_score_pct_tiny 
    , subs.avg_published_score_pct_small 
    , subs.avg_published_score_pct_medium
    , subs.avg_published_score_pct_large
    , subs.avg_published_score_pct_major
    , subs.avg_score_pct_unweighted
    , subs.avg_published_score_pct_weighted
    , subs.avg_published_score_pct_without_due_date
    , subs.avg_published_score_pct_with_due_date 
    , subs.avg_published_score
    , subs.avg_published_score_pct_tiny_cumulative 
    , subs.avg_published_score_pct_small_cumulative  
    , subs.avg_published_score_pct_medium_cumulative 
    , subs.avg_published_score_pct_large_cumulative 
    , subs.avg_published_score_pct_major_cumulative 
    , subs.avg_score_pct_unweighted_cumulative 
    , subs.avg_published_score_pct_weighted_cumulative 
    , subs.avg_published_score_pct_without_due_date_cumulative 
    , subs.avg_published_score_pct_with_due_date_cumulative 
    , subs.avg_published_score_cumulative

    -- discussion metrics
    , COALESCE(wd.discussion_entry_count,0) AS discussion_entry_count 
    , COALESCE(wd.discussion_post_count,0) AS discussion_post_count 
    , COALESCE(wd.discussion_reply_count,0) AS discussion_reply_count 
    , COALESCE(wd.discussion_count,0) AS discussion_count 
    , COALESCE(wd.assignment_discussion_count,0) AS assignment_discussion_count 
    , COALESCE(wd.threaded_discussion_count,0) AS threaded_discussion_count 
    , COALESCE(wd.side_comment_discussion_count,0) AS side_comment_discussion_count 
    , COALESCE(wd.total_discussion_count,0) AS total_discussion_count 
    , COALESCE(wd.total_assignment_discussion_count,0) AS total_assignment_discussion_count 
    , COALESCE(wd.total_threaded_discussion_count,0) AS total_threaded_discussion_count 
    , COALESCE(wd.total_side_comment_discussion_count,0) AS total_side_comment_discussion_count 
    , wd.avg_discussion_entry_length 
    , wd.avg_discussion_post_length
    , wd.avg_discussion_reply_length

    -- time spent and view days metrics
    , COALESCE(tsvd.view_days,0) AS view_days 
    , COALESCE(tsvd.num_sessions_10min,0) as num_sessions_10min
    , COALESCE(tsvd.total_time_seconds_10min,0) as total_time_seconds_10min
    , COALESCE(tsvd.total_actions_10min,0) as total_actions_10min
    , tsvd.avg_time_seconds_10min 
    , tsvd.avg_actions_10min 
    , COALESCE(tsvd.num_sessions_20min,0) as num_sessions_20min
    , COALESCE(tsvd.total_time_seconds_20min,0) as total_time_seconds_20min
    , COALESCE(tsvd.total_actions_20min,0) as total_actions_20min
    , tsvd.avg_time_seconds_20min 
    , tsvd.avg_actions_20min 
    , COALESCE(tsvd.num_sessions_30min,0) as num_sessions_30min
    , COALESCE(tsvd.total_time_seconds_30min,0) as total_time_seconds_30min
    , COALESCE(tsvd.total_actions_30min,0) as total_actions_30min
    , tsvd.avg_time_seconds_30min 
    , tsvd.avg_actions_30min 

    -- tool launch metrics
    , COALESCE(tl.num_tool_launches,0) AS num_tool_launches
    , COALESCE(tl.num_tools_launched,0) AS num_tools_launched
    , tl.tool_launch_detail 
    
    -- file view metrics
    , COALESCE(fa.file_views,0) AS file_views
    , COALESCE(fa.num_files_viewed,0) AS num_files_viewed
    , fa.file_access_detail

FROM 
    framework_data AS fd
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON fd.course_offering_id = co.course_offering_id 
INNER JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON fd.person_id = p.person_id
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__level1_assignments_submissions AS subs ON fd.person_id = subs.person_id AND fd.course_offering_id = subs.course_offering_id AND fd.week_in_term = subs.week_in_term
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__taskforce__weekly_discussions AS wd ON fd.person_id = wd.person_id AND fd.course_offering_id = wd.course_offering_id AND fd.week_in_term = wd.week_in_term
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.event__taskforce__time_spent_view_days AS tsvd ON fd.person_id = tsvd.person_id AND fd.course_offering_id = tsvd.course_offering_id AND fd.week_in_term = tsvd.week_in_term
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.event__taskforce__tool_launches AS tl ON fd.person_id = tl.person_id AND fd.course_offering_id = tl.course_offering_id AND fd.week_in_term = tl.week_in_term
LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.event__taskforce__file_accesses AS fa ON fd.person_id = fa.person_id AND fd.course_offering_id = fa.course_offering_id AND fd.week_in_term = fa.week_in_term

