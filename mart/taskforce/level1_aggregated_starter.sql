DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_taskforce.level1_aggregated;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_taskforce.level1_aggregated(
        udp_person_id INT64
    ,   lms_person_id STRING
    ,   udp_course_offering_id INT64
    ,   lms_course_offering_id STRING
    ,   week_in_term INT64
    ,   week_start_date DATE 
    ,   week_end_date DATE 
    ,   num_tiny_submissions INT64
    ,   num_small_submissions INT64
    ,   num_medium_submissions INT64
    ,   num_large_submissions INT64
    ,   num_major_submissions INT64
    ,   num_unweighted_submissions INT64
    ,   num_weighted_submissions INT64 
    ,   num_submissions_without_due_date INT64
    ,   num_submissions_with_due_date INT64
    ,   num_submissions INT64
    ,   num_tiny_assignments INT64
    ,   num_small_assignments INT64
    ,   num_medium_assignments INT64
    ,   num_large_assignments INT64
    ,   num_major_assignments INT64
    ,   num_unweighted_assignments INT64
    ,   num_weighted_assignments INT64
    ,   num_assignments_without_due_date INT64
    ,   num_assignments_with_due_date INT64
    ,   num_assignments INT64
    ,   num_tiny_missing_submissions INT64
    ,   num_small_missing_submissions INT64
    ,   num_medium_missing_submissions INT64
    ,   num_large_missing_submissions INT64
    ,   num_major_missing_submissions INT64
    ,   num_unweighted_missing_submissions INT64
    ,   num_weighted_missing_submissions INT64
    ,   num_missing_submissions INT64
    ,   num_tiny_late_submissions INT64
    ,   num_small_late_submissions INT64
    ,   num_medium_late_submissions INT64
    ,   num_large_late_submissions INT64
    ,   num_major_late_submissions INT64
    ,   num_unweighted_late_submissions INT64
    ,   num_weighted_late_submissions INT64
    ,   num_late_submissions INT64
    ,   avg_time_buffer_hrs_tiny FLOAT64
    ,   avg_time_buffer_hrs_small FLOAT64
    ,   avg_time_buffer_hrs_medium FLOAT64
    ,   avg_time_buffer_hrs_large FLOAT64
    ,   avg_time_buffer_hrs_major FLOAT64
    ,   avg_time_buffer_hrs_unweighted FLOAT64
    ,   avg_time_buffer_hrs_weighted FLOAT64
    ,   avg_time_buffer_hrs FLOAT64
    ,   avg_published_score_pct_tiny NUMERIC
    ,   avg_published_score_pct_small NUMERIC
    ,   avg_published_score_pct_medium NUMERIC
    ,   avg_published_score_pct_large NUMERIC
    ,   avg_published_score_pct_major NUMERIC
    ,   avg_score_pct_unweighted NUMERIC 
    ,   avg_published_score_pct_weighted NUMERIC
    ,   avg_published_score_pct_without_due_date NUMERIC 
    ,   avg_published_score_pct_with_due_date NUMERIC
    ,   avg_published_score NUMERIC
    ,   avg_published_score_pct_tiny_cumulative NUMERIC
    ,   avg_published_score_pct_small_cumulative NUMERIC
    ,   avg_published_score_pct_medium_cumulative NUMERIC
    ,   avg_published_score_pct_large_cumulative NUMERIC
    ,   avg_published_score_pct_major_cumulative NUMERIC
    ,   avg_score_pct_unweighted_cumulative NUMERIC 
    ,   avg_published_score_pct_weighted_cumulative NUMERIC
    ,   avg_published_score_pct_without_due_date_cumulative NUMERIC 
    ,   avg_published_score_pct_with_due_date_cumulative NUMERIC
    ,   avg_published_score_cumulative NUMERIC
    ,   discussion_entry_count INT64    
    ,   discussion_post_count INT64    
    ,   discussion_reply_count INT64    
    ,   discussion_count INT64    
    ,   assignment_discussion_count INT64    
    ,   threaded_discussion_count INT64    
    ,   side_comment_discussion_count INT64    
    ,   total_discussion_count INT64    
    ,   total_assignment_discussion_count INT64    
    ,   total_threaded_discussion_count INT64    
    ,   total_side_comment_discussion_count INT64    
    ,   avg_discussion_entry_length FLOAT64
    ,   avg_discussion_post_length FLOAT64
    ,   avg_discussion_reply_length FLOAT64
    ,   view_days INT64
    ,   num_sessions_10min INT64 
    ,   total_time_seconds_10min INT64
    ,   total_actions_10min INT64 
    ,   avg_time_seconds_10min FLOAT64 
    ,   avg_actions_10min FLOAT64 
    ,   num_sessions_20min INT64 
    ,   total_time_seconds_20min INT64
    ,   total_actions_20min INT64 
    ,   avg_time_seconds_20min FLOAT64 
    ,   avg_actions_20min FLOAT64 
    ,   num_sessions_30min INT64 
    ,   total_time_seconds_30min INT64
    ,   total_actions_30min INT64 
    ,   avg_time_seconds_30min FLOAT64 
    ,   avg_actions_30min FLOAT64 
    ,   num_tool_launches INT64 
    ,   num_tools_launched INT64 
    ,   tool_launch_detail 
            ARRAY<
                STRUCT<
                    launch_app_name STRING,
                    num_launches INT64>>
    ,   file_views INT64 
    ,   num_files_viewed INT64 
    ,   file_access_detail 
            ARRAY<
                STRUCT<
                    file_id INT64,
                    display_name STRING,
                    content_type STRING,
                    content_sub_type STRING,
                    num_times_viewed INT64>>
    )
