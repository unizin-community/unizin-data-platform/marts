/* 
  Mart / Taskforce / Level 2 Course Week Tools
    Aggregates all interactions of students with tools for a course in a given week.
    
  Properties:

    Export table:  mart_taskforce.level2_course_week_tools
    Run frequency: Every Day
    Run operation: Overwrite
  Dependencies:
    * Mart / Taskforce / Level 1 Aggregated
  To do:
    *
*/

with course_week_tools as (
select 
    udp_course_offering_id
    ,week_in_term
    ,week_start_date
    ,week_end_date
    ,tool_launch_detail.launch_app_name as launch_app_name
    ,tool_launch_detail,num_launches
from _WRITE_TO_PROJECT_.mart_taskforce.level1_aggregated,unnest(tool_launch_detail) as tool_launch_detail
order by udp_course_offering_id,week_in_term
)

,
course_week_tools_aggregated as (
select 
    udp_course_offering_id
    ,week_in_term
    , week_start_date 
    , week_end_date
    ,launch_app_name
    ,sum(num_launches) as total_launches
from course_week_tools
group by udp_course_offering_id,week_in_term,week_start_date,week_end_date,launch_app_name 
)

select 
    udp_course_offering_id
    ,week_in_term
    , week_start_date 
    , week_end_date
    ,array_agg(launch_app_name order by launch_app_name) as launch_app_names
    ,array_agg(total_launches order by launch_app_name) as total_launches
from course_week_tools_aggregated cwt 
group by udp_course_offering_id,cwt.week_in_term,cwt.week_start_date,cwt.week_end_date
