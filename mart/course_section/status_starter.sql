DROP TABLE IF EXISTS `_WRITE_TO_PROJECT_.mart_course_section.status`;

CREATE TABLE 
    `_WRITE_TO_PROJECT_.mart_course_section.status` (
        udp_course_section_id INT64
    ,   lms_course_section_id STRING
    ,   combined_section_basis STRING
    ,   combined_section_id STRING
    ,   delivery_mode STRING
    ,   is_combined_section_parent INT64
    ,   is_default INT64
    ,   is_graded INT64
    ,   is_honors INT64
    ,   udp_course_offering_id INT64
    ,   lms_course_offering_id INT64
    ,   academic_term_name STRING
    ,   academic_term_start_date DATE
    ,   organization_name STRING
    ,   course_offering_title STRING
    ,   course_offering_start_date DATE
    ,   course_offering_subject STRING
    ,   course_offering_number STRING
    ,   course_offering_code STRING
    ,   instructor_name_array ARRAY<STRING>
    ,   instructor_lms_id_array ARRAY<STRING>
    ,   instructor_display STRING
    ,   instructor_email_address_array ARRAY<STRING>
    ,   instructor_email_address_display ARRAY<STRING>
    ,   status STRING
    ,   reported_status STRING
    ,   publish_time DATETIME
    ,   num_students INT64
    ,   published_la INT64
    ,   unpublished_la INT64
    ,   published_quiz INT64
    ,   unpublished_quiz INT64
    ,   active_module INT64
    ,   unpublished_module INT64
    )

/*DROP MATERIALIZED VIEW IF EXISTS `_WRITE_TO_PROJECT_.mart_course_section.status`;

CREATE MATERIALIZED VIEW `_WRITE_TO_PROJECT_.mart_course_section.status`
as select *
from `_WRITE_TO_PROJECT_.mart_course_section.status`; */
