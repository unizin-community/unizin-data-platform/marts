/*
  Marts / Course section / Last activity

    Generate data about the last activity performed
    by students in a course section.

  Properties:

    Export table:  mart_course_section.last_activity
    Run frequency: Every hour
    Run operation: Overwrite

  Dependencies:
    * Event ETL / Course offering / Last activity 
    * Context / Base / Course offering
    * Context / Base / Course section
    * Context / Base / Academic term
    * Context / Base / Person
    * Context / Base / Academic organization
    * Context / Course section / Enrollment
    * Context / Course offering / By Person / Learner activity result

  To do:
    *
*/

WITH course_section_enrollment AS (
  SELECT
    -- Basic course information
    co.course_offering_id AS udp_course_offering_id
    , co.lms_id AS lms_course_offering_id
    , cs.course_section_id as udp_course_section_id
    , cs.lms_id as lms_course_section_id
    , cs.sis_id as sis_course_section_id
    , cse.person_id AS udp_person_id
    , cse.lms_person_id AS lms_person_id

    -- Academic organization
    , cs.academic_organization_id AS academic_organization_id
    , ao.name AS organization_name

    -- Academic term
    , tt.name AS academic_term_name
    , tt.term_begin_date AS academic_term_start_date

    -- Course offering
    , co.title AS course_offering_title
    , co.start_date AS course_offering_start_date
    , co.subject as course_offering_subject
    , co.number as course_offering_number
    , CONCAT(co.subject, ' ', co.number) as course_offering_code

    -- Person information
    , CONCAT(p.last_name, ', ', p.first_name) AS person_name

    -- Role 
    , cse.role AS role

    -- Instructor information
    , wcse.instructor_display
    , TO_JSON_STRING(wcse.instructor_name_array) as instructor_name_array
    , wcse.instructor_email_address_display
    , TO_JSON_STRING(wcse.instructor_email_address_array) as instructor_email_address_array

  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON cse.person_id=p.person_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON cse.course_section_id=cs.course_section_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON COALESCE(cs.le_current_course_offering_id,cs.course_offering_id)=co.course_offering_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section__enrollment AS wcse ON cs.course_section_id = wcse.course_section_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_organization AS ao ON cs.academic_organization_id=ao.academic_organization_id

  GROUP BY
    cse.person_id
    , cse.lms_person_id
    , co.course_offering_id
    , co.lms_id
    , cs.course_section_id
    , cs.lms_id
    , cs.sis_id
    , cs.academic_organization_id
    , ao.name
    , tt.name
    , tt.term_begin_date
    , co.title
    , co.start_date
    , co.subject
    , co.number
    , wcse.instructor_display
    , instructor_name_array
    , wcse.instructor_email_address_display
    , instructor_email_address_array
    , person_name
    , role
)

SELECT
  -- Basic course information
  cse.udp_course_offering_id AS udp_course_offering_id
  , cse.lms_course_offering_id AS lms_course_offering_id
  , cse.udp_course_section_id AS udp_course_section_id 
  , cse.lms_course_section_id as lms_course_section_id
  , cse.sis_course_section_id as sis_course_section_id
  , cse.udp_person_id AS udp_person_id
  , cse.lms_person_id AS lms_person_id

  -- Academic organization
  , cse.academic_organization_id AS academic_organization_id
  , cse.organization_name AS organization_name

  -- Academic term
  , cse.academic_term_name AS academic_term_name
  , cse.academic_term_start_date AS academic_term_start_date

  -- Course offering
  , cse.course_offering_title AS course_offering_title
  , cse.course_offering_start_date AS course_offering_start_date
  , cse.course_offering_subject as course_offering_subject
  , cse.course_offering_number as course_offering_number
  , cse.course_offering_code as course_offering_code

  -- Instructor
  , cse.instructor_display
  ,JSON_EXTRACT_STRING_ARRAY(cse.instructor_name_array) AS instructor_name_array
  , JSON_EXTRACT_STRING_ARRAY(cse.instructor_email_address_array) AS instructor_email_address_array
  , cse.instructor_email_address_display

  -- Person
  , cse.person_name AS person_name

  -- Role 
  , cse.role AS role

  -- Last activity types for a person
  , la.last_activity AS last_activity
  , la.last_navigation_activity AS last_navigation_activity
  , la.last_media_activity AS last_media_activity
  , la.last_grade_activity AS last_grade_activity
  , la.last_assessment_activity AS last_assessment_activity
  , la.last_assignment_activity AS last_assignment_activity

  /*
    Number of events by that person in that course, by
    the type of event.
  */
  , la.num_events AS num_events
  , la.num_navigation_events AS num_navigation_events
  , la.num_media_events AS num_media_events
  , la.num_grade_events AS num_grade_events
  , la.num_assessment_events AS num_assessment_events
  , la.num_assignment_events AS num_assignment_events

  /*
    Number of completed learning activities, quizzes, discussion entries,
    and modules by person.
  */
/*
  , cobplar.count_is_processed AS num_learner_activity_results
  , cse.num_quiz_results AS num_quiz_results
  , cse.num_discussion_entries AS num_discussion_entries
  , cse.num_completed_modules AS num_completed_modules
*/

FROM course_section_enrollment AS cse

/* Completion metrics */
/*
  LEFT JOIN warehouse_course_offering_by_person.learner_activity_result AS cobplar
    ON cse.udp_person_id=cobplar.person_id AND cse.udp_course_offering_id=cobplar.course_offering_id
*/

-- Event ETL data
LEFT JOIN
  _WRITE_TO_PROJECT_.mart_helper.event__course_offering__last_activity AS la
    ON cse.lms_course_offering_id=la.lms_course_offering_id
    AND cse.lms_person_id=la.lms_person_id
;
