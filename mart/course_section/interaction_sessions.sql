/*
  Marts / Course section / Interaction sessions

    Generate data about the interaction sessions of a student in a course section.

  Properties:

    Export table:  mart_course_section.interaction_sessions
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:
    * Event ETL / Course offering / Interaction sessions
    * Course section
    * Course section enrollment
    * Course offering
    * Academic term
    * Person
    * Academic organization
    * Course section / Enrollment
    
  To do:
    *
*/


SELECT
  -- IDs
  ints.udp_course_offering_id AS udp_course_offering_id
  , co.lms_id AS lms_course_offering_id
  , cs.course_section_id as udp_course_section_id
  , cs.lms_id as lms_course_section_id
  , cs.sis_id as sis_course_section_id
  , ints.udp_person_id AS udp_person_id
  , p.lms_id AS lms_person_id

  -- Academic organization
  , cs.academic_organization_id AS academic_organization_id
   , ao.name AS organization_name

  -- Academic term
  , tt.name AS academic_term_name
  , tt.term_begin_date AS academic_term_start_date

  -- Course offering
  , co.title AS course_offering_title
  , co.start_date AS course_offering_start_date
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , CONCAT(co.subject, ' ', co.number) as course_offering_code

  -- Instructor
  , wcse.instructor_display
  , wcse.instructor_name_array AS instructor_name_array
  , wcse.instructor_email_address_display
  , wcse.instructor_email_address_array AS instructor_email_address_array

  -- Person
  , CASE
    WHEN p.first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.first_name <> '' AND p.last_name <> ''
      THEN CONCAT(p.last_name, ', ', p.first_name)
    WHEN ARRAY_LENGTH(SPLIT(p.name,' ')) > 1 
      THEN CONCAT(
                (
                  SELECT STRING_AGG(name, ' ' ORDER BY OFFSET) 
                  FROM UNNEST(SPLIT(p.name, ' ')) name WITH OFFSET 
                  WHERE OFFSET > 0
                )
                ,', '
                ,SPLIT(p.name,' ')[OFFSET(0)])
    END AS person_name 

  -- Role 
  , cse.role AS role

  -- Interaction sessions 
  , ints.* except(udp_course_offering_id,udp_person_id)

FROM _WRITE_TO_PROJECT_.mart_helper.event__course_offering__interaction_sessions AS ints
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON ints.udp_course_offering_id = co.course_offering_id 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON ints.udp_person_id = p.person_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON co.course_offering_id = COALESCE(cs.le_current_course_offering_id,cs.course_offering_id)
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse ON p.person_id = cse.person_id AND cs.course_section_id = cse.course_section_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section__enrollment AS wcse ON cs.course_section_id = wcse.course_section_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_organization AS ao ON cs.academic_organization_id=ao.academic_organization_id
;

