DROP TABLE IF EXISTS `_WRITE_TO_PROJECT_.mart_course_section.file_interaction`;

CREATE TABLE 
    `_WRITE_TO_PROJECT_.mart_course_section.file_interaction` (
        udp_course_offering_id INT64
    ,   udp_file_id INT64
    ,   lms_int_file_id STRING
    ,   num_views INT64
    ,   num_distinct_students INT64
    ,   num_enrolled_students INT64
    ,   student_udp_id_array ARRAY<INT64>
    ,   instructor_name_array ARRAY<STRING>
    ,   instructor_display STRING
    ,   instructor_email_address_array ARRAY<STRING>
    ,   instructor_email_address_display STRING
    ,   course_offering_subject STRING
    ,   course_offering_number STRING
    ,   lms_course_offering_id STRING
    ,   course_offering_code STRING
    ,   udp_course_section_id INT64
    ,   lms_course_section_id STRING
    ,   sis_course_section_id STRING
    ,   academic_term_name STRING
    ,   term_begin_date DATE
    ,   term_end_date DATE
    ,   learner_activity_id INT64
    ,   learner_activity_title STRING
    ,   learner_activity_due_date DATETIME
    ,   uploader_id INT64
    ,   quiz_id INT64
    ,   quiz_title STRING
    ,   quiz_due_date DATETIME
    ,   content_type STRING
    ,   content_sub_type STRING
    ,   display_name STRING
    ,   owner_entity_type STRING
    ,   size NUMERIC
    ,   created_date DATETIME
    ,   unlocked_date DATETIME
    ,   updated_date DATETIME
    ,   accessible_date DATETIME
    ,   most_recent_version_date DATETIME
    ,   students_who_viewed_udp_id_array ARRAY<INT64>
    ,   students_who_did_not_view_udp_id_array ARRAY<INT64>
    ,   pct_class_viewed NUMERIC
    )
PARTITION BY
  DATETIME_TRUNC(created_date, MONTH);

/*DROP MATERIALIZED VIEW IF EXISTS `_WRITE_TO_PROJECT_.mart_course_section.file_interaction`;

CREATE MATERIALIZED VIEW `_WRITE_TO_PROJECT_.mart_course_section.file_interaction`
as select *
from `_WRITE_TO_PROJECT_.mart_course_section.file_interaction`; */
