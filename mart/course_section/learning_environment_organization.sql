/*
  Marts / Course offering / Learning environment organization

    Generate data about the learning environment organization associated with an enrollment

  Properties:

    Export table:  mart_course_offering.learning_environment_organization
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:
    * Context / Learning environment organization expanded
    * Course section
    * Course section enrollment
    * Course offering
    * Person
    * Course offering / Enrollment

  To do:
    *
*/

WITH course_section_enrollment AS (
  SELECT
    DISTINCT 
    -- IDs
    cse.person_id AS udp_person_id 
    , cse.course_section_id AS udp_course_section_id
    , co.course_offering_id AS udp_course_offering_id

    -- Role
    , cse.role AS role
    , cse.role_status AS role_status
    , cse.enrollment_status AS enrollment_status
    
  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON cse.course_section_id=cs.course_section_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON COALESCE(cs.le_current_course_offering_id,cs.course_offering_id)=co.course_offering_id
)

SELECT
  -- IDs
  cse.udp_course_section_id AS udp_course_section_id
  , cs.lms_id AS lms_course_section_id
  , cse.udp_course_offering_id AS udp_course_offering_id
  , co.lms_id AS lms_course_offering_id
  , cse.udp_person_id AS udp_person_id
  , p.lms_id AS lms_person_id
  , leo.learning_environment_organization_id AS udp_learning_environment_organization_id
  , leo.lms_id AS lms_learning_environment_organization_id

  -- Academic term
  , tt.name AS academic_term_name
  , tt.term_begin_date AS academic_term_start_date

  -- Course offering
  , co.title AS course_offering_title
  , co.start_date AS course_offering_start_date
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , CONCAT(co.subject, ' ', co.number) as course_offering_code

  -- Instructor
  , wcse.instructor_display
  , wcse.instructor_name_array AS instructor_name_array
  , wcse.instructor_email_address_display
  , wcse.instructor_email_address_array AS instructor_email_address_array

  -- Person
  , CASE
    WHEN p.preferred_first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.preferred_first_name <> '' AND p.last_name <> ''
      THEN CONCAT(p.last_name, ', ', p.preferred_first_name)
    WHEN p.first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.first_name <> '' AND p.last_name <> ''
      THEN CONCAT(p.last_name, ', ', p.first_name)
    WHEN ARRAY_LENGTH(SPLIT(p.name,' ')) > 1 
      THEN CONCAT(
                (
                  SELECT STRING_AGG(name, ' ' ORDER BY OFFSET) 
                  FROM UNNEST(SPLIT(p.name, ' ')) name WITH OFFSET 
                  WHERE OFFSET > 0
                )
                ,', '
                ,SPLIT(p.name,' ')[OFFSET(0)])
    END AS person_name 

  -- Role 
  , cse.role AS role
  , cse.role_status AS role_status
  , cse.enrollment_status AS enrollment_status

  -- Learning environment organization information 
  , leo.* except(learning_environment_organization_id,lms_id)

FROM course_section_enrollment cse 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON cse.udp_course_section_id = cs.course_section_id 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON cse.udp_course_offering_id = co.course_offering_id 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON cse.udp_person_id = p.person_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section__enrollment AS wcse ON cse.udp_course_section_id = wcse.course_section_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__learning_environment_organization AS leo ON co.learning_environment_organization_id = leo.learning_environment_organization_id
;


