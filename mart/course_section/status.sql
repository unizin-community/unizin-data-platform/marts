/*
  Marts / Course section / Status

    Generates an updated view of the course
    sections at an institution and their status
    in the learning environment.

  Properties:

    Export table:  mart_course_section.status
    Run frequency: Every hour
    Run operation: Overwrite

  To do:

*/

SELECT 
    --Course section specific fields
    wb_cs.course_section_id as udp_course_section_id,
    wb_cs.lms_id as lms_course_section_id,
    wb_cs.combined_section_basis,
    wb_cs.combined_section_id,
    wb_cs.delivery_mode,
    wb_cs.is_combined_section_parent,
    wb_cs.is_default,
    wb_cs.is_graded,
    wb_cs.is_honors,

    --Bring in rest of data from the course_offering mart
    mco_s.*
FROM
    _WRITE_TO_PROJECT_.mart_helper.context__course_section as wb_cs
left join
    _WRITE_TO_PROJECT_.mart_course_offering.status as mco_s
    on COALESCE(wb_cs.le_current_course_offering_id,wb_cs.course_offering_id) = mco_s.udp_course_offering_id
