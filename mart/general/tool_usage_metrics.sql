/*
  Mart / General / Tool usage metrics

  Defines usage for all tools in the event store

  Properties:

    Export table:  mart_general.tool_usage_metrics
    Run frequency: Every hour
    Run operation: Overwrite

  To do:
    *

*/

with tool_usage_metrics as (
select 
  ed_app_id,
  sum(num_events) as total_events,
  /* Define the total events for various time frames */
  sum(CASE WHEN latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR) THEN num_events ELSE 0 END) as total_events_1hour,
  sum(CASE WHEN latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 6 HOUR) THEN num_events ELSE 0 END) as total_events_6hour,
  sum(CASE WHEN latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 12 HOUR) THEN num_events ELSE 0 END) as total_events_12hour,
  sum(CASE WHEN latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 DAY) THEN num_events ELSE 0 END) as total_events_day,
  sum(CASE WHEN event_date >= DATE_SUB(CURRENT_DATE(), INTERVAL 1 WEEK) THEN num_events ELSE 0 END) as total_events_week,
  sum(CASE WHEN latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 MONTH) THEN num_events ELSE 0 END) as total_events_month,
  sum(CASE WHEN latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 YEAR) THEN num_events ELSE 0 END) as total_events_year,
  /* Define the earliest and latest event for various time frames */
  min(earliest_event) as earliest_event_time,
  max(latest_event) as latest_event_time,
  min(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR) THEN earliest_event END) as earliest_event_time_1hour,
  max(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR) THEN latest_event END) as latest_event_time_1hour,
  min(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 6 HOUR) THEN earliest_event END) as earliest_event_time_6hour,
  max(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 6 HOUR) THEN latest_event END) as latest_event_time_6hour,
  min(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 12 HOUR) THEN earliest_event END) as earliest_event_time_12hour,
  max(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 12 HOUR) THEN latest_event END) as latest_event_time_12hour,
  min(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 DAY) THEN earliest_event END) as earliest_event_time_day,
  max(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 DAY) THEN latest_event END) as latest_event_time_day,
  min(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 WEEK) THEN earliest_event END) as earliest_event_time_week,
  max(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 WEEK) THEN latest_event END) as latest_event_time_week,
  min(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 MONTH) THEN earliest_event END) as earliest_event_time_month,
  max(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 MONTH) THEN latest_event END) as latest_event_time_month,
  min(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 YEAR) THEN earliest_event END) as earliest_event_time_year,
  max(case when latest_event >= DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 YEAR) THEN latest_event END) as latest_event_time_year
from _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_preagg
group by ed_app_id
),

-- For tools that have at least one year of active use
-- we look at what was typical for this event hour, this week in previous year(s)
whats_typical_year as (
  select  ed_app_id, 
          avg(num_events) as avg_num_events,
          stddev(num_events) as std_num_events,
  from _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_preagg
  where event_hour = EXTRACT(HOUR FROM DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR))
    and week_number = EXTRACT(WEEK FROM DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR))
    and EXTRACT(YEAR FROM event_date) < EXTRACT(YEAR FROM DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR))
  group by ed_app_id
),

-- For tools that are newer, less than one year
-- We look at what was typical for this event hour, over the last 4 weeks.
whats_typical_6weeks as (
  select  ed_app_id, 
          avg(num_events) as avg_num_events,
          stddev(num_events) as std_num_events,
  from _WRITE_TO_PROJECT_.mart_helper.event__general__tool_usage_preagg
  where event_date >= EXTRACT(DATE FROM DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 6 WEEK))
  and event_hour = EXTRACT(HOUR FROM DATETIME_SUB(CURRENT_DATETIME(), INTERVAL 1 HOUR))
  group by ed_app_id
),

low_checks as (
  select tum.*,
  CASE
    WHEN wty.ed_app_id is null THEN null
    WHEN wty.avg_num_events < 1000 then null
    WHEN tum.total_events_1hour = 0 then 1
    WHEN (tum.total_events_1hour - wty.avg_num_events) / wty.std_num_events < -1.5 THEN 1
    ELSE 0
  END as low_hourly_events_flag_year,
  CASE
    WHEN wt6w.ed_app_id is null THEN null
    WHEN wt6w.avg_num_events < 1000 then null
    WHEN tum.total_events_1hour = 0 then 1
    WHEN (tum.total_events_1hour - wt6w.avg_num_events) / wt6w.std_num_events < -1.5 THEN 1
    ELSE 0
  END as low_hourly_events_flag_6weeks
  from tool_usage_metrics as tum
  left join whats_typical_year as wty
    on wty.ed_app_id = tum.ed_app_id
  left join whats_typical_6weeks as wt6w
    on wt6w.ed_app_id = tum.ed_app_id
)

select * except (low_hourly_events_flag_year, low_hourly_events_flag_6weeks),
  COALESCE(low_hourly_events_flag_year, low_hourly_events_flag_6weeks) as low_hourly_events_flag
from low_checks;
