/*
  Marts / General / LMS Tool use

    Generates a full history of LMS tool use
    with the latest tool names.

  Properties:

    Export table:  mart.lms_tool
    Run frequency: Every hour
    Run operation: Overwrite

  To do:
    *
*/

SELECT
  -- Basic course information.
  co.course_offering_id AS udp_course_offering_id
  , lt.lms_course_offering_id AS lms_course_offering_id
  , co.sis_id AS sis_course_offering_id
  , p.person_id AS udp_person_id
  , lt.lms_person_id AS lms_person_id
  , p.sis_id AS sis_person_id

  -- Role
  , lt.role AS role

  -- Academic term
  , tt.name AS academic_term_name
  , tt.term_begin_date AS academic_term_start_date

  -- Academic organization
  , coao.academic_organization_array AS academic_organization_array
  , coao.academic_organization_display AS academic_organization_display

  -- Course offering
  , co.title AS course_offering_title
  , co.start_date AS course_offering_start_date
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , CONCAT(co.subject, ' ', co.number) as course_offering_code
  , coe.num_students AS num_students

  -- Course section
  , cs.course_section_id as udp_course_section_id
  , cs.lms_id as lms_course_section_id
  , cs.sis_id as sis_course_section_id

  -- Instructor names and emails.
  , coe.instructor_name_array AS instructor_name_array
  , coe.instructor_lms_id_array AS instructor_lms_id_array
  , coe.instructor_display AS instructor_display
  , coe.instructor_email_address_array AS instructor_email_address_array
  , coe.instructor_email_address_display as instructor_email_address_display

  -- Launch information
  , lt.event_time AS event_time
  , lt.event_day AS event_day
  , lt.event_hour AS event_hour

  -- Tool use data
  , lt.canvas_tool AS canvas_tool
  , lt.asset_type AS asset_type
  , lt.asset_type_id AS asset_type_id
  , lt.asset_subtype AS asset_subtype
  , lt.asset_subtype_id AS asset_subtype_id
  , lt.module_item_id AS module_item_id
  , lt.learner_activity_id AS learner_activity_id

  -- UDP IDs for tools 
  , COALESCE(kd.id,mi.discussion_id) AS udp_discussion_id
  , COALESCE(kq.id,f.quiz_id,mi.quiz_id) as udp_quiz_id
  , kmi.id as udp_module_item_id
  , COALESCE(kf.id,mi.file_id) as udp_file_id
  , COALESCE(kwp.id,mi.wiki_page_id) as udp_wiki_page_id
  , COALESCE(kla.id,d.learner_activity_id,q.learner_activity_id,mi.learner_activity_id,f.learner_activity_id) as udp_learner_activity_id

FROM
  _WRITE_TO_PROJECT_.mart_helper.event__general__lms_tool AS lt
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON lt.lms_course_offering_id=co.lms_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_academic_organization AS coao ON co.course_offering_id=coao.udp_course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON lt.lms_person_id=p.lms_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering__enrollment AS coe ON co.course_offering_id=coe.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_section_enrollment as cose on p.person_id = cose.udp_person_id and co.course_offering_id = cose.udp_course_offering_id
LEFT join _WRITE_TO_PROJECT_.mart_helper.context__course_section as cs on cose.udp_course_section_id = cs.course_section_id
-- Define UDP IDs for tools 
LEFT JOIN _READ_FROM_PROJECT_.context_store_keymap.learner_activity kla on lt.canvas_tool = 'Learner activity' and lt.asset_type_id = kla.lms_int_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_keymap.discussion kd on lt.canvas_tool = 'Discussion' and lt.asset_type_id = kd.lms_int_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_entity.discussion d on kd.id = d.discussion_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_keymap.quiz kq on lt.canvas_tool = 'Quiz' and lt.asset_type_id = kq.lms_int_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_entity.quiz q on kq.id = q.quiz_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_keymap.module_item kmi on lt.canvas_tool = 'Module item' and lt.asset_type_id = kmi.lms_int_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_entity.module_item mi on kmi.id = mi.module_item_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_keymap.file kf on lt.canvas_tool = 'File' and lt.asset_type_id = kf.lms_int_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_entity.file f on kf.id = f.file_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_keymap.wiki_page kwp on lt.canvas_tool = 'Wiki page' and lt.asset_type_id = kwp.lms_int_id
LEFT JOIN _READ_FROM_PROJECT_.context_store_entity.wiki_page wp on kwp.id = wp.wiki_page_id 
