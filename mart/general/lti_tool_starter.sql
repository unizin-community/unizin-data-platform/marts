DROP TABLE IF EXISTS `_WRITE_TO_PROJECT_.mart_general.lti_tool`;

CREATE TABLE 
    `_WRITE_TO_PROJECT_.mart_general.lti_tool` (
    	udp_course_offering_id INT64
    ,	lms_course_offering_id STRING
    ,   sis_course_offering_id STRING
    ,	udp_person_id INT64
    ,	lms_person_id STRING
    ,   sis_person_id STRING
    ,   role STRING
    ,   academic_term_name STRING
    ,   academic_term_start_date DATE
    ,   academic_organization_array ARRAY<STRING>
    ,   academic_organization_display STRING
    ,   course_offering_title STRING
    ,   course_offering_start_date DATE
    ,   course_offering_subject STRING
    ,   course_offering_number STRING
    ,   course_offering_code STRING
    ,   num_students INT64
    ,   instructor_name_array ARRAY<STRING>
    ,   instructor_lms_id_array ARRAY<STRING>
    ,   instructor_display STRING
    ,   instructor_email_address_array ARRAY<STRING> 
    ,   instructor_email_address_display STRING
    ,   event_time DATETIME
    ,   event_day DATE
    ,   event_hour INT64
    ,   launch_app_url STRING
    ,   launch_app_domain STRING
    ,   launch_app_name STRING
    ,   tool_name STRING
    ,   is_lti_tool BOOLEAN
    ,   is_redirect_tool BOOLEAN
    )
PARTITION BY
  DATE(event_time);

--DROP MATERIALIZED VIEW IF EXISTS `_WRITE_TO_PROJECT_.mart_general.lti_tool`;

/*CREATE MATERIALIZED VIEW `_WRITE_TO_PROJECT_.mart_general.lti_tool`
as select *
from `_WRITE_TO_PROJECT_.mart_general.lti_tool`;*/
