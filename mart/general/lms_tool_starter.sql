DROP TABLE IF EXISTS `_WRITE_TO_PROJECT_.mart_general.lms_tool`;

CREATE TABLE 
    `_WRITE_TO_PROJECT_.mart_general.lms_tool` (
    	udp_course_offering_id INT64
    ,	lms_course_offering_id STRING
    ,   sis_course_offering_id STRING
    ,	udp_person_id INT64
    ,	lms_person_id STRING
    ,   sis_person_id STRING
    ,   role STRING
    ,   academic_term_name STRING
    ,   academic_term_start_date DATE
    ,   academic_organization_array ARRAY<STRING>
    ,   academic_organization_display STRING
    ,   course_offering_title STRING
    ,   course_offering_start_date DATE
    ,   course_offering_subject STRING
    ,   course_offering_number STRING
    ,   course_offering_code STRING
    ,   num_students INT64
    ,   instructor_name_array ARRAY<STRING>
    ,   instructor_lms_id_array ARRAY<STRING>
    ,   instructor_display STRING
    ,   instructor_email_address_array ARRAY<STRING> 
    ,   instructor_email_address_display STRING
    ,   event_time DATETIME
    ,   event_day DATE
    ,   event_hour INT64
    ,   canvas_tool STRING
    ,   asset_type STRING
    ,   asset_type_id STRING
    ,   asset_subtype STRING
    ,   asset_subtype_id STRING
    ,   module_item_id STRING
    ,   learner_activity_id STRING
    ,   udp_discussion_id INT64
    ,   udp_quiz_id INT64
    ,   udp_module_item_id INT64 
    ,   udp_file_id INT64 
    ,   udp_wiki_page_id INT64 
    ,   udp_learner_activity_id INT64
    )
PARTITION BY
  DATE(event_time);


/*DROP MATERIALIZED VIEW IF EXISTS `_WRITE_TO_PROJECT_.mart_general.lms_tool`;

CREATE MATERIALIZED VIEW `_WRITE_TO_PROJECT_.mart_general.lms_tool`
as select *
from `_WRITE_TO_PROJECT_.mart_general.lms_tool`;*/
