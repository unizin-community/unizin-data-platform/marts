/*
  Marts / General / Tool launches

    Generates a full history of LTI tool launches
    with the latest tool names.

  Properties:

    Export table:  mart.lti_tool
    Run frequency: Every hour
    Run operation: Overwrite

  To do:
    *
*/

WITH lti_tool AS (
 
SELECT 
  -- Basic course information.
  co.course_offering_id AS udp_course_offering_id
  , tl.lms_course_offering_id AS lms_course_offering_id
  , co.sis_id AS sis_course_offering_id
  , p.person_id AS udp_person_id
  , tl.lms_person_id AS lms_person_id
  , p.sis_id AS sis_person_id

  -- Role
  , tl.role AS role

  -- Academic term
  , tt.name AS academic_term_name
  , tt.term_begin_date AS academic_term_start_date

  -- Academic organization
  , coao.academic_organization_array AS academic_organization_array
  , coao.academic_organization_display AS academic_organization_display

  -- Course offering
  , co.title AS course_offering_title
  , co.start_date AS course_offering_start_date
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , CONCAT(co.subject, ' ', co.number) as course_offering_code
  , coe.num_students AS num_students

  -- Course section
  , cs.course_section_id as udp_course_section_id
  , cs.lms_id as lms_course_section_id
  , cs.sis_id as sis_course_section_id

  -- Instructor names and emails.
  , coe.instructor_name_array AS instructor_name_array
  , coe.instructor_lms_id_array AS instructor_lms_id_array
  , coe.instructor_display AS instructor_display
  , coe.instructor_email_address_array AS instructor_email_address_array
  , coe.instructor_email_address_display as instructor_email_address_display

  -- Launch information
  , tl.event_time AS event_time
  , tl.event_day AS event_day
  , tl.event_hour AS event_hour

  -- Tool launch data
  , tl.launch_app_url AS launch_app_url
  , tl.launch_app_domain AS launch_app_domain
  , tl.launch_app_name AS launch_app_name
  , CASE
      WHEN tl.launch_app_url IS NOT NULL THEN COALESCE(tfu_for_url.tool, 'Unknown tool')
      WHEN tl.launch_app_url IS NULL AND tl.launch_app_domain IS NOT NULL THEN COALESCE(tfu_for_domain.tool, 'Unknown tool') -- Fix using new join on app_domain
      WHEN tl.launch_app_url IS NULL AND tl.launch_app_domain IS NULL AND tl.launch_app_name IS NOT NULL THEN COALESCE(tfn.tool, 'Unknown tool')
      END
    AS tool_name

  -- Is tool or is redirect?
  , tl.is_lti_tool AS is_lti_tool
  , tl.is_redirect_tool AS is_redirect_tool

FROM
  _WRITE_TO_PROJECT_.mart_helper.event__general__lti_tool AS tl

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.utility__from_tool_url AS tfu_for_url ON tl.launch_app_url=tfu_for_url.url 
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.utility__from_tool_url AS tfu_for_domain ON tl.launch_app_domain=tfu_for_domain.url
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.utility__from_tool_name AS tfn ON tl.launch_app_name=tfn.name

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON tl.lms_course_offering_id=co.lms_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_academic_organization AS coao ON co.course_offering_id=coao.udp_course_offering_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON tl.lms_person_id=p.lms_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering__enrollment AS coe ON co.course_offering_id=coe.course_offering_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_section_enrollment as cose on p.person_id = cose.udp_person_id and co.course_offering_id = cose.udp_course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section as cs on cose.udp_course_section_id = cs.course_section_id

WHERE tl.is_redirect_tool IS FALSE
),

redirect_tool AS (

SELECT 
  -- Basic course information.
  co.course_offering_id AS udp_course_offering_id
  , tl.lms_course_offering_id AS lms_course_offering_id
  , co.sis_id AS sis_course_offering_id
  , p.person_id AS udp_person_id
  , tl.lms_person_id AS lms_person_id
  , p.sis_id AS sis_person_id

  -- Role
  , tl.role AS role

  -- Academic term
  , tt.name AS academic_term_name
  , tt.term_begin_date AS academic_term_start_date

  -- Academic organization
  , coao.academic_organization_array
  , coao.academic_organization_display

  -- Course offering
  , co.title AS course_offering_title
  , co.start_date AS course_offering_start_date
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , CONCAT(co.subject, ' ', co.number) as course_offering_code
  , coe.num_students AS num_students

  -- Course section
  , cs.course_section_id as udp_course_section_id
  , cs.lms_id as lms_course_section_id
  , cs.sis_id as sis_course_section_id

  -- Instructor names and emails.
  , coe.instructor_name_array AS instructor_name_array
  , coe.instructor_lms_id_array AS instructor_lms_id_array
  , coe.instructor_display AS instructor_display
  , coe.instructor_email_address_array AS instructor_email_address_array
  , coe.instructor_email_address_display as instructor_email_address_display

  -- Launch information
  , tl.event_time AS event_time
  , tl.event_day AS event_day
  , tl.event_hour AS event_hour

  -- Tool launch data
  , tl.launch_app_url AS launch_app_url
  , tl.launch_app_domain AS launch_app_domain
  , tl.launch_app_name AS launch_app_name
  , COALESCE(launch_app_name, 'Unknown redirect') as tool_name 
      

  -- Is tool or is redirect?
  , tl.is_lti_tool as is_lti_tool
  , tl.is_redirect_tool AS is_redirect_tool

FROM
  _WRITE_TO_PROJECT_.mart_helper.event__general__lti_tool AS tl

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON tl.lms_course_offering_id=co.lms_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_academic_organization AS coao ON co.course_offering_id=coao.udp_course_offering_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON tl.lms_person_id=p.lms_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering__enrollment AS coe ON co.course_offering_id=coe.course_offering_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_section_enrollment as cose on p.person_id = cose.udp_person_id and co.course_offering_id = cose.udp_course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section as cs on cose.udp_course_section_id = cs.course_section_id

WHERE tl.is_redirect_tool IS TRUE

)

SELECT *
FROM lti_tool 
UNION ALL 
SELECT *
FROM redirect_tool
