/*
  Marts / Course offering / Last activity

    Generate data about the last activity performed
    by students in a course.

  Properties:

    Export table:  mart_course_offering.last_activity
    Run frequency: Every hour
    Run operation: Overwrite

  Dependencies:
    * Course offering
    * Academic term
    * Academic session
    * Person
    * Academic organization
    * Course offering / By Person / Learner activity result

  To do:
    *
*/

WITH course_offering_enrollment AS (
  SELECT
    DISTINCT 
    -- Basic course information
    co.course_offering_id AS udp_course_offering_id
    , co.lms_id AS lms_course_offering_id
    , cse.person_id AS udp_person_id
    , cse.lms_person_id AS lms_person_id

    -- Academic organization
    , coao.academic_organization_display AS academic_organization_display
    , TO_JSON_STRING(coao.academic_organization_array) AS academic_organization_array

    -- Academic term
    , tt.name AS academic_term_name
    , tt.term_begin_date AS academic_term_start_date

    -- Course offering
    , co.title AS course_offering_title
    , co.start_date AS course_offering_start_date
    , co.subject as course_offering_subject
    , co.number as course_offering_number
    , CONCAT(co.subject, ' ', co.number) as course_offering_code

    -- Person information
    , CONCAT(p.last_name, ', ', p.first_name) AS person_name

    -- Role
    , cse.role AS role

    -- Instructor information
    , coe.instructor_display
    , TO_JSON_STRING(coe.instructor_name_array) as instructor_name_array
    , coe.instructor_email_address_display
    , TO_JSON_STRING(coe.instructor_email_address_array) as instructor_email_address_array

  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON cse.person_id=p.person_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON cse.course_section_id=cs.course_section_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON COALESCE(cs.le_current_course_offering_id,cs.course_offering_id)=co.course_offering_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering__enrollment AS coe ON co.course_offering_id = coe.course_offering_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
  LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_academic_organization AS coao ON co.course_offering_id = coao.udp_course_offering_id
  GROUP BY
    cse.person_id
    , cse.lms_person_id
    , co.course_offering_id
    , co.lms_id
    , coao.academic_organization_display
    , academic_organization_array
    , tt.name
    , tt.term_begin_date
    , co.title
    , co.start_date
    , co.subject
    , co.number
    , coe.instructor_display
    , instructor_name_array
    , coe.instructor_email_address_display
    , instructor_email_address_array
    , person_name
    , role
)

SELECT
  -- Basic course information
  coe.udp_course_offering_id AS udp_course_offering_id
  , coe.lms_course_offering_id AS lms_course_offering_id
  , coe.udp_person_id AS udp_person_id
  , coe.lms_person_id AS lms_person_id

  -- Academic organization
  , coe.academic_organization_display AS academic_organization_display
  , JSON_EXTRACT_STRING_ARRAY(coe.academic_organization_array) AS academic_organization_array

  -- Academic term
  , coe.academic_term_name AS academic_term_name
  , coe.academic_term_start_date AS academic_term_start_date

  -- Course offering
  , coe.course_offering_title AS course_offering_title
  , coe.course_offering_start_date AS course_offering_start_date

  -- Instructor
  , coe.instructor_display
  ,JSON_EXTRACT_STRING_ARRAY(coe.instructor_name_array) AS instructor_name_array
  , coe.instructor_email_address_display
  , JSON_EXTRACT_STRING_ARRAY(coe.instructor_email_address_array) AS instructor_email_address_array

  -- Person
  , coe.person_name AS person_name

  -- Role 
  , coe.role AS role

  -- Last activity types for a person
  , la.last_activity AS last_activity
  , la.last_navigation_activity AS last_navigation_activity
  , la.last_media_activity AS last_media_activity
  , la.last_grade_activity AS last_grade_activity
  , la.last_assessment_activity AS last_assessment_activity
  , la.last_assignment_activity AS last_assignment_activity

  /*
    Number of events by that person in that course, by
    the type of event.
  */
  , la.num_events AS num_events
  , la.num_navigation_events AS num_navigation_events
  , la.num_media_events AS num_media_events
  , la.num_grade_events AS num_grade_events
  , la.num_assessment_events AS num_assessment_events
  , la.num_assignment_events AS num_assignment_events

  /*
    Number of completed learning activities, quizzes, discussion entries,
    and modules by person.
  */
/*
  , cobplar.count_is_processed AS num_learner_activity_results
  , cse.num_quiz_results AS num_quiz_results
  , cse.num_discussion_entries AS num_discussion_entries
  , cse.num_completed_modules AS num_completed_modules
*/

FROM course_offering_enrollment AS coe

/* Completion metrics */
/*
  LEFT JOIN warehouse_course_offering_by_person.learner_activity_result AS cobplar
    ON cse.udp_person_id=cobplar.person_id AND cse.udp_course_offering_id=cobplar.course_offering_id
*/

-- Event ETL data
LEFT JOIN
  _WRITE_TO_PROJECT_.mart_helper.event__course_offering__last_activity AS la
    ON coe.lms_course_offering_id=la.lms_course_offering_id
    AND coe.lms_person_id=la.lms_person_id
;
