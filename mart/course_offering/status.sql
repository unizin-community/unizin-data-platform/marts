/*
  Marts / Course offering / Status

    Generates an updated view of the course
    offerings at an institution and their status
    in the learning environment.

  Properties:

    Export table:  mart_course_offering.status
    Run frequency: Every hour
    Run operation: Overwrite

  To do:
    * Perhaps a change is required to the a table?

*/

SELECT
  -- Basic course information.
  co.course_offering_id AS udp_course_offering_id
  , co.lms_id AS lms_course_offering_id

  -- Academic term
  , tt.name AS academic_term_name
  , tt.term_begin_date AS academic_term_start_date

  -- Academic organization
  , coao.academic_organization_array AS academic_organization_array
  , coao.academic_organization_display AS academic_organization_display

  -- Course offering
  , co.title AS course_offering_title
  , co.start_date AS course_offering_start_date
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , CONCAT(co.subject, ' ', co.number) as course_offering_code

  -- Instructor names and emails.
  , coe.instructor_name_array AS instructor_name_array
  , coe.instructor_lms_id_array AS instructor_lms_id_array
  , coe.instructor_display AS instructor_display
  , coe.instructor_email_address_array AS instructor_email_address_array
  , coe.instructor_email_address_display as instructor_email_address_display

  -- Learning environment status.
  , COALESCE(eeos.status, co.le_status) AS status

  , CASE
    -- These status values are from Canvas Data.
    WHEN COALESCE(eeos.status, co.le_status) = 'created' THEN 'Not published'
    WHEN COALESCE(eeos.status, co.le_status) = 'available' THEN 'Published'
    WHEN COALESCE(eeos.status, co.le_status) = 'completed' THEN 'Completed'
    WHEN COALESCE(eeos.status, co.le_status) = 'deleted' THEN 'Deleted'
    WHEN COALESCE(eeos.status, co.le_status) = 'claimed' THEN 'Not published'

    -- These are in the event data.
    WHEN COALESCE(eeos.status, co.le_status) = 'published' THEN 'Published'
    WHEN COALESCE(eeos.status, co.le_status) = 'unpublished' THEN 'Not published'
    WHEN COALESCE(eeos.status, co.le_status) = 'active' THEN 'Published'
    ELSE COALESCE(eeos.status, co.le_status)
    END AS reported_status

  -- Publish etime
  , eecopt.publish_time AS publish_time

  -- Course enrollments
  , coe.num_students AS num_students

  -- Course metrics.
  , wcoela.count_is_status_published AS published_la
  , wcoela.count_is_status_unpublished AS unpublished_la
  , wcoeq.count_is_status_published AS published_quiz
  , wcoeq.count_is_status_unpublished AS unpublished_quiz
  , wcoem.count_is_status_active AS active_module
  , wcoem.count_is_status_unpublished AS unpublished_module

FROM _WRITE_TO_PROJECT_.mart_helper.context__course_offering co

INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_academic_organization AS coao ON co.course_offering_id=coao.udp_course_offering_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering__enrollment coe ON co.course_offering_id=coe.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__learner_activity wcoela ON co.course_offering_id=wcoela.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__quiz wcoeq ON co.course_offering_id=wcoeq.course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_entity__module wcoem ON co.course_offering_id=wcoem.course_offering_id

LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.event__course_offering__status eeos ON co.lms_id=eeos.lms_course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.event__course_offering__publish_time eecopt ON co.lms_id=eecopt.lms_course_offering_id
