DROP TABLE IF EXISTS `_WRITE_TO_PROJECT_.mart_course_offering.last_activity`;

CREATE TABLE 
    `_WRITE_TO_PROJECT_.mart_course_offering.last_activity` (
    	udp_course_offering_id INT64
    ,	lms_course_offering_id STRING
    ,	udp_person_id INT64
    ,	lms_person_id STRING
    ,   academic_organization_display STRING
    ,   academic_organization_array ARRAY<STRING>
    ,   academic_term_name STRING
    ,   academic_term_start_date DATE
    ,   course_offering_title STRING
    ,   course_offering_start_date DATE
    ,   course_offering_subject STRING 
    ,   course_offering_number STRING
    ,   course_offering_code STRING
    ,   instructor_display STRING
    ,   instructor_name_array ARRAY<STRING>
    ,   instructor_email_address_array ARRAY<STRING>
    ,   instructor_email_address_display STRING
    ,   person_name STRING
    ,   role STRING
    ,   last_activity DATETIME
    ,   last_navigation_activity DATETIME
    ,   last_media_activity DATETIME
    ,   last_grade_activity DATETIME
    ,   last_assessment_activity DATETIME
    ,   last_assignment_activity DATETIME
    ,   num_events INT64
    ,   num_navigation_events INT64
    ,   num_media_events INT64
    ,   num_grade_events INT64
    ,   num_assessment_events INT64
    ,   num_assignment_events INT64
    )
PARTITION BY
  DATE(last_activity);

/*DROP MATERIALIZED VIEW IF EXISTS `_WRITE_TO_PROJECT_.mart_course_offering.last_activity`;

CREATE MATERIALIZED VIEW `_WRITE_TO_PROJECT_.mart_course_offering.last_activity`
as select *
from `_WRITE_TO_PROJECT_.mart_course_offering.last_activity`;*/
