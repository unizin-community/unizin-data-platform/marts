/*
  Marts / Course offering / Interaction sessions

    Generate data about the interaction sessions of a student in a course offering.

  Properties:

    Export table:  mart_course_offering.interaction_sessions
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:
    * Event ETL / Course offering / Interaction sessions
    * Course section
    * Course section enrollment
    * Course offering
    * Academic term
    * Person
    * Course offering academic organization
    * Course offering / Enrollment

  To do:
    *
*/

WITH course_offering_enrollment AS (
  SELECT
    DISTINCT 
    -- IDs
    cse.person_id AS udp_person_id 
    , co.course_offering_id AS udp_course_offering_id

    -- Role
    , cse.role AS role
    
  FROM _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_section AS cs ON cse.course_section_id=cs.course_section_id
  INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON COALESCE(cs.le_current_course_offering_id,cs.course_offering_id)=co.course_offering_id
)

SELECT
  -- IDs
  ints.udp_course_offering_id AS udp_course_offering_id
  , co.lms_id AS lms_course_offering_id
  , ints.udp_person_id AS udp_person_id
  , p.lms_id AS lms_person_id

  -- Academic organization
  , coao.academic_organization_display AS academic_organization_display
  , coao.academic_organization_array AS academic_organization_array

  -- Academic term
  , tt.name AS academic_term_name
  , tt.term_begin_date AS academic_term_start_date

  -- Course offering
  , co.title AS course_offering_title
  , co.start_date AS course_offering_start_date
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , CONCAT(co.subject, ' ', co.number) as course_offering_code

  -- Instructor
  , wcoe.instructor_display
  , wcoe.instructor_name_array AS instructor_name_array
  , wcoe.instructor_email_address_display
  , wcoe.instructor_email_address_array AS instructor_email_address_array

  -- Person
  , CASE
    WHEN p.first_name IS NOT NULL AND p.last_name IS NOT NULL AND p.first_name <> '' AND p.last_name <> ''
      THEN CONCAT(p.last_name, ', ', p.first_name)
    WHEN ARRAY_LENGTH(SPLIT(p.name,' ')) > 1 
      THEN CONCAT(
                (
                  SELECT STRING_AGG(name, ' ' ORDER BY OFFSET) 
                  FROM UNNEST(SPLIT(p.name, ' ')) name WITH OFFSET 
                  WHERE OFFSET > 0
                )
                ,', '
                ,SPLIT(p.name,' ')[OFFSET(0)])
    END AS person_name 

  -- Role 
  , coe.role AS role

  -- Week in term
  , ints.week_in_term
  , CASE
      WHEN acs.instruction_begin_date is not null then DATE_TRUNC(DATE_ADD(acs.instruction_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN tt.term_begin_date is not null then DATE_TRUNC(DATE_ADD(tt.term_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN co.start_date is not null then DATE_TRUNC(DATE_ADD(co.start_date, INTERVAL week_in_term - 1 WEEK),WEEK)
    ELSE null end as week_start_date
  , CASE
      WHEN acs.instruction_begin_date is not null then LAST_DAY(DATE_ADD(acs.instruction_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN tt.term_begin_date is not null then LAST_DAY(DATE_ADD(tt.term_begin_date, INTERVAL week_in_term - 1 WEEK),WEEK)
      WHEN co.start_date is not null then LAST_DAY(DATE_ADD(co.start_date, INTERVAL week_in_term - 1 WEEK),WEEK)
    ELSE null end as week_end_date
  -- Interaction sessions 
  , ints.* except(udp_course_offering_id,udp_person_id,week_in_term)

FROM _WRITE_TO_PROJECT_.mart_helper.event__course_offering__interaction_sessions AS ints
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co ON ints.udp_course_offering_id = co.course_offering_id 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__person AS p ON ints.udp_person_id = p.person_id
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering__enrollment AS wcoe ON co.course_offering_id = wcoe.course_offering_id
INNER JOIN course_offering_enrollment AS coe ON ints.udp_course_offering_id = coe.udp_course_offering_id AND ints.udp_person_id = coe.udp_person_id 
INNER JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term AS tt ON co.academic_term_id=tt.academic_term_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering_academic_organization AS coao ON co.course_offering_id = coao.udp_course_offering_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session acs ON co.academic_session_id = acs.academic_session_id
;

