/*
  Marts / Course offering / File interaction

    Generates a mart of file interactions
    in course offering.

  Properties:

    Export table:  mart_course_offering.file_interactions
    Run frequency: Every hour
    Run operation: Overwrite

  Dependencies:
    * Event ETL / Course offering / Object interaction
    * Context / Base / File
    * Context / Course section / Enrollment

  To do:
    * Pull the uploader role into the underlying Event ETL mart.
*/

/*
  From the raw file data, generate metrics about a
  object's use in a course section.
*/
WITH course_offering_object_interaction AS
(
  SELECT
    COALESCE(co.course_offering_id,cooi.udp_course_offering_id) AS udp_course_offering_id

    /*
      For Canvas Live Events, we're stuck here
      with the equivalent of the LMS internal ID.
    */
    , cooi.object_id AS lms_object_id
    , cooi.object_type AS object_type

    , COUNT(1) AS num_views
    , COUNT(DISTINCT cooi.udp_person_id) AS num_distinct_students
    , ARRAY_AGG(DISTINCT cooi.udp_person_id) AS students_who_viewed_udp_id_array

  FROM
    _WRITE_TO_PROJECT_.mart_helper.event__course_offering__object_interaction AS cooi
  LEFT JOIN 
    _WRITE_TO_PROJECT_.mart_helper.context__course_offering AS co 
    ON cooi.lms_course_offering_id = co.lms_id

  WHERE
    cooi.udp_person_id IS NOT NULL
    AND udp_course_offering_id IS NOT NULL
    AND cooi.object_id IS NOT NULL
    AND cooi.role = 'Learner'

  GROUP BY
    udp_course_offering_id
    , cooi.object_id
    , cooi.object_type
),

/*
  For this mart, we only want students' interactions with files
  that are uploaded, not all of what the LMS counts as an object.

  We take the inner product with the File entity to eliminate any
  object that is not a file (this is imperfect, since IDs could
  be shared across entities).

  We'll also pull in a variety of File attributes.
*/
course_offering_file_interaction as (
  SELECT
    f.course_offering_id AS udp_course_offering_id
    , f.lms_int_id AS lms_int_file_id
    , f.file_id AS udp_file_id
    , f.content_type

    /* File interaction metrics */
    , COALESCE(csoi.num_views, 0) AS num_views
    , COALESCE(csoi.num_distinct_students, 0) AS num_distinct_students
    , csoi.students_who_viewed_udp_id_array AS students_who_viewed_udp_id_array

  FROM
    _WRITE_TO_PROJECT_.mart_helper.context__file as f
  left join 
    course_offering_object_interaction AS csoi
    ON csoi.lms_object_id=f.lms_int_id
    AND csoi.udp_course_offering_id = f.course_offering_id

  WHERE
    f.owner_entity_type IS NOT NULL
    AND f.size > 0
    AND f.size IS NOT NULL
    AND f.is_status_available = 1
    AND uploader_id IS NOT NULL
    AND uploader_id IN (
      SELECT
        DISTINCT person_id
      FROM
        _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment AS cse
      WHERE
        cse.role IN ('Teacher','TeachingAssistant')
    )
)

/*
  For the final presentation, we will present a complete
  view of the files that have had interactions. We will
  also compute the array and percentage of students who
  have not viewed the file.
*/
SELECT
  /* Course offering file interactions */
  cofi.udp_course_offering_id AS udp_course_offering_id
  , cofi.udp_file_id AS udp_file_id
  , cofi.lms_int_file_id AS lms_int_file_id

  , cofi.num_views AS num_views
  , cofi.num_distinct_students AS num_distinct_students

  /* Course offering enrollments */
  , coe.num_students as num_enrolled_students
  , coe.student_udp_id_array AS student_udp_id_array
  , coe.instructor_name_array
  , coe.instructor_display
  , coe.instructor_email_address_array
  , coe.instructor_email_address_display

  /* Course Offering */
  , co.subject as course_offering_subject
  , co.number as course_offering_number
  , co.lms_id as lms_course_offering_id
  , CONCAT(co.subject, ' ', co.number) as course_offering_code

  /* Academic Term */
  , act.name as academic_term_name
  , act.term_begin_date
  , act.term_end_date

  /* File */
  , f.learner_activity_id
  , f.learner_activity_title
  , f.learner_activity_due_date
  , f.uploader_id
  , f.quiz_id
  , f.quiz_title
  , f.quiz_due_date
  , SPLIT(f.content_type, '/')[OFFSET(0)] as content_type 
  , CASE WHEN ARRAY_LENGTH(SPLIT(f.content_type, '/')) <= 1 THEN 'None' ELSE SPLIT(f.content_type, '/')[OFFSET(1)] END as content_sub_type
  , f.display_name
  , f.owner_entity_type
  , f.size
  , f.created_date
  , f.unlocked_date
  , f.updated_date
  , f.accessible_date
  , f.most_recent_version_date


  , ARRAY(
      SELECT * from UNNEST(cofi.students_who_viewed_udp_id_array)
      AS a where a in UNNEST(coe.student_udp_id_array))   AS students_who_viewed_udp_id_array

  , CASE
    WHEN num_views = 0 THEN coe.student_udp_id_array
    ELSE ARRAY(SELECT * FROM coe.student_udp_id_array EXCEPT DISTINCT (SELECT * FROM cofi.students_who_viewed_udp_id_array))
    END AS students_who_did_not_view_udp_id_array

  , CASE
    WHEN num_views = 0 THEN 0
    WHEN ARRAY_LENGTH(coe.student_udp_id_array) = 0 THEN 0
    ELSE ARRAY_LENGTH(
          ARRAY(
            SELECT * from UNNEST(cofi.students_who_viewed_udp_id_array)
            AS a where a in UNNEST(coe.student_udp_id_array))
          ) / ARRAY_LENGTH(coe.student_udp_id_array)
    END AS pct_class_viewed

FROM
  _WRITE_TO_PROJECT_.mart_helper.context__file as f
inner join
  course_offering_file_interaction AS cofi on f.file_id = cofi.udp_file_id
inner JOIN
  _WRITE_TO_PROJECT_.mart_helper.context__course_offering__enrollment AS coe ON cofi.udp_course_offering_id=coe.course_offering_id
inner JOIN
  _WRITE_TO_PROJECT_.mart_helper.context__course_offering as co on co.course_offering_id = cofi.udp_course_offering_id
inner JOIN 
  _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act on act.academic_term_id = co.academic_term_id
