/*
  Marts / Course offering / Long Inactivity

    For all actively enrolled students in the current term(s),
    highlight which students have not been active in the LE
    for at least 5 days

  Properties:

    Export table:  mart_course_offering.long_inactivity
    Run frequency: Every hour
    Run operation: Overwrite

  Dependencies:
    * Course offering
    * Academic term
    * Course Section Enrollemnt
    * Last Activity

  To do:
    *
*/

with determine_current_term as (
  select academic_term_id, name, term_type, term_year, term_begin_date, term_end_date
  from _WRITE_TO_PROJECT_.mart_helper.context__academic_term
  where term_begin_date <= CURRENT_DATE()
  and term_end_date >= CURRENT_DATE()
),

determine_inactive_dropped_students as (
  select distinct person_id, course_offering_id
  from _WRITE_TO_PROJECT_.mart_helper.context__course_section_enrollment
  where is_role_status_dropped = 1 
     or is_role_status_wait_listed = 1
     or is_role_status_not_enrolled = 1
     or is_role_status_no_data = 1
     or is_role_status_none = 1
     or is_role_status_completed = 1
     or is_enrollment_status_inactive = 1
     or is_enrollment_status_not_enrolled = 1
     or is_enrollment_status_no_data = 1
     or is_enrollment_status_none = 1
     or is_enrollment_status_completed = 1
),

filtered_la as (
  select * 
  from _WRITE_TO_PROJECT_.mart_course_offering.last_activity as la
  inner join _WRITE_TO_PROJECT_.mart_helper.context__course_offering as co
    on co.course_offering_id = la.udp_course_offering_id
  inner join determine_current_term as current_term
    on current_term.academic_term_id = co.academic_term_id
  left join determine_inactive_dropped_students as inactive_dropped
    on inactive_dropped.course_offering_id = la.udp_course_offering_id
    and inactive_dropped.person_id = la.udp_person_id
  where inactive_dropped.person_id is null
  and la.role = 'Student'
  and (
      (la.last_activity is not null
        and la.last_activity >= current_term.term_begin_date
        and la.last_activity <= current_term.term_end_date
      ) or
      (la.last_activity is null)
  )
  and co.start_date <= CURRENT_DATE()
  and co.end_date >= CURRENT_DATE()
)

select udp_course_offering_id
  , lms_course_offering_id
  , udp_person_id
  , lms_person_id
  , academic_organization_display
  , academic_organization_array
  , academic_term_name
  , term_begin_date
  , term_end_date
  , course_offering_title
  , start_date as course_start_date
  , end_date as course_end_date
  , instructor_display
  , instructor_name_array
  , instructor_email_address_display
  , instructor_email_address_array
  , person_name
  , last_activity
  , CASE 
      WHEN last_activity is null then 1
      else 0
    END as has_no_activity
  , COALESCE(DATE_DIFF(current_datetime(),last_activity, DAY), null) as days_since_last_activity
  , CASE
      WHEN last_activity is null then null
      WHEN DATE_DIFF(current_datetime(),last_activity, DAY) >= 5 then 1 
      else 0
    END AS is_5_days
  , CASE
      WHEN last_activity is null then null
      WHEN DATE_DIFF(current_datetime(),last_activity, DAY) >= 7 then 1 
      else 0
    END AS is_7_days
  , CASE
      WHEN last_activity is null then null
      WHEN DATE_DIFF(current_datetime(),last_activity, DAY) >= 10 then 1 
      else 0
    END AS is_10_days
  , CASE
      WHEN last_activity is null then null
      WHEN DATE_DIFF(current_datetime(),last_activity, DAY) >= 14 then 1 
      else 0
    END AS is_14_days
from filtered_la
where DATE_DIFF(current_datetime(),last_activity, DAY) >= 5
  or last_activity is null

