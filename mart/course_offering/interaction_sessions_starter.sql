DROP TABLE IF EXISTS _WRITE_TO_PROJECT_.mart_course_offering.interaction_sessions;

CREATE TABLE 
    _WRITE_TO_PROJECT_.mart_course_offering.interaction_sessions(
    	udp_course_offering_id INT64
    ,	lms_course_offering_id STRING
    ,	udp_person_id INT64
    ,	lms_person_id STRING
    ,   academic_organization_display STRING
    ,   academic_organization_array ARRAY<STRING>
    ,   academic_term_name STRING
    ,   academic_term_start_date DATE
    ,   course_offering_title STRING
    ,   course_offering_start_date DATE
    ,   course_offering_subject STRING 
    ,   course_offering_number STRING
    ,   course_offering_code STRING
    ,   instructor_display STRING
    ,   instructor_name_array ARRAY<STRING>
    ,   instructor_email_address_array ARRAY<STRING>
    ,   instructor_email_address_display STRING
    ,   person_name STRING
    ,   role STRING
    ,   week_in_term INT64 
    ,   week_start_date DATE 
    ,   week_end_date DATE 
    ,   session_date DATE 
    ,   num_sessions_10min INT64 
    ,   total_time_seconds_10min INT64 
    ,   total_actions_10min INT64 
    ,   avg_time_seconds_10min FLOAT64 
    ,   avg_actions_10min FLOAT64
    ,   num_sessions_20min INT64 
    ,   total_time_seconds_20min INT64 
    ,   total_actions_20min INT64 
    ,   avg_time_seconds_20min FLOAT64 
    ,   avg_actions_20min FLOAT64
    ,   num_sessions_30min INT64 
    ,   total_time_seconds_30min INT64 
    ,   total_actions_30min INT64 
    ,   avg_time_seconds_30min FLOAT64 
    ,   avg_actions_30min FLOAT64

    )
PARTITION BY
  session_date;

