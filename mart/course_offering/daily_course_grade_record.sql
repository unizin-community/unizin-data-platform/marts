/*
  Marts / Course offering / Daily course grade record

    Define 

  Properties:

    Export table:  mart_course_offering.daily_course_grade_record
    Run frequency: Every day
    Run operation: Append

  Dependencies:
    * Course grade
    * Course section 
    * Course offering
    * Academic term

*/

select 
  DISTINCT 
  cg.person_id as udp_person_id,
  cg.lms_person_id,
  co.course_offering_id as udp_course_offering_id,
  co.lms_id as lms_course_offering_id,
  act.academic_term_id as udp_academic_term_id,
  act.lms_id as lms_academic_term_id,
  act.name as term_name,
  act.term_type,
  act.term_year,
  act.term_begin_date as term_begin_date,
  act.term_end_date as term_end_date,
  cg.created_date,
  cg.updated_date,
  cg.le_final_score_status,
  cg.le_current_score,
  cg.le_final_score,
  cg.le_hidden_current_score,
  cg.le_hidden_final_score,
  CURRENT_DATE() as run_date,
  CURRENT_DATETIME() as run_time
from _WRITE_TO_PROJECT_.mart_helper.context__course_grade cg 
join _WRITE_TO_PROJECT_.mart_helper.context__course_section cs on cg.course_section_id = cs.course_section_id 
join _WRITE_TO_PROJECT_.mart_helper.context__course_offering co on coalesce(cs.le_current_course_offering_id,cs.course_offering_id) = co.course_offering_id 
left join _WRITE_TO_PROJECT_.mart_helper.context__academic_term act on co.academic_term_id = act.academic_term_id 
where 
  act.term_begin_date <= CURRENT_DATE()
  and act.term_end_date >= CURRENT_DATE()
  and COALESCE(cg.le_current_score,cg.le_hidden_current_score,cg.le_final_score,cg.le_hidden_final_score) IS NOT NULL
  and cg.le_final_score_status is distinct from 'deleted'
