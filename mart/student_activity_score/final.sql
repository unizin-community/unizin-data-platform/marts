/*
  Mart / Student activity score / Final 

    Computes the final student activity score for a student in a course.

  Properties:

    Export table:  mart_student_activity_score.final
    Run frequency: Every day
    Run operation: Overwrite
    
  Dependencies:
    - mart_student_activity_score.student_course_metrics
    
  To do:
    *
*/

-- subquery to get metrics from student_course_metrics table
WITH assignment_submission_time AS (
SELECT university_id,
        global_user_id,
        canvas_user_id,
        term_name,
        session_name,
        campus_name,
        academic_program,
        academic_level_code,
        academic_level_description,
        course_code,
        course_global_id,
        course_canvas_id,
        week_number,
        week_start_date,
        week_end_date,
        navigation_time as total_active_time,
        num_sessions as total_sessions,
        SAFE_DIVIDE(submissions,assignments_due) AS assignment_submission_rate,
        SAFE_DIVIDE(submissions_cumulative,assignments_due_cumulative) AS assignment_submission_rate_cumulative
FROM _WRITE_TO_PROJECT_.mart_student_activity_score.student_course_metrics
WHERE navigation_time IS NOT NULL AND 
        num_sessions IS NOT NULL AND 
        submissions IS NOT NULL AND 
        assignments_due IS DISTINCT FROM 0 AND 
        submissions_cumulative IS NOT NULL AND 
        assignments_due_cumulative IS DISTINCT FROM 0
),

-- number of students in the course
course_stats AS (
SELECT  course_code,
        count(distinct university_id) as num_students
FROM assignment_submission_time 
GROUP BY course_code
),

-- compute averages and standard deviations of metrics for each course per week
course_week_stats AS (
SELECT  course_code,
        week_number,
        week_start_date,
        week_end_date,
        avg(total_active_time) AS avg_total_active_time,
        stddev(total_active_time) AS sd_total_active_time,
        avg(total_sessions) as avg_total_sessions,
        stddev(total_sessions) as sd_total_sessions,
        avg(assignment_submission_rate) AS avg_assignment_submission_rate,
        stddev(assignment_submission_rate) AS sd_assignment_submission_rate,
        avg(assignment_submission_rate_cumulative) AS avg_assignment_submission_rate_cumulative,
        stddev(assignment_submission_rate_cumulative) AS sd_assignment_submission_rate_cumulative
FROM assignment_submission_time ast   
GROUP BY course_code,week_number,week_start_date,week_end_date
),

-- compute the z score of student's metrics for a course
assignment_submission_time_z AS (
SELECT ast.university_id,
        ast.global_user_id,
        ast.canvas_user_id,
        ast.term_name,
        ast.session_name,
        ast.campus_name,
        ast.academic_program,
        ast.academic_level_code,
        ast.academic_level_description,
        cws.course_code,
        ast.course_global_id,
        ast.course_canvas_id,
        cws.week_number,
        cws.week_start_date,
        cws.week_end_date,
        CASE WHEN sd_total_active_time <> 0 THEN 
        ((total_active_time-avg_total_active_time)/sd_total_active_time)
        ELSE 0 END AS total_active_time_z,
        CASE WHEN sd_total_sessions <> 0 THEN 
        ((total_sessions-avg_total_sessions)/sd_total_sessions) 
        ELSE 0 END AS total_sessions_z,
        CASE WHEN sd_assignment_submission_rate <> 0 THEN
        ((assignment_submission_rate-avg_assignment_submission_rate)/sd_assignment_submission_rate) 
        ELSE 0 END AS assignment_submission_rate_z,
        CASE WHEN sd_assignment_submission_rate_cumulative <> 0 THEN
        ((assignment_submission_rate_cumulative-avg_assignment_submission_rate_cumulative)/sd_assignment_submission_rate_cumulative) 
        ELSE 0 END AS assignment_submission_rate_z_cumulative
FROM assignment_submission_time AS ast 
INNER JOIN course_week_stats AS cws ON ast.course_code = cws.course_code and ast.week_number = cws.week_number
INNER JOIN course_stats AS cs ON ast.course_code = cs.course_code 
WHERE cs.num_students >= 10 -- filter out courses with less than 10 students
),

-- compute the student activity score for a student at the course level 
student_activity_score as (
SELECT  *,
        (5*(astz.assignment_submission_rate_z))+(2*(astz.total_active_time_z))+(total_sessions_z) AS student_activity_score,
        (5*(astz.assignment_submission_rate_z_cumulative))+(2*(astz.total_active_time_z))+(total_sessions_z) AS student_activity_score_cumulative

FROM assignment_submission_time_z as astz
),

-- compute the metrics for student activity scores per week in course 
course_student_activity_score_stats as (
SELECT  course_code,
        week_number,
        avg(sas.student_activity_score) AS avg_student_activity_score,
        stddev(sas.student_activity_score) AS sd_student_activity_score,
        avg(sas.student_activity_score_cumulative) AS avg_student_activity_score_cumulative,
        stddev(sas.student_activity_score_cumulative) AS sd_student_activity_score_cumulative
FROM student_activity_score sas
GROUP BY course_code,week_number
),

-- compute the z score of a student's student activity score for a week in a course
student_activity_score_z as (
SELECT sas.*,
        CASE WHEN csas.sd_student_activity_score <> 0 THEN 
        ((sas.student_activity_score-csas.avg_student_activity_score)/csas.sd_student_activity_score)
        ELSE 0 END AS student_activity_score_z,
        CASE WHEN csas.sd_student_activity_score_cumulative <> 0 THEN 
        ((sas.student_activity_score_cumulative-csas.avg_student_activity_score_cumulative)/csas.sd_student_activity_score_cumulative)
        ELSE 0 END AS student_activity_score_z_cumulative

FROM student_activity_score AS sas 
INNER JOIN course_student_activity_score_stats AS csas ON sas.course_code = csas.course_code and sas.week_number = csas.week_number
),

-- compute the average of a student's student activity scores across weeks in all courses in a term
student_activity_score_average as (
select 
university_id
  , global_user_id
  , canvas_user_id
  , term_name
  , campus_name
  , academic_program
  , academic_level_code
  , academic_level_description
  , week_number
  , week_start_date
  , week_end_date
  ,avg(student_activity_score_z) as avg_student_activity_score
  , avg(student_activity_score_z_cumulative) AS avg_student_activity_score_cumulative
from student_activity_score_z sasz
group by university_id,global_user_id,canvas_user_id,term_name,campus_name,academic_program,academic_level_code,academic_level_description,week_number,week_start_date,week_end_date
)

-- present the student, average student activity score, and their student activity score octile within their academic program.  
select *,
CASE
  WHEN sasa.avg_student_activity_score IS NULL THEN NULL
  WHEN sasa.avg_student_activity_score <= PERCENTILE_CONT(sasa.avg_student_activity_score, 0.125) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.125
  WHEN sasa.avg_student_activity_score <= PERCENTILE_CONT(sasa.avg_student_activity_score, 0.250) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.250
  WHEN sasa.avg_student_activity_score <= PERCENTILE_CONT(sasa.avg_student_activity_score, 0.375) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.375
  WHEN sasa.avg_student_activity_score <= PERCENTILE_CONT(sasa.avg_student_activity_score, 0.500) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.500
  WHEN sasa.avg_student_activity_score <= PERCENTILE_CONT(sasa.avg_student_activity_score, 0.625) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.625
  WHEN sasa.avg_student_activity_score <= PERCENTILE_CONT(sasa.avg_student_activity_score, 0.750) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.750
  WHEN sasa.avg_student_activity_score <= PERCENTILE_CONT(sasa.avg_student_activity_score, 0.875) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.875          
  ELSE 1.00
  END as student_activity_score_octile,
CASE
  WHEN sasa.avg_student_activity_score_cumulative IS NULL THEN NULL
  WHEN sasa.avg_student_activity_score_cumulative <= PERCENTILE_CONT(sasa.avg_student_activity_score_cumulative, 0.125) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.125
  WHEN sasa.avg_student_activity_score_cumulative <= PERCENTILE_CONT(sasa.avg_student_activity_score_cumulative, 0.250) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.250
  WHEN sasa.avg_student_activity_score_cumulative <= PERCENTILE_CONT(sasa.avg_student_activity_score_cumulative, 0.375) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.375
  WHEN sasa.avg_student_activity_score_cumulative <= PERCENTILE_CONT(sasa.avg_student_activity_score_cumulative, 0.500) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.500
  WHEN sasa.avg_student_activity_score_cumulative <= PERCENTILE_CONT(sasa.avg_student_activity_score_cumulative, 0.625) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.625
  WHEN sasa.avg_student_activity_score_cumulative <= PERCENTILE_CONT(sasa.avg_student_activity_score_cumulative, 0.750) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.750
  WHEN sasa.avg_student_activity_score_cumulative <= PERCENTILE_CONT(sasa.avg_student_activity_score_cumulative, 0.875) OVER (PARTITION BY term_name, academic_program,week_number) THEN 0.875          
  ELSE 1.00
  END as student_activity_score_octile_cumulative

from student_activity_score_average sasa 
order by university_id
