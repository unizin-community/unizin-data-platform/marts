# Student Activity Score 
The marts in the _mart_student_activity_score_ are based on Indiana University's Student Activity Score, developed by Matthew Rust. 

## Student Course Metrics

- [Schema](#schema)
- [Assignments Due](#assignments-due)
- [Submissions](#submissions)
- [Navigation time and Number of Sessions](#navigation-time-and-number-of-sessions)

The _mart_student_activity_score.student_course_metrics_ mart aggregates data about a student’s performance and activities in a course on a weekly basis.

#### BQ Prod Dataset Locations
- mart_student_activity_score

### Schema 

#### mart/student_activity_score/student_course_metrics

| Field name | Type | Description |
| ---      |  ------  | -------- |
| university_id | STRING | The SIS ID of the student. |
| global_user_id | INTEGER | The UDP ID of the student. |
| canvas_user_id | STRING | The Canvas ID of the student. |
| term_name | STRING | The academic term the student is enrolled in, i.e. _Fall 2020_.|
| week_number | INTEGER | The week of the academic term. |
| campus_name | STRING | The campus of the student for the given academic term. |
| academic_program | STRING | The academic program the student is enrolled in for the given academic term. |
| course_code | STRING | The SIS ID of the Course offering. |
| course_global_id | INTEGER | The UDP ID of the Course offering. |
| course_canvas_id | STRING | The Canvas ID of the Course offering. |
| navigation_time | FLOAT | The amount of time the student spent in Canvas for the given week in the course. |
| num_sessions | INTEGER | The number of Canvas sessions of the student for the given week in the course. |
| assignments_due | INTEGER | The number of assignments due in the course for the given week. |
| submissions | INTEGER | The number of assignments submitted by the student for the given week in the course. |

### Assignments Due

The number of assignments due in a course offering for a given week is defined in the _mart_helper.context__student_activity_score__assignments_ table. The _week_number_ of an assignment is determined based on the difference in weeks between the due date of the assignment and the start date of the academic term. If the start date of the academic term is null, the start date of the course offering is used. The student activity score only includes published assignments with due dates and non-zero points possible. 

### Submissions
The number of assignments submitted by a student in a course offering for a given week is defined in the _mart_helper.context__student_activity_score__submissions_ table. The _week_number_ of a submission is also defined as the difference in weeks between the due date of the assignment and the start date of the academic term or the course offering.  We only count submissions for published assignments with due dates, non-zero points possible, and allowed submission types of 'on_paper', 'Assignments', 'not_graded', 'none', ' ', or 'external_tool'. 

### Navigation time and Number of Sessions

The amount of time spent and number of sessions in the learning environment for a student in a course is defined in the _mart_helper.event__student_activity_score__navigation_time_ table. The _week_number_ of an event is defined as the difference in weeks between the event time and the start date of the academic term or course. The _navigation_time_ for a given week in a course for a student is the amount of time spent in the learning environment for the prior two weeks. The _num_sessions_ is the number of sessions for the prior two weeks. We define the two week range for a week based on the latest event time for the given week in the course. Periods of inactivity, 25 minutes or more between two consecutive events, are removed from our aggregations. 

## Student Course Section Metrics

- [Schema](#schema-1)

The _mart_student_activity_score.student_course_section_metrics_ mart aggregates data about a student’s performance and activities in a course section on a weekly basis.

#### BQ Prod Dataset Locations
- mart_student_activity_score

### Schema 

#### mart/student_activity_score/student_course_section_metrics

The _student_course_section_metrics_ mart includes all the fields found in the _student_course_metrics_ mart, along with three extra fields:

| Field name | Type | Description |
| ---      |  ------  | -------- |
| course_section_code | STRING | The SIS ID of the Course section. |
| course_section_global_id | INTEGER | The UDP ID of the Course section. |
| course_section_canvas_id | STRING | The Canvas ID of the Course section. |

## Final

- [Schema](#schema-2)
- [Student Activity Score](#student-activity-score)
- [Average Student Activity Score](#average-student-activity-score)
- [Student Activity Score Octile](#student-activity-score-octile)

The _mart_student_activity_score.final_ mart presents the final student activity score for a student in an academic term for a given week in the term. 

### BQ Prod Dataset Locations
- mart_student_activity_score

### Schema 

#### mart/student_activity_score/final

| Field name | Type | Description |
| ---      |  ------  | -------- |
| university_id | STRING | The SIS ID of the student. |
| term_name | STRING | The academic term the student is enrolled in, i.e. _Fall 2020_.|
| campus_name | STRING | The campus of the student for the given academic term. |
| academic_program | STRING | The academic program the student is enrolled in for the given academic term. |
| week_number | INTEGER | The week of the academic term. |
| avg_student_activity_score | FLOAT | The average student activity z-score for the given week in the academic term. |
| student_activity_score_octile | FLOAT | The octile of the student's average student activity z-score in comparison to the average student activity score for a cohort of students based on their academic program. |

### Student Activity Score

The student activity score defines a "score" for a student's activity in a course's learning environment. The score is calculated using the formula:
$$
5*[submissions / assignments\_due] + 2*[navigation\_time] + [num\_sessions]
$$ 
The metrics in brackets are z-scores. The z-score for a student is computed for a week in the course. 

### Average Student Activity Score

The _average_student_activity_score_ field is the average student activity z-score across all courses a student is enrolled in for a given week in an academic term. 

### Student Activity Score Octile

The _student_activity_score_octile_ is the percentile of a student's activity z-score compared to other students. Students are cohorted based on their academic program for the given academic term. The octile of the student's activity score is computed using the `PERCENTILE_CONT` function in BigQuery. 

## Course Final

- [Schema](#schema-3)

The _mart_student_activity_score.course_final_ mart presents the final student activity score for a student in an course for a given week in the term. 

### BQ Prod Dataset Locations
- mart_student_activity_score

### Schema 

#### mart/student_activity_score/course_final

| Field name | Type | Description |
| ---      |  ------  | -------- |
| university_id | STRING | The SIS ID of the student. |
| global_user_id | INTEGER | The UDP ID of the student. |
| canvas_user_id | STRING | The Canvas ID of the student. |
| term_name | STRING | The academic term the student is enrolled in, i.e. _Fall 2020_.|
| campus_name | STRING | The campus of the student for the given academic term. |
| academic_program | STRING | The academic program the student is enrolled in for the given academic term. |
| course_code | STRING | The SIS ID of the Course offering. |
| course_global_id | INTEGER | The UDP ID of the Course offering. |
| course_canvas_id | STRING | The Canvas ID of the Course offering. |
| week_number | INTEGER | The week of the academic term. |
| student_activity_score | FLOAT | The student activity score for the student in the course for the given week in the academic term. |
| student_activity_score_z | FLOAT | The z-score of the student activity score for the student in the course for the given week in the academic term. |
| student_activity_score_octile | FLOAT | The octile of the student's average student activity z-score in comparison to the average student activity score for the course. |


## Course Section Final

- [Schema](#schema-4)

The _mart_student_activity_score.course_section_final_ mart presents the final student activity score for a student in an course section for a given week in the term. 

### BQ Prod Dataset Locations
- mart_student_activity_score

### Schema 

#### mart/student_activity_score/course_section_final

| Field name | Type | Description |
| ---      |  ------  | -------- |
| university_id | STRING | The SIS ID of the student. |
| global_user_id | INTEGER | The UDP ID of the student. |
| canvas_user_id | STRING | The Canvas ID of the student. |
| term_name | STRING | The academic term the student is enrolled in, i.e. _Fall 2020_.|
| campus_name | STRING | The campus of the student for the given academic term. |
| academic_program | STRING | The academic program the student is enrolled in for the given academic term. |
| course_code | STRING | The SIS ID of the Course offering. |
| course_global_id | INTEGER | The UDP ID of the Course offering. |
| course_canvas_id | STRING | The Canvas ID of the Course offering. |
| course_section_code | STRING | The SIS ID of the Course section. |
| course_section_global_id | INTEGER | The UDP ID of the Course section. |
| course_section_canvas_id | STRING | The Canvas ID of the Course section. |
| week_number | INTEGER | The week of the academic term. |
| student_activity_score | FLOAT | The student activity score for the student in the course for the given week in the academic term. |
| student_activity_score_z | FLOAT | The z-score of the student activity score for the student in the course section for the given week in the academic term. |
| student_activity_score_octile | FLOAT | The octile of the student's average student activity z-score in comparison to the average student activity score for the course section. |

