/*
  Mart / Student activity score / Student course metrics

    Defines the metrics for students in a course.

  Properties:

    Export table:  mart_helper.student_course_metrics
    Run frequency: Every day
    Run operation: Overwrite

  Dependencies:
    - mart_helper.context__student_activity_score__enrollments
    - mart_helper.context__student_activity_score__assignments
    - mart_helper.context__student_activity_score__submissions
    - mart_helper.event__student_activity_score__navigation_time
    - mart_helper.context__student_activity_score__student_term_campus
    - mart_helper.context__student_activity_score__student_term_academic_level
    - mart_helper.context__student_activity_score__student_term_program

  To do:
    *
*/

/* Define week numbers for enrollments */
WITH week_enrollments AS (
SELECT 
    DISTINCT  
    university_id,
    global_user_id,
    canvas_user_id,
    course_code,
    course_global_id,
    course_canvas_id,
    term_name,
    session_name,
    week_number,
    CASE
      WHEN acs.instruction_begin_date is not null then DATE_TRUNC(DATE_ADD(acs.instruction_begin_date, INTERVAL week_number - 1 WEEK),WEEK)
      WHEN act.term_begin_date is not null then DATE_TRUNC(DATE_ADD(act.term_begin_date, INTERVAL week_number - 1 WEEK),WEEK)
      WHEN co.start_date is not null then DATE_TRUNC(DATE_ADD(co.start_date, INTERVAL week_number - 1 WEEK),WEEK)
    ELSE null end as week_start_date,
    CASE
      WHEN acs.instruction_begin_date is not null then LAST_DAY(DATE_ADD(acs.instruction_begin_date, INTERVAL week_number - 1 WEEK),WEEK)
      WHEN act.term_begin_date is not null then LAST_DAY(DATE_ADD(act.term_begin_date, INTERVAL week_number - 1 WEEK),WEEK)
      WHEN co.start_date is not null then LAST_DAY(DATE_ADD(co.start_date, INTERVAL week_number - 1 WEEK),WEEK)
    ELSE null end as week_end_date
FROM 
    _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__enrollments en, UNNEST(GENERATE_ARRAY(1,COALESCE(term_length,17))) AS week_number
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__course_offering as co
	  on en.course_global_id = co.course_offering_id 
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_session as acs 
    on acs.academic_session_id = co.academic_session_id
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__academic_term as act
	  on act.academic_term_id = co.academic_term_id
),

initial_metrics as (
SELECT  
  we.university_id,
  we.global_user_id,
  we.canvas_user_id,
  we.term_name,
  we.session_name,
  we.week_number,
  we.week_start_date,
  we.week_end_date,
  stc.campus_name,
  stp.academic_program,
  stal.academic_level_code,
  stal.academic_level_description,
  we.course_code,
  we.course_global_id,
  we.course_canvas_id,
  COALESCE(nav.navigationTime,0) AS navigation_time,
  nav.num_sessions,
  COALESCE(assn.assignments_due,0) AS assignments_due,
  COALESCE(sub.submissions,0) AS submissions
FROM week_enrollments AS we 
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__assignments AS assn ON we.course_global_id = assn.course_global_id and we.week_number = assn.week_number
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__submissions AS sub ON we.course_global_id = sub.course_global_id AND we.global_user_id = sub.user_id and we.week_number = sub.week_number 
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.event__student_activity_score__navigation_time AS nav ON nav.university_id = we.university_id AND nav.course_code = we.course_code AND nav.week_number = we.week_number
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__student_term_campus AS stc ON we.university_id = stc.university_id AND we.term_name = stc.term_name
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__student_term_program AS stp ON we.university_id = stp.university_id AND we.term_name = stp.term_name
LEFT JOIN _WRITE_TO_PROJECT_.mart_helper.context__student_activity_score__student_term_academic_level AS stal ON we.university_id = stal.university_id AND we.term_name = stal.term_name 
WHERE 
  we.course_code IS NOT NULL
)

select *
  , SUM(assignments_due) OVER (
      PARTITION BY university_id, course_global_id 
      ORDER BY week_number
      ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as assignments_due_cumulative
  , SUM(submissions) OVER (
      PARTITION BY university_id, course_global_id 
      ORDER BY week_number
      ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as submissions_cumulative
from initial_metrics
