# Data marts

The data marts in `batch-features` are used by Unizin Data Services to:

1. Catalyze member stakeholder use of learning data and analytics
1. Build reporting and dashboard that illustrates the utility of learning data and analytics

As of this writing, for example, Unizin Data Services maintains a [set of demo dashboards](https://datastudio.google.com/reporting/e1d03670-f8ec-45dc-88ee-ffa104c67afc/page/UOguB) that are powered by the data marts described in this folder.

Several marts depend on event data and, in particular, a set of queries and tables that simulate basic event stream ETL. These queries and marts are found in the `/event` and `/context` directories. The tables generated from this directory are what end users ultimately see as materialized views in their `prod` environments. No other piece of `batch-features` is exposed to members.

There are no direct dependencies on the UDP at this level. All queries pull from either `event`, `context` or both.

## starter SQL

If a `mart` table has a dependency on an `event` table, we employ the same `*_starter.sql` considerations for scanning cost considerations. More detail about the `starter.sql` is in the `event` README.

The refresh frequencies also often reflect the `event` frequencies. It's rare that a mart table updates only once a day or has no `event` dependency.

## course_offering

The main pivot for these marts is again by `course_offering`. We have these marts documented on our resources site, so we list the names here with links to each mart's doc page:
- [file_interaction](https://resources.unizin.org/display/UDP/File+Interaction)
- [interaction_sessions](https://resources.unizin.org/display/UDP/Interaction+sessions)
- [last_activity](https://resources.unizin.org/display/UDP/Last+Activity)
- [status](https://resources.unizin.org/display/UDP/Course+Status)

## course_section

The marts here mirror the same ones in `course_offering`. The only change here is that we do one extra join in the logic to resolve the `course_section` in scope. We often don't get `course_section_id` for free, so we have to do joins both on the `course_offering_id` and `person_id` on the enrollment table to figure out which course_section is in scope.

## general

- [lms_tool](https://resources.unizin.org/display/UDP/LMS+Tool+Use)
- [lti_tool](https://resources.unizin.org/display/UDP/LTI+Tool+Use)

## taskforce

- [course_profile](https://resources.unizin.org/display/UDP/Taskforce+Mart+-+Course+Profile)
- [level1_aggregated](https://resources.unizin.org/display/UDP/Taskforce+Mart+-+Level+1+Aggregated)
- [level2_aggregated](https://resources.unizin.org/display/UDP/Taskforce+Mart+-+Level+2+Aggregated)
- [level2_course_weekly_distribution_summary](https://resources.unizin.org/display/UDP/Taskforce+Mart+-+Level+2+Course+Weekly+Distribution+Summary)
- [student_term_profile](https://resources.unizin.org/display/UDP/Taskforce+Mart+-+Student+Term+Profile)

There are three other SQL files that need to run but aren't surfaced to end users:
- level2_course_week_files
- level2_course_week_metrics
- level2_course_week_tools

The level2 tables for the taskforce generated a lot more SQL than anticipated, so we split it up into multiple chunks. These are more "building blocks", but it makes sense to keep them in the `mart` directory because they only have dependency on either `event`, `context` or both.

## config.yaml

This file takes the same format as the `config.yaml` files in `context` and `event`

- file_path - relative path to the SQL file
- refresh_frequency - feeds into BQ scheduled query; how often to run it
- refresh_write - what to do with results; often `overwrite` for the `context` tables
- scheduled_query_name - what BQ should name the scheduled query upon creation
- bq_table_name - destination table for data
- bq_dataset - destination dataset for data
- partitioned - True or False; often applicable to `event` tables
- partition_field - if True for `partitioned`, this is the name of the field. Otherwise, it takes a value of `None`.