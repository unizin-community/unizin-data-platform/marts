# import required module
import os
from google.cloud import bigquery
from alive_progress import alive_bar
from .template  import sql_template
import pdb



def create_distributions(client, config):
    # defie categorical and continuous directories
    categorical_variables = os.path.join(os.path.dirname(__file__), 'udp_distributions/categorical')
    continuous_variables = os.path.join(os.path.dirname(__file__), 'udp_distributions/continuous')

    # iterate over the SQL files for categorical fields  
    print('CATEGORICAL VARIABLES')
    with alive_bar(len(os.listdir(categorical_variables))) as bar:
        for file_name in os.listdir(categorical_variables):
            # define empty list of SQL queries
            query_list = []

            # define field names
            field_names = file_name.replace('.sql','')

            # define query job config for final BQ table
            distribution_dataset = f'{config.project_id}.{config.datasets["distribution"]}.field'
            job_config = bigquery.QueryJobConfig(allow_large_results=True,destination=distribution_dataset.replace('field',field_names))
            job_config.write_disposition = "WRITE_TRUNCATE"
            # count number of fields defined in the file
            field_count = file_name.count('__')

            # loop over all tenants of interest 
            for tenant in config.tenants: 
                # define full file path
                full_file_name = categorical_variables + '/' + file_name 
                tenant_sql_file = sql_template(full_file_name)(tenant=tenant)
                query_list.append(tenant_sql_file)
            
            # split field_names into list 
            field_name_list = field_names.split('__')
            # separate fields by commas
            final_query_field_names = ','.join(field_name_list)
            # define list of fields without last field
            reduced_field_name_list = field_name_list[:-1]
            # define fields to query and group by in the subquery
            subquery_field_names = ','.join(reduced_field_name_list)

            # define the first subquery to be a union of SQL queries per tenant
            union_subquery = 'with union_subquery as ( \n' + '\n UNION ALL \n'.join(query_list) + ')'
            
            # if there is only one field of interest 
            if (field_count == 0):
                # if field is default_learner_activity_name
                if field_names in ['default_learner_activity_name']:
                    subquery = '\n , \n filter_subquery as ( \n SELECT field, \n SUM(num_records) AS num_records \n FROM union_subquery \n GROUP BY field \n ORDER BY num_records DESC \n LIMIT 10 \n ) \n , \n subquery as ( \n SELECT *, \nSUM(num_records) OVER() as  total_records \n FROM filter_subquery \n )'
                    subquery = subquery.replace('field',final_query_field_names)
                else:     
                    # define subquery to compute total_records field
                    subquery = '\n , \n subquery as ( \n SELECT *, \nSUM(num_records) OVER() as  total_records \n FROM union_subquery \n )'

            # if there's more than one field of interest
            if (field_count > 0):
                # define subquery to compute total_records field 
                subquery = '\n , \n subquery as ( \n SELECT *, \n SUM(num_records) OVER(PARTITION BY field) as total_records \n FROM union_subquery \n )'
                subquery = subquery.replace('field',subquery_field_names)
            # define final query to compute overall pct_records 
            final_query = union_subquery + subquery + '\n SELECT field, \n SUM(num_records)/total_records as pct_records \n FROM subquery \n GROUP BY field,total_records'
            final_query = final_query.replace('field',final_query_field_names)
            
            # run query job in BQ and read to table 
            query_job = client.query(final_query,job_config=job_config)
            bar()


    # iterate over the files for continuous fields   
    print('CONTINUOUS VARIABLES')
    with alive_bar(len(os.listdir(continuous_variables))) as bar:
        for file_name in os.listdir(continuous_variables):

            # define empty list of SQL queries
            query_list = []

            # define field names
            field_names = file_name.replace('.sql','')

            # define query job config for final BQ table
            distribution_dataset = f'{config.project_id}.{config.datasets["distribution"]}.field'
            job_config = bigquery.QueryJobConfig(allow_large_results=True,destination=distribution_dataset.replace('field',field_names))
            job_config.write_disposition = "WRITE_TRUNCATE"

            # count number of fields defined in the file
            field_count = file_name.count('__')

            # loop over all tenants of interest 
            for tenant in config.tenants: 
                full_file_name = continuous_variables + '/' + file_name 
                # replace TENANT string with specific tenant
                tenant_sql_file = sql_template(full_file_name)(tenant=tenant, project=config.project_id)
                # add the SQL query to query_list
                query_list.append(tenant_sql_file)

            # split field_names into list
            field_name_list = field_names.split('__')
            # separate fields by commas
            final_query_field_names = ','.join(field_name_list)
            # define list of fields without last field
            reduced_field_name_list = field_name_list[:-1]
            # define fields to query and group by in the subquery
            group_field_names = ','.join(reduced_field_name_list)
            # define final field in file
            final_field = field_name_list[-1]

            # define the first subquery to be a union of SQL queries per tenant
            union_subquery = 'with union_subquery as ( \n' + '\n UNION ALL \n'.join(query_list) + ')'
            
            # if there is only one field of interest 
            if (field_count == 0):
                # define final_query to compute metrics for field of interest
                final_query = union_subquery + '\n SELECT AVG(field) AS avg_field, \n STDDEV(field) AS sd_field, \n MIN(field) AS min_field, \n MAX(field) AS max_field  \n FROM union_subquery'
            # if there's more than one field of interest
            if (field_count > 0):
                # define final_query to compute metrics for fields of interest
                final_query = union_subquery + '\n SELECT group_fields, \n AVG(field) AS avg_field, \n STDDEV(field) AS sd_field, \n MIN(field) AS min_field, \n MAX(field) AS max_field, \n FROM union_subquery \n GROUP BY group_fields'
                final_query = final_query.replace('group_fields',group_field_names)
            final_query = final_query.replace('field',final_field)
            
            # run query job in BQ and read to table
            query_job = client.query(final_query,job_config=job_config)
            bar()

def run(client, config):
    print('Creating udp_distributions!')
    create_distributions(client, config)

